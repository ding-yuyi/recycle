package com.woniu.recycle.housekeeping.dto;

import lombok.Data;

import java.util.List;

/**
 * TODO
 *
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/23 0023
 */
@Data
public class TaskLogPageDto {
    private Long total;
    private Long size;
    private Long current;
    private List<SearchTaskLogDto> logs;
}
