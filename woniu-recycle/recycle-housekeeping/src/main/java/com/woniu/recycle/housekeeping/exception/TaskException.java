package com.woniu.recycle.housekeeping.exception;

import com.woniu.recycle.commons.core.exception.RecycleException;

/**
 * TODO
 *
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/19 0019
 */
public class TaskException extends RecycleException {
    private static final long serialVersionUID = 1L;

    private Integer code;

    private String msg;

    //全参构造方法
    public TaskException(Integer code, String msg){
        super(msg, code);
    }
}
