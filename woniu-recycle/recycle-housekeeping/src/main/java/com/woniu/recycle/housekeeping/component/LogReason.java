package com.woniu.recycle.housekeeping.component;

/**
 * TODO
 *日志生成原因
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/23 0023
 */
public class LogReason {
    //生成订单日志消息
    public static final String CREATE_ORDER="生成订单";
    //生成工单日志消息
    public static final String CREATE_TASK="生成工单";

    public static final String PAY="订单付款";

    public static final String TASK_DISTRIBUTE="派发工单";

    public static final String ADMIN_MODIFY_TASK="管理员修改工单";

    public static final String USER_MODIFY_TASK="用户修改工单";

    public static final String CONFIRM_ARRIVE="确认服务人员到达";

    public static final String ORDER_FINISH="订单完成";

    public static final String TASK_FINISH="工单完成";

    public static final String COMMENT="订单评论";

    public static final String CANCEL_ORDER="取消订单";

    public static final String CANCEL_TASK="取消工单";

    public static final String REFUND="订单退款";

    public static final String DELETE_ORDER="删除订单";

    public static final String SYSTEM_CANCEL="超时系统自动取消";

}
