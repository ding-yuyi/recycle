package com.woniu.recycle.housekeeping.domain;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 家政订单表
 * </p>
 *
 * @author cy/lyl
 * @since 2021-08-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="HousekeepingOrder对象", description="家政订单表")
public class HousekeepingOrder implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "家政订单id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "订单编号")
    private String housekeepingOrderNum;

    @ApiModelProperty(value = "家政商品id")
    private Integer productId;

    @ApiModelProperty(value = "商品图片路径")
    private String productUrl;

    @ApiModelProperty(value = "家政商品价格")
    private BigDecimal price;

    @ApiModelProperty(value = "商品数量")
    private Integer count;

    @ApiModelProperty(value = "商品总价")
    private BigDecimal total;

    @ApiModelProperty(value = "预约上门时间")
    private Long visitTime;

    @ApiModelProperty(value = "地址id")
    private Integer addrId;

    @ApiModelProperty(value = "用户id")
    private Integer userId;

    @ApiModelProperty(value = "收货地址")
    private String addrDesc;

    @ApiModelProperty(value = "家政员名字")
    private String housekeeperName;

    @ApiModelProperty(value = "家政员电话")
    private String housekeeperTel;

    @ApiModelProperty(value = "家政员id")
    private Integer housekeeperId;

    @ApiModelProperty(value = "订单状态 0 未收货 | 1 已收货（订单结束）| 2 已评价 | 3 已取消")
    private Integer status;

    @ApiModelProperty(value = "取消时间")
    private Long cancelTime;

    @ApiModelProperty(value = "取消原因")
    private String cancelReason;

    @ApiModelProperty(value = "评价内容（可不评价）")
    private String comment;

    @ApiModelProperty(value = "评价等级 1 | 2 | 3 | 4 | 5  5星最好")
    private Integer level;

    @ApiModelProperty(value = "订单结束时间")
    private Long finishTime;

    @ApiModelProperty(value = "备注")
    private String notes;

    @ApiModelProperty(value = "逻辑删除 0 未删除 | 1 已删除（用于用户端查询）")
    private Integer deleted;

    @ApiModelProperty(value = "创建时间")
    private Long createTime;

    @ApiModelProperty(value = "记录添加时间")
    private Date rawAddTime;

    @ApiModelProperty(value = "记录更新时间")
    private Date rawUpdateTime;


}
