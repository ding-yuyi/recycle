package com.woniu.recycle.housekeeping.controller;


import com.woniu.recycle.commons.core.model.ResponseEntity;
import com.woniu.recycle.commons.web.component.BaseController;
import com.woniu.recycle.housekeeping.controller.form.SearchOrderLogByUserForm;
import com.woniu.recycle.housekeeping.controller.form.SearchOrderLogForm;
import com.woniu.recycle.housekeeping.dto.ListOrderLogDto;
import com.woniu.recycle.housekeeping.dto.TaskLogPageDto;
import com.woniu.recycle.housekeeping.service.HousekeepingTaskLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 * 家政工单日志表 前端控制器
 * </p>
 *
 * @author Jhurry
 * @since 2021-08-12
 */
@RestController
@RequestMapping("/taskLog")
@Slf4j
@Api(tags = "家政服务工单日志管理")
public class HousekeepingTaskLogController extends BaseController {
    @Resource
    private HousekeepingTaskLogService logService;

    @GetMapping("auth/searchByOrderId")
    @ApiOperation(value = "根据订单id查询订工单日志")
    public ResponseEntity<TaskLogPageDto> searchByOrderId(@Valid SearchOrderLogForm searchOrderLogForm, BindingResult result){
        log.info("入参:{}",searchOrderLogForm);

        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;

        TaskLogPageDto taskLogPageDto = logService.searchByOrderId(searchOrderLogForm);
        return ResponseEntity.BuildSuccess(TaskLogPageDto.class).setMsg("查询成功").setCode(200).setData(taskLogPageDto);
    }

    @GetMapping("auth/searchByUserId")
    @ApiOperation(value = "根据用户id查询工单日志")
    public ResponseEntity<TaskLogPageDto> searchByUserId(@Valid SearchOrderLogByUserForm searchOrderLogByUserForm, BindingResult result){
        log.info("入参:{}",searchOrderLogByUserForm);

        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;

        TaskLogPageDto taskLogPageDto = logService.searchByUserId(searchOrderLogByUserForm);
        return ResponseEntity.BuildSuccess(TaskLogPageDto.class).setMsg("查询成功").setCode(200).setData(taskLogPageDto);
    }
}

