package com.woniu.recycle.housekeeping.controller.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * TODO
 *评论订单接口入参对象
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/16 0016
 */
@Data
public class CommentOrderForm {
    @ApiModelProperty(value = "订单id")
    @NotNull
    @Min(value = 1)
    private Integer orderId;

    @ApiModelProperty(value = "评论星级")
    @Min(value = 1)
    @Max(value = 5)
    private Integer level;

    @ApiModelProperty(value = "评论内容")
    private String comment;
}
