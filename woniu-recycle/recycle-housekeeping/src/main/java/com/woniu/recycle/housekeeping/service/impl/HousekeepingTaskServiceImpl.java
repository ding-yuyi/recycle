package com.woniu.recycle.housekeeping.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.recycle.commons.web.util.BeanCopyUtil;
import com.woniu.recycle.commons.web.util.DateUtil;
import com.woniu.recycle.commons.web.util.ServletUtil;
import com.woniu.recycle.housekeeping.component.*;
import com.woniu.recycle.housekeeping.controller.form.*;
import com.woniu.recycle.housekeeping.domain.*;
import com.woniu.recycle.housekeeping.dto.*;
import com.woniu.recycle.housekeeping.exception.OrderException;
import com.woniu.recycle.housekeeping.exception.TaskException;
import com.woniu.recycle.housekeeping.exception.UserException;
import com.woniu.recycle.housekeeping.mapper.HousekeeperMapper;
import com.woniu.recycle.housekeeping.mapper.HousekeepingOrderMapper;
import com.woniu.recycle.housekeeping.mapper.HousekeepingTaskMapper;
import com.woniu.recycle.housekeeping.service.HousekeepingTaskService;
import com.woniu.recycle.housekeeping.utils.TaskLogUtil;
import com.woniu.recycle.housekeeping.utils.UserUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 家政工单表 服务实现类
 * </p>
 *
 * @author jhurry
 * @since 2021-08-12
 */
@Service
@Slf4j
public class HousekeepingTaskServiceImpl extends ServiceImpl<HousekeepingTaskMapper, HousekeepingTask> implements HousekeepingTaskService {
    @Resource
    private HousekeepingOrderMapper housekeepingOrderMapper;

    @Resource
    private HousekeepingTaskMapper housekeepingTaskMapper;

    @Resource
    private HousekeeperMapper housekeeperMapper;

    @Resource
    private UserUtil userUtil;

    @Resource
    private RocketMQTemplate rocketMQTemplate;

    /**
     * 工单生成方法
     * @param createTaskForm
     * @return
     */
    @Override
    public Boolean createTask(CreateTaskForm createTaskForm) {
       log.info("taskService入参：{}",createTaskForm);
        HousekeepingOrder housekeepingOrder = housekeepingOrderMapper.selectById(createTaskForm.getOrderId());
        if(ObjectUtils.isEmpty(housekeepingOrder)){
            throw new OrderException(OrderEnum.ORDER_NOT_EXIST.getCode(),OrderEnum.ORDER_NOT_EXIST.getMsg());
        }
        //生成工单信息
        HousekeepingTask housekeepingTask = BeanCopyUtil.copyObject(createTaskForm, HousekeepingTask::new);
        housekeepingTask.setAddress(housekeepingOrder.getAddrDesc());
        housekeepingTask.setCreateTime(System.currentTimeMillis());
        housekeepingTask.setOrderId(housekeepingOrder.getId());
        housekeepingTask.setProductId(housekeepingOrder.getProductId());
        housekeepingTask.setStatus(OrderStatus.DEFAULT_STATUS);
        housekeepingTask.setVisitTime(housekeepingOrder.getVisitTime());
        boolean save = save(housekeepingTask);

        //记录日志
        HousekeepingTaskLog taskLog = TaskLogUtil.getLog(housekeepingTask, LogType.NORMAL_USER, LogReason.CREATE_TASK, createTaskForm.getUserId());
        Message<HousekeepingTaskLog> message = MessageBuilder.withPayload(taskLog).build();
        rocketMQTemplate.asyncSend(ConsumerTopic.TASK_LOG, message, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
            }

            @Override
            public void onException(Throwable throwable) {
                log.warn("消息发送失败,日志保存失败,{}",log);
            }
        });
        return save;
    }

    /**
     * 派单方法
     * @param distributeTaskForm
     * @return
     */
    @Override
    public DistributeTaskDto distributeTask(DistributeTaskForm distributeTaskForm) {
        log.info("TaskService入参：{}",distributeTaskForm);
        //解析token获取当前用户id并从redis中取出user对象
        User user = userUtil.getUser(ServletUtil.getRequest());
        if(ObjectUtils.isEmpty(user)){
            throw new UserException(UserEnum.UESR_NOT_EXIST.getCode(),UserEnum.UESR_NOT_EXIST.getMsg());
        }
        QueryWrapper<HousekeepingTask> wrapper = new QueryWrapper<>();
        wrapper.eq("order_id",distributeTaskForm.getOrderId());
        HousekeepingTask housekeepingTask = housekeepingTaskMapper.selectOne(wrapper);
        if(ObjectUtils.isEmpty(housekeepingTask)){
            throw new TaskException(OrderEnum.ORDER_NOT_EXIST.getCode(),OrderEnum.ORDER_NOT_EXIST.getMsg());
        }
        Housekeeper housekeeper = housekeeperMapper.selectById(distributeTaskForm.getHousekeeperId());
        if(ObjectUtils.isEmpty(housekeeper)){
            throw new TaskException(TaskEnum.TASK_INFO_ERROR.getCode(),TaskEnum.TASK_INFO_ERROR.getMsg());
        }

        HousekeepingOrder housekeepingOrder = housekeepingOrderMapper.selectById(housekeepingTask.getOrderId());
        if(ObjectUtils.isEmpty(housekeepingOrder)){
            throw new TaskException(TaskEnum.TASK_INFO_ERROR.getCode(),TaskEnum.TASK_INFO_ERROR.getMsg());
        }
        if(housekeepingTask.getStatus()!=TaskStatus.DEFAULT_STATUS){
            throw new TaskException(TaskEnum.CAN_NOT_DISTRIBUTE.getCode(),TaskEnum.CAN_NOT_DISTRIBUTE.getMsg());
        }
        if(housekeepingOrder.getStatus()!=OrderStatus.PAYED){
            throw new OrderException(TaskEnum.CAN_NOT_DISTRIBUTEBYORDER.getCode(),TaskEnum.CAN_NOT_DISTRIBUTEBYORDER.getMsg());
        }
        housekeepingTask.setStatus(TaskStatus.ON_THE_WAY);
        housekeepingTask.setSendTime(System.currentTimeMillis());
        housekeepingTask.setHousekeeperName(housekeeper.getHousekeeperName());
        housekeepingTask.setHousekeeperPhone(housekeeper.getHousekeeperTel());
        //同时将信息更新到订单
        housekeepingOrder.setStatus(OrderStatus.ORDER_IN_PROGRESS);
        housekeepingOrder.setHousekeeperId(housekeeper.getId());
        housekeepingOrder.setHousekeeperName(housekeeper.getHousekeeperName());
        housekeepingOrder.setHousekeeperTel(housekeeper.getHousekeeperTel());
        housekeepingTaskMapper.updateById(housekeepingTask);
        housekeepingOrderMapper.updateById(housekeepingOrder);
        DistributeTaskDto distributeTaskDto = BeanCopyUtil.copyObject(housekeepingTask, DistributeTaskDto::new);
        //记录日志
        HousekeepingTaskLog taskLog = TaskLogUtil.getLog(housekeepingTask, LogType.MANAGER, LogReason.TASK_DISTRIBUTE, user.getId());
        Message<HousekeepingTaskLog> message = MessageBuilder.withPayload(taskLog).build();
        rocketMQTemplate.asyncSend(ConsumerTopic.TASK_LOG, message, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
            }

            @Override
            public void onException(Throwable throwable) {
                log.warn("消息发送失败,日志保存失败,{}",log);
            }
        });

        return distributeTaskDto;
    }

    /**
     * 工单确认到达时间方法（用户）
     * @param confirmTaskForm
     * @return
     */
    @Override
    public ConfirmTaskDto confirmTask(ConfirmTaskForm confirmTaskForm) {
        log.info("入参：{}",confirmTaskForm);
        //解析token获取当前用户id并从redis中取出user对象
        User user = userUtil.getUser(ServletUtil.getRequest());
        if(ObjectUtils.isEmpty(user)){
            throw new UserException(UserEnum.UESR_NOT_EXIST.getCode(),UserEnum.UESR_NOT_EXIST.getMsg());
        }
        QueryWrapper<HousekeepingTask> wrapper = new QueryWrapper<>();
        wrapper.eq("order_id",confirmTaskForm.getOrderId());
        HousekeepingTask housekeepingTask = housekeepingTaskMapper.selectOne(wrapper);
        if(ObjectUtils.isEmpty(housekeepingTask)){
            throw new TaskException(TaskEnum.TASK_NOT_EXIST.getCode(),TaskEnum.TASK_NOT_EXIST.getMsg());
        }
        if(!housekeepingTask.getUserPhone().equals(user.getTel())){
            throw new TaskException(TaskEnum.TASK_INFO_ERROR.getCode(),TaskEnum.TASK_INFO_ERROR.getMsg());
        }
        if(housekeepingTask.getStatus()!=TaskStatus.ON_THE_WAY){
            throw new TaskException(TaskEnum.TASK_INFO_ERROR.getCode(),TaskEnum.TASK_INFO_ERROR.getMsg());
        }
        housekeepingTask.setArriveTime(System.currentTimeMillis());
        housekeepingTask.setStatus(TaskStatus.IN_PROGRESS);
        ConfirmTaskDto confirmTaskDto = BeanCopyUtil.copyObject(housekeepingTask, ConfirmTaskDto::new);
        housekeepingTaskMapper.updateById(housekeepingTask);
        //记录日志
        HousekeepingTaskLog taskLog = TaskLogUtil.getLog(housekeepingTask, LogType.NORMAL_USER, LogReason.CONFIRM_ARRIVE, user.getId());
        Message<HousekeepingTaskLog> message = MessageBuilder.withPayload(taskLog).build();
        rocketMQTemplate.asyncSend(ConsumerTopic.TASK_LOG, message, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
            }

            @Override
            public void onException(Throwable throwable) {
                log.warn("消息发送失败,日志保存失败,{}",log);
            }
        });
        return confirmTaskDto;
    }

    /**
     * 取消工单方法
     * @param cancelTaskForm
     * @return
     */
    @Override
    public CancelTaskDto cancelTask(CancelTaskForm cancelTaskForm) {
        log.info("taskService入参:{}",cancelTaskForm);
        //解析token获取当前用户id并从redis中取出user对象
        User user = userUtil.getUser(ServletUtil.getRequest());
        if(ObjectUtils.isEmpty(user)){
            throw new UserException(UserEnum.UESR_NOT_EXIST.getCode(),UserEnum.UESR_NOT_EXIST.getMsg());
        }
        QueryWrapper<HousekeepingTask> wrapper = new QueryWrapper<>();
        wrapper.eq("order_id",cancelTaskForm.getOrderId());
        HousekeepingTask housekeepingTask = housekeepingTaskMapper.selectOne(wrapper);
        if(ObjectUtils.isEmpty(housekeepingTask)){
            throw new TaskException(TaskEnum.TASK_INFO_ERROR.getCode(),TaskEnum.TASK_INFO_ERROR.getMsg());
        }
        //未派单前可取消工单
        if (housekeepingTask.getStatus()!=TaskStatus.DEFAULT_STATUS){
            throw new TaskException(TaskEnum.TASK_STATUS_ERROR.getCode(),TaskEnum.TASK_STATUS_ERROR.getMsg());
        }
        housekeepingTask.setStatus(TaskStatus.CANCELED);
        housekeepingTask.setCancelTime(System.currentTimeMillis());
        if(null!=cancelTaskForm.getCancelReason()){
            housekeepingTask.setCancelReason(cancelTaskForm.getCancelReason());
        }
        housekeepingTaskMapper.updateById(housekeepingTask);
        CancelTaskDto cancelTaskDto = BeanCopyUtil.copyObject(housekeepingTask, CancelTaskDto::new);
        //记录日志
        HousekeepingTaskLog taskLog = TaskLogUtil.getLog(housekeepingTask, LogType.NORMAL_USER, LogReason.CANCEL_ORDER, user.getId());
        Message<HousekeepingTaskLog> message = MessageBuilder.withPayload(taskLog).build();
        rocketMQTemplate.asyncSend(ConsumerTopic.TASK_LOG, message, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
            }

            @Override
            public void onException(Throwable throwable) {
                log.warn("消息发送失败,日志保存失败,{}",log);
            }
        });
        return cancelTaskDto;
    }

    /**
     * 工单提交方法
     * @param submitTaskForm
     * @return
     */
    @Override
    public SubmitTaskDto submitTask(SubmitTaskForm submitTaskForm) {
        log.info("taskService入参:{}",submitTaskForm);
        //解析token获取当前用户id并从redis中取出user对象
        User user = userUtil.getUser(ServletUtil.getRequest());
        if(ObjectUtils.isEmpty(user)){
            throw new UserException(UserEnum.UESR_NOT_EXIST.getCode(),UserEnum.UESR_NOT_EXIST.getMsg());
        }
        QueryWrapper<HousekeepingTask> wrapper = new QueryWrapper<>();
        wrapper.eq("order_id",submitTaskForm.getOrderId());
        HousekeepingTask housekeepingTask = housekeepingTaskMapper.selectOne(wrapper);
        if (ObjectUtils.isEmpty(housekeepingTask)){
            throw new TaskException(TaskEnum.TASK_INFO_ERROR.getCode(),TaskEnum.TASK_INFO_ERROR.getMsg());
        }
        if(housekeepingTask.getStatus()!=TaskStatus.IN_PROGRESS){
            throw new TaskException(TaskEnum.CAN_NOT_SUBMIT.getCode(),TaskEnum.CAN_NOT_SUBMIT.getMsg());
        }
//        HousekeepingOrder housekeepingOrder = housekeepingOrderMapper.selectById(housekeepingTask.getOrderId());
//        if(ObjectUtils.isEmpty(housekeepingOrder)){
//            throw new OrderException(OrderEnum.ORDER_ERROR.getCode(),OrderEnum.ORDER_ERROR.getMsg());
//        }
//        housekeepingOrder.setStatus(OrderStatus.FINISHED);
//        housekeepingOrder.setFinishTime(System.currentTimeMillis());
        housekeepingTask.setStatus(TaskStatus.Finished);
        housekeepingTask.setFinishTime(System.currentTimeMillis());
        housekeepingTaskMapper.updateById(housekeepingTask);
//        housekeepingOrderMapper.updateById(housekeepingOrder);
        SubmitTaskDto submitTaskDto = BeanCopyUtil.copyObject(housekeepingTask, SubmitTaskDto::new);
        //记录日志
        HousekeepingTaskLog taskLog = TaskLogUtil.getLog(housekeepingTask, LogType.NORMAL_USER, LogReason.TASK_FINISH, user.getId());
        Message<HousekeepingTaskLog> message = MessageBuilder.withPayload(taskLog).build();
        rocketMQTemplate.asyncSend(ConsumerTopic.TASK_LOG, message, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
            }

            @Override
            public void onException(Throwable throwable) {
                log.warn("消息发送失败,日志保存失败,{}",log);
            }
        });
        return submitTaskDto;
    }

    /**
     * 管理员修改工单方法
     * @param adminModifyForm
     * @return
     */
    @Override
    public AdminModifyDto adminModifyTask(AdminModifyForm adminModifyForm) {
        log.info("taskService入参:{}",adminModifyForm);
        //解析token获取当前用户id并从redis中取出user对象
        User user = userUtil.getUser(ServletUtil.getRequest());
        if(ObjectUtils.isEmpty(user)){
            throw new UserException(UserEnum.UESR_NOT_EXIST.getCode(),UserEnum.UESR_NOT_EXIST.getMsg());
        }
        QueryWrapper<HousekeepingTask> wrapper = new QueryWrapper<>();
        wrapper.eq("order_id",adminModifyForm.getOrderId());
        HousekeepingTask housekeepingTask = housekeepingTaskMapper.selectOne(wrapper);
        if(ObjectUtils.isEmpty(housekeepingTask)){
            throw new TaskException(TaskEnum.TASK_NOT_EXIST.getCode(),TaskEnum.TASK_NOT_EXIST.getMsg());
        }
        Housekeeper housekeeper = housekeeperMapper.selectById(adminModifyForm.getHousekeeperId());
        if(ObjectUtils.isEmpty(housekeeper)){
            throw new TaskException(TaskEnum.TASK_INFO_ERROR.getCode(),TaskEnum.TASK_INFO_ERROR.getMsg());
        }
        if(housekeepingTask.getStatus()!=TaskStatus.ON_THE_WAY){
            throw new TaskException(TaskEnum.CAN_NOT_MODIFY.getCode(),TaskEnum.CAN_NOT_MODIFY.getMsg());
        }
        if(housekeepingTask.getHousekeeperPhone().equals(housekeeper.getHousekeeperTel())){
            throw new TaskException(TaskEnum.SAME_HOUSEKEEPER.getCode(),TaskEnum.SAME_HOUSEKEEPER.getMsg());
        }
        housekeepingTask.setHousekeeperName(housekeeper.getHousekeeperName());
        housekeepingTask.setHousekeeperPhone(housekeeper.getHousekeeperTel());
        housekeepingTaskMapper.updateById(housekeepingTask);
        AdminModifyDto adminModifyDto = BeanCopyUtil.copyObject(housekeepingTask, AdminModifyDto::new);
        //记录日志
        HousekeepingTaskLog taskLog = TaskLogUtil.getLog(housekeepingTask, LogType.MANAGER, LogReason.ADMIN_MODIFY_TASK, user.getId());
        Message<HousekeepingTaskLog> message = MessageBuilder.withPayload(taskLog).build();
        rocketMQTemplate.asyncSend(ConsumerTopic.TASK_LOG, message, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
            }

            @Override
            public void onException(Throwable throwable) {
                log.warn("消息发送失败,日志保存失败,{}",log);
            }
        });
        return adminModifyDto;
    }

    /**
     * 用户修改工单方法
     * @param userModifyForm
     * @return
     */
    @Override
    public UserModifyDto userModifyTask(UserModifyForm userModifyForm) {
        log.info("taskService入参:{}",userModifyForm);
        //解析token获取当前用户id并从redis中取出user对象
        User user = userUtil.getUser(ServletUtil.getRequest());
        if(ObjectUtils.isEmpty(user)){
            throw new UserException(UserEnum.UESR_NOT_EXIST.getCode(),UserEnum.UESR_NOT_EXIST.getMsg());
        }
        HousekeepingTask housekeepingTask = housekeepingTaskMapper.selectById(userModifyForm.getTaskId());
        if(ObjectUtils.isEmpty(housekeepingTask)){
            throw new TaskException(TaskEnum.TASK_INFO_ERROR.getCode(),TaskEnum.TASK_INFO_ERROR.getMsg());
        }
        if(user.getTel()!=housekeepingTask.getUserPhone()){
            throw new TaskException(TaskEnum.TASK_INFO_ERROR.getCode(),TaskEnum.TASK_INFO_ERROR.getMsg());
        }
        if(housekeepingTask.getStatus()!=TaskStatus.DEFAULT_STATUS){
            throw new TaskException(TaskEnum.CAN_NOT_MODIFY.getCode(),TaskEnum.CAN_NOT_MODIFY.getMsg());
        }
        Long visitTime=null;
        try{
            visitTime = DateUtil.parse(userModifyForm.getVisitTime(), "yyyy-MM-dd HH:mm:ss");
        }catch (Exception e){
            throw new TaskException(TaskEnum.TIME_ERROR.getCode(),TaskEnum.TIME_ERROR.getMsg());
        }
        housekeepingTask.setVisitTime(visitTime);
        UserModifyDto userModifyDto = BeanCopyUtil.copyObject(housekeepingTask, UserModifyDto::new);
        //记录日志
        HousekeepingTaskLog taskLog = TaskLogUtil.getLog(housekeepingTask, LogType.NORMAL_USER, LogReason.USER_MODIFY_TASK, user.getId());
        Message<HousekeepingTaskLog> message = MessageBuilder.withPayload(taskLog).build();
        rocketMQTemplate.asyncSend(ConsumerTopic.TASK_LOG, message, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
            }

            @Override
            public void onException(Throwable throwable) {
                log.warn("消息发送失败,日志保存失败,{}",log);
            }
        });
        return userModifyDto;
    }

    /**
     * 工单查询方法
     * @param searchTaskForm
     * @return
     */
    @Override
    public TaskListDto searchTask(SearchTaskForm searchTaskForm) {
        log.info("入参：{}",searchTaskForm);
        //解析token获取当前用户id并从redis中取出user对象
        Page<HousekeepingTask> page = new Page<>(searchTaskForm.getCurrent(), searchTaskForm.getPageSize());
        QueryWrapper<HousekeepingTask> wrapper = new QueryWrapper<>();
        //判断条件是否存在
        wrapper.eq(!ObjectUtils.isEmpty(searchTaskForm.getStatus()),"status",searchTaskForm.getStatus());
        Long startTime =null;
        if (null!=searchTaskForm.getStartTime()){
            try{
                startTime=DateUtil.parse(searchTaskForm.getStartTime(),"yyyy-MM-dd HH:mm:ss");
                if(startTime>System.currentTimeMillis()){
                    throw new OrderException(OrderEnum.Time_ERROR.getCode(),OrderEnum.Time_ERROR.getMsg());
                }
                wrapper.lt("create_time",startTime);
            }catch (Exception e){
                throw new OrderException(OrderEnum.ORDER_ERROR.getCode(),OrderEnum.ORDER_ERROR.getMsg());
            }
        }
        if (null!=searchTaskForm.getEndTime()){
            try{
                Long endTime =DateUtil.parse(searchTaskForm.getEndTime(),"yyyy-MM-dd HH:mm:ss");
                if(null!=startTime){
                    if(startTime>endTime){
                        throw new OrderException(OrderEnum.Time_ERROR.getCode(),OrderEnum.Time_ERROR.getMsg());
                    }
                }
                wrapper.lt("create_time",endTime);
            }catch (Exception e){
                throw new OrderException(OrderEnum.ORDER_ERROR.getCode(),OrderEnum.ORDER_ERROR.getMsg());
            }
        }
        if (null!=searchTaskForm.getUsername()){
            wrapper.eq("username",searchTaskForm.getUsername());
        }
        wrapper.eq(null!=searchTaskForm.getStatus(),"status",searchTaskForm.getStatus());
        Page<HousekeepingTask> housekeepingTaskPage = housekeepingTaskMapper.selectPage(page, wrapper);
        if (housekeepingTaskPage.getRecords().size()<=0){
            throw new TaskException(TaskEnum.EMPTY_DATA.getCode(),TaskEnum.EMPTY_DATA.getMsg());
        }
        TaskListDto taskListDto = BeanCopyUtil.copyObject(housekeepingTaskPage, TaskListDto::new);
        List<SearchTaskDto> searchTaskDtos = BeanCopyUtil.copyList(housekeepingTaskPage.getRecords(), SearchTaskDto::new);
        taskListDto.setTasks(searchTaskDtos);
        return taskListDto;

    }

}
