package com.woniu.recycle.housekeeping.service;

import com.woniu.recycle.housekeeping.controller.form.*;
import com.woniu.recycle.housekeeping.domain.HousekeepingOrder;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.recycle.housekeeping.dto.*;

/**
 * <p>
 * 家政订单表 服务类
 * </p>
 *
 * @author cy/lyl
 * @since 2021-08-11
 */
public interface HousekeepingOrderService extends IService<HousekeepingOrder> {

    CreateOrderDto createOrder(CreateOrderForm createOrderForm);

    OrderListDto searchOrder(SearchOrderForm searchOrderForm);

    void confirmOrder(Integer orderId);

    void cancelOrder(CancelOrderForm cancelOrderForm);

    CommentOrderDto commentOrder(CommentOrderForm commentOrderForm);

    DeleteOrderDto deleteOrder(DeleteOrderForm deleteOrderForm);

    OrderListDto adminSearchOrder(AdminSearchOrderForm adminSearchOrderForm);

    PayOrderDto payOrder(PayOrderForm payOrderForm);

    RefundDto refund(RefundForm refundForm);
}
