package com.woniu.recycle.housekeeping.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * TODO
 *订单日志dto对象
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/23 0023
 */
@Data
public class SearchOrderLogDto {
    @ApiModelProperty(value = "回收商品订单日志id")
    private Integer id;

    @ApiModelProperty(value = "订单id")
    private Integer orderId;

    @ApiModelProperty(value = "订单编号")
    private String orderNum;

    @ApiModelProperty(value = "日记创建时间")
    private Long logCreateTime;

    @ApiModelProperty(value = "订单状态")
    private Integer orderStatus;

    @ApiModelProperty(value = "记录日记生成原因")
    private String logReason;

    @ApiModelProperty(value = "操作者类型,0普通用户,1管理员")
    private Integer logType;

    @ApiModelProperty(value = "操作者ID")
    private Integer userId;

}
