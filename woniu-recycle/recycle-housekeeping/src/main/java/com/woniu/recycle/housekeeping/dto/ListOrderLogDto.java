package com.woniu.recycle.housekeeping.dto;

import lombok.Data;

import java.util.List;

/**
 * TODO
 *日志查询接口返回对象
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/23 0023
 */
@Data
public class ListOrderLogDto {
    private Long total;
    private Long size;
    private Long current;
    private List<SearchOrderLogDto> logs;
}
