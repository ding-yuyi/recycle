package com.woniu.recycle.housekeeping.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * TODO
 *查询订单返回对象
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/14 0014
 */
@Data
public class SearchOrderDto {
    private Integer id;

    private String housekeepingOrderNum;

    private Integer productId;

    private Integer productUrl;

    private String price;

    private String count;

    private BigDecimal total;

    private BigDecimal visitTime;

    private Integer addrId;

    private Integer userId;

    private String userName;

    private String addrDesc;

    private Integer housekeeperName;

    private String housekeeperTel;

    private Integer housekeeperId;

    private Integer status;

    private Long createTime;

}
