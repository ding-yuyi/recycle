package com.woniu.recycle.housekeeping.utils;

import com.woniu.recycle.commons.web.util.DateUtil;

import java.util.Random;


/**
 * @ClassName OrderNumUtil
 * @Description TODO
 * @Author JHurry
 * @Date 2021/8/12 15:43
 * @Version 1.0
 */
public class OrderNumUtil {

    /**
     * 生成订单编号
     * @param userId
     * @return
     */
    public static String getNum(Integer userId){
        String date2String = DateUtil.parseToTime(System.currentTimeMillis(), "yyyyMMddHHmmss");
        Random random = new Random();
        String str = "";
        for (int i = 0; i < 6; i++) {
            str = str + random.nextInt(10);
        }
        String orderNum = date2String +userId + str;
        return orderNum;
    }
}
