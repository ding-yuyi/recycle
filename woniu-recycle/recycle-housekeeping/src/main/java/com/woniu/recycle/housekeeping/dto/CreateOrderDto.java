package com.woniu.recycle.housekeeping.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * TODO
 *创建订单接口返回对象
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/14 0014
 */
@Data
public class CreateOrderDto {

    private Integer id;

    private String housekeepingOrderNum;

    private Integer productId;

    private String productUrl;

    private BigDecimal price;

    private Integer count;

    private BigDecimal total;

    private Long visitTime;

    private Integer addrId;

    private String userName;

    private String addrDesc;

    private Integer status;

    private Long createTime;

}
