package com.woniu.recycle.housekeeping.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.recycle.commons.web.util.BeanCopyUtil;
import com.woniu.recycle.housekeeping.component.OrderEnum;
import com.woniu.recycle.housekeeping.controller.form.SearchOrderLogByUserForm;
import com.woniu.recycle.housekeeping.controller.form.SearchOrderLogForm;
import com.woniu.recycle.housekeeping.domain.HousekeepingOrderLog;
import com.woniu.recycle.housekeeping.domain.HousekeepingTaskLog;
import com.woniu.recycle.housekeeping.dto.SearchTaskLogDto;
import com.woniu.recycle.housekeeping.dto.TaskLogPageDto;
import com.woniu.recycle.housekeeping.exception.OrderException;
import com.woniu.recycle.housekeeping.mapper.HousekeepingTaskLogMapper;
import com.woniu.recycle.housekeeping.service.HousekeepingTaskLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 家政工单日志表 服务实现类
 * </p>
 *
 * @author wjh
 * @since 2021-08-12
 */
@Service
@Slf4j
public class HousekeepingTaskLogServiceImpl extends ServiceImpl<HousekeepingTaskLogMapper, HousekeepingTaskLog> implements HousekeepingTaskLogService {
    @Resource
    private HousekeepingTaskLogMapper housekeepingTaskLogMapper;
    @Override
    public TaskLogPageDto searchByOrderId(SearchOrderLogForm searchOrderLogForm) {
        log.info("service入参：{}",searchOrderLogForm);
        Page<HousekeepingTaskLog> page = new Page<>(searchOrderLogForm.getCurrent(), searchOrderLogForm.getPageSize());
        QueryWrapper<HousekeepingTaskLog> wrapper = new QueryWrapper<>();
        //判断条件是否存在og
        wrapper.eq("order_id",searchOrderLogForm.getOrderId());
        Page<HousekeepingTaskLog> housekeepingTaskLogPage = housekeepingTaskLogMapper.selectPage(page, wrapper);
        if(housekeepingTaskLogPage.getRecords().size()<=0){
            throw new OrderException(OrderEnum.EMPTY_DATA.getCode(),OrderEnum.EMPTY_DATA.getMsg());
        }
        TaskLogPageDto taskLogPageDto = BeanCopyUtil.copyObject(housekeepingTaskLogPage, TaskLogPageDto::new);
        List<SearchTaskLogDto> searchTaskLogDtos = BeanCopyUtil.copyList(housekeepingTaskLogPage.getRecords(), SearchTaskLogDto::new);
        taskLogPageDto.setLogs(searchTaskLogDtos);
        return taskLogPageDto;
    }

    @Override
    public TaskLogPageDto searchByUserId(SearchOrderLogByUserForm searchOrderLogByUserForm) {
        log.info("service入参：{}",searchOrderLogByUserForm);
        Page<HousekeepingTaskLog> page = new Page<>(searchOrderLogByUserForm.getCurrent(), searchOrderLogByUserForm.getPageSize());
        QueryWrapper<HousekeepingTaskLog> wrapper = new QueryWrapper<>();
        //判断条件是否存在
        if(null!=searchOrderLogByUserForm.getUserId()){
            wrapper.eq("user_id",searchOrderLogByUserForm.getUserId());
        }
        Page<HousekeepingTaskLog> housekeepingTaskLogPage = housekeepingTaskLogMapper.selectPage(page, wrapper);
        if(housekeepingTaskLogPage.getRecords().size()<=0){
            throw new OrderException(OrderEnum.EMPTY_DATA.getCode(),OrderEnum.EMPTY_DATA.getMsg());
        }
        TaskLogPageDto taskLogPageDto = BeanCopyUtil.copyObject(housekeepingTaskLogPage, TaskLogPageDto::new);
        List<SearchTaskLogDto> searchTaskLogDtos = BeanCopyUtil.copyList(housekeepingTaskLogPage.getRecords(), SearchTaskLogDto::new);
        taskLogPageDto.setLogs(searchTaskLogDtos);
        return taskLogPageDto;
    }


}
