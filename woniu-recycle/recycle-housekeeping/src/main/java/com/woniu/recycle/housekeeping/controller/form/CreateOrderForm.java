package com.woniu.recycle.housekeeping.controller.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class CreateOrderForm {
    @ApiModelProperty(value = "家政商品id")
    @NotNull
    @Min(value = 1)
    private Integer productId;

    @ApiModelProperty(value = "商品数量")
    @NotNull
    @Min(value = 1)
    private Integer count;

    @ApiModelProperty(value = "预约上门时间")
    @NotNull
    private String visitTime;

    @ApiModelProperty(value = "地址id")
    @NotNull
    @Min(value = 1)
    private Integer addrId;

    @ApiModelProperty(value = "备注")
    private String notes;
}
