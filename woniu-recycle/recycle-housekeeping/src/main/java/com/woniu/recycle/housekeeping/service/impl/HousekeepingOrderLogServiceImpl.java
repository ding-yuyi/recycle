package com.woniu.recycle.housekeeping.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.recycle.commons.web.util.BeanCopyUtil;
import com.woniu.recycle.housekeeping.component.OrderEnum;
import com.woniu.recycle.housekeeping.controller.form.SearchOrderLogByUserForm;
import com.woniu.recycle.housekeeping.controller.form.SearchOrderLogForm;
import com.woniu.recycle.housekeeping.domain.HousekeepingOrder;
import com.woniu.recycle.housekeeping.domain.HousekeepingOrderLog;
import com.woniu.recycle.housekeeping.dto.ListOrderLogDto;
import com.woniu.recycle.housekeeping.dto.SearchOrderLogDto;
import com.woniu.recycle.housekeeping.exception.OrderException;
import com.woniu.recycle.housekeeping.mapper.HousekeepingOrderLogMapper;
import com.woniu.recycle.housekeeping.service.HousekeepingOrderLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 家政商品订单日志表 服务实现类
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-11
 */
@Service
@Slf4j
public class HousekeepingOrderLogServiceImpl extends ServiceImpl<HousekeepingOrderLogMapper, HousekeepingOrderLog> implements HousekeepingOrderLogService {
   @Resource
   private HousekeepingOrderLogMapper housekeepingOrderLogMapper;


    /**
     * 根据订单id查询订单日志
     * @param searchOrderLogForm
     * @return
     */
    @Override
    public ListOrderLogDto searchByOrderId(SearchOrderLogForm searchOrderLogForm) {
        log.info("service入参：{}",searchOrderLogForm);
        Page<HousekeepingOrderLog> page = new Page<>(searchOrderLogForm.getCurrent(), searchOrderLogForm.getPageSize());
        QueryWrapper<HousekeepingOrderLog> wrapper = new QueryWrapper<>();
        //判断条件是否存在og
        wrapper.eq("order_id",searchOrderLogForm.getOrderId());
        Page<HousekeepingOrderLog> housekeepingOrderLogPage = housekeepingOrderLogMapper.selectPage(page, wrapper);
        if(housekeepingOrderLogPage.getRecords().size()<=0){
            throw new OrderException(OrderEnum.EMPTY_DATA.getCode(),OrderEnum.EMPTY_DATA.getMsg());
        }
        ListOrderLogDto listOrderLogDto = BeanCopyUtil.copyObject(housekeepingOrderLogPage, ListOrderLogDto::new);
        List<SearchOrderLogDto> searchOrderLogDtos = BeanCopyUtil.copyList(housekeepingOrderLogPage.getRecords(), SearchOrderLogDto::new);
        listOrderLogDto.setLogs(searchOrderLogDtos);
        return listOrderLogDto;
    }

    /**
     * 根据用户id查询订单日志
     * @param searchOrderLogByUserForm
     * @return
     */
    @Override
    public ListOrderLogDto searchByUserId(SearchOrderLogByUserForm searchOrderLogByUserForm) {
        log.info("service入参：{}",searchOrderLogByUserForm);

        Page<HousekeepingOrderLog> page = new Page<>(searchOrderLogByUserForm.getCurrent(), searchOrderLogByUserForm.getPageSize());
        QueryWrapper<HousekeepingOrderLog> wrapper = new QueryWrapper<>();
        //判断条件是否存在
        if(null!=searchOrderLogByUserForm.getUserId()){
            wrapper.eq("user_id",searchOrderLogByUserForm.getUserId());
        }
        Page<HousekeepingOrderLog> housekeepingOrderLogPage = housekeepingOrderLogMapper.selectPage(page, wrapper);
        if(housekeepingOrderLogPage.getRecords().size()<=0){
            throw new OrderException(OrderEnum.EMPTY_DATA.getCode(),OrderEnum.EMPTY_DATA.getMsg());
        }
        ListOrderLogDto listOrderLogDto = BeanCopyUtil.copyObject(housekeepingOrderLogPage, ListOrderLogDto::new);
        List<SearchOrderLogDto> searchOrderLogDtos = BeanCopyUtil.copyList(housekeepingOrderLogPage.getRecords(), SearchOrderLogDto::new);
        listOrderLogDto.setLogs(searchOrderLogDtos);
        return listOrderLogDto;
    }
}
