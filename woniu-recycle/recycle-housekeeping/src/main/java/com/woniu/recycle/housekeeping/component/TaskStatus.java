package com.woniu.recycle.housekeeping.component;

/**
 * TODO
 *工单数据常量类
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/19 0019
 */
public class TaskStatus {
    //生成工单单默认状态
    public static final int DEFAULT_STATUS=0;
    //工单上门中状态
    public static final int ON_THE_WAY=1;
    //工单服务中状态
    public static final int IN_PROGRESS=2;
    //工单取消状态
    public static final int CANCELED=4;
    //工单完成状态
    public static final int Finished=3;
}
