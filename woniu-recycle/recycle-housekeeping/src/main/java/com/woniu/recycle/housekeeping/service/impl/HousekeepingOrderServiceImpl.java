package com.woniu.recycle.housekeeping.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.recycle.commons.core.component.ResponseEnum;
import com.woniu.recycle.commons.core.model.ResponseEntity;
import com.woniu.recycle.commons.web.util.BeanCopyUtil;
import com.woniu.recycle.commons.web.util.DateUtil;
import com.woniu.recycle.commons.web.util.ServletUtil;
import com.woniu.recycle.housekeeping.component.*;
import com.woniu.recycle.housekeeping.controller.form.*;
import com.woniu.recycle.housekeeping.domain.HousekeepingOrder;
import com.woniu.recycle.housekeeping.domain.HousekeepingOrderLog;
import com.woniu.recycle.housekeeping.domain.HousekeepingTask;
import com.woniu.recycle.housekeeping.domain.User;
import com.woniu.recycle.housekeeping.dto.*;
import com.woniu.recycle.housekeeping.exception.OrderException;
import com.woniu.recycle.housekeeping.exception.TaskException;
import com.woniu.recycle.housekeeping.exception.UserException;
import com.woniu.recycle.housekeeping.feign.client.ProductClient;
import com.woniu.recycle.housekeeping.feign.client.UserClient;
import com.woniu.recycle.housekeeping.mapper.HousekeepingOrderMapper;
import com.woniu.recycle.housekeeping.mapper.HousekeepingTaskMapper;
import com.woniu.recycle.housekeeping.service.HousekeepingOrderLogService;
import com.woniu.recycle.housekeeping.service.HousekeepingOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.recycle.housekeeping.service.HousekeepingTaskService;
import com.woniu.recycle.housekeeping.utils.OrderLogUtil;
import com.woniu.recycle.housekeeping.utils.OrderNumUtil;
import com.woniu.recycle.housekeeping.utils.UserUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;

import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 家政订单表 服务实现类
 * </p>
 *
 * @author Jhurry
 * @since 2021-08-11
 */
@Service
@Slf4j
public class HousekeepingOrderServiceImpl extends ServiceImpl<HousekeepingOrderMapper, HousekeepingOrder> implements HousekeepingOrderService {

    @Resource
    private HousekeepingOrderLogService logService;

    @Resource
    private HousekeepingOrderMapper housekeepingOrderMapper;

    @Resource
    private UserUtil userUtil;

    @Resource
    private ProductClient productClient;

    @Resource
    private UserClient userClient;

    @Resource
    private HousekeepingTaskService housekeepingTaskService;

    @Resource
    private HousekeepingTaskMapper housekeepingTaskMapper;

    @Resource
    private RocketMQTemplate rocketMQTemplate;

    /**
     *创建订单
     * @param createOrderForm
     * @return CreateOrderDto
     */
    @Override
    public CreateOrderDto createOrder(CreateOrderForm createOrderForm) {
        log.info("service入参：{}",createOrderForm);
        //解析token获取当前用户id并从redis中取出user对象
        User user = userUtil.getUser(ServletUtil.getRequest());
        if(ObjectUtils.isEmpty(user)){
            throw new UserException(UserEnum.UESR_NOT_EXIST.getCode(),UserEnum.UESR_NOT_EXIST.getMsg());
        }
        if(null==user.getTel()){
            throw new OrderException(OrderEnum.TEL_ERROR.getCode(),OrderEnum.TEL_ERROR.getMsg());
        }
        //调用openfeign查询验证商品信息
        ResponseEntity<HouseKeepingProductDto> productById = productClient.findProductById(createOrderForm.getProductId());
        if(null==productById.getData()){
            throw new OrderException(OrderEnum.PRODUCT_NOT_EXIST.getCode(),OrderEnum.PRODUCT_NOT_EXIST.getMsg());
        }
        HouseKeepingProductDto housekeepingProduct = productById.getData();
        //调用feign查询用户的所有地址
        ResponseEntity<AddrFrom> addrById = userClient.findAddrById(createOrderForm.getAddrId());
        if(null==addrById.getData()){
            throw new OrderException(OrderEnum.ADDR_ERROR.getCode(),OrderEnum.ADDR_ERROR.getMsg());
        }

        //生成订单信息
        HousekeepingOrder housekeepingOrder = BeanCopyUtil.copyObject(createOrderForm, HousekeepingOrder::new);
        housekeepingOrder.setUserId(user.getId());
        housekeepingOrder.setAddrDesc(addrById.getData().getAddrDesc());
        housekeepingOrder.setPrice(housekeepingProduct.getPrice());
        housekeepingOrder.setCount(createOrderForm.getCount());
        housekeepingOrder.setProductUrl(housekeepingProduct.getPictureUrl());
        if(null!=createOrderForm.getNotes()) {
            housekeepingOrder.setNotes(createOrderForm.getNotes());
        }
        Long visitTime=null;
        try{
            visitTime = DateUtil.parse(createOrderForm.getVisitTime(), "yyyy-MM-dd HH:mm:ss");
        }catch (Exception e){
            throw new OrderException(OrderEnum.ORDER_ERROR.getCode(),OrderEnum.ORDER_ERROR.getMsg());
        }
        if(visitTime<System.currentTimeMillis()){
            throw new OrderException(OrderEnum.Time_ERROR.getCode(),OrderEnum.Time_ERROR.getMsg());
        }
        housekeepingOrder.setVisitTime(visitTime);
        housekeepingOrder.setTotal(housekeepingOrder.getPrice().multiply(new BigDecimal(createOrderForm.getCount())));
        housekeepingOrder.setStatus(OrderStatus.DEFAULT_STATUS);
        housekeepingOrder.setCreateTime(System.currentTimeMillis());
        //根据userId生成订单编号
        housekeepingOrder.setHousekeepingOrderNum(OrderNumUtil.getNum(user.getId()));
        CreateOrderDto createOrderDto = BeanCopyUtil.copyObject(housekeepingOrder, CreateOrderDto::new);
        boolean save = save(housekeepingOrder);

        if(!save)throw new OrderException(OrderEnum.ORDER_ERROR.getCode(),OrderEnum.ORDER_ERROR.getMsg());
        createOrderDto.setId(housekeepingOrder.getId());
        createOrderDto.setUserName(user.getUsername());
        //生成工单信息
        CreateTaskForm createTaskForm = new CreateTaskForm();
        createTaskForm.setOrderId(housekeepingOrder.getId());
        createTaskForm.setProductName(housekeepingProduct.getName());
        createTaskForm.setProductDescription(housekeepingProduct.getDescription());
        createTaskForm.setUsername(user.getUsername());
        createTaskForm.setUserPhone(user.getTel());
        createTaskForm.setUserId(user.getId());
        Boolean task = housekeepingTaskService.createTask(createTaskForm);
        if(!task)throw new TaskException(TaskEnum.TASK_CREATE_ERROR.getCode(),TaskEnum.TASK_CREATE_ERROR.getMsg());
        //发送延时消息消息超时自动取消订单
        Message<Integer> orderMessage = MessageBuilder.withPayload(housekeepingOrder.getId()).build();
        rocketMQTemplate.syncSend(ConsumerTopic.ORDER,orderMessage,3000,10);
        //生成日志信息
        HousekeepingOrderLog housekeepingOrderLog = OrderLogUtil.getLog(housekeepingOrder, LogType.NORMAL_USER, LogReason.CREATE_ORDER, user.getId());
        Message<HousekeepingOrderLog> message = MessageBuilder.withPayload(housekeepingOrderLog).build();
        rocketMQTemplate.asyncSend(ConsumerTopic.ORDER_LOG, message, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
            }

            @Override
            public void onException(Throwable throwable) {
                log.warn("消息发送失败,日志保存失败,{}",housekeepingOrderLog);
            }
        });

        return createOrderDto;
    }

    /**
     * 用户订单查询
     * @param searchOrderForm
     * @return OderListDto
     */
    @Override
    public OrderListDto searchOrder(SearchOrderForm searchOrderForm) {
        log.info("service入参：{}",searchOrderForm);
        //解析token获取当前用户id并从redis中取出user对象
        User user = userUtil.getUser(ServletUtil.getRequest());
        if(ObjectUtils.isEmpty(user)){
            throw new UserException(UserEnum.UESR_NOT_EXIST.getCode(),UserEnum.UESR_NOT_EXIST.getMsg());
        }
        Page<HousekeepingOrder> page = new Page<>(searchOrderForm.getCurrent(), searchOrderForm.getPageSize());
        QueryWrapper<HousekeepingOrder> wrapper = new QueryWrapper<>();
        //判断条件是否存在
        wrapper.eq(null!=searchOrderForm.getStatus(),"status",searchOrderForm.getStatus());
        Long startTime =null;
        if (null!=searchOrderForm.getStartTime()){
            try{
               startTime=DateUtil.parse(searchOrderForm.getStartTime(), "yyyy-MM-dd HH:mm:ss");
                if(startTime>System.currentTimeMillis()){
                    throw new OrderException(OrderEnum.Time_ERROR.getCode(),OrderEnum.Time_ERROR.getMsg());
            }
                wrapper.gt("create_time",startTime);
            }catch (Exception e){
                throw new OrderException(OrderEnum.ORDER_ERROR.getCode(),OrderEnum.ORDER_ERROR.getMsg());
            }

        }
        if (null!=searchOrderForm.getEndTime()){
            try{
                Long endTime = DateUtil.parse(searchOrderForm.getEndTime(), "yyyy-MM-dd HH:mm:ss");
                if(null!=startTime){
                    if(startTime>endTime){
                        throw new OrderException(OrderEnum.Time_ERROR.getCode(),OrderEnum.Time_ERROR.getMsg());
                    }
                }
                wrapper.lt("create_time",endTime);
            }catch (Exception e){
                throw new OrderException(OrderEnum.ORDER_ERROR.getCode(),OrderEnum.ORDER_ERROR.getMsg());
            }
        }
        wrapper.eq("deleted",OrderStatus.NOT_LOGIC_DELETED);
        wrapper.eq("user_id",user.getId());
        Page<HousekeepingOrder> housekeepingOrderPage = housekeepingOrderMapper.selectPage(page, wrapper);
        if (housekeepingOrderPage.getRecords().size()<=0){
            throw new OrderException(OrderEnum.EMPTY_DATA.getCode(),OrderEnum.EMPTY_DATA.getMsg());
        }
        OrderListDto orderListDto = BeanCopyUtil.copyObject(housekeepingOrderPage, OrderListDto::new);
        log.info("{}",housekeepingOrderPage.getRecords());
        List<SearchOrderDto> searchOrderDtos = BeanCopyUtil.copyList(housekeepingOrderPage.getRecords(), SearchOrderDto::new);
        orderListDto.setOrders(searchOrderDtos);
        log.info("{}",orderListDto);
        return orderListDto;
    }

    /**
     * 用户确认完成
     * @param orderId
     */
    @Override
    public void confirmOrder(Integer orderId) {
        log.info("service入参:{}",orderId);
        //解析token获取当前用户id并从redis中取出user对象
        User user = userUtil.getUser(ServletUtil.getRequest());
        if(ObjectUtils.isEmpty(user)){
            throw new UserException(UserEnum.UESR_NOT_EXIST.getCode(),UserEnum.UESR_NOT_EXIST.getMsg());
        }
        //判断参数
        if(null==orderId){
            throw new OrderException(OrderEnum.ORDER_ERROR.getCode(),OrderEnum.ORDER_ERROR.getMsg());
        }

        //判断订单是否存在
        QueryWrapper<HousekeepingOrder> wrapper = new QueryWrapper<>();
        wrapper.eq("id",orderId);
        HousekeepingOrder housekeepingOrder = housekeepingOrderMapper.selectOne(wrapper);
        if(ObjectUtils.isEmpty(housekeepingOrder)){
            throw new OrderException(OrderEnum.ORDER_NOT_EXIST.getCode(),OrderEnum.ORDER_NOT_EXIST.getMsg());
        }
        //判断订单是否为当前用户的
        if(housekeepingOrder.getUserId()!=user.getId()){
            throw new OrderException(OrderEnum.ORDER_NOT_EXIST.getCode(),OrderEnum.ORDER_NOT_EXIST.getMsg());
        }
        //判断订单状态
        if(housekeepingOrder.getStatus()!=OrderStatus.ORDER_IN_PROGRESS){
            throw new OrderException(OrderEnum.ORDER_ERROR.getCode(),OrderEnum.ORDER_ERROR.getMsg());
        }
        //判断工单状态，只有确认服务人员到达后才能提交订单
        QueryWrapper<HousekeepingTask> taskWrapper = new QueryWrapper<>();
        taskWrapper.eq("order_id",orderId);
        HousekeepingTask housekeepingTask = housekeepingTaskMapper.selectOne(taskWrapper);
        if(null==housekeepingTask){
            throw new TaskException(TaskEnum.SYSTEM_ERROR.getCode(),TaskEnum.SYSTEM_ERROR.getMsg());
        }
        if(housekeepingTask.getStatus()!=TaskStatus.IN_PROGRESS&&housekeepingTask.getStatus()!=TaskStatus.Finished){
            throw new TaskException(TaskEnum.CAN_NOT_DISTRIBUTE.getCode(),TaskEnum.CAN_NOT_DISTRIBUTE.getMsg());
        }
        housekeepingOrder.setStatus(OrderStatus.FINISHED);
        housekeepingOrder.setFinishTime(System.currentTimeMillis());
        housekeepingOrderMapper.updateById(housekeepingOrder);

        //记录日志
        HousekeepingOrderLog housekeepingOrderLog = OrderLogUtil.getLog(housekeepingOrder, LogType.NORMAL_USER, LogReason.ORDER_FINISH, user.getId());
        Message<HousekeepingOrderLog> message = MessageBuilder.withPayload(housekeepingOrderLog).build();
        rocketMQTemplate.asyncSend(ConsumerTopic.ORDER_LOG, message, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
            }

            @Override
            public void onException(Throwable throwable) {
                log.warn("消息发送失败,日志保存失败,{}",housekeepingOrderLog);
            }
        });

    }

    /**
     * 订单取消
     * @param cancelOrderForm
     */
    @Override
    public void cancelOrder(CancelOrderForm cancelOrderForm) {
        log.info("service入参:{}",cancelOrderForm);
        //解析token获取当前用户id并从redis中取出user对象
        User user = userUtil.getUser(ServletUtil.getRequest());
        if(ObjectUtils.isEmpty(user)){
            throw new UserException(UserEnum.UESR_NOT_EXIST.getCode(),UserEnum.UESR_NOT_EXIST.getMsg());
        }

        //判断订单是否存在
        QueryWrapper<HousekeepingOrder> wrapper = new QueryWrapper<>();
        wrapper.eq("id",cancelOrderForm.getOrderId());
        HousekeepingOrder housekeepingOrder = housekeepingOrderMapper.selectOne(wrapper);
        if(ObjectUtils.isEmpty(housekeepingOrder)){
            throw new OrderException(OrderEnum.ORDER_NOT_EXIST.getCode(),OrderEnum.ORDER_NOT_EXIST.getMsg());
        }
        //判断订单是否为当前用户的
        if(!housekeepingOrder.getUserId().equals(user.getId())){
            throw new OrderException(OrderEnum.ORDER_NOT_EXIST.getCode(),OrderEnum.ORDER_NOT_EXIST.getMsg());
        }
        //判断订单状态
        if(housekeepingOrder.getStatus()!=OrderStatus.DEFAULT_STATUS&&housekeepingOrder.getStatus()!=OrderStatus.PAYED){
            throw new OrderException(OrderEnum.ORDER_IN_PROGRESS.getCode(),OrderEnum.ORDER_IN_PROGRESS.getMsg());
        }
        if(housekeepingOrder.getStatus()==OrderStatus.DEFAULT_STATUS){
            housekeepingOrder.setStatus(OrderStatus.CANCELLED);
        }else {
            housekeepingOrder.setStatus(OrderStatus.PAYED_CANCELLED);
        }
        //修改订单状态
        housekeepingOrder.setCancelTime(System.currentTimeMillis());
        if(null!=cancelOrderForm.getCancelReason()) {
            housekeepingOrder.setCancelReason(cancelOrderForm.getCancelReason());
        }
        housekeepingOrderMapper.updateById(housekeepingOrder);

        //记录日志
        HousekeepingOrderLog housekeepingOrderLog = OrderLogUtil.getLog(housekeepingOrder, LogType.NORMAL_USER, LogReason.CANCEL_ORDER, user.getId());
        Message<HousekeepingOrderLog> message = MessageBuilder.withPayload(housekeepingOrderLog).build();
        rocketMQTemplate.asyncSend(ConsumerTopic.ORDER_LOG, message, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
            }

            @Override
            public void onException(Throwable throwable) {
                log.warn("消息发送失败,日志保存失败,{}",housekeepingOrderLog);
            }
        });
    }

    /**
     * 评论订单
     * @param commentOrderForm
     * @return CommentOrderDto
     */
    @Override
    public CommentOrderDto commentOrder(CommentOrderForm commentOrderForm) {
        log.info("service入参:{}",commentOrderForm);
        //解析token获取当前用户id并从redis中取出user对象
        User user = userUtil.getUser(ServletUtil.getRequest());
        if(ObjectUtils.isEmpty(user)){
            throw new UserException(UserEnum.UESR_NOT_EXIST.getCode(),UserEnum.UESR_NOT_EXIST.getMsg());
        }

        //判断订单是否存在
        QueryWrapper<HousekeepingOrder> wrapper = new QueryWrapper<>();
        wrapper.eq("id",commentOrderForm.getOrderId());
        HousekeepingOrder housekeepingOrder = housekeepingOrderMapper.selectOne(wrapper);
        if(ObjectUtils.isEmpty(housekeepingOrder)){
            throw new OrderException(OrderEnum.ORDER_NOT_EXIST.getCode(),OrderEnum.ORDER_NOT_EXIST.getMsg());
        }
        //判断订单是否为当前用户的
        if(housekeepingOrder.getUserId()!=user.getId()){
            throw new OrderException(OrderEnum.ORDER_NOT_EXIST.getCode(),OrderEnum.ORDER_NOT_EXIST.getMsg());
        }
        //判断订单状态
        if(housekeepingOrder.getStatus()!=OrderStatus.FINISHED){
            throw new OrderException(OrderEnum.ORDER_ERROR.getCode(),OrderEnum.ORDER_ERROR.getMsg());
        }
        //修改订单状态
        if(null!=commentOrderForm.getComment()){
            housekeepingOrder.setComment(commentOrderForm.getComment());
        }
        if(null!=commentOrderForm.getLevel()){
            if(commentOrderForm.getLevel()<OrderStatus.ORDER_LOWEST_LEVEL||commentOrderForm.getLevel()>OrderStatus.ORDER_HIGHEST_LEVEL){
                housekeepingOrder.setLevel(OrderStatus.ORDER_HIGHEST_LEVEL);
            }else{
                housekeepingOrder.setLevel(commentOrderForm.getLevel());
            }
        }
        housekeepingOrder.setStatus(OrderStatus.COMMENTED);
        housekeepingOrderMapper.updateById(housekeepingOrder);
        CommentOrderDto commentOrderDto = BeanCopyUtil.copyObject(housekeepingOrder, CommentOrderDto::new);

        //记录日志
        HousekeepingOrderLog housekeepingOrderLog = OrderLogUtil.getLog(housekeepingOrder, LogType.NORMAL_USER, LogReason.COMMENT, user.getId());
        Message<HousekeepingOrderLog> message = MessageBuilder.withPayload(housekeepingOrderLog).build();
        rocketMQTemplate.asyncSend(ConsumerTopic.ORDER_LOG, message, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
            }

            @Override
            public void onException(Throwable throwable) {
                log.warn("消息发送失败,日志保存失败,{}",housekeepingOrderLog);
            }
        });

        return commentOrderDto;
    }

    /**
     * 删除订单
     * @param deleteOrderForm
     * @return
     */
    @Override
    public DeleteOrderDto deleteOrder(DeleteOrderForm deleteOrderForm) {
        log.info("service入参：{}",deleteOrderForm);
        //解析token获取当前用户id并从redis中取出user对象
        User user = userUtil.getUser(ServletUtil.getRequest());
        if(ObjectUtils.isEmpty(user)){
            throw new UserException(UserEnum.UESR_NOT_EXIST.getCode(),UserEnum.UESR_NOT_EXIST.getMsg());
        }

        //判断订单是否存在
        QueryWrapper<HousekeepingOrder> wrapper = new QueryWrapper<>();
        wrapper.eq("id",deleteOrderForm.getOrderId());
        HousekeepingOrder housekeepingOrder = housekeepingOrderMapper.selectOne(wrapper);
        if(ObjectUtils.isEmpty(housekeepingOrder)){
            throw new OrderException(OrderEnum.ORDER_NOT_EXIST.getCode(),OrderEnum.ORDER_NOT_EXIST.getMsg());
        }
        //判断订单是否为当前用户的
        if(!housekeepingOrder.getUserId().equals(user.getId())){
            throw new OrderException(OrderEnum.ORDER_NOT_EXIST.getCode(),OrderEnum.ORDER_NOT_EXIST.getMsg());
        }
        //判断订单状态
        if(housekeepingOrder.getStatus()!=OrderStatus.FINISHED||housekeepingOrder.getStatus()!=OrderStatus.COMMENTED){
            throw new OrderException(OrderEnum.CAN_NOT_DELETE.getCode(),OrderEnum.CAN_NOT_DELETE.getMsg());
        }
        housekeepingOrder.setDeleted(OrderStatus.LOGIC_DELETED);
        DeleteOrderDto deleteOrderDto = BeanCopyUtil.copyObject(housekeepingOrder, DeleteOrderDto::new);
        housekeepingOrderMapper.updateById(housekeepingOrder);
        //记录日志
        HousekeepingOrderLog housekeepingOrderLog = OrderLogUtil.getLog(housekeepingOrder, LogType.NORMAL_USER, LogReason.DELETE_ORDER, user.getId());
        Message<HousekeepingOrderLog> message = MessageBuilder.withPayload(housekeepingOrderLog).build();
        rocketMQTemplate.asyncSend(ConsumerTopic.ORDER_LOG, message, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
            }

            @Override
            public void onException(Throwable throwable) {
                log.warn("消息发送失败,日志保存失败,{}",housekeepingOrderLog);
            }
        });
        return deleteOrderDto;
    }

    /**
     * 管理员订单查询方法
     * @param adminSearchOrderForm
     * @return
     */
    @Override
    public OrderListDto adminSearchOrder(AdminSearchOrderForm adminSearchOrderForm) {
        log.info("service入参：{}",adminSearchOrderForm);
        //发openfeign验证用户是否存在
        if(null!=adminSearchOrderForm.getUserId()){
            ResponseEntity<User> userResponseEntity = userClient.UserById(adminSearchOrderForm.getUserId());
            User user = userResponseEntity.getData();
            if(ObjectUtils.isEmpty(user)){
                throw new UserException(UserEnum.UESR_NOT_EXIST.getCode(),UserEnum.UESR_NOT_EXIST.getMsg());
            }
        }
        Page<HousekeepingOrder> page = new Page<>(adminSearchOrderForm.getCurrent(), adminSearchOrderForm.getPageSize());
        QueryWrapper<HousekeepingOrder> wrapper = new QueryWrapper<>();
        //判断条件是否存在
        wrapper.eq(null!=adminSearchOrderForm.getStatus(),"status",adminSearchOrderForm.getStatus());
        Long startTime =null;
        if (null!=adminSearchOrderForm.getStartTime()){
            try{
                startTime=DateUtil.parse(adminSearchOrderForm.getStartTime(),"yyyy-MM-dd HH:mm:ss");
                if(startTime>System.currentTimeMillis()){
                    throw new OrderException(OrderEnum.Time_ERROR.getCode(),OrderEnum.Time_ERROR.getMsg());
                }
                wrapper.lt("create_time",startTime);
            }catch (Exception e){
                throw new OrderException(OrderEnum.ORDER_ERROR.getCode(),OrderEnum.ORDER_ERROR.getMsg());
            }
        }
        if (null!=adminSearchOrderForm.getEndTime()){
            try{
                Long endTime =DateUtil.parse(adminSearchOrderForm.getEndTime(),"yyyy-MM-dd HH:mm:ss");
                if(null!=startTime){
                    if(startTime>endTime){
                        throw new OrderException(OrderEnum.Time_ERROR.getCode(),OrderEnum.Time_ERROR.getMsg());
                    }
                }
                wrapper.lt("create_time",endTime);
            }catch (Exception e){
                throw new OrderException(OrderEnum.ORDER_ERROR.getCode(),OrderEnum.ORDER_ERROR.getMsg());
            }
        }
       wrapper.orderByDesc("create_time");
        Page<HousekeepingOrder> housekeepingOrderPage = housekeepingOrderMapper.selectPage(page, wrapper);
        if (housekeepingOrderPage.getRecords().size()<=0){
            throw new OrderException(OrderEnum.EMPTY_DATA.getCode(),OrderEnum.EMPTY_DATA.getMsg());
        }
        OrderListDto orderListDto = BeanCopyUtil.copyObject(housekeepingOrderPage, OrderListDto::new);
        List<SearchOrderDto> searchOrderDtos = BeanCopyUtil.copyList(housekeepingOrderPage.getRecords(), SearchOrderDto::new);
        orderListDto.setOrders(searchOrderDtos);
        return orderListDto;
    }

    /**
     * 付款方法
     * @param payOrderForm
     * @return
     */
    @Override
    public PayOrderDto payOrder(PayOrderForm payOrderForm) {
        //解析token获取当前用户id并从redis中取出user对象
        User user = userUtil.getUser(ServletUtil.getRequest());
        if(ObjectUtils.isEmpty(user)){
            throw new UserException(UserEnum.UESR_NOT_EXIST.getCode(),UserEnum.UESR_NOT_EXIST.getMsg());
        }
        HousekeepingOrder housekeepingOrder = housekeepingOrderMapper.selectById(payOrderForm.getOrderId());
        if(ObjectUtils.isEmpty(housekeepingOrder)){
            throw new OrderException(OrderEnum.ORDER_NOT_EXIST.getCode(),OrderEnum.ORDER_NOT_EXIST.getMsg());
        }
        if(housekeepingOrder.getUserId()!=user.getId()){
            throw new OrderException(OrderEnum.ORDER_ERROR.getCode(),OrderEnum.ORDER_ERROR.getMsg());
        }
        if(housekeepingOrder.getStatus()!=OrderStatus.DEFAULT_STATUS){
            throw new OrderException(OrderEnum.ORDER_ERROR.getCode(),OrderEnum.ORDER_ERROR.getMsg());
        }
        BalanceChangeForm balanceChangeForm = new BalanceChangeForm();
        balanceChangeForm.setId(user.getId());
        balanceChangeForm.setBalance(housekeepingOrder.getTotal());
        balanceChangeForm.setPayNum(housekeepingOrder.getHousekeepingOrderNum());
        ResponseEntity<BalanceChangeDto> responseEntity = userClient.balanceSel(balanceChangeForm);
        if(!responseEntity.getCode().equals(ResponseEnum.RES_SUCCESS.getCode())){
            throw new OrderException(responseEntity.getCode(),responseEntity.getMsg());
        }
        housekeepingOrder.setStatus(OrderStatus.PAYED);
        PayOrderDto payOrderDto = BeanCopyUtil.copyObject(housekeepingOrder, PayOrderDto::new);
        housekeepingOrderMapper.updateById(housekeepingOrder);
        //记录日志
        HousekeepingOrderLog housekeepingOrderLog = OrderLogUtil.getLog(housekeepingOrder, LogType.NORMAL_USER, LogReason.PAY, user.getId());
        Message<HousekeepingOrderLog> message = MessageBuilder.withPayload(housekeepingOrderLog).build();
        rocketMQTemplate.asyncSend(ConsumerTopic.ORDER_LOG, message, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
            }

            @Override
            public void onException(Throwable throwable) {
                log.warn("消息发送失败,日志保存失败,{}",housekeepingOrderLog);
            }
        });
        return payOrderDto;
    }

    /**
     * 退款方法
     * @param refundForm
     * @return
     */
    @Override
    public RefundDto refund(RefundForm refundForm) {
        //解析token获取当前用户id并从redis中取出user对象
        User user = userUtil.getUser(ServletUtil.getRequest());
        if(ObjectUtils.isEmpty(user)){
            throw new UserException(UserEnum.UESR_NOT_EXIST.getCode(),UserEnum.UESR_NOT_EXIST.getMsg());
        }
        HousekeepingOrder housekeepingOrder = housekeepingOrderMapper.selectById(refundForm.getOrderId());
        if(ObjectUtils.isEmpty(housekeepingOrder)){
            throw new OrderException(OrderEnum.ORDER_NOT_EXIST.getCode(),OrderEnum.ORDER_NOT_EXIST.getMsg());
        }
        if(housekeepingOrder.getUserId()!=user.getId()){
            throw new OrderException(OrderEnum.ORDER_ERROR.getCode(),OrderEnum.ORDER_ERROR.getMsg());
        }
        if(housekeepingOrder.getStatus()!=OrderStatus.PAYED_CANCELLED){
            throw new OrderException(OrderEnum.STUTAS_ERROR.getCode(),OrderEnum.STUTAS_ERROR.getMsg());
        }
        BalanceChangeForm balanceChangeForm = new BalanceChangeForm();
        balanceChangeForm.setId(user.getId());
        balanceChangeForm.setBalance(housekeepingOrder.getTotal());
        balanceChangeForm.setPayNum(housekeepingOrder.getHousekeepingOrderNum());
        ResponseEntity<BalanceChangeDto> responseEntity = userClient.balanceAdd(balanceChangeForm);
        if(!responseEntity.getCode().equals(ResponseEnum.RES_SUCCESS.getCode()) ){
            throw new OrderException(responseEntity.getCode(),responseEntity.getMsg());
        }
        housekeepingOrder.setStatus(OrderStatus.CANCELLED);
        RefundDto refundDto = BeanCopyUtil.copyObject(housekeepingOrder, RefundDto::new);
        housekeepingOrderMapper.updateById(housekeepingOrder);
        //记录日志
        HousekeepingOrderLog housekeepingOrderLog = OrderLogUtil.getLog(housekeepingOrder, LogType.NORMAL_USER, LogReason.REFUND, user.getId());
        Message<HousekeepingOrderLog> message = MessageBuilder.withPayload(housekeepingOrderLog).build();
        rocketMQTemplate.asyncSend(ConsumerTopic.ORDER_LOG, message, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
            }

            @Override
            public void onException(Throwable throwable) {
                log.warn("消息发送失败,日志保存失败,{}",housekeepingOrderLog);
            }
        });
        return refundDto;
    }
}
