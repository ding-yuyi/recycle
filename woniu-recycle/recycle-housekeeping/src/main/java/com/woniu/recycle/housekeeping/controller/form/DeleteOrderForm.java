package com.woniu.recycle.housekeeping.controller.form;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * TODO
 *删除评论接口入参对象
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/16 0016
 */
@Data
public class DeleteOrderForm {
    @ApiModelProperty(value = "家政订单id")
    @NotNull
    @Min(value = 1)
    private Integer orderId;
}
