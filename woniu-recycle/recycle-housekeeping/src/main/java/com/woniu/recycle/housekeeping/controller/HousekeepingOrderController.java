package com.woniu.recycle.housekeeping.controller;


import com.woniu.recycle.commons.core.model.ResponseEntity;
import com.woniu.recycle.commons.web.component.BaseController;
import com.woniu.recycle.housekeeping.controller.form.*;
import com.woniu.recycle.housekeeping.dto.*;
import com.woniu.recycle.housekeeping.service.HousekeepingOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;


/**
 * <p>
 * 家政订单表 前端控制器
 * </p>
 *
 * @author Jhurry
 * @since 2021-08-11
 */
@RestController
@RequestMapping("/order")
@Slf4j
@Api(tags = "家政服务订单管理")
public class HousekeepingOrderController extends BaseController {

    @Resource
    private HousekeepingOrderService hks;


    /**
     *订单生成接口（用户）
     * @param createOrderForm
     * @return ResponseEntity<CreateOrderDto>
     */
    @ApiOperation(value = "订单生成接口")
    @PostMapping("auth/orderInfo")
    public ResponseEntity<CreateOrderDto> createOrder(@RequestBody @Valid CreateOrderForm createOrderForm ,BindingResult result){
        log.info("入参:{}",createOrderForm);
        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;

        CreateOrderDto order = hks.createOrder(createOrderForm);
        return ResponseEntity.BuildSuccess(CreateOrderDto.class).setCode(200).setMsg("订单创建成功").setData(order);
    }


    /**
     *前台条件查询订单（用户）
     * @param searchOrderForm
     * @return ResponseEntity<OrderListDto>
     */
    @ApiOperation(value = "前台条件查询订单")
    @GetMapping("auth/orderInfoByCondition")
    public ResponseEntity<OrderListDto> searchOrder(@Valid SearchOrderForm searchOrderForm,BindingResult result){
        log.info("入参:{}",searchOrderForm);
        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;
        OrderListDto orderListDto = hks.searchOrder(searchOrderForm);
        return ResponseEntity.BuildSuccess(OrderListDto.class).setData(orderListDto).setMsg("查询成功").setCode(200);
    }

    /**
     * 订单确认接口（用户）
     * @param orderId
     * @return ResponseEntity
     */
    @ApiOperation(value = "用户确认订单完成")
    @PostMapping("auth/orderConfirmation")
    public ResponseEntity confirmOrder(Integer orderId){
            log.info("入参：{}",orderId);
            hks.confirmOrder(orderId);
            return ResponseEntity.BuildSuccess().setCode(200).setMsg("操作成功");
    }

    /**
     * 取消订单接口（用户）
     * @param cancelOrderForm
     * @return ResponseEntity
     */
    @ApiOperation(value = "用户取消订单")
    @PostMapping("auth/orderCancellation")
    public  ResponseEntity cancelOrder(@RequestBody @Valid CancelOrderForm cancelOrderForm,BindingResult result) {
        log.info("入参：{}",cancelOrderForm);
        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;
        hks.cancelOrder(cancelOrderForm);
        return ResponseEntity.BuildSuccess().setCode(200).setMsg("操作成功");
    }

    /**
     * 评论订单接口(用户)
     * @param commentOrderForm
     * @return ResponseEntity<CommentOrderDto>
     */
    @ApiOperation(value = "用户评论订单")
    @PostMapping("auth/orderComment")
    public ResponseEntity<CommentOrderDto> orderComment(@RequestBody @Valid CommentOrderForm commentOrderForm,BindingResult result){
        log.info("入参：{}",commentOrderForm);
        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;
        CommentOrderDto commentOrderDto = hks.commentOrder(commentOrderForm);
        return ResponseEntity.BuildSuccess(CommentOrderDto.class).setMsg("操作成功").setCode(200).setData(commentOrderDto);
    }

    /**
     * 订单删除接口（用户）
     * @param deleteOrderForm
     * @return ResponseEntity<DeleteOrderDto>
     */
    @ApiOperation(value = "删除订单")
    @PostMapping("auth/orderDeletion")
    public ResponseEntity<DeleteOrderDto> deleteOrder(@RequestBody @Valid DeleteOrderForm deleteOrderForm,BindingResult result){
        log.info("入参：{}",deleteOrderForm);
        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;
        DeleteOrderDto deleteOrderDto = hks.deleteOrder(deleteOrderForm);
        return ResponseEntity.BuildSuccess(DeleteOrderDto.class).setData(deleteOrderDto).setCode(200).setMsg("操作成功");
    }

    /**
     * 查询订单（管理员）
     * @param adminSearchOrderForm
     * @return ResponseEntity<OrderListDto>
     */
    @ApiOperation(value = "管理员订单查询")
    @GetMapping("auth/adminOrderInfo")
    public ResponseEntity<OrderListDto> adminSearchOrder(@Valid AdminSearchOrderForm adminSearchOrderForm,BindingResult result){
        log.info("入参：{}",adminSearchOrderForm);
        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;
        OrderListDto orderListDto = hks.adminSearchOrder(adminSearchOrderForm);
        return ResponseEntity.BuildSuccess(OrderListDto.class).setMsg("查询成功").setData(orderListDto).setCode(200);

    }

    /**
     * 付款接口
     * @param payOrderForm
     * @return
     */
    @PostMapping("auth/payment")
    @ApiOperation(value = "付款接口")
    public ResponseEntity<PayOrderDto> payOrder(@RequestBody @Valid PayOrderForm payOrderForm,BindingResult result){
        log.info("入参：{}",payOrderForm);
        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;
        PayOrderDto payOrderDto = hks.payOrder(payOrderForm);
        return ResponseEntity.BuildSuccess(PayOrderDto.class).setData(payOrderDto).setCode(200).setMsg("付款成功");
    }

    /**
     * 退款接口
     * @param refundForm
     * @param result
     * @return
     */
    @PostMapping("auth/refund")
    @ApiOperation(value = "退款接口")
    public ResponseEntity refund(@RequestBody @Valid RefundForm refundForm,BindingResult result){
        log.info("入参：{}",refundForm);
        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;
        RefundDto refundDto = hks.refund(refundForm);
        return ResponseEntity.BuildSuccess(RefundDto.class).setCode(200).setMsg("退款成功").setData(refundDto);
    }

}

