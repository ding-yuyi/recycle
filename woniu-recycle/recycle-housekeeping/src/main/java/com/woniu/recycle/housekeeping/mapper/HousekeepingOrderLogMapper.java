package com.woniu.recycle.housekeeping.mapper;

import com.woniu.recycle.housekeeping.domain.HousekeepingOrderLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 家政商品订单日志表 Mapper 接口
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-11
 */
public interface HousekeepingOrderLogMapper extends BaseMapper<HousekeepingOrderLog> {

}
