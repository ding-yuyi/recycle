package com.woniu.recycle.housekeeping.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 家政工单日志表
 * </p>
 *
 * @author wjh
 * @since 2021-08-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="HousekeepingTaskLog对象", description="家政工单日志表")
public class HousekeepingTaskLog implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "工单id")
    private Integer taskId;

    @ApiModelProperty(value = "记录日志生成原因，状态")
    private String logReason;

    @ApiModelProperty(value = "操作者类型 0 普通用户 | 1 管理员")
    private Integer logType;

    @ApiModelProperty(value = "操作者id")
    private Integer userId;

    @ApiModelProperty(value = "工单状态")
    private Integer taskStatus;

    @ApiModelProperty(value = "订单id")
    private Integer orderId;

    @ApiModelProperty(value = "日志生成时间")
    private Long createTime;

    @ApiModelProperty(value = "记录添加时间")
    private Date rawAddTime;

    @ApiModelProperty(value = "数据最新更新时间")
    private Date rawUpdateTime;


}
