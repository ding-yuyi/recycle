package com.woniu.recycle.housekeeping.controller.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * TODO
 *工单查询接口入参对象
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/20 0020
 */
@Data
public class SearchTaskForm {

    @ApiModelProperty(value = "当前页码")
    @NotNull
    @Min(value = 1)
    private Long current;

    @ApiModelProperty(value = "每页容量")
    @NotNull
    @Min(value = 1)
    private Long pageSize;

    @ApiModelProperty(value = "用户姓名")
    private String username;

    @ApiModelProperty(value = "起始时间")
    private String startTime;

    @ApiModelProperty(value = "终止时间")
    private String endTime;

    @ApiModelProperty(value = "工单状态")
    private Integer status;
}
