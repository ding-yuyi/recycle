package com.woniu.recycle.housekeeping.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.recycle.housekeeping.controller.form.*;
import com.woniu.recycle.housekeeping.domain.HousekeepingTask;
import com.woniu.recycle.housekeeping.dto.*;

/**
 * <p>
 * 家政工单表 服务类
 * </p>
 *
 * @author wjh
 * @since 2021-08-12
 */
public interface HousekeepingTaskService extends IService<HousekeepingTask> {

    Boolean createTask(CreateTaskForm createTaskForm);

    DistributeTaskDto distributeTask(DistributeTaskForm distributeTaskForm);

    ConfirmTaskDto confirmTask(ConfirmTaskForm confirmTaskForm);

    CancelTaskDto cancelTask(CancelTaskForm cancelTaskForm);

    SubmitTaskDto submitTask(SubmitTaskForm submitTaskForm);

    AdminModifyDto adminModifyTask(AdminModifyForm adminModifyForm);

    UserModifyDto userModifyTask(UserModifyForm userModifyForm);

    TaskListDto searchTask(SearchTaskForm searchTaskForm);
}
