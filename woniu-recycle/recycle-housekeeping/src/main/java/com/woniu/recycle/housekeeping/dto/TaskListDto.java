package com.woniu.recycle.housekeeping.dto;

import lombok.Data;

import java.util.List;

/**
 * TODO
 *查询工单接口返回对象
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/20 0020
 */
@Data
public class TaskListDto {
    private Long total;
    private Long size;
    private Long current;
    private List<SearchTaskDto> tasks;
}
