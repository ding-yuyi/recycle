package com.woniu.recycle.housekeeping.controller.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * TODO
 *根据用户id查询订单日志入参对象
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/23 0023
 */
@Data
public class SearchOrderLogByUserForm {
    @ApiModelProperty(value = "用户id")
    @Min(value = 1)
    private Integer userId;

    @ApiModelProperty(value = "当前页码")
    @NotNull
    @Min(value = 1)
    private Long current;

    @ApiModelProperty(value = "每页容量")
    @NotNull
    @Min(value = 1)
    private Long pageSize;
}
