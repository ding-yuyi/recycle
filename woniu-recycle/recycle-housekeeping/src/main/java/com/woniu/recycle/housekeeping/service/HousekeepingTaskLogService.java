package com.woniu.recycle.housekeeping.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.recycle.housekeeping.controller.form.SearchOrderLogByUserForm;
import com.woniu.recycle.housekeeping.controller.form.SearchOrderLogForm;
import com.woniu.recycle.housekeeping.domain.HousekeepingTaskLog;
import com.woniu.recycle.housekeeping.dto.ListOrderLogDto;
import com.woniu.recycle.housekeeping.dto.TaskLogPageDto;

/**
 * <p>
 * 家政工单日志表 服务类
 * </p>
 *
 * @author wjh
 * @since 2021-08-12
 */
public interface HousekeepingTaskLogService extends IService<HousekeepingTaskLog> {

    TaskLogPageDto searchByOrderId(SearchOrderLogForm searchOrderLogForm);

    TaskLogPageDto searchByUserId(SearchOrderLogByUserForm searchOrderLogByUserForm);
}
