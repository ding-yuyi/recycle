package com.woniu.recycle.housekeeping.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 家政工单表
 * </p>
 *
 * @author wjh
 * @since 2021-08-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="HousekeepingTask对象", description="家政工单表")
public class HousekeepingTask implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "家政服务人员姓名")
    private String housekeeperName;

    @ApiModelProperty(value = "家政人员电话")
    private String housekeeperPhone;

    @ApiModelProperty(value = "用户姓名")
    private String username;

    @ApiModelProperty(value = "用户电话")
    private String userPhone;

    @ApiModelProperty(value = "地址")
    private String address;

    @ApiModelProperty(value = "预约上门时间")
    private Long visitTime;

    @ApiModelProperty(value = "到达时间")
    private Long arriveTime;

    @ApiModelProperty(value = "取消时间")
    private Long cancelTime;

    @ApiModelProperty(value = "取消原因")
    private String cancelReason;

    @ApiModelProperty(value = "工单状态 0  未派单 | 1 上门中 | 2 服务中 | 3 完成 | 4  已取消")
    private Integer status;

    @ApiModelProperty(value = "完成时间")
    private Long finishTime;

    @ApiModelProperty(value = "订单id")
    private Integer orderId;

    @ApiModelProperty(value = "派单时间")
    private Long sendTime;

    @ApiModelProperty(value = "工单生成时间")
    private Long createTime;

    @ApiModelProperty(value = "商品id")
    private Integer productId;

    @ApiModelProperty(value = "商品名称")
    private String productName;

    @ApiModelProperty(value = "商品描述")
    private String productDescription;

    @ApiModelProperty(value = "记录添加时间")
    private Date rawAddTime;

    @ApiModelProperty(value = "数据最新更新时间")
    private Date rawUpdateTime;


}
