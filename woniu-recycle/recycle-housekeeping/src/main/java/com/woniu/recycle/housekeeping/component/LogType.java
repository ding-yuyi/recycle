package com.woniu.recycle.housekeeping.component;

/**
 * @ClassName LogType
 * @Description TODO
 * @Author DingYuyi
 * @Date 2021/8/13 15:08
 * @Version 1.0
 */
public class LogType {
    /**
     * 普通用户
     */
    public static final Integer NORMAL_USER = 0;

    /**
     * 管理员
     */
    public static final Integer MANAGER = 1;

    //系统
    public static final Integer SYSTEM = 2;
}
