package com.woniu.recycle.housekeeping.controller.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * TODO
 *工单确认接口入参对象
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/19 0019
 */
@Data
public class ConfirmTaskForm {

    @ApiModelProperty(value = "订单id")
    @NotNull
    @Min(value = 1)
    private Integer orderId;
}
