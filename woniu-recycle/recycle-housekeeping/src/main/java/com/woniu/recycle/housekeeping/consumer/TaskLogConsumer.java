package com.woniu.recycle.housekeeping.consumer;

import com.woniu.recycle.housekeeping.component.ConsumerTopic;
import com.woniu.recycle.housekeeping.domain.HousekeepingTaskLog;
import com.woniu.recycle.housekeeping.service.HousekeepingTaskLogService;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * TODO
 *工单日志记录
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/23 0023
 */
@RocketMQMessageListener(consumerGroup = "recycleTaskLog",topic = ConsumerTopic.TASK_LOG)
@Component
@Slf4j
public class TaskLogConsumer implements RocketMQListener<HousekeepingTaskLog> {

    @Resource
    private HousekeepingTaskLogService housekeepingTaskLogService;

    @Override
    public void onMessage(HousekeepingTaskLog housekeepingTaskLog) {
        boolean save = housekeepingTaskLogService.save(housekeepingTaskLog);
        if (!save){
            log.warn("工单日志保存失败");
        }
    }
}
