package com.woniu.recycle.housekeeping.controller.form;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * TODO
 *
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/18 0018
 */@Data
public class CreateTaskForm {

    @ApiModelProperty(value = "用户姓名")
    @NotNull
    private String username;

    @ApiModelProperty(value = "用户电话")
    @NotNull
    private String userPhone;

    @ApiModelProperty(value = "订单id")
    @NotNull
    @Min(value = 1)
    private Integer orderId;

    @ApiModelProperty(value = "用户id")
    private Integer userId;

    @ApiModelProperty(value = "商品名称")
    @NotNull
    private String productName;

    @ApiModelProperty(value = "商品描述")
    @NotNull
    private String productDescription;

}
