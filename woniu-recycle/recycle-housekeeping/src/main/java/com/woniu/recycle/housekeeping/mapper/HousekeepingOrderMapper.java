package com.woniu.recycle.housekeeping.mapper;

import com.woniu.recycle.housekeeping.domain.HousekeepingOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 家政订单表 Mapper 接口
 * </p>
 *
 * @author cy/lyl
 * @since 2021-08-11
 */
public interface HousekeepingOrderMapper extends BaseMapper<HousekeepingOrder> {

}
