package com.woniu.recycle.housekeeping.component;

import org.aspectj.apache.bcel.classfile.Code;

/**
 * 订单枚举类
 *
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/14 0014
 */
public enum OrderEnum {
    ORDER_ERROR(2001,"订单信息有误"),
    ORDER_NOT_EXIST(2004,"订单不存在"),
    EMPTY_DATA(2003,"没有数据"),
    ORDER_IN_PROGRESS(2005,"订单进行中，无法取消"),
    CAN_NOT_DELETE(2006,"订单状态有误，无法删除"),
    ADDR_ERROR(2007,"地址有误"),
    PRODUCT_NOT_EXIST(2008,"商品不存在"),
    TEL_ERROR(2009,"请先填写电话号码后再下单"),
    Time_ERROR(2010,"请输入正确的时间"),
    STUTAS_ERROR(2011,"订单状态有误"),
    ID_NULL(2012,"订单id不能为空"),
    CREATE_ERROR(2002,"新增失败");


    Integer code;
    String msg;

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    OrderEnum(Integer code,String msg) {
        this.code = code;
        this.msg = msg;
    }
}
