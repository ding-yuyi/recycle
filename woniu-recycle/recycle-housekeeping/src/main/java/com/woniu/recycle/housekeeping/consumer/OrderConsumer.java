package com.woniu.recycle.housekeeping.consumer;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.woniu.recycle.housekeeping.component.*;
import com.woniu.recycle.housekeeping.domain.HousekeepingOrder;
import com.woniu.recycle.housekeeping.domain.HousekeepingOrderLog;
import com.woniu.recycle.housekeeping.domain.HousekeepingTask;
import com.woniu.recycle.housekeeping.domain.HousekeepingTaskLog;
import com.woniu.recycle.housekeeping.mapper.HousekeepingOrderMapper;
import com.woniu.recycle.housekeeping.mapper.HousekeepingTaskMapper;
import com.woniu.recycle.housekeeping.service.HousekeepingOrderLogService;
import com.woniu.recycle.housekeeping.service.HousekeepingTaskLogService;
import com.woniu.recycle.housekeeping.utils.OrderLogUtil;
import com.woniu.recycle.housekeeping.utils.TaskLogUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;

/**
 * TODO
 *消息队列处理未及时付款订单
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/23 0023
 */
@RocketMQMessageListener(consumerGroup = "recycleOrder",topic = ConsumerTopic.ORDER)
@Component
@Slf4j
public class OrderConsumer implements RocketMQListener<Integer> {

    @Resource
    private HousekeepingOrderMapper housekeepingOrderMapper;

    @Resource
    private HousekeepingOrderLogService housekeepingOrderLogService;

    @Resource
    private HousekeepingTaskLogService housekeepingTaskLogService;

    @Resource
    private HousekeepingTaskMapper housekeepingTaskMapper;

    @Override
    public void onMessage(Integer orderId) {
        HousekeepingOrder housekeepingOrder = housekeepingOrderMapper.selectById(orderId);
        if(ObjectUtils.isEmpty(housekeepingOrder)){
            log.warn("自动处理订单出错");
        }
        if(housekeepingOrder.getStatus()== OrderStatus.DEFAULT_STATUS){
            //超时未付款自动取消订单
            housekeepingOrder.setStatus(OrderStatus.CANCELLED);
            housekeepingOrder.setCancelReason(OrderStatus.SYSTEM_CANCEL);
            housekeepingOrder.setCancelTime(System.currentTimeMillis());
            housekeepingOrderMapper.updateById(housekeepingOrder);
            //取消关联订单
            QueryWrapper<HousekeepingTask> wrapper = new QueryWrapper<>();
            wrapper.eq("order_id",housekeepingOrder.getId());
            HousekeepingTask housekeepingTask = housekeepingTaskMapper.selectOne(wrapper);
            housekeepingTask.setStatus(TaskStatus.CANCELED);
            housekeepingTask.setCancelReason(OrderStatus.SYSTEM_CANCEL);
            housekeepingTask.setCancelTime(System.currentTimeMillis());
            housekeepingTaskMapper.updateById(housekeepingTask);
            //保存日志
            HousekeepingOrderLog orderLog = OrderLogUtil.getLog(housekeepingOrder, LogType.SYSTEM, LogReason.SYSTEM_CANCEL, SystemStatus.SYSTEM_ID);
            housekeepingOrderLogService.save(orderLog);
            HousekeepingTaskLog taskLog = TaskLogUtil.getLog(housekeepingTask, LogType.SYSTEM, LogReason.SYSTEM_CANCEL, SystemStatus.SYSTEM_ID);
            housekeepingTaskLogService.save(taskLog);

        }
    }
}
