package com.woniu.recycle.housekeeping.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * TODO
 *
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/23 0023
 */
@Data
public class SearchTaskLogDto {
    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "工单id")
    private Integer taskId;

    @ApiModelProperty(value = "记录日志生成原因，状态")
    private String logReason;

    @ApiModelProperty(value = "操作者类型 0 普通用户 | 1 管理员")
    private Integer logType;

    @ApiModelProperty(value = "操作者id")
    private Integer userId;

    @ApiModelProperty(value = "工单状态")
    private Integer taskStatus;

    @ApiModelProperty(value = "订单id")
    private Integer orderId;

    @ApiModelProperty(value = "日志生成时间")
    private Long createTime;
}
