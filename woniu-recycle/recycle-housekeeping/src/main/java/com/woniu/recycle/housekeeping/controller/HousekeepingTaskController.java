package com.woniu.recycle.housekeeping.controller;


import com.woniu.recycle.commons.core.model.ResponseEntity;
import com.woniu.recycle.commons.web.component.BaseController;
import com.woniu.recycle.housekeeping.controller.form.*;
import com.woniu.recycle.housekeeping.dto.*;
import com.woniu.recycle.housekeeping.service.HousekeepingTaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 * 家政工单表 前端控制器
 * </p>
 *
 * @author Jhurry
 * @since 2021-08-12
 */
@RestController
@RequestMapping("/task")
@Slf4j
@Api(tags = "家政服务工单管理")
public class HousekeepingTaskController extends BaseController {

    @Resource
    private HousekeepingTaskService housekeepingTaskService;

    /**
     * 派单接口（管理员）
     * @param distributeTaskForm
     * @return ResponseEntity<DistributeTaskDto>
     */
    @PostMapping("auth/taskDistribution")
    @ApiOperation(value = "派单接口")
    public ResponseEntity<DistributeTaskDto> distributeTask(@RequestBody @Valid DistributeTaskForm distributeTaskForm, BindingResult result){
        log.info("入参：{}",distributeTaskForm);
        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;
        DistributeTaskDto distributeTaskDto = housekeepingTaskService.distributeTask(distributeTaskForm);
        return ResponseEntity.BuildSuccess(DistributeTaskDto.class).setCode(200).setMsg("派单成功").setData(distributeTaskDto);
    }

    /**
     * 工单确认上门时间接口
     * @param confirmTaskForm
     * @return
     */
    @PostMapping("auth/taskConfirmation")
    @ApiOperation(value = "工单确认上门时间")
    public ResponseEntity<ConfirmTaskDto> confirmTask(@RequestBody @Valid ConfirmTaskForm confirmTaskForm,BindingResult result){
        log.info("入参：{}",confirmTaskForm);
        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;
        ConfirmTaskDto confirmTaskDto = housekeepingTaskService.confirmTask(confirmTaskForm);
        return ResponseEntity.BuildSuccess(ConfirmTaskDto.class).setData(confirmTaskDto).setMsg("操作成功").setCode(200);
    }

    /**
     * 工单取消接口（管理员）
     * @param cancelTaskForm
     * @return
     */
    @PostMapping("auth/taskCancellation")
    @ApiOperation(value = "取消工单接口")
    public ResponseEntity<CancelTaskDto> cancelTask(@RequestBody @Valid CancelTaskForm cancelTaskForm,BindingResult result){
        log.info("入参：{}",cancelTaskForm);
        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;
        CancelTaskDto cancelTaskDto = housekeepingTaskService.cancelTask(cancelTaskForm);
        return ResponseEntity.BuildSuccess(CancelTaskDto.class).setCode(200).setMsg("操作成功").setData(cancelTaskDto);
    }

    /**
     * 工单提交接口（管理员）
     * @param submitTaskForm
     * @return
     */
    @PostMapping("auth/taskSubmission")
    @ApiOperation(value = "确认工单完成接口")
    public ResponseEntity<SubmitTaskDto> submitTask(@RequestBody @Valid SubmitTaskForm submitTaskForm,BindingResult result){
        log.info("入参：{}",submitTaskForm);
        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;
        SubmitTaskDto submitTaskDto = housekeepingTaskService.submitTask(submitTaskForm);
        return ResponseEntity.BuildSuccess(SubmitTaskDto.class).setMsg("操作成功").setCode(200).setData(submitTaskDto);
    }

    /**
     * 工单修改接口（管理员）
     * @param adminModifyForm
     * @return
     */
    @PostMapping("auth/adminTask")
    @ApiOperation(value = "管理员修改工单接口")
    public ResponseEntity<AdminModifyDto> adminModifyTask(@RequestBody @Valid AdminModifyForm adminModifyForm,BindingResult result){
        log.info("入参：{}",adminModifyForm);
        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;
        AdminModifyDto adminModifyDto = housekeepingTaskService.adminModifyTask(adminModifyForm);
        return ResponseEntity.BuildSuccess(AdminModifyDto.class).setData(adminModifyDto).setCode(200).setMsg("修改成功");

    }

    /**
     * 工单修改接口（用户）
     * @param userModifyForm
     * @return
     */
    @PostMapping("auth/userTask")
    @ApiOperation(value = "用户修改工单接口")
    public ResponseEntity<UserModifyDto> userModifyTask(@RequestBody @Valid UserModifyForm userModifyForm,BindingResult result){
        log.info("入参：{}",userModifyForm);
        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;
        UserModifyDto userModifyDto = housekeepingTaskService.userModifyTask(userModifyForm);
        return ResponseEntity.BuildSuccess(UserModifyDto.class).setMsg("修改成功").setCode(200).setData(userModifyDto);
    }

    /**
     * 工单查询接口
     * @param searchTaskForm
     * @return
     */
    @PostMapping("auth/TaskInfo")
    @ApiOperation(value = "工单查询接口")
    public ResponseEntity searchTask(@Valid SearchTaskForm searchTaskForm,BindingResult result){
        log.info("入参：{}",searchTaskForm);
        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;
        TaskListDto taskListDto = housekeepingTaskService.searchTask(searchTaskForm);
        return ResponseEntity.BuildSuccess(TaskListDto.class).setMsg("查询成功").setCode(200).setData(taskListDto);
    }

}

