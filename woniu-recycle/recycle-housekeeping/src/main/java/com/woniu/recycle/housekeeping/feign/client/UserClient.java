package com.woniu.recycle.housekeeping.feign.client;

import com.woniu.recycle.commons.core.model.ResponseEntity;
import com.woniu.recycle.housekeeping.controller.form.AddrFrom;
import com.woniu.recycle.housekeeping.controller.form.BalanceChangeForm;
import com.woniu.recycle.housekeeping.domain.User;
import com.woniu.recycle.housekeeping.dto.BalanceChangeDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * feign调用user服务
 */
@FeignClient("user-server")
public interface UserClient {
    @PostMapping("user/addr/auth/findAddrById")
    ResponseEntity<AddrFrom> findAddrById(@RequestParam Integer id);

    @GetMapping("/user/userModule/auth/UserById")
    ResponseEntity<User> UserById(@RequestParam Integer userId);

    @PutMapping("user/userModule/auth/balanceSel")
    ResponseEntity<BalanceChangeDto> balanceSel(@SpringQueryMap BalanceChangeForm balanceChangeForm);

    @PutMapping("user/userModule/auth/balanceAdd")
    ResponseEntity<BalanceChangeDto> balanceAdd(@SpringQueryMap BalanceChangeForm balanceChangeForm);
}
