package com.woniu.recycle.housekeeping.exception;

import com.woniu.recycle.commons.core.exception.RecycleException;

public class OrderException extends RecycleException {
    private static final long serialVersionUID = 1L;

    private Integer code;

    private String msg;

    //全参构造方法
    public OrderException(Integer code, String msg){
        super(msg, code);
    }
}
