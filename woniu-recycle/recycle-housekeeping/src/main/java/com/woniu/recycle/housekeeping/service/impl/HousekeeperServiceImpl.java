package com.woniu.recycle.housekeeping.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.recycle.housekeeping.mapper.HousekeeperMapper;
import com.woniu.recycle.housekeeping.domain.Housekeeper;
import com.woniu.recycle.housekeeping.service.HousekeeperService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 家政员工表 服务实现类
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
@Service
public class HousekeeperServiceImpl extends ServiceImpl<HousekeeperMapper, Housekeeper> implements HousekeeperService {

}
