package com.woniu.recycle.housekeeping.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * TODO
 *退款接口返回对象
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/23 0023
 */
@Data
public class RefundDto {

    @ApiModelProperty(value = "家政订单id")
    private Integer id;

    @ApiModelProperty(value = "订单编号")
    private String housekeepingOrderNum;

    @ApiModelProperty(value = "家政商品id")
    private Integer productId;

    @ApiModelProperty(value = "商品图片路径")
    private String productUrl;

    @ApiModelProperty(value = "家政商品价格")
    private BigDecimal price;

    @ApiModelProperty(value = "商品数量")
    private Integer count;

    @ApiModelProperty(value = "商品总价")
    private BigDecimal total;

    @ApiModelProperty(value = "预约上门时间")
    private Long visitTime;

    @ApiModelProperty(value = "地址id")
    private Integer addrId;

    @ApiModelProperty(value = "收货地址")
    private String addrDesc;

    @ApiModelProperty(value = "订单状态 0 未收货 | 1 已收货（订单结束）| 2 已评价 | 3 已取消")
    private Integer status;

    @ApiModelProperty(value = "备注")
    private String notes;

    @ApiModelProperty(value = "创建时间")
    private Long createTime;
}
