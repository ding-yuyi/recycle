package com.woniu.recycle.housekeeping.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 家政商品订单日志表
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="HousekeepingOrderLog对象", description="家政商品订单日志表")
public class HousekeepingOrderLog implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "回收商品订单日志id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "订单id")
    private Integer orderId;

    @ApiModelProperty(value = "订单编号")
    private String orderNum;

    @ApiModelProperty(value = "日记创建时间")
    private Long logCreateTime;

    @ApiModelProperty(value = "订单状态")
    private Integer orderStatus;

    @ApiModelProperty(value = "记录日记生成原因")
    private String logReason;

    @ApiModelProperty(value = "操作者类型,0普通用户,1管理员")
    private Integer logType;

    @ApiModelProperty(value = "操作者ID")
    private Integer userId;

    @ApiModelProperty(value = "记录添加时间")
    private Date rawAddTime;

    @ApiModelProperty(value = "记录更新时间")
    private Date rawUpdateTime;


}
