package com.woniu.recycle.housekeeping.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.recycle.housekeeping.domain.Housekeeper;

/**
 * <p>
 * 家政员工表 服务类
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
public interface HousekeeperService extends IService<Housekeeper> {

}
