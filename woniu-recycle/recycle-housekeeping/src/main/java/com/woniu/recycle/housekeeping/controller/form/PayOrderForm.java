package com.woniu.recycle.housekeeping.controller.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * TODO
 *付款接口入参对象
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/21 0021
 */
@Data
public class PayOrderForm {

    @ApiModelProperty(value = "订单id")
    @NotNull
    @Min(value = 1)
    private Integer orderId;

}
