package com.woniu.recycle.housekeeping.component;

/**
 * TODO
 *消息队列消费者topic常量
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/23 0023
 */
public class ConsumerTopic {
    public static final String ORDER_LOG="orderLog";
    public static final String TASK_LOG="taskLog";
    public static final String ORDER="order";
}
