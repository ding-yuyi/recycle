package com.woniu.recycle.housekeeping.component;

/**
 * TODO
 *订单数据常量类
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/14 0014
 */
public class OrderStatus {
    //生成订单默认状态
    public static final int DEFAULT_STATUS=0;
    //订单逻辑删除未删除
    public static final int NOT_LOGIC_DELETED=0;
    //订单逻辑删除已删除
    public static final int LOGIC_DELETED=1;
    //订单进行中状态
    public static final int ORDER_IN_PROGRESS=1;
    //订单已完成状态
    public static final int FINISHED=2;
    //订单已取消状态
    public static final  int CANCELLED=4;
    //订单已评论状态
    public static final int COMMENTED=3;
    //订单评论最低星级
    public static final int ORDER_LOWEST_LEVEL=1;
    //订单评论最高星级
    public static final int ORDER_HIGHEST_LEVEL=5;
    //订单已付款状态
    public static final int PAYED=5;
    //订单付款后取消状态
    public static final int PAYED_CANCELLED=6;
    //订单未付款系统自动取消
    public static final String SYSTEM_CANCEL="用户未付款，系统自动取消";

}
