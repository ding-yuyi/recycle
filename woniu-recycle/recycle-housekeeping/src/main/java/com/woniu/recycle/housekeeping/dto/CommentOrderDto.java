package com.woniu.recycle.housekeeping.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * TODO
 *订单评论接口返回对象
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/16 0016
 */
@Data
public class CommentOrderDto {
    @ApiModelProperty(value = "家政订单id")
    private Integer id;

    @ApiModelProperty(value = "订单编号")
    private String housekeepingOrderNum;

    @ApiModelProperty(value = "评价内容（可不评价）")
    private String comment;

    @ApiModelProperty(value = "评价等级 1 | 2 | 3 | 4 | 5  5星最好")
    private Integer level;
}
