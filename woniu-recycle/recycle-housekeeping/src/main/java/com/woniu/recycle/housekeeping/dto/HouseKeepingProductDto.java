package com.woniu.recycle.housekeeping.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 *
 */
@Data
@ApiModel("家政商品dto")
public class HouseKeepingProductDto {

    @ApiModelProperty(value = "主键id",example = "1")
    private Integer id;

    @ApiModelProperty(value = "商品名称")
    private String name;

    @ApiModelProperty(value = "价格",example = "1")
    private BigDecimal price;

    @ApiModelProperty(value = "商品描述")
    private String description;

    @ApiModelProperty(value = "商品图片地址")
    private String pictureUrl;

    @ApiModelProperty(value = "注意事项")
    private String attention;

    @ApiModelProperty(value = "上下架状态 0 下架 | 1 上架",example = "1")
    private Integer status;
}
