package com.woniu.recycle;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @ClassName OrderStart
 * @Description TODO
 * @Author Aierbing
 * @Date 2021/8/11 19:31
 * @Version 1.0
 */
@SpringBootApplication
@MapperScan("com.woniu.recycle.housekeeping.mapper")
@EnableFeignClients(basePackages = "com.woniu.recycle.housekeeping.feign.client")
public class HouseKeepingStart {
    public static void main(String[] args) {
        SpringApplication.run(HouseKeepingStart.class,args);
    }
}
