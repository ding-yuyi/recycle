package com.woniu.recycle.housekeeping.consumer;

import com.woniu.recycle.housekeeping.component.ConsumerTopic;
import com.woniu.recycle.housekeeping.domain.HousekeepingOrderLog;
import com.woniu.recycle.housekeeping.service.HousekeepingOrderLogService;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * TODO
 *订单日志记录
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/23 0023
 */
@RocketMQMessageListener(consumerGroup = "recycleOrderLog",topic = ConsumerTopic.ORDER_LOG)
@Component
@Slf4j
public class OrderLogConsumer implements RocketMQListener<HousekeepingOrderLog> {
    @Resource
    private HousekeepingOrderLogService housekeepingOrderLogService;

    @Override
    public void onMessage(HousekeepingOrderLog housekeepingOrderLog) {
        boolean save = housekeepingOrderLogService.save(housekeepingOrderLog);
        if(!save){
            log.warn("订单日志保存失败");
        }
    }
}
