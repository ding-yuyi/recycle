package com.woniu.recycle.housekeeping.utils;

import com.woniu.recycle.housekeeping.domain.HousekeepingOrder;
import com.woniu.recycle.housekeeping.domain.HousekeepingOrderLog;
import com.woniu.recycle.housekeeping.domain.HousekeepingTask;
import com.woniu.recycle.housekeeping.domain.HousekeepingTaskLog;

/**
 * TODO
 *工单日志工具类
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/23 0
 */
public class TaskLogUtil {
    public static HousekeepingTaskLog getLog(HousekeepingTask housekeepingTask, Integer logType, String reason, Integer userId){
        HousekeepingTaskLog housekeepingOrderLog = new HousekeepingTaskLog();
        housekeepingOrderLog.setCreateTime(System.currentTimeMillis());
        housekeepingOrderLog.setLogReason(reason);
        housekeepingOrderLog.setLogType(logType);
        housekeepingOrderLog.setOrderId(housekeepingTask.getOrderId());
        housekeepingOrderLog.setTaskStatus(housekeepingTask.getStatus());
        housekeepingOrderLog.setUserId(userId);
        housekeepingOrderLog.setTaskId(housekeepingTask.getId());
        return housekeepingOrderLog;
    }
}
