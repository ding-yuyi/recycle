package com.woniu.recycle.housekeeping.controller.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * TODO
 *取消订单入参对象
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/16 0016
 */
@Data
public class CancelOrderForm {
    @ApiModelProperty(value = "订单id")
    @NotNull
    @Min(value = 1)
    private Integer orderId;

    @ApiModelProperty(value = "取消原因(可不填)")
    private String cancelReason;
}
