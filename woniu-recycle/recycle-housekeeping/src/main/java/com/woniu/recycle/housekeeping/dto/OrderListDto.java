package com.woniu.recycle.housekeeping.dto;

import com.woniu.recycle.housekeeping.domain.HousekeepingOrder;
import com.woniu.recycle.housekeeping.dto.SearchOrderDto;
import lombok.Data;

import java.util.List;

/**
 * TODO
 *
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/14 0014
 */
@Data
public class OrderListDto {
    private Long total;
    private Long size;
    private Long current;
    private List<SearchOrderDto> orders;
}
