package com.woniu.recycle.housekeeping.exception;

import com.woniu.recycle.commons.core.exception.RecycleException;

/**
 * TODO
 *
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/17 0017
 */
public class UserException extends RecycleException {
    private static final long serialVersionUID = 1L;

    private Integer code;

    private String msg;

    //全参构造方法
    public UserException(Integer code, String msg){
        super(msg, code);
    }
}
