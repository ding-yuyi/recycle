package com.woniu.recycle.housekeeping.controller.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * TODO
 *用户修改工单接口入参对象
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/20 0020
 */
@Data
public class UserModifyForm {

    @ApiModelProperty(value = "工单id")
    @NotNull
    @Min(value = 1)
    private Integer TaskId;

    @ApiModelProperty(value = "预约上门时间")
    @NotNull
    private String visitTime;
}
