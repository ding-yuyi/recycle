package com.woniu.recycle.housekeeping.controller.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * TODO
 *管理员修改工单入参对象
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/20 0020
 */
@Data
public class AdminModifyForm {
    @ApiModelProperty(value = "订单id")
    @NotNull
    @Min(value = 1)
    private Integer orderId;

    @ApiModelProperty(value = "家政人员id")
    @NotNull
    @Min(value = 1)
    private Integer housekeeperId;
}
