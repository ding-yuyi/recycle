package com.woniu.recycle.housekeeping.component;

/**
 * TODO
 *工单相关信息枚举类
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/19 0019
 */
public enum TaskEnum {
    TASK_CREATE_ERROR(3001,"工单创建失败"),
    TASK_INFO_ERROR(3002,"工单信息有误"),
    TASK_NOT_EXIST(3003,"工单不存在"),
    TASK_STATUS_ERROR(3004,"工单无法取消"),
    CAN_NOT_SUBMIT(3005,"当前工单无法提交"),
    CAN_NOT_MODIFY(3006,"当前工单无法修改"),
    TIME_ERROR(3007,"时间格式有误"),
    EMPTY_DATA(3008,"数据为空"),
    SYSTEM_ERROR(3009,"系统错误，稍后再试"),
    CAN_NOT_DISTRIBUTE(3010,"工单状态有误"),
    SAME_HOUSEKEEPER(3011,"该人员正在服务当前工单"),
    CAN_NOT_DISTRIBUTEBYORDER(3011,"订单状态有误,无法为该订单派单"),
    CREATE_ERROR(2002,"新增失败");


    Integer code;
    String msg;

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    TaskEnum(Integer code,String msg) {
        this.code = code;
        this.msg = msg;
    }
}
