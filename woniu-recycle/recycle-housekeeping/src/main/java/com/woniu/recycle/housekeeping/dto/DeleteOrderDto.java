package com.woniu.recycle.housekeeping.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * TODO
 *
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/17 0017
 */
@Data
public class DeleteOrderDto {
    @ApiModelProperty(value = "家政订单id")
    private Integer id;

    @ApiModelProperty(value = "订单编号")
    private String housekeepingOrderNum;

    @ApiModelProperty(value = "删除状态")
    private Integer deleted;
}
