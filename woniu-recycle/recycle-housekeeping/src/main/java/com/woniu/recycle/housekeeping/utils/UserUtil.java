package com.woniu.recycle.housekeeping.utils;

import com.woniu.recycle.commons.core.model.ResponseEntity;
import com.woniu.recycle.commons.core.util.JwtTemplate;
import com.woniu.recycle.commons.web.util.ObjectMapperUtil;
import com.woniu.recycle.housekeeping.domain.User;
import com.woniu.recycle.housekeeping.feign.client.UserClient;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @ClassName UserUtil
 * @Description TODO
 * @Author DingYuyi
 * @Date 2021/8/17 14:45
 * @Version 1.0
 */
@Component
@Slf4j
public class UserUtil {
    @Resource
    private JwtTemplate jwtTemplate;

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private UserClient userClient;

    public Integer getUserId(HttpServletRequest request){
        log.info("进入工具类");
        //解析token
        String token = request.getHeader("auth_token");
        Claims claims = jwtTemplate.parseJwt(token);
        String userId = claims.get("userId").toString();
        log.info("{}",userId);
        return Integer.valueOf(userId);
    }

    public User getUser(HttpServletRequest request){
        Integer userId = getUserId(request);

        //得到用户数据
        String strUser = stringRedisTemplate.opsForValue().get("user:id" + userId);
        log.info("{}",strUser);

        User user = null;
        if (null == strUser) {
            ResponseEntity<User> userResponseEntity = userClient.UserById(Integer.valueOf(userId));
            user = userResponseEntity.getData();
        }else{
            user = ObjectMapperUtil.parseObject(strUser, User.class);
        }
        return user;
    }
}
