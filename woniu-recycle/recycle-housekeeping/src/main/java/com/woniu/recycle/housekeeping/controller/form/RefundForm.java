package com.woniu.recycle.housekeeping.controller.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * TODO
 *退款接口入参对象
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/23 0023
 */
@Data
public class RefundForm {
    @ApiModelProperty(value = "订单id")
    @NotNull
    private Integer orderId;
}
