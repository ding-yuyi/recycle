package com.woniu.recycle.housekeeping.service;

import com.woniu.recycle.housekeeping.controller.form.SearchOrderLogByUserForm;
import com.woniu.recycle.housekeeping.controller.form.SearchOrderLogForm;
import com.woniu.recycle.housekeeping.domain.HousekeepingOrderLog;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.recycle.housekeeping.dto.ListOrderLogDto;
import com.woniu.recycle.housekeeping.dto.SearchOrderLogDto;

import java.util.List;

/**
 * <p>
 * 家政商品订单日志表 服务类
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-11
 */
public interface HousekeepingOrderLogService extends IService<HousekeepingOrderLog> {


    ListOrderLogDto searchByOrderId(SearchOrderLogForm searchOrderLogForm);

    ListOrderLogDto searchByUserId(SearchOrderLogByUserForm searchOrderLogByUserForm);
}
