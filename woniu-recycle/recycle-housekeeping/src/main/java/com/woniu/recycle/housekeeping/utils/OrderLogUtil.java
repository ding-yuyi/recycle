package com.woniu.recycle.housekeeping.utils;

import com.woniu.recycle.commons.web.util.ObjectMapperUtil;
import com.woniu.recycle.housekeeping.component.LogReason;
import com.woniu.recycle.housekeeping.component.LogType;
import com.woniu.recycle.housekeeping.component.OrderStatus;
import com.woniu.recycle.housekeeping.domain.HousekeepingOrder;
import com.woniu.recycle.housekeeping.domain.HousekeepingOrderLog;

/**
 * TODO
 *订单日志工具类
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/23 0023
 */
public class OrderLogUtil {
    public static HousekeepingOrderLog getLog(HousekeepingOrder housekeepingOrder, Integer logType, String reason, Integer userId){
        HousekeepingOrderLog housekeepingOrderLog = new HousekeepingOrderLog();
        housekeepingOrderLog.setLogCreateTime(System.currentTimeMillis());
        housekeepingOrderLog.setLogReason(reason);
        housekeepingOrderLog.setLogType(logType);
        housekeepingOrderLog.setOrderNum(housekeepingOrder.getHousekeepingOrderNum());
        housekeepingOrderLog.setOrderStatus(housekeepingOrder.getStatus());
        housekeepingOrderLog.setUserId(userId);
        housekeepingOrderLog.setOrderId(housekeepingOrder.getId());

        return housekeepingOrderLog;
    }
}
