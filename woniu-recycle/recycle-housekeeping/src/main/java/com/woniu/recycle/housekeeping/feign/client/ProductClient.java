package com.woniu.recycle.housekeeping.feign.client;

import com.woniu.recycle.commons.core.model.ResponseEntity;
import com.woniu.recycle.housekeeping.dto.HouseKeepingProductDto;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("product-server")
public interface ProductClient {
    @GetMapping("product/housekeeping/findhousekeepingbyid")
    ResponseEntity<HouseKeepingProductDto> findProductById(@RequestParam Integer id);
}
