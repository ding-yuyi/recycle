package com.woniu.recycle.housekeeping.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniu.recycle.housekeeping.domain.HousekeepingTaskLog;

/**
 * <p>
 * 家政工单日志表 Mapper 接口
 * </p>
 *
 * @author wjh
 * @since 2021-08-12
 */
public interface HousekeepingTaskLogMapper extends BaseMapper<HousekeepingTaskLog> {

}
