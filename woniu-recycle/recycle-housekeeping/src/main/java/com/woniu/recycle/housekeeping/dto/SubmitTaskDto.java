package com.woniu.recycle.housekeeping.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * TODO
 *工单提交接口返回对象
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/20 0020
 */
@Data
public class SubmitTaskDto {
    @ApiModelProperty(value = "工单id")
    private Integer id;

    @ApiModelProperty(value = "家政服务人员姓名")
    private String housekeeperName;

    @ApiModelProperty(value = "家政人员电话")
    private String housekeeperPhone;

    @ApiModelProperty(value = "用户姓名")
    private String username;

    @ApiModelProperty(value = "用户电话")
    private String userPhone;

    @ApiModelProperty(value = "地址")
    private String address;

    @ApiModelProperty(value = "预约上门时间")
    private Long visitTime;

    @ApiModelProperty(value = "到达时间")
    private Long arriveTime;

    @ApiModelProperty(value = "工单状态 0  未派单 | 1 上门中 | 2 服务中 | 3 完成 | 4  已取消")
    private Integer status;

    @ApiModelProperty(value = "订单id")
    private Integer orderId;

    @ApiModelProperty(value = "派单时间")
    private Long sendTime;

    @ApiModelProperty(value = "商品id")
    private Integer productId;

    @ApiModelProperty(value = "商品名称")
    private String productName;

    @ApiModelProperty(value = "商品描述")
    private String productDescription;

}
