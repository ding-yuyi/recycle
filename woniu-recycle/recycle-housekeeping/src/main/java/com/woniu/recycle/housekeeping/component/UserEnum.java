package com.woniu.recycle.housekeeping.component;

/**
 * TODO
 *
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/17 0017
 */
public enum UserEnum {
   UESR_NOT_EXIST(3001,"用户不存在");


    Integer code;
    String msg;

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    UserEnum(Integer code,String msg) {
        this.code = code;
        this.msg = msg;
    }
}
