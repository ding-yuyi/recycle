package com.woniu.recycle.housekeeping.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniu.recycle.housekeeping.domain.Housekeeper;

/**
 * <p>
 * 家政员工表 Mapper 接口
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
public interface HousekeeperMapper extends BaseMapper<Housekeeper> {

}
