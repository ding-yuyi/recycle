package com.woniu.recycle.housekeeping.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * TODO
 *工单取消接口返回对象
 * @author Jhurry
 * @version 1.0
 * @date 2021/8/20 0020
 */
@Data
public class CancelTaskDto {

    @ApiModelProperty(value = "工单id")
    private Integer id;

    @ApiModelProperty(value = "工单状态 0  未派单 | 1 上门中 | 2 服务中 | 3 完成 | 4  已取消")
    private Integer status;

    @ApiModelProperty(value = "订单id")
    private Integer orderId;

    @ApiModelProperty(value = "取消时间")
    private Long cancelTime;

    @ApiModelProperty(value = "取消原因")
    private String cancelReason;

}
