package com.woniu.recycle.integration.controller.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @ClassName ReturningForm
 * @Description 用户申请退货接口入参对象
 * @Author DingYuyi
 * @Date 2021/8/16 19:13
 * @Version 1.0
 */
@Data
@ApiModel(value="用户申请退货接口入参对象")
public class ReturningForm {
    @ApiModelProperty(value = "退货原因")
    @NotBlank
    private String reason;

    @ApiModelProperty(value = "订单id")
    @NotNull
    private Integer orderId;
}
