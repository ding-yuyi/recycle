package com.woniu.recycle.integration.exception;

import com.woniu.recycle.commons.core.exception.RecycleException;
import com.woniu.recycle.integration.component.exceptionEnum.IntegrationExceptionEnum;

/**
 * @ClassName IntegrationException
 * @Description TODO
 * @Author DingYuyi
 * @Date 2021/8/12 15:33
 * @Version 1.0
 */

public class IntegrationException extends RecycleException {

    public IntegrationException(String msg,Integer code){
        super(msg,code);
    }

    public IntegrationException(IntegrationExceptionEnum info){
        super(info.getMsg(),info.getCode());
    }
}
