package com.woniu.recycle.integration.controller;


import com.woniu.recycle.commons.core.model.ResponseEntity;
import com.woniu.recycle.commons.web.component.BaseController;
import com.woniu.recycle.integration.controller.form.SearchByOrderIdForm;
import com.woniu.recycle.integration.controller.form.SearchByUserIdForm;
import com.woniu.recycle.integration.controller.form.SearchLogTimeForm;
import com.woniu.recycle.integration.controller.form.SearchLogTypeForm;
import com.woniu.recycle.integration.dto.SearchResultDto;
import com.woniu.recycle.integration.service.IntegrationOrderLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 * 积分商品订单日志表 前端控制器
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-11
 */
@RestController
@RequestMapping("log")
@Slf4j
@Api(tags = "日志接口管理")
public class IntegrationOrderLogController extends BaseController {

    @Resource
    private IntegrationOrderLogService logService;

    /**
     *@author DingYuyi
     *@Description 根据操作者类型查询日志
     *@Date 19:07 2021/8/19
     *@param form
     *@Return ResponseEntity<List<OrderLogDto>>
     */
    @ApiOperation(value = "根据操作者类型查询日志")
    @PostMapping("/auth/searchByLogType")
    public ResponseEntity<SearchResultDto> searchByLogType(@RequestBody @Valid SearchLogTypeForm form, BindingResult result){
        log.info("入参信息,{}",form);

        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;

        SearchResultDto resultDto = logService.searchByLogType(form);
        if(null == resultDto || resultDto.getDtoList().size() <= 0) return ResponseEntity.BuildError(SearchResultDto.class).setMsg("查询为空");
        return ResponseEntity.BuildSuccess(SearchResultDto.class).setMsg("查询成功").setData(resultDto);
    }

    /**
     *@author DingYuyi
     *@Description 根据创建时间查询日志
     *@Date 20:08 2021/8/19
     *@param  form
     *@param result
     *@Return ResponseEntity<SearchByUserIdResultDto>
     */
    @ApiOperation(value = "根据创建时间查询日志")
    @PostMapping("/auth/searchByCreateTime")
    public ResponseEntity<SearchResultDto> searchByCreateTime(@RequestBody @Valid SearchLogTimeForm form, BindingResult result){
        log.info("入参信息,{}",form);

        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity ) return responseEntity;
        if(null != form.getLogCreateTimeEnd() && form.getLogCreateTimeStart() > form.getLogCreateTimeEnd()) return ResponseEntity.BuildError(SearchResultDto.class).setMsg("查询结束时间有误");

        SearchResultDto resultDto = logService.searchByCreateTime(form);
        if(null == resultDto || resultDto.getDtoList().size() <= 0) return ResponseEntity.BuildError(SearchResultDto.class).setMsg("查询为空");
        return ResponseEntity.BuildSuccess(SearchResultDto.class).setMsg("查询成功").setData(resultDto);
    }

    /**
     *@author DingYuyi
     *@Description 根据操作者id查询日志
     *@Date 20:22 2021/8/19
     *@param form
     *@param result
     *@Return ResponseEntity<SearchResultDto>
     */
    @ApiOperation(value = "根据操作者id查询日志")
    @PostMapping("/auth/searchByUserId")
    public ResponseEntity<SearchResultDto> searchByUserId(@RequestBody @Valid SearchByUserIdForm form,BindingResult result){
        log.info("入参信息,{}",form);

        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;

        SearchResultDto resultDto = logService.searchByUserId(form);
        if(null == resultDto || resultDto.getDtoList().size() <= 0) return ResponseEntity.BuildError(SearchResultDto.class).setMsg("查询为空");
        return ResponseEntity.BuildSuccess(SearchResultDto.class).setMsg("查询成功").setData(resultDto);

    }

    /**
     *@author DingYuyi
     *@Description 根据订单id查询日志
     *@Date 10:21 2021/8/20
     *@param form
     *@param result
     *@Return ResponseEntity<SearchResultDto>
     */
    @PostMapping("/auth/searchByOrderId")
    @ApiOperation(value = "根据订单id查询日志")
    public ResponseEntity<SearchResultDto> searchByOrderId(@RequestBody @Valid SearchByOrderIdForm form, BindingResult result){
        log.info("入参信息,{}",form);

        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;

        SearchResultDto resultDto = logService.searchByOrderId(form);
        if(null == resultDto || resultDto.getDtoList().size() <= 0) return ResponseEntity.BuildError(SearchResultDto.class).setMsg("查询为空");
        return ResponseEntity.BuildSuccess(SearchResultDto.class).setMsg("查询成功").setData(resultDto);

    }
}

