package com.woniu.recycle.integration.controller.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @ClassName CheckForm
 * @Description 管理员审核取消订单接口的入参对象
 * @Author DingYuyi
 * @Date 2021/8/13 15:45
 * @Version 1.0
 */
@Data
@ApiModel(value="申请取消订单接口入参对象")
public class CheckForm {
    @ApiModelProperty(value = "订单id")
    @NotNull(message = "订单id不能为空")
    private Integer orderId;

    @ApiModelProperty(value = "审核是否通过")
    @NotNull(message = "是否通过状态不能为空")
    private Boolean isPass;
}
