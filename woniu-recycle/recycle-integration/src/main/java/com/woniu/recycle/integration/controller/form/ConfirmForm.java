package com.woniu.recycle.integration.controller.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @ClassName UpdateStatusForm
 * @Description 确认订单接口的入参对象
 * @Author DingYuyi
 * @Date 2021/8/14 16:41
 * @Version 1.0
 */
@Data
@ApiModel(value="确认订单接口的入参对象")
public class ConfirmForm {

    @ApiModelProperty(value = "订单id")
    @NotNull(message = "订单id不能为空")
    private Integer orderId;
}
