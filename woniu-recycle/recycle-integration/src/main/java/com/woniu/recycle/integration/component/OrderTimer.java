package com.woniu.recycle.integration.component;

import com.woniu.recycle.integration.component.constant.ConstantTime;
import com.woniu.recycle.integration.component.constant.LogType;
import com.woniu.recycle.integration.component.constant.OrderStatus;
import com.woniu.recycle.integration.domain.IntegrationOrder;
import com.woniu.recycle.integration.domain.IntegrationOrderLog;
import com.woniu.recycle.integration.mapper.IntegrationOrderMapper;
import com.woniu.recycle.integration.service.IntegrationOrderService;
import com.woniu.recycle.integration.service.impl.IntegrationOrderServiceImpl;
import com.woniu.recycle.integration.util.LogUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.scheduling.annotation.Scheduled;
import java.util.List;

/**
 * @ClassName OrderTimer
 * @Description 定时清理未评论的订单
 * @Author DingYuyi
 * @Date 2021/8/18 18:59
 * @Version 1.0
 */


/**
 * 定时清理订单，如果超过一周没评论，则自动结束订单，每周日凌晨执行
 */
@Slf4j
public class OrderTimer {

    @Value("${rocketmq.producer.group}")
    private String destination;

    private IntegrationOrderService orderService = new IntegrationOrderServiceImpl();

    private RocketMQTemplate rocketMQTemplate = new RocketMQTemplate();
    @Scheduled(cron = "0 0 * * 0 *")
    public void cancelOrder(){
        List<IntegrationOrder> orders = orderService.getList();
        for (int i = 0; i < orders.size(); i++) {
            IntegrationOrder order = orders.get(i);
            if (System.currentTimeMillis() - order.getConfirmTime() >= ConstantTime.COMMENT_TIME) {
                order.setFinishTime(System.currentTimeMillis());
                order.setStatus(OrderStatus.ALREADY_COMMENT);
                boolean update = orderService.updateById(order);
                if (update) {
                    IntegrationOrderLog orderLog = LogUtil.getLog(order, LogType.AUTO, "到期未评论，系统自动闭环", null);

                    Message<IntegrationOrderLog> message = MessageBuilder.withPayload(orderLog).build();
                    rocketMQTemplate.asyncSend(destination, message, new SendCallback() {
                        @Override
                        public void onSuccess(SendResult sendResult) {
                        }
                        @Override
                        public void onException(Throwable throwable) {
                            log.error("消息发送失败,日志保存失败,{}",orderLog);
                        }
                    });
                }
            }
        }
    }
}
