package com.woniu.recycle.integration.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniu.recycle.integration.domain.IntegrationOrderLog;

/**
 * <p>
 * 积分商品订单日志表 Mapper 接口
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-11
 */
public interface IntegrationOrderLogMapper extends BaseMapper<IntegrationOrderLog> {

}
