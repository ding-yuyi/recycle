package com.woniu.recycle.integration.controller.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @ClassName SearchByCreateTime
 * @Description 用户查询订单接口入参对象
 * @Author DingYuyi
 * @Date 2021/8/13 14:18
 * @Version 1.0
 */
@Data
@ApiModel(value="用户查询订单接口入参对象")
public class UserSearchByConditionForm {
    @ApiModelProperty(value = "根据时间段查询开始时间")
    private Long createTimeStart;

    @ApiModelProperty(value = "根据时间段查询的结束时间")
    private Long createTimeEnd;

    @ApiModelProperty(value = "订单总价")
    private BigDecimal total;

    @ApiModelProperty(value = "订单状态")
    private Integer status;

    @ApiModelProperty(value = "当前页码")
    @NotNull(message = "当前页码不能为空")
    private Integer pageIdx;

    @ApiModelProperty(value = "每页数量")
    @NotNull(message = "每页数量不能为空")
    private Integer pageSize;
}
