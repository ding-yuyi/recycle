package com.woniu.recycle.integration.component.constant;

/**
 * @ClassName DeletedStatus
 * @Description 逻辑删除状态
 * @Author DingYuyi
 * @Date 2021/8/13 14:42
 * @Version 1.0
 */
public class DeletedStatus {
    /**
     * 没有被删除
     */
    public static final Integer NOT_DELETED = 0;

    /**
     * 已被删除
     */
    public static final Integer IS_DELETED = 1;
}
