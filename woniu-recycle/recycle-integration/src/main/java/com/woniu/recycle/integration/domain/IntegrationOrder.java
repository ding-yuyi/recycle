package com.woniu.recycle.integration.domain;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 积分商城订单表
 * </p>
 *
 * @author DingYuyi
 * @since 2021-08-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="IntegrationOrder对象", description="积分商城订单表")
public class IntegrationOrder implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "订单id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "订单编号")
    private String integrationOrderNum;

    @ApiModelProperty(value = "积分商品id")
    private Integer productId;

    @ApiModelProperty(value = "商品数量")
    private Integer count;

    @ApiModelProperty(value = "商品名称")
    private String productName;

    @ApiModelProperty(value = "商品图片路径")
    private String pictureUrl;

    @ApiModelProperty(value = "商品单价")
    private Integer price;

    @ApiModelProperty(value = "商品总价")
    private Integer total;

    @ApiModelProperty(value = "用户id")
    private Integer userId;

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "地址id")
    private Integer addrId;

    @ApiModelProperty(value = "收货地址")
    private String addrDesc;

    @ApiModelProperty(value = "创建时间")
    private Long createTime;

    @ApiModelProperty(value = "订单状态 0 未收货 | 1 已收货（订单结束）| 2 已评价 | 3 已取消 | 4 已退货 ")
    private Integer status;

    @ApiModelProperty(value = "申请是否通过 0 未通过 | 1 通过")
    private Integer checkStatus;

    @ApiModelProperty(value = "审核时间")
    private Long checkTime;

    @ApiModelProperty(value = "审核管理员id")
    private Integer checkManagerId;

    @ApiModelProperty(value = "收获时间")
    private Long confirmTime;

    @ApiModelProperty(value = "评价内容（可不评价）")
    private String comment;

    @ApiModelProperty(value = "取消时间")
    private Long cancelTime;

    @ApiModelProperty(value = "取消原因")
    private String cancelReason;

    @ApiModelProperty(value = "评价等级 1 | 2 | 3 | 4 | 5  5星最好")
    private Integer level;

    @ApiModelProperty(value = "订单结束时间（评论时间）")
    private Long finishTime;

    @ApiModelProperty(value = "备注")
    private String notes;

    @ApiModelProperty(value = "退货时间（商家收到货）")
    private Long returnTime;

    @ApiModelProperty(value = "退货原因")
    private String returnReason;

    @ApiModelProperty(value = "退货产品状态 0 完好 | 1 有损坏")
    private Integer productStatus;

    @ApiModelProperty(value = "退给用户的积分")
    private Integer returnIntegration;

    @ApiModelProperty(value = "确认退货的管理员id")
    private Integer returnManagerId;

    @ApiModelProperty(value = "商家收到货的时间")
    private Long salesReturnTime;

    @ApiModelProperty(value = "逻辑删除 0 未删除 | 1 已删除（用于用户端查询）")
    private Integer deleted;
}
