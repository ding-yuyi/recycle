package com.woniu.recycle.integration.feign.model;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 积分商品表
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel("openFeign返回产品对象")
public class IntegrationProduct implements Serializable{

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键id")
    private Integer id;

    @ApiModelProperty(value = "商品名称")
    private String name;

    @ApiModelProperty(value = "商品价格")
    private Integer price;

    @ApiModelProperty(value = "商品类别id",example = "1")
    private Integer classifyId;

    @ApiModelProperty(value = "商品描述")
    private String description;

    @ApiModelProperty(value = "商品图片路径")
    private String pictureUrl;

    @ApiModelProperty(value = "商品库存",example = "1")
    private Integer inventory;

    @ApiModelProperty(value = "上下架状态",example = "1")
    @TableField("STATUS")
    private Integer status;

    @ApiModelProperty(value = "记录添加时间")
    private Date rawAddTime;

    @ApiModelProperty(value = "记录更新时间")
    private Date rawUpdateTime;
}
