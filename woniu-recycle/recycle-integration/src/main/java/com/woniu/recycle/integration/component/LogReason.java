package com.woniu.recycle.integration.component;

import com.woniu.recycle.integration.domain.IntegrationOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName LogReason
 * @Description TODO
 * @Author DingYuyi
 * @Date 2021/8/16 11:19
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LogReason {
    private String reason;
    private IntegrationOrder order;
}
