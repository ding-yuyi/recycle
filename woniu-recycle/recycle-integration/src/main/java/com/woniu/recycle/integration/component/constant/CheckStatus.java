package com.woniu.recycle.integration.component.constant;

/**
 * @ClassName CancelStatus
 * @Description TODO
 * @Author DingYuyi
 * @Date 2021/8/14 10:33
 * @Version 1.0
 */
public class CheckStatus {

    public static final Integer PASS = 1;       //申请取消审核通过

    public static final Integer NOT_PASS = 0;   //申请取消审核不通过
}
