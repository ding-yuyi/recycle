package com.woniu.recycle.integration.controller.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @ClassName CreateOrderForm
 * @Description 创建订单接口的入参对象
 * @Author DingYuyi
 * @Date 2021/8/12 15:16
 * @Version 1.0
 */
@Data
@ApiModel(value="创建订单接口的入参对象")
public class CreateOrderForm {

//    @ApiModelProperty(value = "图片地址")
//    @NotBlank
//    private String pictureUrl;

    @ApiModelProperty(value = "商品id")
    @NotNull(message = "商品id不能为空")
    private Integer productId;

    @ApiModelProperty(value = "商品数量")
    @NotNull(message = "商品数量id不能为空")
    private Integer count;

//    @ApiModelProperty(value = "商品名称")
//    @NotBlank(message = "商品名称不能为空")
//    private String productName;

//    @ApiModelProperty(value = "商品单价")
//    @NotNull
//    private BigDecimal price;

//    @ApiModelProperty(value = "订单总价")
//    @NotNull(message = "订单总价不能为空")
//    private BigDecimal total;

    @ApiModelProperty(value = "地址id")
    @NotNull(message = "地址id不能为空")
    private Integer addrId;

//    @ApiModelProperty(value = "地址详情")
//    @NotBlank
//    private String addrDesc;

    @ApiModelProperty(value = "订单备注")
    private String notes;
}
