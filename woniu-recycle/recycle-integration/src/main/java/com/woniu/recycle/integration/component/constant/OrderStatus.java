package com.woniu.recycle.integration.component.constant;

/**
 * @ClassName OrderStatus
 * @Description 订单状态常量
 * @Author DingYuyi
 * @Date 2021/8/14 10:46
 * @Version 1.0
 */
public class OrderStatus {
    public static final Integer NOT_RECEIVE = 0;            //用户未收货

    public static final Integer ALREADY_RECEIVE = 1;        //用户已收货

    public static final Integer ALREADY_COMMENT = 5;        //用户已评价

    public static final Integer ALREADY_CANCEL = 2;         //用户已取消

    public static final Integer RETURNING = 3;              //退货中

    public static final Integer ALREADY_RETURN = 4;         //已退货（商家收到货）

    public static final Integer ON_CHECK = 6;               //审核中
}
