package com.woniu.recycle.integration.controller.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @ClassName SearchLogTypeForm
 * @Description 根据操作者类型查询日志接口入参对象
 * @Author DingYuyi
 * @Date 2021/8/19 19:26
 * @Version 1.0
 */
@Data
@ApiModel(value = "根据操作者类型查询日志接口入参对象")
public class SearchLogTypeForm {
    @ApiModelProperty(value = "操作者类型,0普通用户,1管理员,2系统自动")
    @NotNull
    @Min(0)
    @Max(2)
    private Integer logType;

    @NotNull
    @ApiModelProperty(value = "当前页码")
    private Integer current;

    @NotNull
    @ApiModelProperty(value = "每页数量")
    private Integer size;
}
