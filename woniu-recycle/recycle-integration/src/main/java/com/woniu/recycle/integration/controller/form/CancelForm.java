package com.woniu.recycle.integration.controller.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @ClassName CancelForm
 * @Description 申请取消订单接口入参对象
 * @Author DingYuyi
 * @Date 2021/8/13 15:20
 * @Version 1.0
 */
@Data
@ApiModel(value="申请取消订单接口入参对象")
public class CancelForm {

    @ApiModelProperty(value = "订单id")
    @NotNull(message = "订单id不能为空")
    private Integer OrderId;

    @ApiModelProperty(value = "取消原因不能为空")
    @NotBlank(message = "取消原因不能为空")
    private String cancelReason;
}
