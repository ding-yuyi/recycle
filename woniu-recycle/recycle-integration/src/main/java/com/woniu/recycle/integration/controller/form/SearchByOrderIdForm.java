package com.woniu.recycle.integration.controller.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @ClassName SearchByOrderIdForm
 * @Description 根据订单id查询日志接口入参对象
 * @Author DingYuyi
 * @Date 2021/8/19 20:24
 * @Version 1.0
 */
@Data
@ApiModel(value = "根据订单id查询日志接口入参对象")
public class SearchByOrderIdForm {
    @NotNull(message = "id不能为空")
    @ApiModelProperty(value = "订单id")
    @Min(value = 1,message = "id不能小于1")
    private Integer orderId;

    @NotNull(message = "当前页码不能为空")
    @ApiModelProperty(value = "当前页码")
    private Integer current;

    @NotNull(message = "每页数量不能为空")
    @ApiModelProperty(value = "每页数量")
    private Integer size;
}
