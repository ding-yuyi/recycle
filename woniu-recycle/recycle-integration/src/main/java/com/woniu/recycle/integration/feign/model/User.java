package com.woniu.recycle.integration.feign.model;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel("openFeign返回用户对象")
public class User {
    @ApiModelProperty(value = "用户id")
    private Integer id;

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "密码")
    private String pwd;

    @ApiModelProperty(value = "电话号码")
    private String tel;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "qq号")
    private String qqNum;

    @ApiModelProperty(value = "角色 0 普通用户 | 1 管理员 | 2 超级管理员")
    private Integer role;

    @ApiModelProperty(value = "头像地址")
    private String iconUrl;

    @ApiModelProperty(value = "性别 0 保密 | 1 男 | 2 女")
    private Integer gender;

    @ApiModelProperty(value = "昵称")
    private String nickname;

    @ApiModelProperty(value = "个人简介")
    private String introduction;

    @ApiModelProperty(value = "积分")
    private Integer integration;

    @ApiModelProperty(value = "余额")
    private BigDecimal balance;

    @ApiModelProperty(value = "二维码地址")
    @TableField("QR_url")
    private String qrUrl;

    @ApiModelProperty(value = "注册时间")
    private Long createTime;

    @ApiModelProperty(value = "上次登录时间")
    private Long lastLoginTime;

    @ApiModelProperty(value = "推荐二维码地址")
    private String qrInvite;
}
