package com.woniu.recycle.integration.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @ClassName ConfirmReturningDto
 * @Description 管理员确认退货接口返回对象
 * @Author DingYuyi
 * @Date 2021/8/16 19:41
 * @Version 1.0
 */
@Data
@ApiModel(value="管理员确认退货接口返回对象")
public class ConfirmReturningDto {
    @ApiModelProperty(value = "订单id")
    private Integer id;

    @ApiModelProperty(value = "订单编号")
    private String integrationOrderNum;

    @ApiModelProperty(value = "积分商品id")
    private Integer productId;

    @ApiModelProperty(value = "商品数量")
    private Integer count;

    @ApiModelProperty(value = "商品名称")
    private String productName;

    @ApiModelProperty(value = "商品图片路径")
    private String pictureUrl;

    @ApiModelProperty(value = "商品单价")
    private Integer price;

    @ApiModelProperty(value = "商品总价")
    private BigDecimal total;

    @ApiModelProperty(value = "用户id")
    private Integer userId;

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "地址id")
    private Integer addrId;

    @ApiModelProperty(value = "收货地址")
    private String addrDesc;

    @ApiModelProperty(value = "创建时间")
    private Long createTime;

    @ApiModelProperty(value = "订单状态 0 未收货 | 1 已收货（订单结束）| 2 已评价 | 3 已取消 | 4 已退货 ")
    private Integer status;

    @ApiModelProperty(value = "申请是否通过 0 未通过 | 1 通过")
    private Integer checkStatus;

    @ApiModelProperty(value = "审核时间")
    private Long checkTime;

    @ApiModelProperty(value = "审核管理员id")
    private Integer checkManagerId;

    @ApiModelProperty(value = "退货时间（商家收到货）")
    private Long returnTime;

    @ApiModelProperty(value = "退货原因")
    private String returnReason;

    @ApiModelProperty(value = "退货产品状态 0 完好 | 1 有损坏")
    private Integer productStatus;

    @ApiModelProperty(value = "退给用户的积分")
    private BigDecimal returnIntegration;

    @ApiModelProperty(value = "确认退货的管理员id")
    private Integer returnManagerId;

    @ApiModelProperty(value = "备注")
    private String notes;
}
