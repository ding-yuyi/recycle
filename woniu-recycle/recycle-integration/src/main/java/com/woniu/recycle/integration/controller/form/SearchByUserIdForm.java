package com.woniu.recycle.integration.controller.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @ClassName SearchByUserIdForm
 * @Description 根据操作者id查询日志接口入参对象
 * @Author DingYuyi
 * @Date 2021/8/19 20:13
 * @Version 1.0
 */
@Data
@ApiModel(value = "根据操作者id查询日志接口入参对象")
public class SearchByUserIdForm {
    @NotNull(message = "id不能为空")
    @ApiModelProperty(value = "操作者id")
    private Integer userId;

    @NotNull(message = "当前页码不能为空")
    @ApiModelProperty(value = "当前页码")
    private Integer current;

    @NotNull(message = "每页数量不能为空")
    @ApiModelProperty(value = "每页数量")
    private Integer size;
}
