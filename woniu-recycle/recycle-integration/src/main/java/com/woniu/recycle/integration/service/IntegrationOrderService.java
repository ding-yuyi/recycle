package com.woniu.recycle.integration.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.recycle.integration.controller.form.*;
import com.woniu.recycle.integration.domain.IntegrationOrder;
import com.woniu.recycle.integration.dto.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 积分商城订单表 服务类
 * </p>
 *
 * @author cy/lyl
 * @since 2021-08-11
 */
public interface IntegrationOrderService extends IService<IntegrationOrder> {

    CreateOrderDto createOrder(CreateOrderForm form) throws InterruptedException;

    UserSearchResultDto userSearchByCondition(UserSearchByConditionForm form);

    Boolean deleteOne(Integer orderId);

    Boolean cancel(CancelForm form);

    CheckDto check(CheckForm form);

    ManagerSearchResultDto managerSearchByCondition(ManagerSearchByConditionForm form);

    ConfirmDto confirm(ConfirmForm form);

    Map<Integer,String> delete(List<Integer> orderIds);

    CommentDto comment(CommentForm form);

    Boolean returning(ReturningForm form);

    ConfirmReturningDto confirmReturning(ConfirmReturningForm form);

    List<IntegrationOrder> getList();

//    List<SearchMDto> searchByStatusM(SearchByStatusForm form);
//
//    List<SearchMDto> searchByCreateTimeM(UserSearchByConditionForm form);
//
//    List<ByIntegrationDto> searchByIntegrationM(SearchByIntegrationForm form);
}
