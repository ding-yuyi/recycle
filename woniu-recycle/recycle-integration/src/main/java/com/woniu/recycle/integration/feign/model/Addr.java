package com.woniu.recycle.integration.feign.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("openFeign返回地址对象")
public class Addr {
    @ApiModelProperty(value = "用户id")
    private Integer userId;

    @ApiModelProperty(value = "国家")
    private String nation;

    @ApiModelProperty(value = "省")
    private String province;

    @ApiModelProperty(value = "市")
    private String city;

    @ApiModelProperty(value = "区或县")
    private String country;

    @ApiModelProperty(value = "地址详情")
    private String addrDesc;

    @ApiModelProperty(value = "0 不是默认地址 | 1 默认地址")
    private Integer isDefault;
}
