package com.woniu.recycle.integration.component.exceptionEnum;

public enum IntegrationExceptionEnum {
    CREATE_FAIL("创建订单失败，请重试",3001),
    SEARCH_FAIL("查询失败",3002),
    ALREADY_DELETED("订单已被删除",3003),
    ORDER_NOT_EXISTS("订单不存在",3004),
    ORDER_UPDATE_FAIL("订单修改失败",3005),
    CAN_NOT_CANCEL("该订单不能取消",3006),
    CAN_NOT_DELETE("订单还未完成，不能删除",3007),
    CANCEL_NOT_COMMIT("申请未提交，无需审核",3008),
    CAN_NOT_RETURN("不满足退货条件",3009),
    INTEGRATION_NOT_ENOUGH("用户积分不足",3010),
    CONFIRM_RETURNING_FAIL("确认退货订单失败",3011),
    CAN_NOT_COMMENT("不能评论",3012),
    CAN_NOT_CONFIRM("不能全确认收货",3013);
    Integer code;
    String msg;

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    IntegrationExceptionEnum(String msg,Integer code) {
        this.code = code;
        this.msg = msg;
    }
}
