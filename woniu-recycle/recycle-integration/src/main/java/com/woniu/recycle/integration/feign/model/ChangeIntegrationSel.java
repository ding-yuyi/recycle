package com.woniu.recycle.integration.feign.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *
 */
@ApiModel("商品积分变动Dto")
@Data
public class ChangeIntegrationSel {

    @ApiModelProperty(value = "id", example = "1")
    private Integer id;

    @ApiModelProperty(value = "积分", example = "1")
    private Integer integration;

}
