package com.woniu.recycle.integration.controller;

import com.woniu.recycle.commons.core.model.ResponseEntity;
import com.woniu.recycle.commons.web.component.BaseController;
import com.woniu.recycle.integration.controller.form.*;
import com.woniu.recycle.integration.dto.*;
import com.woniu.recycle.integration.service.IntegrationOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 积分商城订单表 前端控制器
 * </p>
 *
 * @author DingYuyi
 * @since 2021-08-11
 */
@RestController
@RequestMapping("order")
@Slf4j
@Api(tags = "积分商品订单管理")
public class IntegrationOrderController extends BaseController {

    @Resource
    private IntegrationOrderService service;

    /**
     *@author DingYuyi
     *@Description 新增订单
     *@Date 19:06 2021/8/12
     *@Param form
     *@Return ResponseEntity<IntegrationOrderDto>
     */
    @ApiOperation(value = "新增订单")
    @PostMapping("/auth/create")
    public ResponseEntity<CreateOrderDto> createOrder(@RequestBody @Valid CreateOrderForm form, BindingResult result) throws InterruptedException {
        log.info("创建订单信息:{}",form);

        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;

        CreateOrderDto orderDto = service.createOrder(form);
        if (null == orderDto) return ResponseEntity.BuildError(CreateOrderDto.class).setMsg("新增失败");
        return ResponseEntity.BuildSuccess(CreateOrderDto.class).setMsg("新增成功").setData(orderDto);
    }

    /**
     *@author DingYuyi
     *@Description 用户查询订单(无条件查询，根据订单状态、创建时间、总积分查询)
     *@Date 9:54 2021/8/13
     *@Param UserSearchByConditionForm
     *@Return ResponseEntity<List<UserSearchByConditionDto>>
     */
    @PostMapping("/auth/userSearchByCondition")
    @ApiOperation(value = "用户查询订单(无条件查询，根据订单状态、创建时间、总积分查询)")
    public ResponseEntity<UserSearchResultDto> userSearchByCondition(@RequestBody @Valid UserSearchByConditionForm form, BindingResult result){
        log.info("查询信息,{}",form);

        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;

        UserSearchResultDto resultDto = service.userSearchByCondition(form);
        if (null == resultDto || resultDto.getDtoList().size() <= 0) return ResponseEntity.BuildError(UserSearchResultDto.class).setMsg("查询失败");
        return ResponseEntity.BuildSuccess(UserSearchResultDto.class).setMsg("查询成功").setData(resultDto);
    }

//    /**
//     *@author DingYuyi
//     *@Description 用户根据状态查询
//     *@Date 12:26 2021/8/13
//     *@Param form
//     *@Return ResponseEntity<List<ByStatusDto>>
//     */
//    @PostMapping("/auth/searchByStatus")
//    public ResponseEntity<List<ByStatusDto>> searchByStatus(@RequestBody @Valid SearchByStatusForm form,BindingResult result){
//        log.info("查询信息,{}",form);
//
//        //参数校验
//        ResponseEntity responseEntity = extractError(result);
//        if(null != responseEntity) return responseEntity;
//
//        List<ByStatusDto> dtoList = service.searchByStatus(form);
//        if (null == dtoList || dtoList.size() <= 0) return ResponseEntity.BuildErrorList(ByStatusDto.class).setMsg("查询失败");
//        return ResponseEntity.BuildSuccessList(ByStatusDto.class).setMsg("查询成功").setData(dtoList);
//    }
//
//    /**
//     *@author DingYuyi
//     *@Description 用户根据时间查询
//     *@Date 14:29 2021/8/13
//     *@Param form
//     *@Return ResponseEntity<List<ByCreateTimeDto>>
//     */
//    @PostMapping("/auth/searchByCreateTime")
//    public ResponseEntity<List<ByCreateTimeDto>> searchByCreateTime(@RequestBody @Valid UserSearchByConditionForm form, BindingResult result){
//        log.info("查询信息,{}",form);
//
//        //参数校验
//        ResponseEntity responseEntity = extractError(result);
//        if(null != responseEntity) return responseEntity;
//
//        List<ByCreateTimeDto> dtoList = service.searchByCreateTime(form);
//        if (null == dtoList || dtoList.size() <= 0) return ResponseEntity.BuildErrorList(ByCreateTimeDto.class).setMsg("查询失败");
//        return ResponseEntity.BuildSuccessList(ByCreateTimeDto.class).setMsg("查询成功").setData(dtoList);
//    }
//
//    /**
//     *@author DingYuyi
//     *@Description 用户根据消费总积分查询
//     *@Date 14:35 2021/8/13
//     *@Param form
//     *@Return ResponseEntity<List<ByIntegrationDto>>
//     */
//    @PostMapping("/auth/searchByIntegration")
//    public ResponseEntity<List<ByIntegrationDto>> searchByIntegration(@RequestBody @Valid SearchByIntegrationForm form,BindingResult result){
//        log.info("查询信息,{}",form);
//
//        //参数校验
//        ResponseEntity responseEntity = extractError(result);
//        if(null != responseEntity) return responseEntity;
//
//        List<ByIntegrationDto> dtoList = service.searchByIntegration(form);
//        if (null == dtoList || dtoList.size() <= 0) return ResponseEntity.BuildErrorList(ByIntegrationDto.class).setMsg("查询失败");
//        return ResponseEntity.BuildSuccessList(ByIntegrationDto.class).setMsg("查询成功").setData(dtoList);
//    }

    /**
     *@author DingYuyi
     *@Description 用户删除历史订单
     *@Date 15:18 2021/8/13
     *@Param OrderId
     *@Return ResponseEntity<String>
     */
    @GetMapping("/auth/deleteOne")
    @ApiOperation(value = "用户删除历史订单")
    public ResponseEntity<String> deleteOne(Integer OrderId){
        log.info("订单ID,{}", OrderId);

        //参数校验
        if(null == OrderId) return ResponseEntity.BuildError(String.class).setMsg("参数校验失败");
        Boolean isDeleted = service.deleteOne(OrderId);
        if (null == isDeleted || !isDeleted) return ResponseEntity.BuildError(String.class).setMsg("删除失败");
        return ResponseEntity.BuildSuccess(String.class).setMsg("删除成功");
    }

    /**
     *@author DingYuyi
     *@Description 用户删除多个历史记录
     *@Date 16:22 2021/8/16
     *@Param orderIds
     *@Return ResponseEntity<Map<Integer,String>>
     */
    @PostMapping("/auth/delete")
    @ApiOperation(value = "删除多个")
    public ResponseEntity<Map<Integer,String>> delete(@RequestParam List<Integer> orderIds){
        log.info("入参信息,{}",orderIds);

        //参数校验
        if(orderIds.size() <= 0) return ResponseEntity.BuildErrorMap(Integer.class,String.class).setMsg("参数校验失败");

        Map<Integer, String> map = service.delete(orderIds);
        if (map == null || map.size() <= 0) return ResponseEntity.BuildErrorMap(Integer.class,String.class).setMsg("删除失败");
        return ResponseEntity.BuildSuccessMap(Integer.class,String.class).setMsg("删除成功").setData(map);

    }
    /**
     *@author DingYuyi
     *@Description 用户申请取消订单
     *@Date 15:34 2021/8/13
     *@Param form
     *@Return ResponseEntity<String>
     */
    @PostMapping("/auth/cancel")
    @ApiOperation(value = "用户申请取消订单")
    public ResponseEntity<String> cancel(@Valid CancelForm form,BindingResult result){
        log.info("入参信息:{}",form);

        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;

        Boolean cancelFlag = service.cancel(form);
        if (null == cancelFlag || !cancelFlag) return ResponseEntity.BuildError(String.class).setMsg("取消失败");
        return ResponseEntity.BuildSuccess(String.class).setMsg("进入审核");
    }

    /**
     *@author DingYuyi
     *@Description 用户申请退货
     *@Date 19:22 2021/8/16
     *@Param form
     *@Return ResponseEntity<String>
     */
    @PostMapping("/auth/returning")
    @ApiOperation(value = "用户申请退货")
    public ResponseEntity<String> returning(@RequestBody @Valid ReturningForm form,BindingResult result){
        log.info("入参信息,{}",form);

        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;

        Boolean returningFlag = service.returning(form);
        if (null == returningFlag || !returningFlag) return ResponseEntity.BuildError(String.class).setMsg("取消失败");
        return ResponseEntity.BuildSuccess(String.class).setMsg("进入审核");
    }

    /**
     *@author DingYuyi
     *@Description 后台审核订单
     *@Date 11:11 2021/8/14
     *@Param form
     * @param: result
     *@Return ResponseEntity<CheckDto>
     */
    @PostMapping("/auth/check")
    @ApiOperation(value = "后台审核订单")
    public ResponseEntity<CheckDto> check(@Valid CheckForm form,BindingResult result){
        log.info("入参信息,{}",form);

        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;

        CheckDto checkDto = service.check(form);
        if (null == checkDto) return ResponseEntity.BuildError(CheckDto.class).setMsg("审核失败");
        return ResponseEntity.BuildSuccess(CheckDto.class).setMsg("审核成功");
    }

    /**
     *@author DingYuyi
     *@Description 管理员确认退货
     *@Date 19:56 2021/8/16
     *@Param form
     *@Return ResponseEntity<ConfirmReturningDto>
     */
    @ApiOperation(value = "管理员确认退货")
    @PostMapping("/auth/confirmReturning")
    public ResponseEntity<ConfirmReturningDto> confirmReturning(@RequestBody @Valid ConfirmReturningForm form,BindingResult result){
        log.info("入参信息,{}",form);

        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;

        ConfirmReturningDto returningDto = service.confirmReturning(form);
        if (null == returningDto) return ResponseEntity.BuildError(ConfirmReturningDto.class).setMsg("确认退货失败");
        return ResponseEntity.BuildSuccess(ConfirmReturningDto.class).setMsg("确认退货成功").setData(returningDto);
    }

    /**
     *@author DingYuyi
     *@Description 后台管理员查询订单（无条件、根据状态、创建时间、总积分查询）
     *@Date 15:20 2021/8/14
     *@Param form
     * @param: result
     *@Return ResponseEntity<List<SearchAllDto>>
     */
    @PostMapping("/auth/managerSearchByCondition")
    @ApiOperation(value = "后台管理员查询订单（无条件、根据状态、创建时间、总积分查询）")
    public ResponseEntity<ManagerSearchResultDto> managerSearchByCondition(@RequestBody @Valid ManagerSearchByConditionForm form, BindingResult result){
        log.info("入参信息:{}",form);

        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;

        ManagerSearchResultDto resultDto = service.managerSearchByCondition(form);
        if (null == resultDto || resultDto.getDtoList().size() <= 0) return ResponseEntity.BuildError(ManagerSearchResultDto.class).setMsg("查询失败");
        return ResponseEntity.BuildSuccess(ManagerSearchResultDto.class).setMsg("查询成功").setData(resultDto);
    }

    /**
     *@author DingYuyi
     *@Description 用户确认收货
     *@Date 16:49 2021/8/14
     *@Param form
     *@param: result
     *@Return ResponseEntity<UpdateStatusDto>
     */
    @PostMapping("/auth/updateStatus")
    @ApiOperation(value = "用户确认收货")
    public ResponseEntity<ConfirmDto> confirm(@RequestBody @Valid ConfirmForm form, BindingResult result){
        log.info("入参信息:{}",form);

        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;

        ConfirmDto updateStatusDto = service.confirm(form);
        if (null == updateStatusDto) return ResponseEntity.BuildError(ConfirmDto.class).setMsg("确认失败");
        return ResponseEntity.BuildSuccess(ConfirmDto.class).setMsg("确认成功").setData(updateStatusDto);
    }

    /**
     *@author DingYuyi
     *@Description 用户评论
     *@Date 20:11 2021/8/16
     *@param form
     *@Return ResponseEntity<UpdateStatusDto>
     */
    @PostMapping("/auth/comment")
    @ApiOperation(value = "评论接口")
    public ResponseEntity<CommentDto> comment(@RequestBody CommentForm form, BindingResult result){
        log.info("入参信息,{}",form);

        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;

        CommentDto commentDto = service.comment(form);
        if (null == commentDto) return ResponseEntity.BuildError(CommentDto.class).setMsg("评论失败");
        return ResponseEntity.BuildSuccess(CommentDto.class).setMsg("评论成功").setData(commentDto);
    }

//    /**
//     *@author DingYuyi
//     *@Description 后台管理根据状态查询订单
//     *@Date 15:38 2021/8/14
//     *@Param form
//     * @param: result
//     *@Return ResponseEntity<List<SearchMDto>>
//     */
//    @PostMapping("/auth/searchByStatusM")
//    public ResponseEntity<List<SearchMDto>> searchByStatusM(@RequestBody @Valid SearchByStatusForm form,BindingResult result){
//        log.info("查询信息,{}",form);
//
//        //参数校验
//        ResponseEntity responseEntity = extractError(result);
//        if(null != responseEntity) return responseEntity;
//
//        List<SearchMDto> dtoList = service.searchByStatusM(form);
//        if (null == dtoList || dtoList.size() <= 0) return ResponseEntity.BuildErrorList(SearchMDto.class).setMsg("查询失败");
//        return ResponseEntity.BuildSuccessList(SearchMDto.class).setMsg("查询成功").setData(dtoList);
//    }
//
//    /**
//     *@author DingYuyi
//     *@Description 后台根据订单生成时间查询
//     *@Date 15:53 2021/8/14
//     *@Param form
//     * @param: result
//     *@Return ResponseEntity<List<SearchMDto>>
//     */
//    @PostMapping("/auth/searchByCreateTimeM")
//    public ResponseEntity<List<SearchMDto>> searchByCreateTimeM(@RequestBody @Valid UserSearchByConditionForm form, BindingResult result){
//        log.info("查询信息,{}",form);
//
//        //参数校验
//        ResponseEntity responseEntity = extractError(result);
//        if(null != responseEntity) return responseEntity;
//
//        List<SearchMDto> dtoList = service.searchByCreateTimeM(form);
//        if (null == dtoList || dtoList.size() <= 0) return ResponseEntity.BuildErrorList(SearchMDto.class).setMsg("查询失败");
//        return ResponseEntity.BuildSuccessList(SearchMDto.class).setMsg("查询成功").setData(dtoList);
//    }
//
//    @PostMapping("/auth/searchByIntegrationM")
//    public ResponseEntity<List<SearchMDto>> searchByIntegrationM(@RequestBody @Valid SearchByIntegrationForm form,BindingResult result){
//        log.info("查询信息,{}",form);
//
//        //参数校验
//        ResponseEntity responseEntity = extractError(result);
//        if(null != responseEntity) return responseEntity;
//
//        List<ByIntegrationDto> dtoList = service.searchByIntegrationM(form);
//        if (null == dtoList || dtoList.size() <= 0) return ResponseEntity.BuildErrorList(SearchMDto.class).setMsg("查询失败");
//        return ResponseEntity.BuildSuccessList(SearchMDto.class).setMsg("查询成功").setData(dtoList);
//
//    }

}

