package com.woniu.recycle.integration.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @ClassName UserSearchResultDto
 * @Description 后用户查询接口返回对象
 * @Author DingYuyi
 * @Date 2021/8/16 14:55
 * @Version 1.0
 */
@Data
@ApiModel(value="用户查询接口返回对象")
public class UserSearchResultDto {
    @ApiModelProperty(value = "当前页面")
    private Long current;

    @ApiModelProperty(value = "每页数量")
    private Long size;

    @ApiModelProperty(value = "查询总数")
    private Long total;

    @ApiModelProperty(value = "查询订单的具体数据")
    private List<UserSearchByConditionDto> dtoList;
}
