package com.woniu.recycle.integration.util;

import com.woniu.recycle.commons.web.util.ObjectMapperUtil;
import com.woniu.recycle.integration.component.LogReason;
import com.woniu.recycle.integration.domain.IntegrationOrder;
import com.woniu.recycle.integration.domain.IntegrationOrderLog;

/**
 * @ClassName LogUtil
 * @Description TODO
 * @Author DingYuyi
 * @Date 2021/8/16 11:21
 * @Version 1.0
 */
public class LogUtil {

    public static IntegrationOrderLog getLog(IntegrationOrder order,Integer logType,String reason,Integer userId){
        IntegrationOrderLog orderLog = new IntegrationOrderLog();
        orderLog.setLogCreateTime(System.currentTimeMillis());
        orderLog.setOrderId(order.getId());
        orderLog.setOrderNum(order.getIntegrationOrderNum());
        orderLog.setUserId(userId);//需要修改为token解析出来的userId
        orderLog.setLogType(logType);
        orderLog.setOrderStatus(order.getStatus());
        orderLog.setLogReason(ObjectMapperUtil.parseObject(new LogReason(reason, order)));

        return orderLog;
    }
}
