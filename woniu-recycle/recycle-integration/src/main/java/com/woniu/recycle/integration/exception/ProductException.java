package com.woniu.recycle.integration.exception;

import com.woniu.recycle.commons.core.exception.RecycleException;
import com.woniu.recycle.integration.component.exceptionEnum.IntegrationProductEnum;

public class ProductException extends RecycleException {
    //私有静态常量long类型序列化id
    private static  final  long serialVersionUID = 1L;

    public ProductException(IntegrationProductEnum info){
        super(info.getMessage(),info.getCode());
    }
}
