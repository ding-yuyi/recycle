package com.woniu.recycle.integration.feign.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotNull;


/**
 *
 */
@ApiModel("商品积分变动form")
@Data
@AllArgsConstructor
public class ChangeIntegationSelForm {

    @ApiModelProperty(value = "id", example = "1")
    private Integer id;


    @NotNull
    @ApiModelProperty(value = "商品总价", example = "1")
    private Integer integration;


}
