package com.woniu.recycle.integration.component.exceptionEnum;

/**
 * @ClassName UserExceptionEnum
 * @Description TODO
 * @Author DingYuyi
 * @Date 2021/8/17 17:58
 * @Version 1.0
 */
public enum  UserExceptionEnum {
    ID_NULL("当前id不存在",1030);
    Integer code;
    String msg;

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    UserExceptionEnum(String msg,Integer code) {
        this.code = code;
        this.msg = msg;
    }
}
