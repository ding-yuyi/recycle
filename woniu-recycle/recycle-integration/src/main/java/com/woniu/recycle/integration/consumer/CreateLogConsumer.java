package com.woniu.recycle.integration.consumer;

import com.woniu.recycle.integration.domain.IntegrationOrderLog;
import com.woniu.recycle.integration.service.IntegrationOrderLogService;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @ClassName CreateLogConsumer
 * @Description 记录日志的消费者
 * @Author DingYuyi
 * @Date 2021/8/16 12:03
 * @Version 1.0
 */
@Component
@Slf4j
@RocketMQMessageListener(consumerGroup = "createLog",topic = "integration")
public class CreateLogConsumer implements RocketMQListener<IntegrationOrderLog> {

    @Resource
    private IntegrationOrderLogService logService;

    @Override
    public void onMessage(IntegrationOrderLog orderLog) {
        log.info("开始保存日志");
        boolean logFlag = logService.save(orderLog);
        if (!logFlag) log.warn("日志保存失败:,{}",orderLog);
    }
}
