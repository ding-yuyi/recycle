package com.woniu.recycle.integration.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @ClassName searchByUserIdResultDto
 * @Description 日志查询接口返回对象
 * @Author DingYuyi
 * @Date 2021/8/19 19:35
 * @Version 1.0
 */
@Data
@ApiModel("日志查询接口返回对象")
public class SearchResultDto {
    @ApiModelProperty(value = "当前页面")
    private Long current;

    @ApiModelProperty(value = "每页数量")
    private Long size;

    @ApiModelProperty(value = "查询总数")
    private Long total;

    @ApiModelProperty(value = "日志的具体数据")
    private List<OrderLogDto> dtoList;
}
