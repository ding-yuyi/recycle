package com.woniu.recycle.integration.controller.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @ClassName CommentForm
 * @Description 评论接口的入参对象
 * @Author DingYuyi
 * @Date 2021/8/16 16:58
 * @Version 1.0
 */
@Data
@ApiModel(value="评论接口的入参对象")
public class CommentForm {
    @ApiModelProperty(value = "评价等级 1 | 2 | 3 | 4 | 5  5星最好")
    private Integer level;

    @ApiModelProperty(value = "评价内容（可不评价）")
    private String comment;

    @ApiModelProperty(value = "操作者类型,0普通用户,1管理员")
    private Integer logType;

    @ApiModelProperty(value = "订单id")
    @NotNull
    private Integer orderId;
}
