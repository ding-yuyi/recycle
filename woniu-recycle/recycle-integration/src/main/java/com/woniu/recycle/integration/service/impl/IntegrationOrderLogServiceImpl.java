package com.woniu.recycle.integration.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.recycle.commons.web.util.BeanCopyUtil;
import com.woniu.recycle.commons.web.util.ObjectMapperUtil;
import com.woniu.recycle.integration.component.LogReason;
import com.woniu.recycle.integration.controller.form.SearchByOrderIdForm;
import com.woniu.recycle.integration.controller.form.SearchByUserIdForm;
import com.woniu.recycle.integration.controller.form.SearchLogTimeForm;
import com.woniu.recycle.integration.controller.form.SearchLogTypeForm;
import com.woniu.recycle.integration.domain.IntegrationOrderLog;
import com.woniu.recycle.integration.dto.OrderLogDto;
import com.woniu.recycle.integration.dto.SearchResultDto;
import com.woniu.recycle.integration.mapper.IntegrationOrderLogMapper;
import com.woniu.recycle.integration.service.IntegrationOrderLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * <p>
 * 积分商品订单日志表 服务实现类
 * </p>
 *
 * @author DingYuyi
 * @since 2021-08-11
 */
@Service
@Slf4j
public class IntegrationOrderLogServiceImpl extends ServiceImpl<IntegrationOrderLogMapper, IntegrationOrderLog> implements IntegrationOrderLogService {

    @Override
    public SearchResultDto searchByLogType(SearchLogTypeForm form) {
        QueryWrapper<IntegrationOrderLog> wrapper = new QueryWrapper<>();
        wrapper.eq("log_type",form.getLogType())
                .orderByDesc("log_create_time");

        Page page = page(new Page(form.getCurrent(), form.getSize()), wrapper);
        SearchResultDto resultDto = null;
        if(!(page.getRecords().size() <= 0 || null == page)) {
            resultDto = BeanCopyUtil.copyObject(page, SearchResultDto::new);
            List<OrderLogDto> logDtos = BeanCopyUtil.copyList(page.getRecords(), OrderLogDto::new);
            for (int i = 0; i < logDtos.size(); i++) {
                IntegrationOrderLog integrationOrderLog = (IntegrationOrderLog) page.getRecords().get(i);
                logDtos.get(i).setLogReason(ObjectMapperUtil.parseObject(integrationOrderLog.getLogReason(), LogReason.class));
            }
            resultDto.setDtoList(logDtos);
        }
        return resultDto;
    }

    @Override
    public SearchResultDto searchByCreateTime(SearchLogTimeForm form) {
        QueryWrapper<IntegrationOrderLog> wrapper = new QueryWrapper<>();
        wrapper.ge("log_create_time",form.getLogCreateTimeStart())
                .le(form.getLogCreateTimeEnd() != null,"log_create_time",form.getLogCreateTimeEnd())
                .orderByDesc("log_create_time");

        Page page = page(new Page(form.getCurrent(), form.getSize()), wrapper);
        SearchResultDto resultDto = null;
        if(!(page.getRecords().size() <= 0 || null == page)) {
            resultDto = BeanCopyUtil.copyObject(page, SearchResultDto::new);
            List<OrderLogDto> logDtos = BeanCopyUtil.copyList(page.getRecords(), OrderLogDto::new);
            for (int i = 0; i < logDtos.size(); i++) {
                IntegrationOrderLog integrationOrderLog = (IntegrationOrderLog) page.getRecords().get(i);
                logDtos.get(i).setLogReason(ObjectMapperUtil.parseObject(integrationOrderLog.getLogReason(), LogReason.class));
            }
            resultDto.setDtoList(logDtos);
            resultDto.setDtoList(logDtos);
        }
        return resultDto;
    }

    @Override
    public SearchResultDto searchByUserId(SearchByUserIdForm form) {
        QueryWrapper<IntegrationOrderLog> wrapper = new QueryWrapper<>();
        wrapper.ge("user_id",form.getUserId())
                .orderByDesc("log_create_time");

        Page page = page(new Page(form.getCurrent(), form.getSize()), wrapper);
        SearchResultDto resultDto = null;
        if(!(page.getRecords().size() <= 0 || null == page)) {
            resultDto = BeanCopyUtil.copyObject(page, SearchResultDto::new);
            List<OrderLogDto> logDtos = BeanCopyUtil.copyList(page.getRecords(), OrderLogDto::new);
            for (int i = 0; i < logDtos.size(); i++) {
                IntegrationOrderLog integrationOrderLog = (IntegrationOrderLog) page.getRecords().get(i);
                logDtos.get(i).setLogReason(ObjectMapperUtil.parseObject(integrationOrderLog.getLogReason(), LogReason.class));
            }
            resultDto.setDtoList(logDtos);
            resultDto.setDtoList(logDtos);
        }
        return resultDto;
    }

    @Override
    public SearchResultDto searchByOrderId(SearchByOrderIdForm form) {
        QueryWrapper<IntegrationOrderLog> wrapper = new QueryWrapper<>();
        wrapper.ge("order_id",form.getOrderId())
                .orderByDesc("log_create_time");

        Page page = page(new Page(form.getCurrent(), form.getSize()), wrapper);
        SearchResultDto resultDto = null;
        if(!(page.getRecords().size() <= 0 || null == page)) {
            resultDto = BeanCopyUtil.copyObject(page, SearchResultDto::new);
            List<OrderLogDto> logDtos = BeanCopyUtil.copyList(page.getRecords(), OrderLogDto::new);
            for (int i = 0; i < logDtos.size(); i++) {
                IntegrationOrderLog integrationOrderLog = (IntegrationOrderLog) page.getRecords().get(i);
                logDtos.get(i).setLogReason(ObjectMapperUtil.parseObject(integrationOrderLog.getLogReason(), LogReason.class));
            }
            resultDto.setDtoList(logDtos);
            resultDto.setDtoList(logDtos);
        }
        return resultDto;
    }
}
