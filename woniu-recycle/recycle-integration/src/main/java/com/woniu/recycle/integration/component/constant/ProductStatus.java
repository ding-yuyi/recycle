package com.woniu.recycle.integration.component.constant;

/**
 * @ClassName ProductStatus
 * @Description TODO
 * @Author DingYuyi
 * @Date 2021/8/18 16:54
 * @Version 1.0
 */
public class ProductStatus {
    public static final Integer INTACT = 0;       //完好的

    public static final Integer DAMAGED = 1;      //有损坏的
}
