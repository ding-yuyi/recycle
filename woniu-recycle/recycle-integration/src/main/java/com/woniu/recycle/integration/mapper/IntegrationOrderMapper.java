package com.woniu.recycle.integration.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniu.recycle.integration.domain.IntegrationOrder;

/**
 * <p>
 * 积分商城订单表 Mapper 接口
 * </p>
 *
 * @author cy/lyl
 * @since 2021-08-11
 */
public interface IntegrationOrderMapper extends BaseMapper<IntegrationOrder> {

}
