package com.woniu.recycle.integration.exception;

import com.woniu.recycle.commons.core.exception.RecycleException;
import com.woniu.recycle.integration.component.exceptionEnum.UserExceptionEnum;

public class UserException extends RecycleException {

    public UserException(UserExceptionEnum info){
        super(info.getMsg(),info.getCode());
    }
}
