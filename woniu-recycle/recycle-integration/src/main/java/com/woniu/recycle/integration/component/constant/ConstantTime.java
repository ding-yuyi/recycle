package com.woniu.recycle.integration.component.constant;

/**
 * @ClassName ExpireTime
 * @Description redis过期时间常量类
 * @Author DingYuyi
 * @Date 2021/8/13 9:49
 * @Version 1.0
 */
public class ConstantTime {
    public static final Long ONE_DAY = 24 * 60 * 60L;
    public static final Long ONE_MONTH = 30 * 24 * 60 * 60L;
    public static final Long COMMENT_TIME = 7 * 24 * 60 * 60L;
}
