package com.woniu.recycle.integration.feign;

import com.woniu.recycle.commons.core.model.ResponseEntity;
import com.woniu.recycle.integration.feign.form.ChangeProductNum;
import com.woniu.recycle.integration.feign.model.IntegrationProduct;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;


@FeignClient(name = "product-server")
public interface ProductFeignClient {
    @GetMapping("/product/integrationProduct/selectbyid")
    ResponseEntity<IntegrationProduct> selectById(@RequestParam Integer productId);

    @PutMapping("/product/integrationProduct/auth/changeinventorsel")
    ResponseEntity<IntegrationProduct> changeNumsel(@SpringQueryMap ChangeProductNum changeProductNum);

    @PutMapping("/product/integrationProduct/auth/changeinventoradd")
    ResponseEntity<IntegrationProduct> changeNum(@SpringQueryMap ChangeProductNum changeProductNum);
}
