package com.woniu.recycle.integration.feign;

import com.woniu.recycle.commons.core.model.ResponseEntity;
import com.woniu.recycle.integration.feign.form.ChangeIntegationSelForm;
import com.woniu.recycle.integration.feign.model.Addr;
import com.woniu.recycle.integration.feign.model.ChangeIntegrationSel;
import com.woniu.recycle.integration.feign.model.User;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "user-server")
public interface UserFeignClient {

    @GetMapping("/user/userModule/auth/UserById")
    ResponseEntity<User> UserById(@RequestParam Integer userId);

    @PostMapping("/user/addr/auth/findAddrById")
    ResponseEntity<Addr> findAddrById(@RequestParam Integer id);

    @PutMapping("/user/userModule/auth/changeDel")
    ResponseEntity<ChangeIntegrationSel> changeDel(@SpringQueryMap ChangeIntegationSelForm changeIntegationSelForms);

    @PutMapping("/user/userModule/auth/changeAdd")
    ResponseEntity<ChangeIntegrationSel> changeAdd(@SpringQueryMap ChangeIntegationSelForm changeIntegationSelForms);
}
