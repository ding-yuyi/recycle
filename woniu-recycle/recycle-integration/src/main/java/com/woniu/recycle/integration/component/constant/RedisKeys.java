package com.woniu.recycle.integration.component.constant;

/**
 * @ClassName RedisKeys
 * @Description TODO
 * @Author DingYuyi
 * @Date 2021/8/12 19:36
 * @Version 1.0
 */
public class RedisKeys {
    /**
     * 订单的key前缀
     */
    public static final String INTEGRATION_KEY = "IntegrationOrder:id:";
}
