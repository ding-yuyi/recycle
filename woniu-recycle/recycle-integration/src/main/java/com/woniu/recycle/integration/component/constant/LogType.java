package com.woniu.recycle.integration.component.constant;

/**
 * @ClassName LogType
 * @Description TODO
 * @Author DingYuyi
 * @Date 2021/8/13 15:08
 * @Version 1.0
 */
public class LogType {

    public static final Integer NORMAL_USER = 0;    //普通用户

    public static final Integer MANAGER = 1;        //管理员

    public static final Integer AUTO = 2;           //到期自动
}
