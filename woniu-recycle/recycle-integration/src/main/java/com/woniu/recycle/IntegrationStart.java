package com.woniu.recycle;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @ClassName OrderStart
 * @Description 启动类
 * @Author DingYuyi
 * @Date 2021/8/11 19:31
 * @Version 1.0
 */
@SpringBootApplication
@MapperScan("com.woniu.recycle.integration.mapper")
@EnableTransactionManagement
@EnableFeignClients
@EnableScheduling
public class IntegrationStart {
    public static void main(String[] args) {
        SpringApplication.run(IntegrationStart.class,args);
    }
}
