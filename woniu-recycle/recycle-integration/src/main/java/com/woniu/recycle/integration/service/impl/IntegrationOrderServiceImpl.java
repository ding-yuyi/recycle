package com.woniu.recycle.integration.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.recycle.commons.core.model.ResponseEntity;
import com.woniu.recycle.commons.web.util.BeanCopyUtil;
import com.woniu.recycle.commons.web.util.ObjectMapperUtil;
import com.woniu.recycle.commons.web.util.ServletUtil;
import com.woniu.recycle.integration.component.constant.*;
import com.woniu.recycle.integration.component.exceptionEnum.IntegrationExceptionEnum;
import com.woniu.recycle.integration.controller.form.*;
import com.woniu.recycle.integration.domain.IntegrationOrder;
import com.woniu.recycle.integration.domain.IntegrationOrderLog;
import com.woniu.recycle.integration.feign.UserFeignClient;
import com.woniu.recycle.integration.feign.form.ChangeIntegationSelForm;
import com.woniu.recycle.integration.feign.form.ChangeProductNum;
import com.woniu.recycle.integration.feign.model.Addr;
import com.woniu.recycle.integration.feign.model.ChangeIntegrationSel;
import com.woniu.recycle.integration.feign.model.IntegrationProduct;
import com.woniu.recycle.integration.feign.model.User;
import com.woniu.recycle.integration.dto.*;
import com.woniu.recycle.integration.exception.IntegrationException;
import com.woniu.recycle.integration.feign.ProductFeignClient;
import com.woniu.recycle.integration.mapper.IntegrationOrderMapper;
import com.woniu.recycle.integration.service.IntegrationOrderService;
import com.woniu.recycle.integration.util.LogUtil;
import com.woniu.recycle.integration.util.OrderNumUtil;
import com.woniu.recycle.integration.util.UserUtil;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.print.attribute.standard.Severity;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 积分商城订单表 服务实现类
 * </p>
 *
 * @author cy/lyl
 * @since 2021-08-11
 */
@Service
@Slf4j
public class IntegrationOrderServiceImpl extends ServiceImpl<IntegrationOrderMapper, IntegrationOrder> implements IntegrationOrderService {

    @Resource
    private RedissonClient redissonClient;

    @Value("${rocketmq.producer.group}")
    private String destination;

    @Resource
    private RocketMQTemplate rocketMQtemplate;

    @Resource
    private IntegrationOrderMapper orderMapper;

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private UserUtil userUtil;

    @Resource
    private ProductFeignClient productFeignClient;

    @Resource
    private UserFeignClient userFeignClient;

    /**
     * 用户下单
     */
    @Override
    @GlobalTransactional
    public CreateOrderDto createOrder(CreateOrderForm form) throws InterruptedException {

        //查询商品是否存在,不存在则抛异常（需要调用openfeign）
        ResponseEntity<IntegrationProduct> responseProduct = productFeignClient.selectById(form.getProductId());
        if(null == responseProduct.getData()) throw new IntegrationException(responseProduct.getMsg(),responseProduct.getCode());
        IntegrationProduct product = responseProduct.getData();

        //查询地址是否存在
        ResponseEntity<Addr> responseAddr = userFeignClient.findAddrById(form.getAddrId());
        if(null == responseAddr.getData()) throw new IntegrationException(responseAddr.getMsg(),responseAddr.getCode());

        //解析token，获取user信息
        User user = userUtil.getUser(ServletUtil.getRequest());

        //查询用户积分是否足够
        int total = product.getPrice() * form.getCount();
        log.info("total,{}",total);
        if(user.getIntegration() ==null || total > user.getIntegration()) throw new IntegrationException(IntegrationExceptionEnum.INTEGRATION_NOT_ENOUGH);

        //适配
        IntegrationOrder order = BeanCopyUtil.copyObject(form, IntegrationOrder::new);
        order.setIntegrationOrderNum(OrderNumUtil.getNum(form.getProductId()));
        order.setCreateTime(System.currentTimeMillis());
        order.setUsername(user.getUsername());
        order.setUserId(user.getId());
        order.setTotal(total);
        order.setProductName(product.getName());
        order.setAddrDesc(responseAddr.getData().getAddrDesc());
        BeanCopyUtil.copyProperties(product,order);
        log.info("新增订单信息,{}",order);
        order.setStatus(OrderStatus.NOT_RECEIVE);

        boolean flag = save(order);
        if (!flag) throw new IntegrationException(IntegrationExceptionEnum.CREATE_FAIL);
        //订单新增成功，保存日志
        order = getById(order.getId());
        IntegrationOrderLog orderLog = LogUtil.getLog(order, LogType.NORMAL_USER, "新增订单", 1);

        Message<IntegrationOrderLog> message = MessageBuilder.withPayload(orderLog).build();
        rocketMQtemplate.asyncSend(destination, message, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
            }

            @Override
            public void onException(Throwable throwable) {
                log.error("消息发送失败,日志保存失败,{}",orderLog);
            }
        });

        //订单新增成功，减少用户积分
        ResponseEntity<ChangeIntegrationSel> changeIntegrationSelResponseEntity = userFeignClient.changeDel(new ChangeIntegationSelForm(order.getUserId(), order.getTotal()));
        if (null == changeIntegrationSelResponseEntity.getData()) throw new IntegrationException(IntegrationExceptionEnum.CREATE_FAIL);

        //订单新增成功，减少商品库存
        //加锁
        RLock lock = redissonClient.getLock("order:product:id:"+form.getProductId());
        try {
            //进行加锁，最多等待20秒，上锁后20秒自动解锁
            boolean res = lock.tryLock(20, 20, TimeUnit.SECONDS);
            //加锁成功执行库存减少业务
            if(res) {
                  ResponseEntity<IntegrationProduct> responseEntity = productFeignClient.changeNumsel(new ChangeProductNum(product.getId(), form.getCount()));
                  if(null == responseEntity.getData()) throw new IntegrationException(IntegrationExceptionEnum.CREATE_FAIL);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
            throw new InterruptedException(e.getMessage());
        } finally {
            //解锁
            if(lock != null) lock.unlock();
        }

        //存入Redis
        String order2String = ObjectMapperUtil.parseObject(order);
        String key = RedisKeys.INTEGRATION_KEY + order.getId();
        stringRedisTemplate.opsForValue().set(key,order2String);
        //设置过期时间,一天，订单状态更改时，重新设置过期时间
        stringRedisTemplate.expire(key, ConstantTime.ONE_DAY, TimeUnit.SECONDS);

        //适配dto
        CreateOrderDto integrationOrderDto = BeanCopyUtil.copyObject(order, CreateOrderDto::new);
        return integrationOrderDto;
    }

    /**
     * 用户查询订单
     */
    @Override
    public UserSearchResultDto userSearchByCondition(UserSearchByConditionForm form) {

        //解析token
        Integer userId = userUtil.getUserId(ServletUtil.getRequest());

        //生成条件构造器
        QueryWrapper<IntegrationOrder> wrapper = new QueryWrapper<>();
        wrapper.eq(form.getStatus()!=null,"status",form.getStatus())
                .ge(form.getCreateTimeStart()!=null,"create_time",form.getCreateTimeStart())
                .le(form.getCreateTimeStart()!=null && form.getCreateTimeEnd() != null,
                        "create_time",form.getCreateTimeEnd())
                .ge(form.getTotal()!=null,"total",form.getTotal())
                .eq("deleted", DeletedStatus.NOT_DELETED)
                .eq("user_id",userId)            //正是需要填进userId
                .orderByDesc("create_time");
        Page selectPage = page(new Page(form.getPageIdx(), form.getPageSize()), wrapper);
        if(selectPage == null || selectPage.getRecords().size() < 0) throw new IntegrationException(IntegrationExceptionEnum.SEARCH_FAIL);
        List<IntegrationOrder> orderList = selectPage.getRecords();
        //判断查询结果
        UserSearchResultDto resultDto = null;
        if(orderList == null || orderList.size() < 0) throw new IntegrationException(IntegrationExceptionEnum.SEARCH_FAIL);
        else {
            log.info("page:{},size:{},total:{}",selectPage.getCurrent(),selectPage.getSize(),selectPage.getTotal());
            resultDto = BeanCopyUtil.copyObject(selectPage,UserSearchResultDto::new);
            log.info("{}",resultDto);
            List<UserSearchByConditionDto> dtoList = BeanCopyUtil.copyList(orderList, UserSearchByConditionDto::new);
            resultDto.setDtoList(dtoList);
        }
        return resultDto;
    }

    /**
     * 用户删除一条历史订单
     */
    @Override
    public Boolean deleteOne(Integer orderId) {
        Integer userId = userUtil.getUserId(ServletUtil.getRequest());
        IntegrationOrder order = getById(orderId);
        if(null == order ||userId != order.getUserId()) throw new IntegrationException(IntegrationExceptionEnum.ORDER_NOT_EXISTS);
        //如果订单已被删除
        if (order.getDeleted() == DeletedStatus.IS_DELETED) throw new IntegrationException(IntegrationExceptionEnum.ALREADY_DELETED);
        if (order.getStatus() != OrderStatus.ALREADY_COMMENT && order.getStatus() != OrderStatus.ALREADY_RETURN && order.getStatus() != OrderStatus.ALREADY_CANCEL) throw new IntegrationException(IntegrationExceptionEnum.CAN_NOT_DELETE);
        order.setDeleted(DeletedStatus.IS_DELETED);
        boolean flag = updateById(order);

        //删除成功，则记录日志
        if (flag){
            IntegrationOrderLog orderLog = LogUtil.getLog(order, LogType.NORMAL_USER, "删除订单", 1);   //userId需要更改为token解析的

            Message<IntegrationOrderLog> message = MessageBuilder.withPayload(orderLog).build();
            rocketMQtemplate.asyncSend(destination, message, new SendCallback() {
                @Override
                public void onSuccess(SendResult sendResult) {
                }

                @Override
                public void onException(Throwable throwable) {
                    log.error("消息发送失败,日志保存失败,{}",orderLog);
                }
            });
        }
        return flag;
    }

    /**
     * 用户申请取消
     */
    @Override
    public Boolean cancel(CancelForm form) {
        Integer userId = userUtil.getUserId(ServletUtil.getRequest());
        IntegrationOrder order = getById(form.getOrderId());

        log.debug("订单详情,{}",order);
        //判断订单能否取消
        if (null == order || userId != order.getUserId()) throw new IntegrationException(IntegrationExceptionEnum.ORDER_NOT_EXISTS);
        if (order.getStatus() != OrderStatus.NOT_RECEIVE) throw new IntegrationException(IntegrationExceptionEnum.CAN_NOT_CANCEL);

        order.setCancelTime(System.currentTimeMillis());
        order.setCancelReason(form.getCancelReason());
        order.setStatus(OrderStatus.ON_CHECK);
        boolean flag = updateById(order);

        //取消设置成功，则记录日志
        if (flag){
            IntegrationOrderLog orderLog = LogUtil.getLog(order, LogType.NORMAL_USER, "申请取消订单", 1);

            Message<IntegrationOrderLog> message = MessageBuilder.withPayload(orderLog).build();
            rocketMQtemplate.asyncSend(destination, message, new SendCallback() {
                @Override
                public void onSuccess(SendResult sendResult) {
                }

                @Override
                public void onException(Throwable throwable) {
                    log.error("消息发送失败,日志保存失败,{}",orderLog);
                }
            });
        }
        return flag;
    }

    /**
     * 用户申请退货
     */
    @Override
    public Boolean returning(ReturningForm form) {
        IntegrationOrder order = getById(form.getOrderId());
        Integer userId = userUtil.getUserId(ServletUtil.getRequest());
        log.debug("订单详情,{}",order);
        //判断订单能否退货
        if (null == order || userId != order.getUserId()) throw new IntegrationException(IntegrationExceptionEnum.ORDER_NOT_EXISTS);
        if (order.getStatus() != OrderStatus.ALREADY_RECEIVE) throw new IntegrationException(IntegrationExceptionEnum.CAN_NOT_RETURN);

        order.setReturnTime(System.currentTimeMillis());
        order.setCancelReason(form.getReason());
        order.setStatus(OrderStatus.ON_CHECK);
        boolean flag = updateById(order);

        //取消设置成功，则记录日志
        if (flag){
            IntegrationOrderLog orderLog = LogUtil.getLog(order, LogType.NORMAL_USER, "申请退货", order.getUserId());

            Message<IntegrationOrderLog> message = MessageBuilder.withPayload(orderLog).build();
            rocketMQtemplate.asyncSend(destination, message, new SendCallback() {
                @Override
                public void onSuccess(SendResult sendResult) {
                }

                @Override
                public void onException(Throwable throwable) {
                    log.error("消息发送失败,日志保存失败,{}",orderLog);
                }
            });
        }
        return flag;
    }

    /**
     * 管理员确认退货
     */
    @GlobalTransactional
    @Override
    public ConfirmReturningDto confirmReturning(ConfirmReturningForm form) {
        IntegrationOrder order = getById(form.getOrderId());
        log.debug("订单详情,{}",order);
        //判断订单能否退货
        if (null == order) throw new IntegrationException(IntegrationExceptionEnum.ORDER_NOT_EXISTS);
        if (order.getStatus() != OrderStatus.RETURNING) throw new IntegrationException(IntegrationExceptionEnum.CAN_NOT_RETURN);

        //解析token
        Integer userId = userUtil.getUserId(ServletUtil.getRequest());

        order.setSalesReturnTime(System.currentTimeMillis());
        if(form.getProductStatus() == ProductStatus.INTACT) order.setReturnIntegration(order.getTotal());
        else order.setReturnIntegration((int)(order.getTotal() * 0.8));
        order.setStatus(OrderStatus.ALREADY_RETURN);
        order.setProductStatus(form.getProductStatus());
        order.setReturnManagerId(userId);
        boolean flag = updateById(order);

        //取消设置成功，则记录日志
        ConfirmReturningDto returningDto = null;
        if (flag){
            IntegrationOrderLog orderLog = LogUtil.getLog(order, LogType.MANAGER, "退货完成", userId);

            //调用商品接口修改商品库存
            ResponseEntity<IntegrationProduct> productResponseEntity = productFeignClient.changeNum(new ChangeProductNum(order.getProductId(), order.getCount()));
            if(null == productResponseEntity.getData()) throw new IntegrationException(IntegrationExceptionEnum.CONFIRM_RETURNING_FAIL);

            //调用用户接口修改用户积分
            ResponseEntity<ChangeIntegrationSel> integrationResponseEntity = userFeignClient.changeAdd(new ChangeIntegationSelForm(order.getUserId(), order.getReturnIntegration()));
            if (null == integrationResponseEntity.getData()) throw new IntegrationException(IntegrationExceptionEnum.CREATE_FAIL);

            Message<IntegrationOrderLog> message = MessageBuilder.withPayload(orderLog).build();
            rocketMQtemplate.asyncSend(destination, message, new SendCallback() {
                @Override
                public void onSuccess(SendResult sendResult) {
                }

                @Override
                public void onException(Throwable throwable) {
                    log.error("消息发送失败,日志保存失败,{}",orderLog);
                }
            });

            returningDto = BeanCopyUtil.copyObject(order, ConfirmReturningDto::new);
        }
        return returningDto;
    }

    @Override
    public List<IntegrationOrder> getList() {
        List<IntegrationOrder> orders = orderMapper.selectList(null);
        return orders;
    }

    /**
     * 管理员查审核
     */
    @GlobalTransactional
    @Override
    public CheckDto check(CheckForm form) {
        //查询订单是否存在
        IntegrationOrder order = getById(form.getOrderId());
        if(null == order) throw new IntegrationException(IntegrationExceptionEnum.ORDER_NOT_EXISTS);
        if(order.getStatus() != OrderStatus.ON_CHECK) throw new IntegrationException(IntegrationExceptionEnum.CANCEL_NOT_COMMIT);

        Integer userId = userUtil.getUserId(ServletUtil.getRequest());
        Boolean orderFlag = null;

        order.setCheckTime(System.currentTimeMillis());
        order.setCheckManagerId(userId);
        //如果审核通过
        if (form.getIsPass()){
            order.setCheckStatus(CheckStatus.PASS);
            if(null != order.getReturnTime()){
                order.setStatus(OrderStatus.RETURNING);
            }else{
                order.setStatus(OrderStatus.ALREADY_CANCEL);
            }
            orderFlag = updateById(order);
        }else {
            order.setCheckStatus(CheckStatus.NOT_PASS);
            if(null != order.getReturnTime()){
                order.setStatus(OrderStatus.ALREADY_RECEIVE);
            }else{
                order.setStatus(OrderStatus.NOT_RECEIVE);
            }
            orderFlag = updateById(order);
        }

        //如果订单修改成功，保存日志
        if (orderFlag){
            IntegrationOrderLog orderLog = LogUtil.getLog(order, LogType.MANAGER, "管理员审核", order.getCheckManagerId());

            Message<IntegrationOrderLog> message = MessageBuilder.withPayload(orderLog).build();
            rocketMQtemplate.asyncSend(destination, message, new SendCallback() {
                @Override
                public void onSuccess(SendResult sendResult) {
                }

                @Override
                public void onException(Throwable throwable) {
                    log.error("消息发送失败,日志保存失败,{}",orderLog);
                }
            });

            if (null != order.getCancelTime()){
                //调用商品接口修改商品库存
                ResponseEntity<IntegrationProduct> productResponseEntity = productFeignClient.changeNum(new ChangeProductNum(order.getProductId(), order.getCount()));
                if(null == productResponseEntity.getData()) throw new IntegrationException(IntegrationExceptionEnum.CONFIRM_RETURNING_FAIL);

                //调用用户接口修改用户积分
                ResponseEntity<ChangeIntegrationSel> integrationResponseEntity = userFeignClient.changeAdd(new ChangeIntegationSelForm(order.getUserId(), order.getTotal()));
                if (null == integrationResponseEntity.getData()) throw new IntegrationException(IntegrationExceptionEnum.CREATE_FAIL);

            }
        }

        CheckDto checkDto = BeanCopyUtil.copyObject(order, CheckDto::new);
        return checkDto;
    }

    /**
     * 管理员查询
     */
    @Override
    public ManagerSearchResultDto managerSearchByCondition(ManagerSearchByConditionForm form) {
        //生成条件构造器
        QueryWrapper<IntegrationOrder> wrapper = new QueryWrapper<>();
        wrapper.eq(form.getStatus()!=null,"status",form.getStatus())
                .ge(form.getCreateTimeStart()!=null,"create_time",form.getCreateTimeStart())
                .le(form.getCreateTimeStart()!=null && form.getCreateTimeEnd() != null,
                        "create_time",form.getCreateTimeEnd())
                .ge(form.getTotal() != null,"total",form.getTotal())
                .orderByDesc("create_time");

        Page page = new Page(form.getPageIdx(), form.getPageSize());
        Page orderPage = page(page, wrapper);
        ManagerSearchResultDto resultDto = null;
        if(orderPage.getRecords() == null || orderPage.getRecords().size() < 0) throw new IntegrationException(IntegrationExceptionEnum.SEARCH_FAIL);
        else {
            resultDto = BeanCopyUtil.copyObject(orderPage, ManagerSearchResultDto::new);
            List<ManagerSearchByConditionDto> dtoList = BeanCopyUtil.copyList(orderPage.getRecords(), ManagerSearchByConditionDto::new);
            resultDto.setDtoList(dtoList);
        }
        return resultDto;
    }

    /**
     * 用户确认订单
     */
    @GlobalTransactional
    @Override
    public ConfirmDto confirm(ConfirmForm form) {
        IntegrationOrder order = getById(form.getOrderId());
        //解析token获取useId
        Integer userId = userUtil.getUserId(ServletUtil.getRequest());
        if (null == order || userId != order.getUserId()) throw new IntegrationException(IntegrationExceptionEnum.ORDER_NOT_EXISTS);
        if(order.getStatus() != OrderStatus.NOT_RECEIVE) throw new IntegrationException(IntegrationExceptionEnum.CAN_NOT_CONFIRM);

        order.setStatus(OrderStatus.ALREADY_RECEIVE);
        order.setConfirmTime(System.currentTimeMillis());


        boolean orderFlag = updateById(order);
        //如果修改改成功，则记录日志
        if (!orderFlag) throw new IntegrationException(IntegrationExceptionEnum.ORDER_UPDATE_FAIL);
        //订单新增成功，生成日志
        IntegrationOrderLog orderLog = LogUtil.getLog(order, LogType.NORMAL_USER, "用户确认订单", userId);

        //发送异步消息，保存日志
        Message<IntegrationOrderLog> message = MessageBuilder.withPayload(orderLog).build();
        rocketMQtemplate.asyncSend(destination, message, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
            }

            @Override
            public void onException(Throwable throwable) {
                log.error("消息发送失败,日志保存失败,{}",orderLog);
            }
        });

        ConfirmDto updateStatusDto = BeanCopyUtil.copyObject(order, ConfirmDto::new);
        return updateStatusDto;
    }

    /**
     * 用户删除多个
     */
    @GlobalTransactional
    @Override
    public Map<Integer,String> delete(List<Integer> orderIds) {
        List<IntegrationOrder> orders = orderMapper.selectBatchIds(orderIds);

        //解析token
        Integer userId = userUtil.getUserId(ServletUtil.getRequest());
        if(orders.size() <= 0) throw new IntegrationException(IntegrationExceptionEnum.ORDER_NOT_EXISTS);

        HashMap<Integer, String> map = new HashMap<>();
        for (int i = 0; i < orders.size(); i++) {
            IntegrationOrder order = orders.get(i);
            if(order.getUserId() != userId){
                map.put(order.getId(),IntegrationExceptionEnum.ORDER_NOT_EXISTS.getMsg());
                continue;
            }
            if (order.getDeleted() == DeletedStatus.IS_DELETED) {
                map.put(order.getId(),IntegrationExceptionEnum.ALREADY_DELETED.getMsg());
                continue;
            }
            if (order.getStatus() != OrderStatus.ALREADY_COMMENT && order.getStatus() != OrderStatus.ALREADY_RETURN && order.getStatus() != OrderStatus.ALREADY_CANCEL) {
                map.put(order.getId(),IntegrationExceptionEnum.CAN_NOT_DELETE.getMsg());
                continue;
            }
            order.setDeleted(DeletedStatus.IS_DELETED);
            boolean flag = updateById(order);

            //删除成功，则记录日志
            if (flag){
                map.put(order.getId(),"删除成功");
                IntegrationOrderLog orderLog = LogUtil.getLog(order, LogType.NORMAL_USER, "删除订单", userId);   //userId需要更改为token解析的

                Message<IntegrationOrderLog> message = MessageBuilder.withPayload(orderLog).build();
                rocketMQtemplate.asyncSend(destination, message, new SendCallback() {
                    @Override
                    public void onSuccess(SendResult sendResult) {
                    }

                    @Override
                    public void onException(Throwable throwable) {
                        log.error("消息发送失败,日志保存失败,{}",orderLog);
                    }
                });
            }
        }
        return map;
    }

    /**
     * 用户评论
     */
    @Override
    @GlobalTransactional
    public CommentDto comment(CommentForm form) {
        //解析token
        Integer userId = userUtil.getUserId(ServletUtil.getRequest());
        IntegrationOrder order = getById(form.getOrderId());
        if (null == order || order.getUserId() != userId) throw new IntegrationException(IntegrationExceptionEnum.ORDER_NOT_EXISTS);
        if (order.getStatus() != OrderStatus.ALREADY_RECEIVE) throw new IntegrationException(IntegrationExceptionEnum.CAN_NOT_COMMENT);

        order.setStatus(OrderStatus.ALREADY_COMMENT);
        order.setComment(form.getComment());
        order.setLevel(form.getLevel());
        order.setFinishTime(System.currentTimeMillis());

        boolean orderFlag = updateById(order);
        //如果修改成功，则记录日志
        if (!orderFlag) throw new IntegrationException(IntegrationExceptionEnum.ORDER_UPDATE_FAIL);
        //订单新增成功，保存日志
        IntegrationOrderLog orderLog = LogUtil.getLog(order, form.getLogType(), "评论并结束订单", form.getLogType() == LogType.NORMAL_USER ? order.getUserId() : null);

        Message<IntegrationOrderLog> message = MessageBuilder.withPayload(orderLog).build();
        rocketMQtemplate.asyncSend(destination, message, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
            }

            @Override
            public void onException(Throwable throwable) {
                log.error("消息发送失败,日志保存失败,{}",orderLog);
            }
        });

        CommentDto commentDto = BeanCopyUtil.copyObject(order, CommentDto::new);
        return commentDto;
    }



//    @Override
//    public List<SearchMDto> searchByStatusM(SearchByStatusForm form) {
//        QueryWrapper<IntegrationOrder> wrapper = new QueryWrapper<>();
//        wrapper.eq("status",form.getStatus());
//
//        Page selectPage = page(new Page(form.getPageIdx(), form.getPageSize()), wrapper);
//        if(selectPage == null || selectPage.getRecords().size() < 0) throw new IntegrationException(IntegrationExceptionEnum.SEARCH_FAIL);
//        List<IntegrationOrder> orderList = selectPage.getRecords();
//
//        //判断查询结果
//        List<SearchMDto> dtoList = null;
//        if(orderList == null || orderList.size() < 0) throw new IntegrationException(IntegrationExceptionEnum.SEARCH_FAIL);
//        else {
//            dtoList = BeanCopyUtil.copyList(orderList, SearchMDto::new);
//        }
//        return dtoList;
//    }
//    @Override
//    public List<SearchMDto> searchByCreateTimeM(UserSearchByConditionForm form) {
//        QueryWrapper<IntegrationOrder> wrapper = new QueryWrapper<>();
//        wrapper.ge("create_time",form.getCreateTimeStart())
//                .le("create_time",form.getCreateTimeEnd());
//
//        Page IPage = page(new Page(form.getPageIdx(), form.getPageSize()), wrapper);
//        if(IPage == null || IPage.getRecords().size() < 0) throw new IntegrationException(IntegrationExceptionEnum.SEARCH_FAIL);
//        List<IntegrationOrder> orderList = IPage.getRecords();
//
//        //判断查询结果
//        List<SearchMDto> dtoList = null;
//        if(orderList == null || orderList.size() < 0) throw new IntegrationException(IntegrationExceptionEnum.SEARCH_FAIL);
//        else {
//            dtoList = BeanCopyUtil.copyList(orderList, SearchMDto::new);
//        }
//        return dtoList;
//    }

}
