package com.woniu.recycle.integration.dto;

import com.woniu.recycle.integration.component.LogReason;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClassName OrderLogDto
 * @Description 日志查询返回Dto对象
 * @Author DingYuyi
 * @Date 2021/8/19 18:00
 * @Version 1.0
 */
@Data
@ApiModel(value="日志查询返回Dto对象")
public class OrderLogDto {

    @ApiModelProperty(value = "积分商品订单日记id")
    private Integer id;

    @ApiModelProperty(value = "订单id")
    private Integer orderId;

    @ApiModelProperty(value = "订单编号")
    private String orderNum;

    @ApiModelProperty(value = "日记创建时间")
    private Long logCreateTime;

    @ApiModelProperty(value = "订单状态")
    private Integer orderStatus;

    @ApiModelProperty(value = "记录日记生成原因")
    private LogReason logReason;

    @ApiModelProperty(value = "操作者类型,0普通用户,1管理员,2系统自动")
    private Integer logType;

    @ApiModelProperty(value = "操作者ID")
    private Integer userId;

}
