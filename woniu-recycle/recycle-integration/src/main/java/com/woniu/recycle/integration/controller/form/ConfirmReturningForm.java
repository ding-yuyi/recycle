package com.woniu.recycle.integration.controller.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @ClassName ConfirmReturningForm
 * @Description 管理员确认退货接口入参对象
 * @Author DingYuyi
 * @Date 2021/8/16 19:36
 * @Version 1.0
 */
@Data
@ApiModel(value="管理员确认退货接口入参对象")
public class ConfirmReturningForm {
    @ApiModelProperty(value = "退货产品状态 0 完好 | 1 有损坏")
    @NotNull(message = "产品状态不能为空")
    @Min(0)
    @Max(1)
    private Integer productStatus;

    @ApiModelProperty(value = "订单id")
    @NotNull(message = "订单id不能为空")
    private Integer orderId;
}
