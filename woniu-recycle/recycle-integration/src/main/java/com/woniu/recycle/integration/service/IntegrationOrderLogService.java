package com.woniu.recycle.integration.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.recycle.integration.controller.form.SearchByOrderIdForm;
import com.woniu.recycle.integration.controller.form.SearchByUserIdForm;
import com.woniu.recycle.integration.controller.form.SearchLogTimeForm;
import com.woniu.recycle.integration.controller.form.SearchLogTypeForm;
import com.woniu.recycle.integration.domain.IntegrationOrderLog;
import com.woniu.recycle.integration.dto.SearchResultDto;

/**
 * <p>
 * 积分商品订单日志表 服务类
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-11
 */
public interface IntegrationOrderLogService extends IService<IntegrationOrderLog> {

    SearchResultDto searchByLogType(SearchLogTypeForm form);

    SearchResultDto searchByCreateTime(SearchLogTimeForm form);

    SearchResultDto searchByUserId(SearchByUserIdForm form);

    SearchResultDto searchByOrderId(SearchByOrderIdForm form);
}
