package com.woniu.recycle.commons.core.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@Data
@ConfigurationProperties(prefix = "jwt")
public class JwtConfig {

	private String secret = "asdfewrfvoibuosisfnoi2845fkj12y3akjnfasjdha2384ikafd8y24";
	
	private Integer expire = 24 * 60;
}
