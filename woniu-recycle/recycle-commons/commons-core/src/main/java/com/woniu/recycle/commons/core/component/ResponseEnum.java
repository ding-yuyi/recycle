package com.woniu.recycle.commons.core.component;

/**
   *   常规响应信息枚举
   *   枚举一种对象,就是用来封装常量,一个枚举对象可以包括多个常量
   *   
 * @author liwei
 *
 */
public enum ResponseEnum {
	
	RES_SUCCESS(200,"访问成功"),
	RES_ERROR(500,"访问错误"),
	
	BLOCK_DEGRADE(8001,"触发熔断"),
	BLOCK_FLOW(8002,"触发流控");
	
	Integer code;
	String msg;
	
	public Integer getCode() {
		return code;
	}

	public String getMsg() {
		return msg;
	}

	ResponseEnum(Integer code,String msg) {
		this.code = code;
		this.msg = msg;
	}

}
