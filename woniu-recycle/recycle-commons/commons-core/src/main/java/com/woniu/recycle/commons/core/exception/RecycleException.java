package com.woniu.recycle.commons.core.exception;

public class RecycleException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	private Integer code;
	
	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public RecycleException() {}
	
	public RecycleException(String msg, Integer code) {
		super(msg);
		this.code = code;
	}
	
	public RecycleException(String msg) {
		super(msg);
	}

}
