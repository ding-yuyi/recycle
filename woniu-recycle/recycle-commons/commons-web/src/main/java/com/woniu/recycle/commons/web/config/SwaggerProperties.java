package com.woniu.recycle.commons.web.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Component
@ConfigurationProperties(prefix = "swagger")
public class SwaggerProperties {
	
	private String title;
	
	private String version;
	
	private String description;
	
	private String basePackage;
	
	private Contact contact;

}
