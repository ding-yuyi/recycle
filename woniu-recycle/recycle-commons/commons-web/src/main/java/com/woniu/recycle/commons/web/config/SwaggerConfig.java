package com.woniu.recycle.commons.web.config;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Autowired
	private SwaggerProperties sp;

	@Bean
	public Docket createDocket() {

		List<Parameter> ps = Arrays.asList(new ParameterBuilder().required(false).modelRef(new ModelRef("string"))
				.parameterType("header").name("auth_token").description("Token").build());

		Docket docket = new Docket(DocumentationType.SWAGGER_2);
		ApiInfo apiInfo = new ApiInfoBuilder()
				.contact(new Contact(sp.getContact().getName(), sp.getContact().getHttpAddr(),
						sp.getContact().getEmail()))
				.title(sp.getTitle()).description(sp.getDescription()).version(sp.getVersion()).build();
		docket.apiInfo(apiInfo).select().apis(RequestHandlerSelectors.basePackage(sp.getBasePackage()))
				.paths(PathSelectors.any()).build().globalOperationParameters(ps);
		return docket;
	}

}
