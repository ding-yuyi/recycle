package com.woniu.recycle.commons.web.exception;


import com.woniu.recycle.commons.core.exception.RecycleException;

public class RecycleJsonException extends RecycleException {

	private static final long serialVersionUID = 1L;
	
	public RecycleJsonException(String msg, Integer code) {
		super(msg,code);
	}
	
	public RecycleJsonException(String msg) {
		super(msg);
	}

}
