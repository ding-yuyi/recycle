package com.woniu.recycle.commons.web.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @PackageName:
 * @ClassName: DateUtil
 * @Description: 日期转化工具
 * @author: 丁予一
 * @date: 2021/8/7 9:17
 */
public class DateUtil {

    /**
     *  字符串转化为Long
     * @param date
     * @param pattern
     * @return
     */
    public static Long parse(String date,String pattern){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return LocalDateTime.parse(date,formatter).toInstant(ZoneOffset.of("+8")).toEpochMilli();
    }

    /**
     * 转化为字符串，不包含时分秒
     * @param time
     * @param pattern
     * @return
     */
    public static String parse(Long time,String pattern){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        Date date = new Date(time);
        LocalDate localDate = date.toInstant().atOffset(ZoneOffset.of("+8")).toLocalDate();
        return localDate.format(formatter);
    }

    /**
     * 转化为字符串，包含时分秒
     * @param time
     * @param pattern
     * @return
     */
    public static String parseToTime(Long time,String pattern){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        Date date = new Date(time);
        LocalDateTime localDateTime = date.toInstant().atOffset(ZoneOffset.of("+8")).toLocalDateTime();
        return localDateTime.format(formatter);
    }
}
