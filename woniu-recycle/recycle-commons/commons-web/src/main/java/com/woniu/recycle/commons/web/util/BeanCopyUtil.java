package com.woniu.recycle.commons.web.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;

import org.springframework.beans.BeanUtils;

/**
   *   对对象以及集合对象属性的复制工具类
 * @author liwei
 *
 */
public class BeanCopyUtil extends BeanUtils{

	/**
	   *   复制List集合数据
	 * @param <S>
	 * @param <T>
	 * @param source
	 * @param target
	 * @return
	 */
	public static <S,T> List<T> copyList(List<S> source,Supplier<T> target) {
		List<T> list = new ArrayList<>(source.size());
        for (S s : source) {
            T t = target.get();
            copyProperties(s, t);
            list.add(t);
        }
        return list;
	}

	/**
	 *   复制Set集合数据
	 * @param <S>
	 * @param <T>
	 * @param source
	 * @param target
	 * @return
	 */
	public static <S,T> Set<T> copySet(Set<S> source, Supplier<T> target) {
		Set<T> set = new HashSet<>(source.size());
		for (S s : source) {
			T t = target.get();
			copyProperties(s, t);
			set.add(t);
		}
		return set;
	}

	/**
	   *   复制对象数据
	 * @param <S>
	 * @param <T>
	 * @param source
	 * @param target
	 * @return
	 */
	public static <S,T> T copyObject(S source,Supplier<T> target) {
		T t = target.get();
		copyProperties(source, t);
		return t;
	}
}




