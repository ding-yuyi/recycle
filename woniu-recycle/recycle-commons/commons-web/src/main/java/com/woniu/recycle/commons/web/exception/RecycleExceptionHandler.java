package com.woniu.recycle.commons.web.exception;

import com.woniu.recycle.commons.core.exception.RecycleException;
import com.woniu.recycle.commons.core.model.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import lombok.extern.slf4j.Slf4j;

@RestControllerAdvice
@Slf4j
public class RecycleExceptionHandler {

	@ExceptionHandler(Exception.class)
	public ResponseEntity<String> handlerException(Exception e) {
		log.error("出现未知异常: {}",e.getMessage());
		return ResponseEntity.BuildError(String.class).setMsg("出现错误");
	}

	@ExceptionHandler(RecycleException.class)
	public ResponseEntity<String> handlerException(RecycleException e) {
		log.error("出现异常: {},{},{}",e.getCode(),e.getMessage(),e.getClass().getName());
//		log.error("出现异常: {}",e);
		return ResponseEntity.BuildError(String.class).setCode(e.getCode()).setMsg(e.getMessage());
	}

}
