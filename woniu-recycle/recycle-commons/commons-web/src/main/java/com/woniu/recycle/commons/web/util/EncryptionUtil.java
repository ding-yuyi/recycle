package com.woniu.recycle.commons.web.util;

import org.springframework.util.DigestUtils;

/**
 * @PackageName:
 * @ClassName: EncryptionUtil
 * @Description:
 * @author: 丁予一
 * @date: 2021/8/6 11:44
 */
public class EncryptionUtil {

    /**
     * 加密方法
     * @param source
     * @return
     */
    public static String encryption(String source){
        String encodeStr = DigestUtils.md5DigestAsHex(source.getBytes());
        return encodeStr;
    }
}
