package com.woniu.recycle.commons.web.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Contact {
	
	private String name;
	
	private String httpAddr;
	
	private String email;

}
