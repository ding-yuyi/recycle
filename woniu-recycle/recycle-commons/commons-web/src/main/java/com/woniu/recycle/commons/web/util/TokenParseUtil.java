package com.woniu.recycle.commons.web.util;

import com.woniu.recycle.commons.core.util.JwtTemplate;
import io.jsonwebtoken.Claims;

import javax.annotation.Resource;

/**
 * @PackageName:
 * @ClassName: TokenParseUtil
 * @Description:
 * @author: 丁予一
 * @date: 2021/8/7 11:58
 */
public class TokenParseUtil {

    @Resource
    private static JwtTemplate jwtTemplate;

    public static String accountParse(){
        String token = ServletUtil.getRequest().getHeader("auth_token");
        Claims claims = jwtTemplate.parseJwt(token);
        String accountId = claims.get("accountId").toString();
        return accountId;
    }
}
