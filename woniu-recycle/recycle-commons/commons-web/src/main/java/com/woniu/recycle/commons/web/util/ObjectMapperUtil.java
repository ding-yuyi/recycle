package com.woniu.recycle.commons.web.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.woniu.recycle.commons.core.exception.RecycleException;
import com.woniu.recycle.commons.web.exception.RecycleJsonException;

public class ObjectMapperUtil {
	
	private static ObjectMapper om = new ObjectMapper();

	public static String parseObject(Object obj) {
		try {
			return om.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			throw new RecycleException(e.getMessage(),1000);
		}
	}
	
	public static <T> T parseObject(String json,Class<T> type) {
		try {
			return om.readValue(json, type);
		} catch (JsonProcessingException e) {
			throw new RecycleJsonException(e.getMessage(),1000);
		}
	}
}
