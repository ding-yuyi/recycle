package com.woniu.recycle.commons.web.component;

import com.woniu.recycle.commons.core.model.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.List;

public abstract class BaseController {

    //参数校验处理方法
    public ResponseEntity extractError(BindingResult result) {
        if (result.hasErrors()) {
            List<FieldError> fieldErrors = result.getFieldErrors();
            StringBuilder msgBuilder = new StringBuilder();
            for (FieldError fieldError : fieldErrors) {
                msgBuilder.append(fieldError.getField() + ":" + fieldError.getDefaultMessage() + ";");
            }
            return ResponseEntity.BuildError().setMsg("参数校验失败："+msgBuilder.toString());
        }
        return null;
    }

}
