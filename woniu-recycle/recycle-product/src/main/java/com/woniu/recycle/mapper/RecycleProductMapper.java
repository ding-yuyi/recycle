package com.woniu.recycle.mapper;

import com.woniu.recycle.domain.RecycleProduct;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 回收商品信息表 Mapper 接口
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-12
 */
public interface RecycleProductMapper extends BaseMapper<RecycleProduct> {

}
