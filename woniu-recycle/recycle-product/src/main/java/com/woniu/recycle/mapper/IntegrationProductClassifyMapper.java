package com.woniu.recycle.mapper;

import com.woniu.recycle.domain.IntegrationProductClassify;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 积分商品类别表 Mapper 接口
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-12
 */
public interface IntegrationProductClassifyMapper extends BaseMapper<IntegrationProductClassify> {

}
