package com.woniu.recycle.domain;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * <p>
 * 家政商品信息表
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "HousekeepingProduct对象", description = "家政商品信息表")
@Document(indexName = "HousekeepingProduct")
public class HousekeepingProduct implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    @Field(analyzer = "ik_smart", type = FieldType.Text)
    @ApiModelProperty(value = "商品名称")
    private String name;
    @Field
    @ApiModelProperty(value = "价格")
    private BigDecimal price;
    @Field
    @ApiModelProperty(value = "商品描述")
    private String description;
    @Field
    @ApiModelProperty(value = "商品图片地址")
    private String pictureUrl;
    @Field
    @ApiModelProperty(value = "注意事项")
    private String attention;
    @Field
    @ApiModelProperty(value = "上下架状态 0 下架 | 1 上架", example = "1")
    private Integer status;

}
