package com.woniu.recycle.controller.form;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 *
 */
@Data
@ApiModel("查询家政商品form")
public class AllHouseKeeping {
    @ApiModelProperty(value = "商品名称", example = "1")
    private Integer id;

    @ApiModelProperty(value = "商品名称")
    private String name;

    @ApiModelProperty(value = "价格", example = "1.0")
    private BigDecimal price;

    @ApiModelProperty(value = "商品描述")
    private String description;

    @ApiModelProperty(value = "商品图片地址")
    private String pictureUrl;

    @ApiModelProperty(value = "注意事项")
    private String attention;

    @ApiModelProperty(value = "上下架状态 0 下架 | 1 上架", example = "1")
    private Integer status;

    @NotNull
    @ApiModelProperty(value = "页数", example = "1")
    private Integer page;

    @NotNull
    @ApiModelProperty(value = "条数", example = "1")
    private Integer num;

}
