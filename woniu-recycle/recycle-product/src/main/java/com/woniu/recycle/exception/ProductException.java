package com.woniu.recycle.exception;

import com.woniu.recycle.commons.core.exception.RecycleException;
import com.woniu.recycle.util.IntegrationProductClassifyEnum;

public class ProductException extends RecycleException {
    //私有静态常量long类型序列化id
    private static  final  long serialVersionUID = 1L;

    //全参构造函数
    public ProductException(String msg,Integer code){
        super(msg,code);
    }

    public ProductException(IntegrationProductClassifyEnum code) {
        super(code.getMessage(),code.getCode());
    }
}
