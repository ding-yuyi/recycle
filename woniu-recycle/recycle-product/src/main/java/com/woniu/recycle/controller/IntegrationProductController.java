package com.woniu.recycle.controller;

import com.woniu.recycle.commons.core.model.ResponseEntity;
import com.woniu.recycle.commons.web.component.BaseController;
import com.woniu.recycle.controller.dto.HousekeepingProductLogDto;
import com.woniu.recycle.controller.dto.IntegrationProductDto;
import com.woniu.recycle.controller.dto.IntegrationProductLogDto;
import com.woniu.recycle.controller.form.*;
import com.woniu.recycle.domain.IntegrationProduct;
import com.woniu.recycle.domain.IntegrationProductLog;
import com.woniu.recycle.service.IntegrationProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 积分商品表 前端控制器
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-12
 */
@Api(tags = "积分商品控制器")
@RestController
@RequestMapping("/integrationProduct")
@Slf4j
public class IntegrationProductController extends BaseController {

    @Resource
    IntegrationProductService ips;

    //新增积分商品接口（新增商品种类）
    @ApiOperation("新增积分商品接口（新增商品种类）")
    @PostMapping("/auth/newgoods")
    public ResponseEntity<List<IntegrationProduct>> newGoods(@Valid @RequestBody NewGoods[] ng, BindingResult bindingResult) {
        //打印日记
        log.info("新增的对象是：{}", ng);
        //参数校验
        ResponseEntity responseEntity = extractError(bindingResult);
        if (responseEntity != null) {
            return responseEntity;
        }
        List<IntegrationProduct> result = ips.newGoods(ng);
        if (result == null||result.size()==0)
            return ResponseEntity.BuildErrorList(IntegrationProduct.class).setData(result).setMsg("新增失败");
        return ResponseEntity.BuildSuccessList(IntegrationProduct.class).setData(result).setMsg("新增成功");
    }

    //修改积分商品状态(上下架商品)
    @ApiOperation("修改积分商品状态(上下架商品)")
    @PutMapping("/auth/changestatus")
    public ResponseEntity<ChangeStatus> changeStatus(@Valid ChangeStatus changeStatus, BindingResult bindingResult) {
        //打印日记
        log.info("传入参数是：{}", changeStatus);
        //参数校验
        ResponseEntity responseEntity = extractError(bindingResult);
        if (responseEntity != null) {
            return responseEntity;
        }
        //主要业务逻辑
        ChangeStatus CS = ips.changeStatus(changeStatus);
        if (CS == null) return ResponseEntity.BuildError(ChangeStatus.class).setMsg("上架失败");
        return ResponseEntity.BuildError(ChangeStatus.class).setMsg("上架成功");
    }


    //修改积分商品信息（只能改信息不能改状态）,根据主键id进行更改
    @ApiOperation("修改积分商品信息（只能改信息不能改状态）,根据主键id进行更改")
        @PutMapping("/auth/changegoods")
    public ResponseEntity<ChangeProduct> change(@Valid ChangeProduct changeProduct, BindingResult bindingResult) {
        //打印日记
        log.info("传入的参数是：{}", changeProduct);
        //参数校验
        ResponseEntity responseEntity = extractError(bindingResult);
        if (responseEntity != null) {
            return responseEntity;
        }
        //主业务逻辑
        ChangeProduct CP = ips.changeProduct(changeProduct);
        if (CP == null) return ResponseEntity.BuildError(ChangeProduct.class).setMsg("修改失败");
        return ResponseEntity.BuildSuccess(ChangeProduct.class).setData(CP).setMsg("修改成功");
    }

    //删除积分商品接口
    @ApiOperation("删除积分商品接口")
    @DeleteMapping("/auth/deletegoods")
    public ResponseEntity<IntegrationProduct> delete(@Valid DeleteProduct delteProduct, BindingResult bindingResult) {
        //日记打印
        log.info("传入参数是：{}", delteProduct);
        //参数校验
        ResponseEntity responseEntity = extractError(bindingResult);
        if (responseEntity != null) {
            return responseEntity;
        }
        //主业务逻辑
        DeleteProduct DP = ips.deleteProduct(delteProduct);
        if (DP == null) return ResponseEntity.BuildError(IntegrationProduct.class).setMsg("删除失败");
        return ResponseEntity.BuildSuccess(IntegrationProduct.class).setMsg("删除成功");
    }


    //查询所有积分商品接口 //分页查询
    @ApiOperation("查询所有积分商品接口 //分页查询")
    @GetMapping("/selectall")
    public ResponseEntity<IntegrationProductDto> allProduct(@Valid AllProduct findid, BindingResult bindingResult) {
        //打印日记
        log.info("传入形参是：{}", findid);
        //参数校验
        ResponseEntity responseEntity = extractError(bindingResult);
        if (responseEntity != null) {
            return responseEntity;
        }
        //主业务逻辑
        IntegrationProductDto IP = ips.allProduct(findid);
        if (IP == null)
            return ResponseEntity.BuildError(IntegrationProductDto.class).setMsg("查询失败");
        return ResponseEntity.BuildSuccess(IntegrationProductDto.class).setData(IP).setMsg("查询成功");
    }

    //按id查询积分商品接口
    @ApiOperation("按id查询积分商品接口")
    @GetMapping("/selectbyid")
    public ResponseEntity<IntegrationProduct> selectById(Integer productId) {
        //打印日记
        log.info("传入形参是：{}", productId);
        //主业务逻辑
        IntegrationProduct result = ips.selectById(productId);
        if (result == null) ResponseEntity.BuildError(IntegrationProduct.class).setMsg("未查询到数据");
        return ResponseEntity.BuildSuccess(IntegrationProduct.class).setData(result).setMsg("查询成功");
    }

    //分页查询积分商品接口(id倒序排列)
    @ApiOperation("分页查询积分商品接口(id倒序排列)")
    @GetMapping("/descselectall")
    public ResponseEntity<IntegrationProductDto> descSelectAll(@Valid AllProduct findid, BindingResult bindingResult) {
        //打印日记
        log.info("传入形参是：{}", findid);
        //参数校验
        ResponseEntity responseEntity = extractError(bindingResult);
        if (responseEntity != null) {
            return responseEntity;
        }
        //主业务逻辑
        IntegrationProductDto IP = ips.descSelectAll(findid);
        if (IP == null) return ResponseEntity.BuildError(IntegrationProductDto.class).setMsg("查询失败");
        return ResponseEntity.BuildSuccess(IntegrationProductDto.class).setData(IP).setMsg("查询成功");
    }

    //根据价格区间查询
    @ApiOperation("根据价格区间查询")
    @GetMapping("/selectprice")
    public ResponseEntity<List<IntegrationProduct>> selectPrice(SelectPrice selectPrice) {
        //打印日记
        log.info("传入形参是：{}", selectPrice);
        //参数校验
        List<IntegrationProduct> IP = ips.selectPrice(selectPrice);
        if (IP == null||IP.size()==0) return ResponseEntity.BuildErrorList(IntegrationProduct.class).setMsg("查询失败");
        return ResponseEntity.BuildSuccessList(IntegrationProduct.class).setData(IP).setMsg("查询成功");
    }

    //根据价格区间查询倒序
    @ApiOperation("根据价格区间查询倒序")
    @GetMapping("/selectpricedesc")
    public ResponseEntity<List<IntegrationProduct>> selectPriceDesc(SelectPrice selectPrice) {
        //打印日记
        log.info("传入形参是：{}", selectPrice);
        //参数校验
        List<IntegrationProduct> IP = ips.selectPriceDesc(selectPrice);
        if (IP == null||IP.size()==0) return ResponseEntity.BuildErrorList(IntegrationProduct.class).setMsg("查询失败");
        return ResponseEntity.BuildSuccessList(IntegrationProduct.class).setData(IP).setMsg("查询成功");
    }

    //修改商品库存
    @ApiOperation("修改商品库存//新增")
    @PutMapping("/auth/changeinventoradd")
    public ResponseEntity<IntegrationProduct> changeNum(@Valid ChangeProductNum changeProductNum, BindingResult bindingResult) {
        //打印日记
        log.info("传入参数是：{}", changeProductNum);
        //参数校验
        ResponseEntity responseEntity = extractError(bindingResult);
        if (responseEntity != null) {
            return responseEntity;
        }
        //主业务逻辑
        IntegrationProduct result = ips.changeInventor(changeProductNum);
        if (result == null) ResponseEntity.BuildError(IntegrationProduct.class).setMsg("修改失败");
        return ResponseEntity.BuildSuccess(IntegrationProduct.class).setData(result).setMsg("修改成功");
    }
        //修改商品库存
        @ApiOperation("修改商品库存//减少")
        @PutMapping("/auth/changeinventorsel")
        public ResponseEntity<IntegrationProduct> changeNumsel(@Valid ChangeProductNum changeProductNum, BindingResult bindingResult) {
            //打印日记
            log.info("传入参数是：{}", changeProductNum);
            //参数校验
            ResponseEntity responseEntity = extractError(bindingResult);
            if (responseEntity != null) {
                return responseEntity;
            }
            //主业务逻辑
            IntegrationProduct result = ips.changeInventorsel(changeProductNum);
            if (result == null) ResponseEntity.BuildError(IntegrationProduct.class).setMsg("修改失败");
            return ResponseEntity.BuildSuccess(IntegrationProduct.class).setData(result).setMsg("修改成功");
        }

    //积分日记搜索接口//分页查询
    @ApiOperation("根据模糊查询积分商品日记接口")
    @GetMapping("/auth/selectIntegrationProductLog")
    public ResponseEntity<IntegrationProductLogDto> selectIntegrationProductLog(@Valid IntegrationProductLogForm integrationProductLogForm, BindingResult bindingResult) {
        //记录日记
        log.info("入参是：{}", integrationProductLogForm);
        //参数校验...
        ResponseEntity responseEntity = extractError(bindingResult);
        if(responseEntity!=null){
            return  responseEntity;
        }
        //主业务逻辑
        IntegrationProductLogDto result = ips.selectIntegrationProductLog(integrationProductLogForm);
        if (result == null)
            return ResponseEntity.BuildSuccess(IntegrationProductLogDto.class).setMsg("查询失败");
        return ResponseEntity.BuildSuccess(IntegrationProductLogDto.class).setMsg("查询成功").setData(result);
    }
    }

