package com.woniu.recycle.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 回收商品日志信息表
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("recycle_product_log")
@ApiModel(value="RecycleProductLog对象", description="回收商品日志信息表")
public class RecycleProductLog implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "商品id")
    private Integer productId;

    @ApiModelProperty(value = "商品名称")
    private String productName;

    @ApiModelProperty(value = "记录日志产生原因")
    private String logReason;

    @ApiModelProperty(value = "操作者类型 0 普通用户 | 1 管理员")
    private Integer logType;

    @ApiModelProperty(value = "操作者id")
    private Integer userId;

    @ApiModelProperty(value = "日志创建时间")
    private Long createTime;

}
