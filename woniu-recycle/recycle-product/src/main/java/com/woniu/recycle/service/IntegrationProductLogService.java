package com.woniu.recycle.service;

import com.woniu.recycle.domain.IntegrationProductLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 积分商品日志信息表 服务类
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-12
 */
public interface IntegrationProductLogService extends IService<IntegrationProductLog> {



}
