package com.woniu.recycle.util;

import com.woniu.recycle.domain.HousekeepingProductLog;
import com.woniu.recycle.domain.IntegrationProduct;
import com.woniu.recycle.domain.IntegrationProductLog;

import java.util.function.Supplier;

/**
 *
 */

public class NewLog {

    public static <T> T addlog(Integer id, String name, String s, Supplier<T> target) {
        T t = target.get();

        if (t instanceof HousekeepingProductLog) {
            //  HousekeepingProductLog hpl = (HousekeepingProductLog) t;
            ((HousekeepingProductLog) t).setProductId(id);
            ((HousekeepingProductLog) t).setProductName(name);
            ((HousekeepingProductLog) t).setLogReason(s);
            ((HousekeepingProductLog) t).setLogType(IntegrationProductClassifyEnum.ADMIN.ordinal());
            ((HousekeepingProductLog) t).setUserId(1);
            long l = System.currentTimeMillis();
            ((HousekeepingProductLog) t).setCreateTime(l);
        }
        if (t instanceof IntegrationProductLog) {
            //  HousekeepingProductLog hpl = (HousekeepingProductLog) t;
            ((IntegrationProductLog) t).setProductId(id);
            ((IntegrationProductLog) t).setProductName(name);
            ((IntegrationProductLog) t).setLogReason(s);
            ((IntegrationProductLog) t).setLogType(IntegrationProductClassifyEnum.ADMIN.ordinal());
            ((IntegrationProductLog) t).setUserId(1);
            long l = System.currentTimeMillis();
            ((IntegrationProductLog) t).setCreateTime(l);
        }
        return t;
    }


}
