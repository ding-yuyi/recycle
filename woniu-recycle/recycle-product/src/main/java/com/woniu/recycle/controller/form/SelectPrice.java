package com.woniu.recycle.controller.form;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@ApiModel("查询价格接口")
@Data
public class SelectPrice {
    @ApiModelProperty(value = "最高价格可空",example = "1")
    //最高价格
    private int maxprice;

    @ApiModelProperty(value = "最低价格可空",example = "1")
    //最低价格
    private int minprice;

    @ApiModelProperty(value = "每页展示数量",example = "1")
    //每页展示数量
    @NotNull
    private Integer num;

    @ApiModelProperty(value = "展示的页数",example = "1")
    //展示的页数
    @NotNull
    private Integer page;

}
