package com.woniu.recycle.service;

import com.woniu.recycle.controller.form.AllRecycleClassify;
import com.woniu.recycle.controller.form.ChangeRecycleClassify;
import com.woniu.recycle.controller.form.DeleteRecycleClassify;
import com.woniu.recycle.controller.form.NewRecycleClassify;
import com.woniu.recycle.domain.RecycleProductClassify;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 回收商品类别信息表 服务类
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-12
 */
public interface RecycleProductClassifyService extends IService<RecycleProductClassify> {


    List<RecycleProductClassify> addRecycle(NewRecycleClassify[] addRecycle);

    List<RecycleProductClassify> deleteRecycle(List<DeleteRecycleClassify> deleteRecycleClassify);

    RecycleProductClassify changecycle(ChangeRecycleClassify changeRecycleClassify);

    List<RecycleProductClassify> findAll(AllRecycleClassify allRecycleClassify);

    RecycleProductClassify findById(Integer id);
}
