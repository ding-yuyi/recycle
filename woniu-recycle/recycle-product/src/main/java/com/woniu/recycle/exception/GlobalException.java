package com.woniu.recycle.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;
//全局异常处理类
//@ControllerAdvice
public class GlobalException {

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Map<String,Object> globalException(Exception e){

        Map<String,Object> map = new HashMap<>();

        if (e instanceof ProductException){
            map.put("code", ((ProductException) e).getCode());
        }
        if (e instanceof ProductException){
            map.put("error", e.getMessage());
        }else{
            map.put("error",e.getMessage());

        }
        System.out.println(map);
        return map;
    }
}
