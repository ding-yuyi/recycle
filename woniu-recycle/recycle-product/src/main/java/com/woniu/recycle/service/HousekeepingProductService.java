package com.woniu.recycle.service;

import com.woniu.recycle.controller.dto.HousekeepingProductLogDto;
import com.woniu.recycle.controller.form.*;
import com.woniu.recycle.domain.HousekeepingProduct;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.recycle.domain.HousekeepingProductLog;

import java.util.List;

/**
 * <p>
 * 家政商品信息表 服务类
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-12
 */
public interface HousekeepingProductService extends IService<HousekeepingProduct> {

    List<HousekeepingProduct> addHouseKeeping(List<NewHouseKeeping> newHouseKeeping);

    List<HousekeepingProduct> deleteHousekeeping(List<DeleteHouseKeeping> deleteHouseKeeping);

    HousekeepingProduct changeHouseKeeping(ChangeHouseKeeping changeHouseKeeping);

    List<HousekeepingProduct> findHouseKeeping(AllHouseKeeping allHouseKeeping);

    HousekeepingProduct findHouseKeepingByid(Integer id);

    HousekeepingProductLogDto selectHouseKeeingLog(HouseKeepingProductLogForm houseKeepingProductLogForm);
}
