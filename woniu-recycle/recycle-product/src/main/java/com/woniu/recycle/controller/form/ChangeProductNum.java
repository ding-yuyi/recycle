package com.woniu.recycle.controller.form;

/**
 *
 */

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@ApiModel("更改商品数量")
@Data
public class ChangeProductNum {

    @NotNull
    @ApiModelProperty(value = "主键", example = "1")
    private Integer id;

    @NotNull
    @ApiModelProperty(value = "更改数量", example = "1")
    private Integer inventory;
}
