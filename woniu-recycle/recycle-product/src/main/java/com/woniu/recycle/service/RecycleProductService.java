package com.woniu.recycle.service;

import com.woniu.recycle.controller.dto.RecycleProductLogDto;
import com.woniu.recycle.controller.form.*;
import com.woniu.recycle.domain.RecycleProduct;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 回收商品信息表 服务类
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-12
 */
public interface RecycleProductService extends IService<RecycleProduct> {

    List<RecycleProduct> addProduct(List<NewRecycleProduct> newRecycleProducts);


    List<RecycleProduct> deleteProduct(List<DeleteCycleProduct> deleteCycleProducts);

    RecycleProduct changeproduct(ChangeCycleProduct changeCycleProduct);

    List<RecycleProduct> findCycleProduct(AllCycleProduct allCycleProduct);

    RecycleProductLogDto selectRecycleProductLog(RecycleProductLogForm recycleProductLogForm);
}
