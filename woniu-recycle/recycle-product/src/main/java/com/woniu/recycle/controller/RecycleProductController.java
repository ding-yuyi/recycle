package com.woniu.recycle.controller;


import com.woniu.recycle.commons.core.model.ResponseEntity;
import com.woniu.recycle.commons.web.component.BaseController;
import com.woniu.recycle.controller.dto.IntegrationProductLogDto;
import com.woniu.recycle.controller.dto.RecycleProductLogDto;
import com.woniu.recycle.controller.form.*;
import com.woniu.recycle.domain.RecycleProduct;
import com.woniu.recycle.service.RecycleProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;


import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 回收商品信息表 前端控制器
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-12
 */

@RestController
@RequestMapping("/recycleProduct")
@Slf4j
@Api(tags = "回收商品控制器")
public class RecycleProductController extends BaseController {

    @Resource
    RecycleProductService rps;

    @PostMapping("/auth/newrecycle")
    @ApiOperation("新增回收商品接口")
    public ResponseEntity<List<RecycleProduct>> addProduct(@Valid @RequestBody List<NewRecycleProduct> newRecycleProducts, BindingResult bindingResult) {
        //记录日记
        log.info("入参是：{}", newRecycleProducts);
        //参数校验
        ResponseEntity responseEntity = extractError(bindingResult);
        if (responseEntity != null) {
            return responseEntity.setMsg("参数错误");
        }
        //主业务逻辑
        List<RecycleProduct> RP = rps.addProduct(newRecycleProducts);
        if (RP == null || RP.size() == 0)
            return ResponseEntity.BuildErrorList(RecycleProduct.class).setMsg("新增失败");
        return ResponseEntity.BuildSuccessList(RecycleProduct.class).setData(RP).setMsg("新增成功");
    }

    @DeleteMapping("/auth/deletecycle")
    @ApiOperation("删除回收商品接口")
    public ResponseEntity<List<RecycleProduct>> delete(@Valid @RequestBody List<DeleteCycleProduct> deleteCycleProducts, BindingResult bindingResult) {
        //记录日记
        log.info("入参是：{}", deleteCycleProducts);
        //参数验证
        ResponseEntity responseEntity = extractError(bindingResult);
        if (responseEntity != null) {
            return responseEntity;
        }
        //主要业务逻辑
        List<RecycleProduct> result = rps.deleteProduct(deleteCycleProducts);
        if (result == null || result.size() == 0)
            return ResponseEntity.BuildErrorList(RecycleProduct.class).setMsg("删除失败");
        return ResponseEntity.BuildSuccessList(RecycleProduct.class).setData(result).setMsg("删除成功");
    }

    @PutMapping("/auth/changecycle")
    @ApiOperation("更改回收商品接口")
    public ResponseEntity<RecycleProduct> changeCycle(@Valid ChangeCycleProduct changeCycleProduct, BindingResult bindingResult) {
        //记录日记
        log.info("入参是：{}", changeCycleProduct);
        //参数验证
        ResponseEntity responseEntity = extractError(bindingResult);
        if (responseEntity != null) {
            return responseEntity;
        }
        RecycleProduct RP = rps.changeproduct(changeCycleProduct);
        if (RP == null)
            return ResponseEntity.BuildError(RecycleProduct.class).setMsg("删除失败");
        return ResponseEntity.BuildSuccess(RecycleProduct.class).setData(RP).setMsg("删除成功");
    }

    //查询商品接口
    @ApiOperation("查询商品接口，分页查询")
    @GetMapping("/allcycleproduct")
    public ResponseEntity<List<RecycleProduct>> findCycleProduct(@Valid  AllCycleProduct allCycleProduct, BindingResult bindingResult) {
        //入参日记
        log.info("入参参数是,{}", allCycleProduct);
        //参数校验
        ResponseEntity responseEntity = extractError(bindingResult);
        if (responseEntity != null) {
            return responseEntity;
        }
        List<RecycleProduct> result = rps.findCycleProduct(allCycleProduct);
        if (result == null||result.size()==0)
            return ResponseEntity.BuildErrorList(RecycleProduct.class).setMsg("查询失败");
        return ResponseEntity.BuildSuccessList(RecycleProduct.class).setData(result).setMsg("查询成功");
    }

    //回收商品日记搜索接口//分页查询
    @ApiOperation("根据模糊查询回收商品日记接口")
    @GetMapping("/auth/selectRecycleProductLog")
    public ResponseEntity<RecycleProductLogDto> selectRecycleProductLog(@Valid RecycleProductLogForm recycleProductLogForm, BindingResult bindingResult) {
        //记录日记
        log.info("入参是：{}", recycleProductLogForm);
        //参数校验...
        ResponseEntity responseEntity = extractError(bindingResult);
        if(responseEntity!=null){
            return  responseEntity;
        }
        //主业务逻辑
        RecycleProductLogDto result = rps.selectRecycleProductLog(recycleProductLogForm);
        if (result == null)
            return ResponseEntity.BuildSuccess(RecycleProductLogDto.class).setMsg("查询失败");
        return ResponseEntity.BuildSuccess(RecycleProductLogDto.class).setMsg("查询成功").setData(result);
    }
    //
}

