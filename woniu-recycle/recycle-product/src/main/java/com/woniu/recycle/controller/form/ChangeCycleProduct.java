package com.woniu.recycle.controller.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 *
 */
@ApiModel("更改回收商品接口form")
@Data
public class ChangeCycleProduct {

    @NotNull
    @ApiModelProperty(value = "id",example = "1")
    private Integer id;

    @ApiModelProperty(value = "商品名称")
    private String name;

    @ApiModelProperty(value = "预估价格",example = "1.0")
    private BigDecimal price;

    @ApiModelProperty(value = "类别id",example = "1")
    private Integer classifyId;

    @ApiModelProperty(value = "商品图片地址")
    private String pictureUrl;

    @ApiModelProperty(value = "计量单位")
    private String units;

    @ApiModelProperty(value = "上下架状态 0 下架 | 1 上架",example = "1")
    private Integer status;

}
