package com.woniu.recycle.controller.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel("查询所有form")
public class AllProduct {
    //第几页数
    @ApiModelProperty(value = "页数", example = "1")
    @NotNull
    private Integer page;
    //每页条数
    @ApiModelProperty(value = "条数", example = "1")
    @NotNull
    private Integer num;
}
