package com.woniu.recycle;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @ClassName ProductStart
 * @Description 启动类
 * @Author DingYuyi
 * @Date 2021/8/11 19:35
 * @Version 1.0
 */
@SpringBootApplication
@EnableTransactionManagement
@MapperScan("com.woniu.recycle.mapper")
public class ProductStart {
    public static void main(String[] args) {
        SpringApplication.run(ProductStart.class,args);
    }
}
