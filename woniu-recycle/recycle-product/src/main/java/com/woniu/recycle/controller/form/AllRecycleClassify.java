package com.woniu.recycle.controller.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 *
 */
@ApiModel("查询回收商品类别参数form")
@Data
public class AllRecycleClassify {
    @ApiModelProperty(value = "ID", example = "1")
    private Integer id;

    @ApiModelProperty(value = "类别名称")
    private String name;

    @ApiModelProperty(value = "商品图片地址")
    private String pictureUrl;

    @ApiModelProperty(value = "上下架状态 0 下架 | 1 上架",example = "1")
    private Integer status;

    @ApiModelProperty(value = "页数",example = "1")
    @NotNull
    private  Integer page;

    @ApiModelProperty(value = "条数",example = "1")
    @NotNull
    private Integer num;
}
