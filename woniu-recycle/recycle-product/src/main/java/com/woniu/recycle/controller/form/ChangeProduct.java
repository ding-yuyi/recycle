package com.woniu.recycle.controller.form;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@ApiModel("修改积分商品form")
public class ChangeProduct {
    //主键id
    @NotNull
    @ApiModelProperty(value = "主键id", example = "1")
    private Integer id;

    @ApiModelProperty(value = "商品名称")
    private String name;

    @ApiModelProperty(value = "商品价格",example = "1")
    private BigDecimal price;

    @ApiModelProperty(value = "商品类别id", example = "1")
    private Integer classifyId;

    @ApiModelProperty(value = "商品描述")
    private String description;

    @ApiModelProperty(value = "商品图片路径")
    private String pictureUrl;

    @ApiModelProperty(value = "商品库存", example = "1")
    private Integer inventory;

}
