package com.woniu.recycle.controller.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 *
 */
@ApiModel("删除家政服务form")
@Data
public class DeleteHouseKeeping {

    @ApiModelProperty(value = "id", example = "1")
    @NotNull
    private Integer id;
}
