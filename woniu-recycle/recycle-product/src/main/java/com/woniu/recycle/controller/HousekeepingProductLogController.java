package com.woniu.recycle.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 家政商品日志信息表 前端控制器
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-12
 */
@RestController
@RequestMapping("/housekeeping-product-log")
public class HousekeepingProductLogController {

}

