package com.woniu.recycle.controller.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 *
 */
@Data
@ApiModel("更改回收商品种类")
public class ChangeRecycleClassify {
    @NotNull
    @ApiModelProperty(value = "id", example = "1")
    private Integer id;

    @NotNull
    @ApiModelProperty(value = "新类别名称")
    private String name;

    @NotNull
    @ApiModelProperty(value = "新商品图片地址")
    private String pictureUrl;

    @NotNull
    @ApiModelProperty(value = "新上下架状态 0 下架 | 1 上架", example = "1")
    private Integer status;

}
