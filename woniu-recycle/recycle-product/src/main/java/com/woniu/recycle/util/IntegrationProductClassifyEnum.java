package com.woniu.recycle.util;

public enum IntegrationProductClassifyEnum {
    //顺序不能变
    USER(0),
    ADMIN(1),
    CODE_NULL("参数为空", 1001),
    GOODS_EXISTS("类别已存在", 1002),
    CODE_ERROR("新增错误", 1003),
    REDIS_EXISTS("redis中已存在", 1004),
    SQL_NULL("查询结果为空", 1005),
    SQL_ERROR("数据库异常", 1006),
    LOG_ERROR("日记异常",1007),
    NEW_GOODS("新增商品"),
    DELETE_GOODS("删除商品"),
    CHANGE_GOODS("修改商品"),
    CHANGE_STATUS("修改商品"),
    NUM_ERROR("参数错误",1008),
    INVENTOR_ERROR("无库存",1009);

    String message;
    Integer code;

    public String getMessage() {
        return message;
    }

    public Integer getCode() {
        return code;
    }

    public String setMessage() {
        return message;
    }

    IntegrationProductClassifyEnum(Integer code){
        this.code=code;
    };
    IntegrationProductClassifyEnum(String message){
        this.message=message;
    };
    IntegrationProductClassifyEnum(String message, Integer code) {
        this.message = message;
        this.code = code;
    }
}
