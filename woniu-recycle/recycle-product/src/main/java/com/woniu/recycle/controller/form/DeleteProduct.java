package com.woniu.recycle.controller.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

//删除积分商品接口
@Data
@ApiModel("删除积分商品form")
public class DeleteProduct {

    @ApiModelProperty(value = "id", example = "1")
    @NotNull
    private Integer id;

    @ApiModelProperty(value = "姓名")
    @NotNull
    private String name;

}
