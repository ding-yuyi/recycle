package com.woniu.recycle.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.woniu.recycle.controller.form.ChangeProductClassify;
import com.woniu.recycle.controller.form.DeleteProductClassify;
import com.woniu.recycle.controller.form.NewProductClassify;
import com.woniu.recycle.exception.ProductException;
import com.woniu.recycle.domain.IntegrationProductClassify;
import com.woniu.recycle.mapper.IntegrationProductClassifyMapper;
import com.woniu.recycle.service.IntegrationProductClassifyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.recycle.util.IntegrationProductClassifyEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 积分商品类别表 服务实现类
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-12
 */
@Service
@Slf4j
@Transactional
public class IntegrationProductClassifyServiceImpl extends ServiceImpl<IntegrationProductClassifyMapper, IntegrationProductClassify> implements IntegrationProductClassifyService {


    @Resource
    IntegrationProductClassifyMapper ipcm;
    @Resource
    StringRedisTemplate srt;


    //新增商品类别
    //添加类别到redis中
    //添加日记到log中
    @Override
    public IntegrationProductClassify newClassify(NewProductClassify nc) {
        //先对nc做判断是否为空
        if (nc.getClassifyName() == null || nc.getClassifyName().length() == 0)
            throw new ProductException(IntegrationProductClassifyEnum.CODE_NULL);
        //先查询redis中是否存在
        if (srt.hasKey("IntegrationProductClassify" + nc.getClassifyName()))
            throw new ProductException(IntegrationProductClassifyEnum.REDIS_EXISTS);
        //查询数据库中是否已经存在
        QueryWrapper<IntegrationProductClassify> integrationProductClassifyQueryWrapper = new QueryWrapper<>();
        QueryWrapper<IntegrationProductClassify> classify_name = integrationProductClassifyQueryWrapper.eq("classify_name", nc.getClassifyName());
        IntegrationProductClassify integrationProductClassify = ipcm.selectOne(classify_name);
        log.info("查询结果为：{}", integrationProductClassify);
        //  判断数据库的的值是否为空，如果为空不能新增,抛商品存在异常
        if (integrationProductClassify != null) throw new ProductException(IntegrationProductClassifyEnum.GOODS_EXISTS);
        //数据库中没有上述商品，可以执行商品类别新增操作;
        IntegrationProductClassify ipc = new IntegrationProductClassify();
        ipc.setClassifyName(nc.getClassifyName());
        int insert = ipcm.insert(ipc);
        if (insert == 0) throw new ProductException(IntegrationProductClassifyEnum.CODE_ERROR);
        //查询新增数据获得id
        Integer id = ipc.getId();
        //数据库新增成功,添加到redis中
        //设置过期时间10S
        long time = 10;
        srt.opsForValue().set("IntegrationProductClassify" + id, String.valueOf(nc), time, TimeUnit.SECONDS);
        return ipc;
    }

    //查询所有类型,返回LIST集合
    //************************差分页查询，补上
    @Override
    public List<IntegrationProductClassify> allClassify() {
        //直接在数据库中查询所有
        List<IntegrationProductClassify> integrationProductClassifies = ipcm.selectList(null);
        if (integrationProductClassifies == null || integrationProductClassifies.size() == 0)
            throw new ProductException(IntegrationProductClassifyEnum.SQL_NULL);
        return integrationProductClassifies;
    }

    //删除接口数据的类别
    @Override
    public List<DeleteProductClassify> deleteClassify(List<DeleteProductClassify> ds) {
        log.info(ds.toString());
        //判断是否为空
        if (ds == null)
            throw new ProductException(IntegrationProductClassifyEnum.CODE_NULL);
        ArrayList<DeleteProductClassify> deleteProductClassifies = new ArrayList<>();
        //不为空，传入参数有效开始业务逻辑
        for (int i = 0; i < ds.size(); i++) {
            //遍历循环
            Integer s = ds.get(i).getId();
            //进数据库查询，进行删除
            QueryWrapper<IntegrationProductClassify> qw = new QueryWrapper<>();
            qw.eq("id", ds.get(i).getId());
            //int delete = ipcm.delete(qw);
            int delete = ipcm.deleteById(ds.get(i).getId());
            if (delete <= 0) throw new ProductException("错误参数：" + ds.get(i).getId(), 1006);
            deleteProductClassifies.add(ds.get(i));
            //判断redis中是否存在，如果存在先在redis中删除
            Boolean aBoolean = srt.hasKey("IntegrationProductClassify" + ds.get(i).getId());
            log.info(String.valueOf("redis积分商品类别删除成功"));
            if (aBoolean) srt.delete("IntegrationProductClassify" + ds.get(i).getId());

        }
        //删除完毕
        return deleteProductClassifies;
    }


    @Override
    public ChangeProductClassify changeClassify(ChangeProductClassify cc) {
        //查询是否为空
        if (cc.getNewClassify().length() == 0 || cc == null || cc.getOldClassify().length() == 0)
            throw new ProductException(IntegrationProductClassifyEnum.CODE_NULL);
        //不为空，进行逻辑处理,查询老的类别
        //进入数据库中查询进行修改
        QueryWrapper<IntegrationProductClassify> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("classify_name", cc.getOldClassify());
        IntegrationProductClassify integrationProductClassify1 = ipcm.selectOne(queryWrapper);
        //如果删除结果小于0
        if (integrationProductClassify1 == null) throw new ProductException(IntegrationProductClassifyEnum.SQL_ERROR);
        IntegrationProductClassify integrationProductClassify = new IntegrationProductClassify();
        integrationProductClassify.setClassifyName(cc.getNewClassify());
        ipcm.update(integrationProductClassify, queryWrapper);
        //redis中查询看有没有，如果有直接删除，再新增
        Boolean aBoolean = srt.hasKey(cc.getOldClassify());
        if (aBoolean) srt.delete(cc.getOldClassify());
        long time = 10;
        srt.opsForValue().set("IntegrationProductClassify" + cc.getNewClassify(), cc.getNewClassify(), time, TimeUnit.SECONDS);
        return cc;
    }

}
