package com.woniu.recycle.controller.dto;

import com.woniu.recycle.domain.HousekeepingProductLog;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@ApiModel("商品日记Dto")
public class HousekeepingProductLogDto {

    @ApiModelProperty(value = "页数",example = "1")
    private Integer page;

    @ApiModelProperty(value = "条数",example = "1")
    private Integer num;
    @ApiModelProperty(value = "总数",example = "1")
    private long total;

    @ApiModelProperty(value = "商品id",example = "1")
    private List<HousekeepingProductLog> housekeepingProductLogs;



}
