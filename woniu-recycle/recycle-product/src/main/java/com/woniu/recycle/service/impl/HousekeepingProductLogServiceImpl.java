package com.woniu.recycle.service.impl;

import com.woniu.recycle.domain.HousekeepingProductLog;
import com.woniu.recycle.mapper.HousekeepingProductLogMapper;
import com.woniu.recycle.service.HousekeepingProductLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 家政商品日志信息表 服务实现类
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-12
 */
@Service
public class HousekeepingProductLogServiceImpl extends ServiceImpl<HousekeepingProductLogMapper, HousekeepingProductLog> implements HousekeepingProductLogService {

}
