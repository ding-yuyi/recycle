package com.woniu.recycle.controller;


import com.woniu.recycle.commons.web.component.BaseController;
import com.woniu.recycle.controller.form.ChangeProductClassify;
import com.woniu.recycle.controller.form.DeleteProductClassify;
import com.woniu.recycle.controller.form.NewProductClassify;
import com.woniu.recycle.commons.core.model.ResponseEntity;
import com.woniu.recycle.domain.IntegrationProductClassify;
import com.woniu.recycle.service.IntegrationProductClassifyService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 积分商品类别表 前端控制器
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-12
 */
@Api(tags = "积分商品种类控制器")
@RestController
@RequestMapping("/integrationProductClassify")
@Slf4j
public class IntegrationProductClassifyController extends BaseController {

    @Resource
    IntegrationProductClassifyService ipcs;

    //新增商品类别接口
    @PostMapping("/auth/newClassify")
    public ResponseEntity<IntegrationProductClassify> newClassify(@Valid @RequestBody NewProductClassify nc, BindingResult result) {
        log.info("entercode:{}", nc);
        //参数校验
        ResponseEntity validResult = extractError(result);
        if(validResult !=null){
            return validResult;
        }
        IntegrationProductClassify ipc = ipcs.newClassify(nc);
        if (ipc != null)
            return ResponseEntity.BuildSuccess(IntegrationProductClassify.class).setMsg("新增成功").setData(ipc);
        return ResponseEntity.BuildError(IntegrationProductClassify.class).setMsg("新增失败");
    }

    //查询商品种类接口(无形参)
    @GetMapping("/findClassify")
    public ResponseEntity<List<IntegrationProductClassify>> allClassify() {
        log.info("执行查询业务");
        List<IntegrationProductClassify> findall = ipcs.allClassify();
        if (findall == null||findall.size()==0) return ResponseEntity.BuildErrorList(IntegrationProductClassify.class).setMsg("查询结果为空");
        return ResponseEntity.BuildSuccessList(IntegrationProductClassify.class).setData(findall);
    }

    //删除接口，根据获取的参数进行删除
    @DeleteMapping("/auth/deleteClassify")
    public ResponseEntity<List<DeleteProductClassify>> deleteClassify(@Valid @RequestBody List<DeleteProductClassify> ds, BindingResult result) {
        log.info("删除业务参数:{}", ds);
        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(responseEntity!=null){
            return responseEntity;
        }
        List<DeleteProductClassify> deleteResult= ipcs.deleteClassify(ds);
        if (deleteResult == null || deleteResult.size() == 0)
            return ResponseEntity.BuildErrorList(DeleteProductClassify.class).setMsg("参数有误，请检查");
        return ResponseEntity.BuildSuccessList(DeleteProductClassify.class).setData(deleteResult).setMsg("已删除以下类别");
    }

    //根据获取参数进行修改业务
    @PutMapping("/auth/changeClassify")
    public ResponseEntity<ChangeProductClassify> changeClassify(@Valid ChangeProductClassify cc, BindingResult bindingResult) {
       //日记记录
        log.info(String.valueOf(cc));
        //参数校验
        ResponseEntity responseEntity = extractError(bindingResult);
        if(responseEntity!=null){
            return responseEntity;
        }
        ChangeProductClassify result = ipcs.changeClassify(cc);
        if (result == null || cc.getNewClassify().length() == 0 || cc.getOldClassify().length() == 0)
            ResponseEntity.BuildError(ChangeProductClassify.class).setMsg("参数异常");
        return ResponseEntity.BuildSuccess(ChangeProductClassify.class).setData(cc);
    }

}

