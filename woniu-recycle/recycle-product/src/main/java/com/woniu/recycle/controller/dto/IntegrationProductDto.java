package com.woniu.recycle.controller.dto;

import com.woniu.recycle.domain.IntegrationProduct;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 *
 */
@Data
public class IntegrationProductDto {

    //总条数
    @ApiModelProperty(value = "总条数",example = "1")
    private long total;
    @ApiModelProperty(value = "页数", example = "1")
    private long size;
    @ApiModelProperty(value = "条数", example = "1")
    private long current;
    @ApiModelProperty(value = "查询结构", example = "1")
    private List<IntegrationProduct> integrationProducts;
}
