package com.woniu.recycle.controller;


import com.woniu.recycle.commons.core.model.ResponseEntity;
import com.woniu.recycle.commons.web.component.BaseController;
import com.woniu.recycle.controller.form.AllRecycleClassify;
import com.woniu.recycle.controller.form.ChangeRecycleClassify;
import com.woniu.recycle.controller.form.DeleteRecycleClassify;
import com.woniu.recycle.controller.form.NewRecycleClassify;
import com.woniu.recycle.domain.RecycleProductClassify;
import com.woniu.recycle.service.RecycleProductClassifyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 回收商品类别信息表 前端控制器
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-12
 */
@Api(tags = "回收商品种类控制器")
@RestController
@RequestMapping("/recycleProductClassify")
@Slf4j
public class RecycleProductClassifyController extends BaseController {

    @Resource
    RecycleProductClassifyService rpcs;

    //新增商品回收//数组形式！//新增时判断数据库是否存在同名的名字的货物
    @ApiOperation("新增回收商品种类")
    @PostMapping("/auth/addrecycle")
    public ResponseEntity<List<RecycleProductClassify>> addRecycle(@Valid @RequestBody NewRecycleClassify[] addRecycle, BindingResult bindingResult) {
        //日记记录
        log.info("进入参数是：{}", addRecycle);
        //参数校验
        ResponseEntity validResult = extractError(bindingResult);
        if (validResult != null) {
            return validResult;
        }
        //主业务逻辑
        List<RecycleProductClassify> recycleProductClassify = rpcs.addRecycle(addRecycle);
        if (recycleProductClassify == null || recycleProductClassify.size() == 0)
            return ResponseEntity.BuildErrorList(RecycleProductClassify.class).setMsg("新增失败");
        return ResponseEntity.BuildSuccessList(RecycleProductClassify.class).setMsg("新增成功").setData(recycleProductClassify);
    }

    //删除商品种类
    @ApiOperation("删除商品种类")
    @DeleteMapping("/auth/deleterecycle")
    public ResponseEntity<List<RecycleProductClassify>> deleteRecycle(@Valid @RequestBody List<DeleteRecycleClassify> deleteRecycleClassify, BindingResult bindingResult) {
        //日记记录
        log.info("入参是：{}", deleteRecycleClassify);
        //参数校验
        ResponseEntity responseEntity = extractError(bindingResult);
        if (responseEntity != null) {
            return responseEntity;
        }
        //主业务：
        List<RecycleProductClassify> result = rpcs.deleteRecycle(deleteRecycleClassify);
        if (result == null || result.size() == 0)
            return ResponseEntity.BuildErrorList(RecycleProductClassify.class).setMsg("删除失败");
        return ResponseEntity.BuildSuccessList(RecycleProductClassify.class).setMsg("删除成功").setData(result);
    }

    //修改回收商品种类
    @ApiOperation("修改回收商品")
    @PutMapping("/auth/changerecycle")
    public ResponseEntity<RecycleProductClassify> changecycle(@Valid ChangeRecycleClassify changeRecycleClassify, BindingResult bindingResult) {
        //记录日记
        log.info("进参是：{}", changeRecycleClassify);
        //参数校验
        ResponseEntity responseEntity = extractError(bindingResult);
        if (responseEntity != null) {
            return responseEntity;
        }
        //主业务逻辑
        RecycleProductClassify result = rpcs.changecycle(changeRecycleClassify);
        if (result == null) return ResponseEntity.BuildSuccess(RecycleProductClassify.class).setMsg("修改错误");
        return responseEntity.BuildSuccess(RecycleProductClassify.class).setMsg("修改成功").setData(result);
    }

    //查询接口//分页查询
    @GetMapping("/selectall")
    @ApiOperation("查询商品类别接口")
    public ResponseEntity<List<RecycleProductClassify>> findAll(@Valid AllRecycleClassify allRecycleClassify, BindingResult bindingResult) {
        //日记记录
        log.info("入参是：{}", allRecycleClassify);
        //参数校验
        ResponseEntity responseEntity = extractError(bindingResult);
        if (responseEntity != null) {
            return responseEntity;
        }
        //主业务逻辑
        List<RecycleProductClassify> result = rpcs.findAll(allRecycleClassify);
        if (result == null || result.size() == 0)
            return ResponseEntity.BuildSuccessList(RecycleProductClassify.class).setMsg("查询错误");
        return responseEntity.BuildSuccessList(RecycleProductClassify.class).setMsg("查询成功").setData(result);
    }

    @GetMapping("/selectbyid")
    @ApiOperation("根据id查询商品类别接口")
    public ResponseEntity<RecycleProductClassify> findById(@Valid Integer id, BindingResult bindingResult) {
        //日记记录
        log.info("入参是：{}", id);
        //参数校验
        ResponseEntity responseEntity = extractError(bindingResult);
        if (responseEntity != null) {
            return responseEntity;
        }
        //主业务逻辑
        RecycleProductClassify result = rpcs.findById(id);
        if (result == null)
            return ResponseEntity.BuildSuccess(RecycleProductClassify.class).setMsg("查询错误");
        return responseEntity.BuildSuccess(RecycleProductClassify.class).setMsg("查询成功").setData(result);
    }
}

