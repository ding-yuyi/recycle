package com.woniu.recycle.controller.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 *
 */
@ApiModel("新增回收商品form")
@Data
public class NewRecycleClassify {
    @NotNull
    @ApiModelProperty(value = "类别名称")
    private String name;
    @NotNull
    @ApiModelProperty(value = "商品图片地址")
    private String pictureUrl;
    @NotNull
    @ApiModelProperty(value = "上下架状态 0 下架 | 1 上架",example = "1")
    private Integer status;


}
