package com.woniu.recycle.controller.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 *
 */
@Data
@ApiModel("删除回收商品form")
public class DeleteRecycleClassify {

    @ApiModelProperty(value = "主键id", example = "1")
    @NotNull
    private int id;
}
