package com.woniu.recycle.controller.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel("上下架积分商品form")
public class ChangeStatus {
    @ApiModelProperty(value = "id", example = "1")
    @NotNull
    private Integer id;

    @ApiModelProperty(value = "status", example = "1")
    @NotNull
    private Integer status;

}
