package com.woniu.recycle.mapper;

import com.woniu.recycle.domain.IntegrationProduct;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 积分商品表 Mapper 接口
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-12
 */
public interface IntegrationProductMapper extends BaseMapper<IntegrationProduct> {

}
