package com.woniu.recycle.service;

import com.woniu.recycle.controller.dto.IntegrationProductDto;
import com.woniu.recycle.controller.dto.IntegrationProductLogDto;
import com.woniu.recycle.controller.form.*;
import com.woniu.recycle.domain.IntegrationProduct;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 积分商品表 服务类
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-12
 */
public interface IntegrationProductService extends IService<IntegrationProduct> {

    IntegrationProductDto allProduct(AllProduct findid);

    List<IntegrationProduct> newGoods(NewGoods[] ng);

    DeleteProduct deleteProduct(@Valid DeleteProduct delteProduct);

    ChangeProduct changeProduct(ChangeProduct changeProduct);

    ChangeStatus changeStatus(ChangeStatus changeStatus);

    IntegrationProduct selectById(Integer productId);

    IntegrationProductDto descSelectAll(AllProduct findid);

    List<IntegrationProduct> selectPrice(SelectPrice selectPrice);

    List<IntegrationProduct> selectPriceDesc(SelectPrice selectPrice);

    IntegrationProduct changeInventor(ChangeProductNum changeProductNum);

    IntegrationProduct changeInventorsel(ChangeProductNum changeProductNum);

    IntegrationProductLogDto selectIntegrationProductLog(IntegrationProductLogForm integrationProductLogForm);
}
