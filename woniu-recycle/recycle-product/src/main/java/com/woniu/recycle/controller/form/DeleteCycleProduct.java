package com.woniu.recycle.controller.form;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 *
 */
@Data
@ApiModel("删除回收商品form")
public class DeleteCycleProduct {
    @NotNull
    @ApiModelProperty(value = "主键id", example = "1")
    private Integer id;
}
