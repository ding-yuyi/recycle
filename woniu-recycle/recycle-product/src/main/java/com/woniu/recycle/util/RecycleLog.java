package com.woniu.recycle.util;


import com.woniu.recycle.domain.RecycleProductLog;
import org.springframework.stereotype.Component;


/**
 *
 */
@Component
public class RecycleLog {

    public RecycleProductLog addlog(Integer id, String name, String reason) {
        RecycleProductLog rpl = new RecycleProductLog();
        rpl.setProductId(id);
        rpl.setProductName(name);
        rpl.setLogType(IntegrationProductClassifyEnum.ADMIN.ordinal());
        rpl.setLogReason("新增回收商品,详细信息：" + reason);
        rpl.setUserId(1);
        long l = System.currentTimeMillis();
        rpl.setCreateTime(l);
        return rpl;
    }

    public RecycleProductLog dellog(Integer id, String name, String reason) {
        RecycleProductLog rpl = new RecycleProductLog();
        rpl.setProductId(id);
        rpl.setProductName(name);
        rpl.setLogType(IntegrationProductClassifyEnum.ADMIN.ordinal());
        rpl.setLogReason("删除回收商品,详细信息：" + reason);
        rpl.setUserId(1);
        long l = System.currentTimeMillis();
        rpl.setCreateTime(l);
        return rpl;
    }

    public RecycleProductLog changelog(Integer id, String name, String reason) {
        RecycleProductLog rpl = new RecycleProductLog();
        rpl.setProductId(id);
        rpl.setProductName(name);
        rpl.setLogType(IntegrationProductClassifyEnum.ADMIN.ordinal());
        rpl.setLogReason("修改回收商品,详细信息：" + reason);
        rpl.setUserId(1);
        long l = System.currentTimeMillis();
        rpl.setCreateTime(l);
        return rpl;
    }

}
