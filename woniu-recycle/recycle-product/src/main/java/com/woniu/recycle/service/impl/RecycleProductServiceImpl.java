package com.woniu.recycle.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.recycle.commons.web.util.BeanCopyUtil;
import com.woniu.recycle.commons.web.util.ObjectMapperUtil;
import com.woniu.recycle.controller.dto.IntegrationProductLogDto;
import com.woniu.recycle.controller.dto.RecycleProductLogDto;
import com.woniu.recycle.controller.form.*;
import com.woniu.recycle.domain.IntegrationProductLog;
import com.woniu.recycle.exception.ProductException;
import com.woniu.recycle.domain.RecycleProduct;
import com.woniu.recycle.mapper.RecycleProductLogMapper;
import com.woniu.recycle.mapper.RecycleProductMapper;
import com.woniu.recycle.domain.RecycleProductLog;
import com.woniu.recycle.service.RecycleProductLogService;
import com.woniu.recycle.service.RecycleProductService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.recycle.util.IntegrationProductClassifyEnum;
import com.woniu.recycle.util.RecycleLog;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 回收商品信息表 服务实现类
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-12
 */
@Service
@Slf4j
@Transactional
public class RecycleProductServiceImpl extends ServiceImpl<RecycleProductMapper, RecycleProduct> implements RecycleProductService {
    @Resource
    RecycleProductMapper rpm;
    @Resource
    StringRedisTemplate srt;
    @Resource
    RecycleProductLogService rpls;

    @Resource
    RecycleProductLogMapper recycleProductLogMapper;
    //新增回收商品操作
    @Override
    public List<RecycleProduct> addProduct(List<NewRecycleProduct> newRecycleProducts) {
        //新增判断不为空
        if (newRecycleProducts.size() == 0 || newRecycleProducts == null)
            throw new ProductException(IntegrationProductClassifyEnum.CODE_NULL);
        ArrayList<RecycleProduct> recycleProducts = new ArrayList<>();
        QueryWrapper<RecycleProduct> wrapper = new QueryWrapper<>();
        //不为空进行新增操作
        for (int i = 0; i < newRecycleProducts.size(); i++) {
            //查询有没有已经存在的商品
            wrapper.eq(newRecycleProducts.get(i).getName() != null, "name", newRecycleProducts.get(i).getName())
                    .eq(newRecycleProducts.get(i).getClassifyId() != null, "classify_id", newRecycleProducts.get(i).getClassifyId());
            RecycleProduct recycleProduct1 = rpm.selectOne(wrapper);
            if (recycleProduct1 != null) throw new ProductException(IntegrationProductClassifyEnum.GOODS_EXISTS);
            RecycleProduct recycleProduct = BeanCopyUtil.copyObject(newRecycleProducts.get(i), RecycleProduct::new);
            int insert = rpm.insert(recycleProduct);
            //查询新增数据
            RecycleProduct msg = rpm.selectById(recycleProduct.getId());
            if (insert <= 0) throw new ProductException(IntegrationProductClassifyEnum.SQL_ERROR);
            recycleProducts.add(msg);
            //存redis
            String s = ObjectMapperUtil.parseObject(msg);
            long l = 60 * 60;
            srt.opsForValue().set("RecycleProduct" + recycleProduct.getId(), s, l, TimeUnit.SECONDS);
            log.info("redis回收商品新增成功");
            //存日记
            RecycleProductLog addlog = new RecycleLog().addlog(msg.getId(), msg.getName(), s);
            boolean save = rpls.save(addlog);
            if (!save) throw new ProductException(IntegrationProductClassifyEnum.LOG_ERROR);

        }
        return recycleProducts;
    }

    //删除回收商品实现类
    @Override
    public List<RecycleProduct> deleteProduct(List<DeleteCycleProduct> deleteCycleProducts) {
        if (deleteCycleProducts.size() == 0 || deleteCycleProducts == null)
            throw new ProductException(IntegrationProductClassifyEnum.CODE_NULL);
        ArrayList<RecycleProduct> recycleProducts = new ArrayList<>();
        //删除业务逻辑
        for (int i = 0; i < deleteCycleProducts.size(); i++) {
            RecycleProduct oldmsg = rpm.selectById(deleteCycleProducts.get(i).getId());
            if (oldmsg == null) throw new ProductException(IntegrationProductClassifyEnum.NUM_ERROR);
            System.out.println(oldmsg);
            //先插再删，不然删了就没得了
            rpm.deleteById(deleteCycleProducts.get(i).getId());
            boolean add = recycleProducts.add(oldmsg);
            if (!add) throw new ProductException(IntegrationProductClassifyEnum.LOG_ERROR);
            //存redis
            if (srt.hasKey("RecycleProduct" + oldmsg.getId()) != null) {
                srt.delete("RecycleProduct" + oldmsg.getId());
                log.info("redis删除成功");
            }
            //存日记
            String s = ObjectMapperUtil.parseObject(oldmsg);
            RecycleProductLog addlog = new RecycleLog().dellog(deleteCycleProducts.get(i).getId(), oldmsg.getName(), s);
            boolean save = rpls.save(addlog);
            if (!save) throw new ProductException(IntegrationProductClassifyEnum.LOG_ERROR);
            System.out.println(save);
        }
        System.out.println(recycleProducts);
        return recycleProducts;
    }

    //修改回收商品类接口
    @Override
    public RecycleProduct changeproduct(ChangeCycleProduct changeCycleProduct) {
        //全部为空的时候
        if (changeCycleProduct.getId() == null) throw new ProductException(IntegrationProductClassifyEnum.NUM_ERROR);
        RecycleProduct oldmsg = rpm.selectById(changeCycleProduct.getId());
        if (oldmsg == null) throw new ProductException(IntegrationProductClassifyEnum.SQL_ERROR);
        //方法：直接copy类复制新生成一个sql的实体类，直接updatebyid
        RecycleProduct recycleProduct = BeanCopyUtil.copyObject(changeCycleProduct, RecycleProduct::new);
        int update = rpm.updateById(recycleProduct);
        RecycleProduct newmsg = rpm.selectById(oldmsg.getId());
        if (update <= 0) throw new ProductException(IntegrationProductClassifyEnum.SQL_ERROR);
        //存redis
        String s = ObjectMapperUtil.parseObject(oldmsg);
        Boolean delete = srt.delete("RecycleProduct" + oldmsg.getId());
        if (delete) log.info("redis修改成功");
        long l = 60 * 60;
        srt.opsForValue().set("RecycleProduct" + newmsg.getId(), s, l, TimeUnit.SECONDS);
        //存日记
        RecycleProductLog addlog = new RecycleLog().changelog(newmsg.getId(), newmsg.getName(), s);
        boolean save = rpls.save(addlog);
        if (!save) throw new ProductException(IntegrationProductClassifyEnum.LOG_ERROR);
        return newmsg;
    }

    //分页查询回收商品,带条件
    @Override
    public List<RecycleProduct> findCycleProduct(AllCycleProduct allCycleProduct) {
        if (allCycleProduct.getNum() == null || allCycleProduct.getPage() == null)
            throw new ProductException(IntegrationProductClassifyEnum.NUM_ERROR);
        //new qw出来
        QueryWrapper<RecycleProduct> wrapper = new QueryWrapper<>();
        wrapper.eq(allCycleProduct.getId() != null, "id", allCycleProduct.getId())
                .like(allCycleProduct.getName() != null, "name", allCycleProduct.getName())
                .eq(allCycleProduct.getPrice() != null, "price", allCycleProduct.getPrice())
                .eq(allCycleProduct.getClassifyId() != null, "classify_id", allCycleProduct.getClassifyId())
                .eq(allCycleProduct.getPictureUrl() != null, "picture_url", allCycleProduct.getPictureUrl())
                .eq(allCycleProduct.getUnits() != null, "units", allCycleProduct.getUnits())
                .eq(allCycleProduct.getStatus() != null, "status", allCycleProduct.getStatus());
        //进行查询
        Page<RecycleProduct> page = new Page<>(allCycleProduct.getPage(), allCycleProduct.getNum());
        Page<RecycleProduct> page1 = rpm.selectPage(page, wrapper);
        List<RecycleProduct> records = page1.getRecords();
        System.out.println(records);
        //存redis
        long l =60*60;
        String s = ObjectMapperUtil.parseObject(records);
        srt.opsForValue().set("RecycleProductfind",s,l,TimeUnit.SECONDS);
        return records;
    }

    @Override
    public RecycleProductLogDto selectRecycleProductLog(RecycleProductLogForm recycleProductLogForm) {
        //分页
        QueryWrapper<RecycleProductLog> integrationProductLogQueryWrapper = new QueryWrapper<>();
        integrationProductLogQueryWrapper.eq(recycleProductLogForm.getProductId() != null, "product_id", recycleProductLogForm.getProductId())
                .like(recycleProductLogForm.getProductName() != null, "product_name", recycleProductLogForm.getProductName())
                .like(recycleProductLogForm.getLogReason() != null, "log_reason", recycleProductLogForm.getLogReason())
                .eq(recycleProductLogForm.getLogType() != null, "log_type", recycleProductLogForm.getLogType())
                .ge(recycleProductLogForm.getMincreateTime() != null, "create_time", recycleProductLogForm.getMincreateTime())
                .le(recycleProductLogForm.getMaxcreateTime() != null, "create_time", recycleProductLogForm.getMaxcreateTime());

        Page<RecycleProductLog> recycleProductLogPage = new Page<>(recycleProductLogForm.getPage(), recycleProductLogForm.getNum());
        Page<RecycleProductLog> recycleProductLogPage1 = recycleProductLogMapper.selectPage(recycleProductLogPage, integrationProductLogQueryWrapper);
        //判断结果
        if (recycleProductLogPage1.getRecords() == null) throw new ProductException(IntegrationProductClassifyEnum.SQL_NULL);
        List<RecycleProductLog> records = recycleProductLogPage1.getRecords();
        //返回参数
        List<RecycleProductLog> recycleProductLogForms = BeanCopyUtil.copyList(records, RecycleProductLog::new);
        RecycleProductLogDto recycleProductLogDto = BeanCopyUtil.copyObject(recycleProductLogForm, RecycleProductLogDto::new);
        recycleProductLogDto.setRecycleProductLogs(recycleProductLogForms);
        recycleProductLogDto.setPage(recycleProductLogForm.getPage());
        recycleProductLogDto.setNum(recycleProductLogForm.getNum());
        recycleProductLogDto.setTotal(recycleProductLogPage1.getTotal());
        log.info("商品日记查询业务执行成功");
        //存redis
        String s = ObjectMapperUtil.parseObject(recycleProductLogDto);
        String s1 = ObjectMapperUtil.parseObject(recycleProductLogDto);
        srt.opsForHash().put(s,String.valueOf(recycleProductLogForm.getNum()),String.valueOf(recycleProductLogPage1.getSize()));
        srt.opsForHash().put(s,String.valueOf(recycleProductLogForm.getPage()),String.valueOf(recycleProductLogPage1.getCurrent()));
        srt.opsForHash().put(s,String.valueOf(recycleProductLogDto.getTotal()),String.valueOf(recycleProductLogPage1.getTotal()));
        srt.opsForHash().put(s,s,s1);
        log.info("商品日记查询redis存储成功");
        return recycleProductLogDto;

    }

}
