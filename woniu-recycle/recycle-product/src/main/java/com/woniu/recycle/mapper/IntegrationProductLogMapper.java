package com.woniu.recycle.mapper;

import com.woniu.recycle.domain.IntegrationProductLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 积分商品日志信息表 Mapper 接口
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-12
 */
public interface IntegrationProductLogMapper extends BaseMapper<IntegrationProductLog> {


}
