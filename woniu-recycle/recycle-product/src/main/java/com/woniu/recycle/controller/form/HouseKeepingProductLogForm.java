package com.woniu.recycle.controller.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel("家政商品日记form")
public class HouseKeepingProductLogForm {

    @NotNull
    @ApiModelProperty(value = "页数",example = "1")
    private Integer page;

    @NotNull
    @ApiModelProperty(value = "条数",example = "1")
    private Integer num;


    @ApiModelProperty(value = "商品id",example = "1")
    private Integer productId;

    @ApiModelProperty(value = "商品名称")
    private String productName;

    @ApiModelProperty(value = "记录日志产生原因")
    private String logReason;

    @ApiModelProperty(value = "操作者类型 0 普通用户 | 1 管理员",example = "1")
    private Integer logType;

    @ApiModelProperty(value = "操作者id",example = "1")
    private Integer userId;

    @ApiModelProperty(value = "创建时间尾",example = "1")
    private Long maxcreateTime;

    @ApiModelProperty(value = "创建时间头",example = "1")
    private Long mincreateTime;

}
