package com.woniu.recycle.controller.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel("删除积分商品类别form")
public class DeleteProductClassify {
    @NotNull
    @ApiModelProperty(value = "id",example = "1")
    private Integer id;

}
