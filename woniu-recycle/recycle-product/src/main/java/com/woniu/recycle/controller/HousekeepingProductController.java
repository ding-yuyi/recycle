package com.woniu.recycle.controller;


import com.woniu.recycle.commons.core.model.ResponseEntity;
import com.woniu.recycle.commons.web.component.BaseController;
import com.woniu.recycle.commons.web.util.BeanCopyUtil;
import com.woniu.recycle.controller.dto.HouseKeepingProductDto;
import com.woniu.recycle.controller.dto.HousekeepingProductLogDto;
import com.woniu.recycle.controller.form.*;
import com.woniu.recycle.domain.HousekeepingProduct;
import com.woniu.recycle.domain.HousekeepingProductLog;
import com.woniu.recycle.domain.IntegrationProduct;
import com.woniu.recycle.service.HousekeepingProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Delete;
import org.springframework.boot.context.properties.bind.BindResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 家政商品信息表 前端控制器
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-12
 */
@RestController
@RequestMapping("/housekeeping")
@Api(tags = "家政服务控制器")
@Slf4j
public class HousekeepingProductController extends BaseController {

    @Resource
    HousekeepingProductService hps;

    @ApiOperation("新增商品种类")
    @PostMapping("/auth/addhousekeeping")
    public ResponseEntity<List<HousekeepingProduct>> addHousekeeping(@Valid @RequestBody List<NewHouseKeeping> newHouseKeeping, BindingResult bindingResult) {
        //进参日记
        log.info("进参是：{}", newHouseKeeping);
        //参数校验
        ResponseEntity responseEntity = extractError(bindingResult);
        if (responseEntity != null) {
            return responseEntity;
        }
        //主业务逻辑参数
        List<HousekeepingProduct> result = hps.addHouseKeeping(newHouseKeeping);
        if (result == null||result.size()==0)
            return ResponseEntity.BuildErrorList(HousekeepingProduct.class).setData(result).setMsg("新增失败");
        return ResponseEntity.BuildSuccessList(HousekeepingProduct.class).setData(result).setMsg("新增成功");
    }

    //删除家政服务
    @ApiOperation("删除家政服务接口")
    @DeleteMapping("/auth/deletehousekeeping")
    public ResponseEntity<List<HousekeepingProduct>> deleteHousekeeping(@Valid @RequestBody List<DeleteHouseKeeping> deleteHouseKeeping, BindingResult bindResult) {
        //进参日记
        log.info("进参参数是：{}", deleteHouseKeeping);
        //参数检验
        ResponseEntity responseEntity = extractError(bindResult);
        if (responseEntity != null) {
            return responseEntity;
        }
        //主业务逻辑
        List<HousekeepingProduct> result = hps.deleteHousekeeping(deleteHouseKeeping);
        if (result == null||result.size()==0)
            return ResponseEntity.BuildErrorList(HousekeepingProduct.class).setMsg("删除失败");
        return ResponseEntity.BuildSuccessList(HousekeepingProduct.class).setData(result).setMsg("删除成功");
    }

    //修改家政业务
    @PutMapping("/auth/changehouseKeeping")
    @ApiOperation("修改家政业务")
    public ResponseEntity<HousekeepingProduct> changeHousekeeping(@Valid ChangeHouseKeeping changeHouseKeeping, BindingResult bindingResult) {
        //日记记录
        log.info("入参参数是：{}", changeHouseKeeping);
        //参数校验
        ResponseEntity responseEntity = extractError(bindingResult);
        if (responseEntity != null) {
            return responseEntity;
        }
        //主业务逻辑
        HousekeepingProduct result = hps.changeHouseKeeping(changeHouseKeeping);
        if (result == null)
            return ResponseEntity.BuildError(HousekeepingProduct.class).setMsg("修改失败");
        return ResponseEntity.BuildSuccess(HousekeepingProduct.class).setMsg("修改成功").setData(result);
    }

    //查询接口
    @ApiOperation("查询家政商品接口")
    @GetMapping("/findhousekeeping")
    public ResponseEntity<List<HousekeepingProduct>> findHouseKeepings(@Valid AllHouseKeeping allHouseKeeping, BindingResult bindingResult) {
        //记录日记
        log.info("入参是：{}", allHouseKeeping);
        //参数校验
        ResponseEntity responseEntity = extractError(bindingResult);
        if (responseEntity != null) {
            return responseEntity;
        }
        //主业务逻辑
        List<HousekeepingProduct> result = hps.findHouseKeeping(allHouseKeeping);
        if (result == null||result.size()==0)
            return ResponseEntity.BuildSuccessList(HousekeepingProduct.class).setMsg("查询失败");
        return ResponseEntity.BuildSuccessList(HousekeepingProduct.class).setMsg("查询成功").setData(result);
    }

    //根据id查询接口
    @ApiOperation("根据id查询家政商品接口")
    @GetMapping("/findhousekeepingbyid")
    public ResponseEntity<HouseKeepingProductDto> findHouseKeepingsByid(Integer id) {
        //记录日记
        log.info("入参是：{}", id);
        //参数校验...
        //主业务逻辑
        HousekeepingProduct result = hps.findHouseKeepingByid(id);
        HouseKeepingProductDto houseKeepingProductDto = BeanCopyUtil.copyObject(result, HouseKeepingProductDto::new);
        if (result == null)
            return ResponseEntity.BuildSuccess(HouseKeepingProductDto.class).setMsg("查询失败");
        return ResponseEntity.BuildSuccess(HouseKeepingProductDto.class).setMsg("查询成功").setData(houseKeepingProductDto);
    }

    //家政日记搜索接口//分页查询
    @ApiOperation("根据模糊查询家政商品日记接口")
    @GetMapping("/auth/selectHouseKeeingLog")
    public ResponseEntity<HousekeepingProductLogDto> selectHouseKeeingLog(@Valid HouseKeepingProductLogForm houseKeepingProductLogForm, BindingResult bindingResult) {
        //记录日记
        log.info("入参是：{}", houseKeepingProductLogForm);
        //参数校验...
        ResponseEntity responseEntity = extractError(bindingResult);
        if(responseEntity!=null){
            return  responseEntity;
        }
        //主业务逻辑
        HousekeepingProductLogDto result = hps.selectHouseKeeingLog(houseKeepingProductLogForm);
        if (result == null)
            return ResponseEntity.BuildSuccess(HousekeepingProductLogDto.class).setMsg("查询失败");
        return ResponseEntity.BuildSuccess(HousekeepingProductLogDto.class).setMsg("查询成功").setData(result);
    }
}




