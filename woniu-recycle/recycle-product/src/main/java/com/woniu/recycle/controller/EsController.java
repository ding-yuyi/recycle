package com.woniu.recycle.controller;

import com.woniu.recycle.domain.HousekeepingProduct;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.math.BigDecimal;

@RestController
public class EsController {
    @Resource
    private ElasticsearchOperations eo;
    @GetMapping("/save")
    public  void go(){
        HousekeepingProduct housekeepingProduct = new HousekeepingProduct();
        housekeepingProduct.setId(6);
        housekeepingProduct.setPrice(new BigDecimal(100));
        housekeepingProduct.setDescription("可以用来拖地");
        housekeepingProduct.setPictureUrl("11111111");
        housekeepingProduct.setAttention("不可以用来洗衣服");
        eo.save(housekeepingProduct);
    }
}
