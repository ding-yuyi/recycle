package com.woniu.recycle.service.impl;

import com.woniu.recycle.domain.IntegrationProductLog;
import com.woniu.recycle.mapper.IntegrationProductLogMapper;
import com.woniu.recycle.service.IntegrationProductLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 积分商品日志信息表 服务实现类
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-12
 */
@Service
public class IntegrationProductLogServiceImpl extends ServiceImpl<IntegrationProductLogMapper, IntegrationProductLog> implements IntegrationProductLogService {



}
