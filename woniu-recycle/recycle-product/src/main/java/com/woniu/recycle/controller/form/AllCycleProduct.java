package com.woniu.recycle.controller.form;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 *
 */
@Data
@ApiModel("查询回收商品前端form")
public class AllCycleProduct {

    @ApiModelProperty(value = "商品名称", example = "1")
    private Integer id;

    @ApiModelProperty(value = "商品名称")
    private String name;

    @ApiModelProperty(value = "预估价格", example = "1.0")
    private BigDecimal price;

    @ApiModelProperty(value = "类别id", example = "1")
    private Integer classifyId;

    @ApiModelProperty(value = "商品图片地址")
    private String pictureUrl;

    @ApiModelProperty(value = "计量单位")
    private String units;

    @ApiModelProperty(value = "上下架状态 0 下架 | 1 上架", example = "1")
    private Integer status;

    @NotNull
    @ApiModelProperty(value = "页数", example = "1")
    private Integer page;

    @NotNull
    @ApiModelProperty(value = "条数", example = "1")
    private Integer num;
}
