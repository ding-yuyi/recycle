package com.woniu.recycle.service;

import com.woniu.recycle.domain.RecycleProductLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 回收商品日志信息表 服务类
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-12
 */
public interface RecycleProductLogService extends IService<RecycleProductLog> {

}
