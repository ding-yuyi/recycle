package com.woniu.recycle.mapper;

import com.woniu.recycle.domain.RecycleProductClassify;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 回收商品类别信息表 Mapper 接口
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-12
 */
public interface RecycleProductClassifyMapper extends BaseMapper<RecycleProductClassify> {

}
