package com.woniu.recycle.service;

import com.woniu.recycle.controller.form.ChangeProductClassify;
import com.woniu.recycle.controller.form.DeleteProductClassify;
import com.woniu.recycle.controller.form.NewProductClassify;
import com.woniu.recycle.domain.IntegrationProductClassify;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 积分商品类别表 服务类
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-12
 */
public interface IntegrationProductClassifyService extends IService<IntegrationProductClassify> {


    IntegrationProductClassify newClassify(NewProductClassify nc);

    List<IntegrationProductClassify> allClassify();

    ChangeProductClassify changeClassify(ChangeProductClassify cc);

    List<DeleteProductClassify> deleteClassify(List<DeleteProductClassify> ds);

}
