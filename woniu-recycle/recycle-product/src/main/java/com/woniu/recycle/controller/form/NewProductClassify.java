package com.woniu.recycle.controller.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel("新建商品种类form")
public class NewProductClassify {
    @ApiModelProperty("积分商品种类")
    @NotNull
    private String classifyName;

}
