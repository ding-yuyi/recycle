package com.woniu.recycle.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 积分商品类别表
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("integration_product_classify")
@ApiModel(value="IntegrationProductClassify对象", description="积分商品类别表")
public class IntegrationProductClassify implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键id",example = "1")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "商品名称")
    private String classifyName;

}
