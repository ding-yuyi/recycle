package com.woniu.recycle.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.recycle.commons.web.util.BeanCopyUtil;
import com.woniu.recycle.commons.web.util.ObjectMapperUtil;
import com.woniu.recycle.controller.form.AllRecycleClassify;
import com.woniu.recycle.controller.form.ChangeRecycleClassify;
import com.woniu.recycle.controller.form.DeleteRecycleClassify;
import com.woniu.recycle.controller.form.NewRecycleClassify;
import com.woniu.recycle.exception.ProductException;
import com.woniu.recycle.domain.RecycleProductClassify;
import com.woniu.recycle.mapper.RecycleProductClassifyMapper;
import com.woniu.recycle.domain.RecycleProductLog;
import com.woniu.recycle.service.RecycleProductClassifyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.recycle.service.RecycleProductLogService;
import com.woniu.recycle.util.IntegrationProductClassifyEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 回收商品类别信息表 服务实现类
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-12
 */
@Service
@Slf4j
@Transactional
public class RecycleProductClassifyServiceImpl extends ServiceImpl<RecycleProductClassifyMapper, RecycleProductClassify> implements RecycleProductClassifyService {

    @Resource
    RecycleProductClassifyMapper rpcm;
    @Resource
    StringRedisTemplate srt;
    @Resource
    RecycleProductLogService rpls;

    //新增商品种类实现类
    @Override
    public List<RecycleProductClassify> addRecycle(NewRecycleClassify[] addRecycle) {
        //判断是否为空
        if (addRecycle == null) throw new ProductException(IntegrationProductClassifyEnum.CODE_NULL);
        //不为空进行新增操作
        ArrayList<RecycleProductClassify> recycleProductClassifies = new ArrayList<>();
        QueryWrapper<RecycleProductClassify> recycleProductClassifyQueryWrapper = new QueryWrapper<>();
        //copy类进行新增
        for (int i = 0; i < addRecycle.length; i++) {
            //判读数据库是否已经有了
            recycleProductClassifyQueryWrapper.eq("name", addRecycle[i].getName());
            RecycleProductClassify recycleProductClassify2 = rpcm.selectOne(recycleProductClassifyQueryWrapper);
            if (recycleProductClassify2 != null)
                throw new ProductException(IntegrationProductClassifyEnum.GOODS_EXISTS);
            //如果没有执行新增操作
            RecycleProductClassify recycleProductClassify = BeanCopyUtil.copyObject(addRecycle[i], RecycleProductClassify::new);
            int insert = rpcm.insert(recycleProductClassify);
            if (insert <= 0) throw new ProductException(IntegrationProductClassifyEnum.SQL_ERROR);
            //查询新增操作
            RecycleProductClassify recycleProductClassify1 = rpcm.selectById(recycleProductClassify.getId());
            System.out.println(recycleProductClassify1);
            recycleProductClassifies.add(recycleProductClassify1);
            //进redis
            //redis默认一小时
            long l = 60 * 60;
            srt.opsForValue().set("RecycleProductClassify" + recycleProductClassify1.getId(), String.valueOf(recycleProductClassify1), l, TimeUnit.SECONDS);
            log.info("reids新增回收商品种类成功");
            //存日记
            RecycleProductLog recycleProductLog = new RecycleProductLog();
            recycleProductLog.setProductId(recycleProductClassify1.getId());
            recycleProductLog.setProductName(recycleProductClassify1.getName());
            recycleProductLog.setLogReason("新增商品种类，详细信息：" + recycleProductClassify1);
            recycleProductLog.setLogType(IntegrationProductClassifyEnum.ADMIN.ordinal());
            //后期获取tokenUserid
            recycleProductLog.setUserId(1);
            long l1 = System.currentTimeMillis();
            recycleProductLog.setCreateTime(l1);
            boolean save = rpls.save(recycleProductLog);
            if (!save) throw new ProductException(IntegrationProductClassifyEnum.LOG_ERROR);
        }
        return recycleProductClassifies;
    }

    //删除商品种类
    @Override
    public List<RecycleProductClassify> deleteRecycle(List<DeleteRecycleClassify> deleteRecycleClassify) {
        if (deleteRecycleClassify == null) throw new ProductException(IntegrationProductClassifyEnum.SQL_NULL);
        ArrayList<RecycleProductClassify> recycleProductClassifies = new ArrayList<>();
        for (int i = 0; i < deleteRecycleClassify.size(); i++) {
            //判断数据库中是否存在
            RecycleProductClassify recycleProductClassify = rpcm.selectById(deleteRecycleClassify.get(i).getId());
            if (recycleProductClassify == null) throw new ProductException(IntegrationProductClassifyEnum.SQL_NULL);
            //存在，执行删除操作
            int i1 = rpcm.deleteById(deleteRecycleClassify.get(i).getId());
            if (i1 <= 0) throw new ProductException(IntegrationProductClassifyEnum.SQL_ERROR);
            boolean add = recycleProductClassifies.add(recycleProductClassify);
            if (!add) throw new ProductException(IntegrationProductClassifyEnum.SQL_ERROR);
            //删除redis
            srt.delete("RecycleProductClassify" + deleteRecycleClassify.get(i).getId());
            //存日记
            RecycleProductLog recycleProductLog = new RecycleProductLog();
            recycleProductLog.setProductId(deleteRecycleClassify.get(i).getId());
            recycleProductLog.setProductName(recycleProductClassify.getName());
            recycleProductLog.setLogReason("删除商品种类，详细信息:" + recycleProductClassify);
            recycleProductLog.setLogType(IntegrationProductClassifyEnum.ADMIN.ordinal());
            //后期获取token
            recycleProductLog.setUserId(1);
            long l = System.currentTimeMillis();
            recycleProductLog.setCreateTime(l);
            boolean save = rpls.save(recycleProductLog);
            if (!save) throw new ProductException(IntegrationProductClassifyEnum.LOG_ERROR);
        }

        return recycleProductClassifies;
    }

    //修改回收商品实现类
    @Override
    public RecycleProductClassify changecycle(ChangeRecycleClassify changeRecycleClassify) {
        if (changeRecycleClassify == null) throw new ProductException(IntegrationProductClassifyEnum.CODE_NULL);
        //查询实体数据
        RecycleProductClassify oldmsg = rpcm.selectById(changeRecycleClassify.getId());
        //判断是否有效
        if (oldmsg == null) throw new ProductException(IntegrationProductClassifyEnum.SQL_ERROR);
        //有效，执行更改逻辑
        RecycleProductClassify newmsg = BeanCopyUtil.copyObject(changeRecycleClassify, RecycleProductClassify::new);
        int i = rpcm.updateById(newmsg);
        if (i <= 0) throw new ProductException(IntegrationProductClassifyEnum.SQL_ERROR);
        //redis操作
        srt.delete("RecycleProductClassify" + newmsg.getId());
        long l = 60 * 60;
        srt.opsForValue().set("RecycleProductClassify" + newmsg.getId(), String.valueOf(newmsg), l, TimeUnit.SECONDS);
        //存储log
        RecycleProductLog recycleProductLog = new RecycleProductLog();
        recycleProductLog.setProductId(newmsg.getId());
        recycleProductLog.setProductName(newmsg.getName());
        String s = ObjectMapperUtil.parseObject(oldmsg);
        String s1 = ObjectMapperUtil.parseObject(newmsg);
        recycleProductLog.setLogReason("修改回收商品类别：" + "更改前：" + s + "更改后：" + s1);
        long l1 = System.currentTimeMillis();
        recycleProductLog.setCreateTime(l1);
        recycleProductLog.setLogType(IntegrationProductClassifyEnum.ADMIN.ordinal());
        //根据token获取
        recycleProductLog.setUserId(1);
        boolean save = rpls.save(recycleProductLog);
        System.out.println(save);
        if (!save) throw new ProductException(IntegrationProductClassifyEnum.LOG_ERROR);
        return newmsg;
    }

    //查询所有商品种类
    @Override
    public List<RecycleProductClassify> findAll(AllRecycleClassify allRecycleClassify) {
        //可以为空，当是空的时候默认查询所有
        long l = 60 * 60;
        if (allRecycleClassify.getId() == null && allRecycleClassify.getName() == null && allRecycleClassify.getPictureUrl() == null && allRecycleClassify.getStatus() == null) {
            //查询所有
            Page<RecycleProductClassify> objectPage = new Page<>(allRecycleClassify.getPage(), allRecycleClassify.getNum());
            Page<RecycleProductClassify> recycleProductClassifyPage = rpcm.selectPage(objectPage, null);
            List<RecycleProductClassify> records = recycleProductClassifyPage.getRecords();
            //存json字符串
            String s = ObjectMapperUtil.parseObject(records);
            srt.opsForValue().set("RecycleProductClassifyall", s, l);
            System.out.println(records);
            return records;
        }
        //当不为空的时候,分类进行模糊查询
        QueryWrapper<RecycleProductClassify> wrapper = new QueryWrapper<>();
        wrapper.eq(allRecycleClassify.getId() != null, "id", allRecycleClassify.getId())
                .like(allRecycleClassify.getName() != null, "name", allRecycleClassify.getName())
                .like(allRecycleClassify.getPictureUrl() != null, "picture_url", allRecycleClassify.getPictureUrl())
                .eq(allRecycleClassify.getStatus() != null, "status", allRecycleClassify.getStatus());
        //分页查询
        Page<RecycleProductClassify> objectPage = new Page<>(allRecycleClassify.getPage(), allRecycleClassify.getNum());
        Page<RecycleProductClassify> recycleProductClassifyPage = rpcm.selectPage(objectPage, wrapper);
        List<RecycleProductClassify> records = recycleProductClassifyPage.getRecords();
        if (records == null || records.size() == 0) throw new ProductException(IntegrationProductClassifyEnum.SQL_NULL);
        //存json格式
        String s = ObjectMapperUtil.parseObject(records);
        srt.opsForValue().set("RecycleProductClassifyall", s, l);
        return records;
    }

    //根据id查数据
    @Override
    public RecycleProductClassify findById(Integer id) {
        RecycleProductClassify recycleProductClassify = rpcm.selectById(id);
        if (recycleProductClassify == null) throw new ProductException(IntegrationProductClassifyEnum.SQL_NULL);
        String s = ObjectMapperUtil.parseObject(recycleProductClassify);
        srt.opsForValue().set("RecycleProductClassify" + id, s, 60 * 60, TimeUnit.SECONDS);
        return recycleProductClassify;
    }
}
