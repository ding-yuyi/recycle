package com.woniu.recycle.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.recycle.commons.web.util.BeanCopyUtil;
import com.woniu.recycle.commons.web.util.ObjectMapperUtil;
import com.woniu.recycle.controller.dto.HousekeepingProductLogDto;
import com.woniu.recycle.controller.form.*;
import com.woniu.recycle.domain.HousekeepingProductLog;
import com.woniu.recycle.exception.ProductException;
import com.woniu.recycle.domain.HousekeepingProduct;
import com.woniu.recycle.mapper.HousekeepingProductLogMapper;
import com.woniu.recycle.mapper.HousekeepingProductMapper;
import com.woniu.recycle.service.HousekeepingProductLogService;
import com.woniu.recycle.service.HousekeepingProductService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.recycle.util.IntegrationProductClassifyEnum;
import com.woniu.recycle.util.NewLog;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 家政商品信息表 服务实现类
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-12
 */
@Service
@Transactional
@Slf4j
public class HousekeepingProductServiceImpl extends ServiceImpl<HousekeepingProductMapper, HousekeepingProduct> implements HousekeepingProductService {

    @Resource
    HousekeepingProductMapper hpm;
    @Resource
    StringRedisTemplate srt;
    @Resource
    HousekeepingProductLogService hpls;
    @Resource
    HousekeepingProductLogMapper housekeepingProductLogMapper;

    @Override
    public List<HousekeepingProduct> addHouseKeeping(List<NewHouseKeeping> newHouseKeeping) {
        //判断
        if (newHouseKeeping.size() == 0 || newHouseKeeping == null)
            throw new ProductException(IntegrationProductClassifyEnum.CODE_NULL);
        ArrayList<HousekeepingProduct> housekeepingProducts = new ArrayList<>();
        QueryWrapper<HousekeepingProduct> wrapper = new QueryWrapper<>();
        //查询是不是已经存在
        for (int i = 0; i < newHouseKeeping.size(); i++) {
            wrapper.eq(newHouseKeeping.get(i).getName() != null, "name", newHouseKeeping.get(i).getName());
            HousekeepingProduct housekeepingProduct = hpm.selectOne(wrapper);
            if (housekeepingProduct != null) throw new ProductException(IntegrationProductClassifyEnum.GOODS_EXISTS);
            //如果是空，执行新增操作
            HousekeepingProduct msg = BeanCopyUtil.copyObject(newHouseKeeping.get(i), HousekeepingProduct::new);
            int insert = hpm.insert(msg);
            if (insert <= 0) throw new ProductException(IntegrationProductClassifyEnum.SQL_ERROR);
            boolean add = housekeepingProducts.add(msg);
            if (!add) throw new ProductException(IntegrationProductClassifyEnum.SQL_ERROR);
            //存redis
            String s = ObjectMapperUtil.parseObject(msg);
            String ss = "新增家政商品,详细情况：" + s;
            long l = 60 * 60;
            srt.opsForValue().set("HousekeepingProduct" + msg.getId(), ss, l, TimeUnit.SECONDS);
            log.info("家政商品新增redis成功");
            //存日记
            HousekeepingProductLog addlog = NewLog.addlog(msg.getId(), msg.getName(), s, HousekeepingProductLog::new);
            System.out.println(addlog);
            hpls.save(addlog);
        }
        return housekeepingProducts;
    }

    @Override
    public List<HousekeepingProduct> deleteHousekeeping(List<DeleteHouseKeeping> deleteHouseKeeping) {
        //判断
        if (deleteHouseKeeping.size() == 0 || deleteHouseKeeping == null)
            throw new ProductException(IntegrationProductClassifyEnum.CODE_NULL);
        //不为空，删除业务
        ArrayList<HousekeepingProduct> housekeepingProducts = new ArrayList<>();
        for (int i = 0; i < deleteHouseKeeping.size(); i++) {
            //先查询！！！！！！！！
            HousekeepingProduct oldmsg = hpm.selectById(deleteHouseKeeping.get(i).getId());
            if (oldmsg == null) throw new ProductException(IntegrationProductClassifyEnum.SQL_ERROR);
            int i1 = hpm.deleteById(deleteHouseKeeping.get(i).getId());
            if (i1 <= 0) throw new ProductException(IntegrationProductClassifyEnum.SQL_ERROR);
            housekeepingProducts.add(oldmsg);
            //删除redis,先查询
            if (srt.hasKey("HousekeepingProduct" + oldmsg.getId())) {
                Boolean delete = srt.delete("HousekeepingProduct" + oldmsg.getId());
                log.info("redis删除家政商品成功,id:" + "HousekeepingProduct" + oldmsg.getId());
            }
            //存日记
            String s = ObjectMapperUtil.parseObject(oldmsg);
            String ss = "删除家政商品，详细情况：" + s;
            HousekeepingProductLog addlog = NewLog.addlog(oldmsg.getId(), oldmsg.getName(), ss, HousekeepingProductLog::new);
            System.out.println(addlog);
            hpls.save(addlog);
        }
        return housekeepingProducts;
    }

    //修改家政商品信息
    @Override
    public HousekeepingProduct changeHouseKeeping(ChangeHouseKeeping changeHouseKeeping) {
        //判断是否空
        if (changeHouseKeeping.getId() == null || changeHouseKeeping == null)
            throw new ProductException(IntegrationProductClassifyEnum.CODE_NULL);
        //修改系统，先查询
        UpdateWrapper<HousekeepingProduct> wrapper = new UpdateWrapper<>();
        HousekeepingProduct oldmsg = hpm.selectById(changeHouseKeeping.getId());
//        wrapper.eq(changeHouseKeeping.getName() != null, "name", changeHouseKeeping.getName())
//                .eq(changeHouseKeeping.getPrice() != null, "price", changeHouseKeeping.getPrice())
//                .eq(changeHouseKeeping.getDescription() != null, "description", changeHouseKeeping.getDescription())
//                .eq(changeHouseKeeping.getAttention() != null, "attention", changeHouseKeeping.getAttention())
//                .eq(changeHouseKeeping != null, "status", changeHouseKeeping.getStatus());
//        int update = hpm.update(oldmsg, wrapper);
        HousekeepingProduct housekeepingProduct = BeanCopyUtil.copyObject(changeHouseKeeping, HousekeepingProduct::new);
        int i = hpm.updateById(housekeepingProduct);
        HousekeepingProduct newmsg = hpm.selectById(changeHouseKeeping.getId());
        //方法2：直接copy工具类生成一个新的对象，updatebyyid实现
        if (i <= 0) throw new ProductException(IntegrationProductClassifyEnum.SQL_ERROR);
        //进redis
        String s = ObjectMapperUtil.parseObject(newmsg);
        String s1 = ObjectMapperUtil.parseObject(oldmsg);
        if (srt.hasKey("HousekeepingProduct" + oldmsg.getId())) {
            srt.delete("HousekeepingProduct" + oldmsg.getId());
            log.info("redis修改删除成功");
        }
        long l = 60 * 60;
        srt.opsForValue().set("HousekeepingProduct" + oldmsg.getId(), s, l, TimeUnit.SECONDS);
        log.info("redis修改新增成功");
        //存日记
        String ss = "修改家政商品，更改前详细情况：" + s1;
        HousekeepingProductLog addlog = NewLog.addlog(oldmsg.getId(), oldmsg.getName(), ss, HousekeepingProductLog::new);
        System.out.println(addlog);
        hpls.save(addlog);
        return newmsg;
    }

    //查询所有方法
    @Override
    public List<HousekeepingProduct> findHouseKeeping(AllHouseKeeping allHouseKeeping) {
        if (allHouseKeeping.getNum() == null || allHouseKeeping.getPage() == null)
            throw new ProductException(IntegrationProductClassifyEnum.CODE_NULL);
        //如果不是空，分页查询
        QueryWrapper<HousekeepingProduct> wrapper = new QueryWrapper<>();
        wrapper.eq(allHouseKeeping.getId() != null, "id", allHouseKeeping.getId())
                .like(allHouseKeeping.getName() != null, "name", allHouseKeeping.getName())
                .eq(allHouseKeeping.getPrice() != null, "price", allHouseKeeping.getPrice())
                .like(allHouseKeeping.getDescription() != null, "description", allHouseKeeping.getDescription())
                .eq(allHouseKeeping.getPictureUrl() != null, "picture_url", allHouseKeeping.getPictureUrl())
                .eq(allHouseKeeping.getAttention() != null, "attention", allHouseKeeping.getAttention())
                .eq(allHouseKeeping.getStatus() != null, "status", allHouseKeeping.getStatus());
        Page<HousekeepingProduct> page = new Page<>(allHouseKeeping.getPage(), allHouseKeeping.getNum());
        Page<HousekeepingProduct> page1 = hpm.selectPage(page, wrapper);
        List<HousekeepingProduct> records = page1.getRecords();
        if (records == null || records.size() == 0) throw new ProductException(IntegrationProductClassifyEnum.SQL_NULL);
        //存redis中
        String s = ObjectMapperUtil.parseObject(records);
        long l = 60 * 60;
        srt.opsForValue().set("HousekeepingProductall", s, l, TimeUnit.SECONDS);
        log.info("redis查询成功");
        //不进log
        return records;
    }

    //根据id查询信息
    @Override
    public HousekeepingProduct findHouseKeepingByid(Integer id) {
        //根据id查询
        HousekeepingProduct msg = hpm.selectById(id);
        if (msg == null) throw new ProductException(IntegrationProductClassifyEnum.SQL_NULL);
        //存redis中
        String s = ObjectMapperUtil.parseObject(msg);
        srt.opsForValue().set("HousekeepingProduct" + id, s);
        return msg;
    }

    //查询家政商品日记接口
    @Override
    public HousekeepingProductLogDto selectHouseKeeingLog(HouseKeepingProductLogForm houseKeepingProductLogForm) {
        //分页
        QueryWrapper<HousekeepingProductLog> housekeepingProductLogQueryWrapper = new QueryWrapper<>();
        housekeepingProductLogQueryWrapper.eq(houseKeepingProductLogForm.getProductId() != null, "product_id", houseKeepingProductLogForm.getProductId())
                .like(houseKeepingProductLogForm.getProductName() != null, "product_name", houseKeepingProductLogForm.getProductName())
                .like(houseKeepingProductLogForm.getLogReason() != null, "log_reason", houseKeepingProductLogForm.getLogReason())
                .eq(houseKeepingProductLogForm.getLogType() != null, "log_type", houseKeepingProductLogForm.getLogType())
                .ge(houseKeepingProductLogForm.getMincreateTime() != null, "create_time", houseKeepingProductLogForm.getMincreateTime())
                .le(houseKeepingProductLogForm.getMaxcreateTime() != null, "create_time", houseKeepingProductLogForm.getMaxcreateTime());
        Page<HousekeepingProductLog> housekeepingProductLogPage = new Page<>(houseKeepingProductLogForm.getPage(), houseKeepingProductLogForm.getNum());
        Page<HousekeepingProductLog> housekeepingProductLogPage1 = housekeepingProductLogMapper.selectPage(housekeepingProductLogPage, housekeepingProductLogQueryWrapper);
        //判断结果
        if (housekeepingProductLogPage1.getRecords() == null) throw new ProductException(IntegrationProductClassifyEnum.SQL_NULL);
        List<HousekeepingProductLog> records = housekeepingProductLogPage1.getRecords();
        //返回参数
        HousekeepingProductLogDto housekeepingProductLogDto = BeanCopyUtil.copyObject(houseKeepingProductLogForm, HousekeepingProductLogDto::new);
        housekeepingProductLogDto.setHousekeepingProductLogs(records);
        housekeepingProductLogDto.setNum(houseKeepingProductLogForm.getNum());
        housekeepingProductLogDto.setNum(houseKeepingProductLogForm.getPage());
        housekeepingProductLogDto.setTotal(housekeepingProductLogPage1.getTotal());
        log.info("商品日记查询业务执行成功");
        //存redis
        String s = ObjectMapperUtil.parseObject(housekeepingProductLogDto);
        String s1 = ObjectMapperUtil.parseObject(houseKeepingProductLogForm);
        srt.opsForValue().set(s1,s,60*60,TimeUnit.SECONDS);
        log.info("商品日记查询redis存储成功");
        return housekeepingProductLogDto;
    }
}
