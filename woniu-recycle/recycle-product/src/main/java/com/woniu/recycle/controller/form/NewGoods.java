package com.woniu.recycle.controller.form;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@ApiModel("新建商品属性form")
public class NewGoods {
    @ApiModelProperty("名字")
    @NotNull
    private String name;

    @ApiModelProperty(value = "积分", example = "1.0")
    @NotNull
    private Integer price;

    @ApiModelProperty(value = "类别id", example = "1")
    @NotNull
    private Integer classifyId;

    @ApiModelProperty("商品描述")
    @NotNull
    private String description;

    @ApiModelProperty("图片url")
    @NotNull
    private String pictureUrl;

    @ApiModelProperty(value = "商品库存",example = "1")
    @NotNull
    private Integer inventory;

    @ApiModelProperty(value = "状态", example = "1")
    @NotNull
    private Integer status;

}
