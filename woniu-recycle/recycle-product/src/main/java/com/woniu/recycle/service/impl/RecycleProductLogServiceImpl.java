package com.woniu.recycle.service.impl;

import com.woniu.recycle.domain.RecycleProductLog;
import com.woniu.recycle.mapper.RecycleProductLogMapper;
import com.woniu.recycle.service.RecycleProductLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 回收商品日志信息表 服务实现类
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-12
 */
@Service
public class RecycleProductLogServiceImpl extends ServiceImpl<RecycleProductLogMapper, RecycleProductLog> implements RecycleProductLogService {

}
