package com.woniu.recycle.controller.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 *
 */
@ApiModel("新增家政商品form")
@Data
public class NewHouseKeeping {

    @NotNull
    @ApiModelProperty(value = "商品名称")
    private String name;

    @ApiModelProperty(value = "价格")
    private BigDecimal price;

    @ApiModelProperty(value = "商品描述")
    private String description;

    @ApiModelProperty(value = "商品图片地址")
    private String pictureUrl;

    @ApiModelProperty(value = "注意事项")
    private String attention;

    @ApiModelProperty(value = "上下架状态 0 下架 | 1 上架", example = "1")
    private Integer status;

}
