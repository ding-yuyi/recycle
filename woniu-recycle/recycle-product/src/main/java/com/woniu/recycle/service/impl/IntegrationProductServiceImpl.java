package com.woniu.recycle.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;
import com.woniu.recycle.commons.web.util.BeanCopyUtil;
import com.woniu.recycle.commons.web.util.ObjectMapperUtil;
import com.woniu.recycle.controller.dto.HousekeepingProductLogDto;
import com.woniu.recycle.controller.dto.IntegrationProductDto;
import com.woniu.recycle.controller.dto.IntegrationProductLogDto;
import com.woniu.recycle.controller.form.*;
import com.woniu.recycle.domain.HousekeepingProductLog;
import com.woniu.recycle.exception.ProductException;
import com.woniu.recycle.domain.IntegrationProduct;
import com.woniu.recycle.mapper.IntegrationProductLogMapper;
import com.woniu.recycle.mapper.IntegrationProductMapper;
import com.woniu.recycle.domain.IntegrationProductLog;
import com.woniu.recycle.service.IntegrationProductLogService;
import com.woniu.recycle.service.IntegrationProductService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.recycle.util.IntegrationProductClassifyEnum;
import com.woniu.recycle.util.NewLog;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 积分商品表 服务实现类
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-12
 */
@Service
@Slf4j
@Transactional
public class IntegrationProductServiceImpl extends ServiceImpl<IntegrationProductMapper, IntegrationProduct> implements IntegrationProductService {


    @Resource
    IntegrationProductMapper ipm;
    @Resource
    StringRedisTemplate srt;
    @Resource
    IntegrationProductLogService ipls;
    @Resource
    IntegrationProductLogMapper integrationProductLogMapper;

    //查询所有商品实现类
    @Override
    public IntegrationProductDto allProduct(AllProduct findid) {
        if (findid == null) throw new ProductException(IntegrationProductClassifyEnum.CODE_NULL);
        Page<IntegrationProduct> objectPage = new Page<>();
        //第几页数：
        objectPage.setCurrent(findid.getPage());
        //每页条数：
        objectPage.setSize(findid.getNum());
        Page<IntegrationProduct> page = page(objectPage, null);
        IntegrationProductDto integrationProductDto = BeanCopyUtil.copyObject(page, IntegrationProductDto::new);
        List<IntegrationProduct> records = page.getRecords();
        integrationProductDto.setIntegrationProducts(records);
        if (records == null || records.size() == 0)
            throw new ProductException(IntegrationProductClassifyEnum.SQL_ERROR);
        log.info("查询结果是{}", records);
        //存reids
        long l = 60 * 60;
        srt.opsForValue().set("IntegrationProduct" + "page" + findid.getPage() + "num" + findid.getNum().toString(), String.valueOf(records), l, TimeUnit.SECONDS);
        log.info("redis存储成功");
        //查询不进入日记
        return integrationProductDto;
    }

    //新增商品实现类
    @Override
    public List<IntegrationProduct> newGoods(NewGoods[] ng) {
        //判断是否为空
        if (ng == null || ng.length == 0) throw new ProductException(IntegrationProductClassifyEnum.CODE_NULL);
        //不为空时，进行逻辑处理，数组：
        List dtoList = Lists.newArrayList();
        for (int i = 0; i < ng.length; i++) {
            QueryWrapper<IntegrationProduct> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("name", ng[i].getName());
            IntegrationProduct integrationProduct = ipm.selectOne(queryWrapper);
            if (integrationProduct != null) throw new ProductException("仓库已存在" + ng[i].getName() + "商品", 1007);
            //如果仓库没有，则执行新增操作
            IntegrationProduct integrationProduct1 = new IntegrationProduct();
            integrationProduct1.setName(ng[i].getName());
            integrationProduct1.setClassifyId(ng[i].getClassifyId());
            integrationProduct1.setPrice(ng[i].getPrice());
            integrationProduct1.setDescription(ng[i].getDescription());
            integrationProduct1.setPictureUrl(ng[i].getPictureUrl());
            integrationProduct1.setInventory(ng[i].getInventory());
            integrationProduct1.setStatus(ng[i].getStatus());
            ipm.insert(integrationProduct1);
            //新增后再查询id
            IntegrationProduct integrationProduct2 = ipm.selectOne(queryWrapper);
            log.info("sql新增成功{}", ng[i]);
            boolean add = dtoList.add(ng[i]);
            if (!add) throw new ProductException(IntegrationProductClassifyEnum.CODE_ERROR);
            //存储到redis中.时间1小时cd
            long l = 60 * 60;
            srt.opsForValue().set("IntegrationProduct" + integrationProduct1.getId().toString(), String.valueOf(integrationProduct1), l, TimeUnit.SECONDS);
            log.info("redis新增成功");
            //存储日记
            IntegrationProductLog integrationProductLog = new IntegrationProductLog();
            integrationProductLog.setProductId(integrationProduct2.getId());
            integrationProductLog.setProductName(integrationProduct2.getName());
            integrationProductLog.setLogReason(String.valueOf(IntegrationProductClassifyEnum.NEW_GOODS));
            integrationProductLog.setLogType(IntegrationProductClassifyEnum.ADMIN.ordinal());
            integrationProductLog.setCreateTime(l);
            //TODO  后期获取userid
            integrationProductLog.setUserId(1);
            boolean save = ipls.save(integrationProductLog);
            System.out.println(integrationProductLog.toString());
            if (!save) throw new ProductException(IntegrationProductClassifyEnum.LOG_ERROR);
            log.info(integrationProductLog.toString());
        }
        return dtoList;
    }

    //删除商品实现类
    @Override
    public DeleteProduct deleteProduct(@Valid DeleteProduct delteProduct) {
        //判断非空
        if (delteProduct == null) throw new ProductException(IntegrationProductClassifyEnum.CODE_NULL);
        int i = ipm.deleteById(delteProduct.getId());
        //删除数据库
        if (i <= 0) {
            throw new ProductException(IntegrationProductClassifyEnum.SQL_ERROR);
        }
        log.info("删除的商品id是：{}", delteProduct.getId());
        //删除reids
        Boolean aBoolean = srt.hasKey("IntegrationProduct" + delteProduct.getId());
        if (aBoolean) {
            srt.delete("IntegrationProduct" + delteProduct.getId());
            log.info("redis删除商品id：{}", delteProduct.getId());
        }
        //记录日记
        IntegrationProductLog integrationProductLog = new IntegrationProductLog();
        integrationProductLog.setProductId(delteProduct.getId());
        integrationProductLog.setProductName(delteProduct.getName());
        integrationProductLog.setLogReason(String.valueOf(IntegrationProductClassifyEnum.DELETE_GOODS));
        integrationProductLog.setLogType(IntegrationProductClassifyEnum.ADMIN.ordinal());
        //获取当前时间
        long l = System.currentTimeMillis();
        integrationProductLog.setCreateTime(l);
        //TODO 后期获取userid
        integrationProductLog.setUserId(1);
        boolean save = ipls.save(integrationProductLog);
        System.out.println(integrationProductLog.toString());
        if (!save) throw new ProductException(IntegrationProductClassifyEnum.LOG_ERROR);
        log.info(integrationProductLog.toString());
        return delteProduct;
    }

    //更改商品信息实现类
    @Override
    public ChangeProduct changeProduct(ChangeProduct changeProduct) {
        //判断非空
        if (changeProduct == null) throw new ProductException(IntegrationProductClassifyEnum.CODE_NULL);
        //业务逻辑
        //查询更新前的商品信息
        IntegrationProduct oldintegrationProduct = ipm.selectById(changeProduct.getId());
        IntegrationProduct integrationProduct = BeanCopyUtil.copyObject(changeProduct, IntegrationProduct::new);
        int i = ipm.updateById(integrationProduct);
        if (i <= 0) throw new ProductException(IntegrationProductClassifyEnum.SQL_ERROR);
        //查询更新后的商品信息
        IntegrationProduct newintegrationProduct = ipm.selectById(changeProduct.getId());
        //redis操作，删除老redis新增新reids
        srt.delete("IntegrationProduct" + changeProduct.getId());
        long l = 60 * 60;
        srt.opsForValue().set("IntegrationProduct" + changeProduct.getId(), newintegrationProduct.toString(), l, TimeUnit.SECONDS);
        //新增商品修改日记
        IntegrationProductLog integrationProductLog = new IntegrationProductLog();
        integrationProductLog.setProductId(changeProduct.getId());
        integrationProductLog.setProductName(changeProduct.getName());
        integrationProductLog.setLogReason(IntegrationProductClassifyEnum.CHANGE_GOODS + "oldmsg:" + oldintegrationProduct);
        integrationProductLog.setLogType(IntegrationProductClassifyEnum.ADMIN.ordinal());
        System.out.println(IntegrationProductClassifyEnum.ADMIN.ordinal());
        //获取当前时间
        integrationProductLog.setCreateTime(l);
        //TODO 后期获取userid
        integrationProductLog.setUserId(1);
        boolean save = ipls.save(integrationProductLog);
        System.out.println(integrationProductLog.toString());
        if (!save) throw new ProductException(IntegrationProductClassifyEnum.LOG_ERROR);
        log.info(integrationProductLog.toString());
        return changeProduct;
    }

    //改变商品状态实现类(上下架)
    @Override
    public ChangeStatus changeStatus(ChangeStatus changeStatus) {
        //判断非空
        if (changeStatus == null) throw new ProductException(IntegrationProductClassifyEnum.CODE_NULL);
        //根据id查询
        IntegrationProduct integrationProduct = ipm.selectById(changeStatus.getId());
        if (integrationProduct == null) throw new ProductException(IntegrationProductClassifyEnum.SQL_ERROR);
        //根据id进行修改状态（上架商品）
        integrationProduct.setStatus(changeStatus.getStatus());
        QueryWrapper<IntegrationProduct> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("id", changeStatus.getId());
        int update = ipm.update(integrationProduct, queryWrapper);
        if (update <= 0) throw new ProductException(IntegrationProductClassifyEnum.SQL_ERROR);
        //查询新状态的商品
        IntegrationProduct newintegrationProduct = ipm.selectById(changeStatus.getId());
        //存redis
        srt.delete("IntegrationProduct" + changeStatus.getId());
        long l = 60 * 60;
        srt.opsForValue().set("IntegrationProduct" + changeStatus.getId(), newintegrationProduct.toString(), l, TimeUnit.SECONDS);
        //存日记
        IntegrationProductLog integrationProductLog = new IntegrationProductLog();
        integrationProductLog.setProductId(changeStatus.getId());
        integrationProductLog.setProductName(integrationProduct.getName());
        integrationProductLog.setLogReason(IntegrationProductClassifyEnum.CHANGE_STATUS + "oldmsg:" + integrationProduct);
        integrationProductLog.setLogType(IntegrationProductClassifyEnum.ADMIN.ordinal());
        System.out.println(IntegrationProductClassifyEnum.ADMIN.ordinal());
        //获取当前时间
        integrationProductLog.setCreateTime(l);
        //TODO 后期获取userid
        integrationProductLog.setUserId(1);
        boolean save = ipls.save(integrationProductLog);
        System.out.println(integrationProductLog.toString());
        if (!save) throw new ProductException(IntegrationProductClassifyEnum.LOG_ERROR);
        log.info(integrationProductLog.toString());
        return changeStatus;
    }

    //根据id查询商品实现类
    @Override
    public IntegrationProduct selectById(Integer productId) {
        IntegrationProduct integrationProduct = ipm.selectById(productId);
        if (integrationProduct == null) throw new ProductException(IntegrationProductClassifyEnum.SQL_NULL);
        if (integrationProduct.getInventory() <= 0)
            throw new ProductException(IntegrationProductClassifyEnum.INVENTOR_ERROR);
        //存redis中
        long l = 60 * 60;
        srt.opsForValue().set("IntegrationProduct" + productId, String.valueOf(integrationProduct), l, TimeUnit.SECONDS);
        log.info("redis查询新增成功", "IntegrationProduct" + productId);
        //不进日记
        return integrationProduct;
    }

    //倒序分页查询实现类
    @Override
    public IntegrationProductDto descSelectAll(AllProduct findid) {
        //判断非空
        if (findid == null) throw new ProductException(IntegrationProductClassifyEnum.CODE_NULL);
        IntegrationProductDto integrationProducts = allProduct(findid);
        if (integrationProducts == null) throw new ProductException(IntegrationProductClassifyEnum.SQL_NULL);
        //倒序输出
        //integrationProducts.sort(Comparator.comparing(IntegrationProduct::getId).reversed().thenComparing());
        Collections.reverse(integrationProducts.getIntegrationProducts());
        //存redis
        long l = 60 * 60;
        srt.opsForValue().set("IntegrationProductDESC" + "page" + findid.getPage() + "num" + findid.getNum().toString(), String.valueOf(integrationProducts), l, TimeUnit.SECONDS);
        log.info("redis存储成功");
        //不进日记
        return integrationProducts;
    }

    //根据价格区间查询数据实现类
    @Override
    public List<IntegrationProduct> selectPrice(SelectPrice selectPrice) {
        //判断参数：
        if (selectPrice == null) throw new ProductException(IntegrationProductClassifyEnum.CODE_NULL);
        //判断参数的合理性
        QueryWrapper<IntegrationProduct> queryWrapper = new QueryWrapper<>();
        //最大值小于最小值
        if (selectPrice.getMaxprice() < selectPrice.getMinprice())
            throw new ProductException(IntegrationProductClassifyEnum.NUM_ERROR);
        //最大最小值都为0的时候默认查询所有
        if (selectPrice.getMinprice() == 0 && selectPrice.getMaxprice() == 0) {
            //分页查询所有
            Page<IntegrationProduct> objectPage = new Page<>(selectPrice.getPage(), selectPrice.getNum());
            Page<IntegrationProduct> page = page(objectPage);
            List<IntegrationProduct> integrationProducts = page.getRecords();
            if (integrationProducts == null) throw new ProductException(IntegrationProductClassifyEnum.SQL_NULL);
            //存进redis
            srt.opsForValue().set("IntegrationProduct" + "max" + selectPrice.getMaxprice() + "min" + selectPrice.getMinprice(), String.valueOf(integrationProducts));
            return integrationProducts;
        }
        //最大值等于最小值时候按值查询一个值，不用分页
        if (selectPrice.getMinprice() == selectPrice.getMaxprice()) {
            queryWrapper.eq("price", selectPrice.getMaxprice());
            List<IntegrationProduct> integrationProducts = ipm.selectList(queryWrapper);
            if (integrationProducts == null) throw new ProductException(IntegrationProductClassifyEnum.SQL_NULL);
            //存进redis
            srt.opsForValue().set("IntegrationProduct" + "max" + selectPrice.getMaxprice() + "min" + selectPrice.getMinprice(), String.valueOf(integrationProducts));
            return integrationProducts;
        }
        //最大最小有区间时
        //ge:大于等于，le：小于等于
        queryWrapper
                .le("price", selectPrice.getMaxprice())
                .ge("price", selectPrice.getMinprice());
        List<IntegrationProduct> integrationProducts = ipm.selectList(queryWrapper);
        if (integrationProducts == null) throw new ProductException(IntegrationProductClassifyEnum.SQL_NULL);
        //存进redis
        srt.opsForValue().set("IntegrationProduct" + "max" + selectPrice.getMaxprice() + "min" + selectPrice.getMinprice(), String.valueOf(integrationProducts));
        return integrationProducts;

    }

    //根据价格区间查询数据实现类，倒序查询
    @Override
    public List<IntegrationProduct> selectPriceDesc(SelectPrice selectPrice) {
        List<IntegrationProduct> integrationProducts = selectPrice(selectPrice);
        integrationProducts.sort(Comparator.comparing(IntegrationProduct::getPrice).reversed());
        return integrationProducts;
    }

    //根据num修改商品库存//加库存
    @Override
    public IntegrationProduct changeInventor(ChangeProductNum changeProductNum) {
        //判断
        if (changeProductNum.getInventory() == null)
            throw new ProductException(IntegrationProductClassifyEnum.CODE_NULL);
        //修改库存//新增
        IntegrationProduct oldmsg = ipm.selectById(changeProductNum.getId());
        Integer oldnum = oldmsg.getInventory();
        changeProductNum.setInventory(oldnum + changeProductNum.getInventory());
        IntegrationProduct msg = BeanCopyUtil.copyObject(changeProductNum, IntegrationProduct::new);
        int i = ipm.updateById(msg);
        IntegrationProduct newmsg = ipm.selectById(changeProductNum.getId());
        if (i <= 0) throw new ProductException(IntegrationProductClassifyEnum.SQL_ERROR);
        //存redis
        String s = ObjectMapperUtil.parseObject(newmsg);
        if (srt.hasKey("IntegrationProduct" + changeProductNum.getId())) {
            srt.delete("IntegrationProduct" + changeProductNum.getId());
        }
        long l = 60 * 60;
        srt.opsForValue().set("IntegrationProduct" + changeProductNum.getId(), s, l, TimeUnit.SECONDS);
        //存日记
        String ss = "修改商品库存,新增库存：" + s;
        IntegrationProductLog addlog = new NewLog().addlog(newmsg.getId(), newmsg.getName(), ss, IntegrationProductLog::new);
        boolean save = ipls.save(addlog);
        if (!save) throw new ProductException(IntegrationProductClassifyEnum.LOG_ERROR);
        return newmsg;
    }

    @Override
    public IntegrationProduct changeInventorsel(ChangeProductNum changeProductNum) {
        //判断
        if (changeProductNum.getInventory() == null)
            throw new ProductException(IntegrationProductClassifyEnum.CODE_NULL);
        //修改库存//减
        IntegrationProduct oldmsg = ipm.selectById(changeProductNum.getId());
        Integer oldnum = oldmsg.getInventory();
        changeProductNum.setInventory(oldnum - changeProductNum.getInventory());
        if (changeProductNum.getInventory() < 0) throw new ProductException(IntegrationProductClassifyEnum.NUM_ERROR);
        IntegrationProduct msg = BeanCopyUtil.copyObject(changeProductNum, IntegrationProduct::new);
        int i = ipm.updateById(msg);
        IntegrationProduct newmsg = ipm.selectById(changeProductNum.getId());
        if (i <= 0) throw new ProductException(IntegrationProductClassifyEnum.SQL_ERROR);
        //存redis
        String s = ObjectMapperUtil.parseObject(newmsg);
        if (srt.hasKey("IntegrationProduct" + changeProductNum.getId())) {
            srt.delete("IntegrationProduct" + changeProductNum.getId());
        }
        long l = 60 * 60;
        srt.opsForValue().set("IntegrationProduct" + changeProductNum.getId(), s, l, TimeUnit.SECONDS);
        //存日记
        String ss = "修改商品库存,减少库存：" + s;
        IntegrationProductLog addlog = new NewLog().addlog(newmsg.getId(), newmsg.getName(), ss, IntegrationProductLog::new);
        boolean save = ipls.save(addlog);
        if (!save) throw new ProductException(IntegrationProductClassifyEnum.LOG_ERROR);
        return newmsg;
    }

    //积分商品查询日记实现类
    @Override
    public IntegrationProductLogDto selectIntegrationProductLog(IntegrationProductLogForm integrationProductLogForm) {
        //分页
        QueryWrapper<IntegrationProductLog> integrationProductLogQueryWrapper = new QueryWrapper<>();
        integrationProductLogQueryWrapper.eq(integrationProductLogForm.getProductId() != null, "product_id", integrationProductLogForm.getProductId())
                .like(integrationProductLogForm.getProductName() != null, "product_name", integrationProductLogForm.getProductName())
                .like(integrationProductLogForm.getLogReason() != null, "log_reason", integrationProductLogForm.getLogReason())
                .eq(integrationProductLogForm.getLogType() != null, "log_type", integrationProductLogForm.getLogType())
                .ge(integrationProductLogForm.getMincreateTime() != null, "create_time", integrationProductLogForm.getMincreateTime())
                .le(integrationProductLogForm.getMaxcreateTime() != null, "create_time", integrationProductLogForm.getMaxcreateTime());
        Page<IntegrationProductLog> integrationProductLogPage = new Page<>(integrationProductLogForm.getPage(), integrationProductLogForm.getNum());
        Page<IntegrationProductLog> integrationProductLogPage1 = integrationProductLogMapper.selectPage(integrationProductLogPage, integrationProductLogQueryWrapper);
        //判断结果
        if (integrationProductLogPage1.getRecords() == null) throw new ProductException(IntegrationProductClassifyEnum.SQL_NULL);
        List<IntegrationProductLog> records = integrationProductLogPage1.getRecords();
        //返回参数
        List<IntegrationProductLogForm> integrationProductLogForms = BeanCopyUtil.copyList(records, IntegrationProductLogForm::new);
        System.out.println("000"+integrationProductLogForms);

        IntegrationProductLogDto integrationProductLogDto = BeanCopyUtil.copyObject(integrationProductLogForm, IntegrationProductLogDto::new);
        integrationProductLogDto.setIntegrationProductLogForm(integrationProductLogForms);
        integrationProductLogDto.setPage(integrationProductLogForm.getPage());
        integrationProductLogDto.setNum(integrationProductLogForm.getNum());
        integrationProductLogDto.setTotal(integrationProductLogPage1.getTotal());
        log.info("商品日记查询业务执行成功");
        //存redis
        String s = ObjectMapperUtil.parseObject(integrationProductLogDto);
        String s1 = ObjectMapperUtil.parseObject(integrationProductLogDto);
        srt.opsForValue().set(s1, s, 60 * 60, TimeUnit.SECONDS);
        log.info("商品日记查询redis存储成功");
        return integrationProductLogDto;
    }
}

