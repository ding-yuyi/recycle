package com.woniu.recycle.controller.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel("更改积分商品种类")
public class ChangeProductClassify {

    @ApiModelProperty("老状态")
    @NotNull
    private String oldClassify;

    @ApiModelProperty("新状态")
    @NotNull
    private String newClassify;
}
