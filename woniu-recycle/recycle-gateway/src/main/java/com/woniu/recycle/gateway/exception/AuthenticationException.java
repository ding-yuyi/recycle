package com.woniu.recycle.gateway.exception;

import com.woniu.recycle.commons.core.exception.RecycleException;
import com.woniu.recycle.gateway.code.AuthenticationExceptionCode;

public class AuthenticationException extends RecycleException {

	private static final long serialVersionUID = 1L;
	
	public AuthenticationException(String msg,Integer code) {
		super(msg,code);
	}
	
	public AuthenticationException(String msg) {
		super(msg);
	}

	public AuthenticationException(AuthenticationExceptionCode code) {
		super(code.getMsg(),code.getCode());
	}

}
