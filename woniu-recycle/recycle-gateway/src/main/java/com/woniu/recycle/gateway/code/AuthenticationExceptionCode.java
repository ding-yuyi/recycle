package com.woniu.recycle.gateway.code;

/**
 * @PackageName:
 * @ClassName: AuthenticationExceptionCode
 * @Description: 异常枚举
 * @author: 丁予一
 * @date: 2021/8/6 18:08
 */
public enum AuthenticationExceptionCode {
    PARSE_ERROR("解析错误",2001),
    NO_AUTHORITY("没有权限",2002),
    NOT_LOGIN("没有登陆",2003);
    private String msg;

    private Integer code;

    AuthenticationExceptionCode(String msg, Integer code) {
        this.msg = msg;
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public Integer getCode() {
        return code;
    }

}
