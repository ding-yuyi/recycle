package com.woniu.recycle.gateway.filter;

import java.util.List;

import com.woniu.recycle.commons.core.util.JwtTemplate;
import com.woniu.recycle.gateway.code.AuthenticationExceptionCode;
import com.woniu.recycle.gateway.exception.AuthenticationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;


import io.jsonwebtoken.Claims;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;

/**
 * @ClassName AuthenticationFilter
 * @Description 权限过滤
 * @Author DingYuyi
 * @Date 2021/8/11 18:48
 * @Version 1.0
 */
@Component
@Slf4j
public class AuthenticationFilter implements GlobalFilter {
	
	@Resource
	private JwtTemplate jwtTemplate;
	
	@Autowired
	private StringRedisTemplate stringRedisTemplate;

	@Override
	public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
		String url = exchange.getRequest().getPath().toString();
		log.info("请求路径:{}",url);
		if(url.contains("/auth")) {
			//认证解析
			List<String> rs = exchange.getRequest().getHeaders().get("auth_token");
			if(rs == null || rs.size() <= 0) throw new AuthenticationException(AuthenticationExceptionCode.NOT_LOGIN);
			String token = rs.get(0);
			Integer userId;
			try {
				Claims claims = jwtTemplate.parseJwt(token);
				userId = (Integer)claims.get("userId");
			}catch(Exception e) {
				throw new AuthenticationException(AuthenticationExceptionCode.PARSE_ERROR);
			}
			log.info("账户ID,{}",userId);
			//授权解析
			String myRule = stringRedisTemplate.opsForHash().get("user:id:" + userId, "role").toString();
			log.info("查询权限:,{}",myRule);
			if(!stringRedisTemplate.opsForSet().isMember("user:role:" + myRule, url)) {
				throw new AuthenticationException(AuthenticationExceptionCode.NO_AUTHORITY);
			}
			
		}
		return chain.filter(exchange);
	}
}
