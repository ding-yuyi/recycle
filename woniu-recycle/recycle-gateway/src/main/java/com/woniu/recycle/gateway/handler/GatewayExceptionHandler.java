package com.woniu.recycle.gateway.handler;

import com.woniu.recycle.commons.core.exception.RecycleException;
import com.woniu.recycle.commons.core.model.ResponseEntity;
import org.springframework.boot.web.reactive.error.ErrorWebExceptionHandler;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;

/**
 * @ClassName GatewayExceptionHandler
 * @Description 异常处理
 * @Author DingYuyi
 * @Date 2021/8/11 18:48
 * @Version 1.0
 */
@Component
@Slf4j
public class GatewayExceptionHandler implements ErrorWebExceptionHandler {

	@Resource
	private ObjectMapper objectMapper;

	@Override
	public Mono<Void> handle(ServerWebExchange exchange, Throwable ex) {
		log.error("出现异常: ",ex);
		//设置响应格式为json
		exchange.getResponse().getHeaders().setContentType(MediaType.APPLICATION_JSON);

		return exchange.getResponse().writeWith(Mono.fromSupplier(() -> {
			ResponseEntity<String> responseEntity = ResponseEntity.BuildError(String.class);
			if(ex instanceof RecycleException) {
				RecycleException recycleException = (RecycleException)ex;
				responseEntity.setCode(recycleException.getCode()).setMsg(recycleException.getMessage()).setData(ex.getClass().getName());
			} else {
				responseEntity.setMsg(ex.getMessage() == null ? "出现未知异常" : ex.getMessage()).setData(ex.getClass().getName());
			}
			byte[] result = null;
			try {
				result = objectMapper.writeValueAsBytes(responseEntity);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
			return exchange.getResponse().bufferFactory().wrap(result);
		}));
	}
}
