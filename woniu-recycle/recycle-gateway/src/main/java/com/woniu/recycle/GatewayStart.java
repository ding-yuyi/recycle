package com.woniu.recycle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ClassName GatewayStart
 * @Description
 * @Author DingYuyi
 * @Date 2021/8/11 18:48
 * @Version 1.0
 */
@SpringBootApplication
public class GatewayStart {
    public static void main(String[] args) {
        SpringApplication.run(GatewayStart.class,args);
    }
}
