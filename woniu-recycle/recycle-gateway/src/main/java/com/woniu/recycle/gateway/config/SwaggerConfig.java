package com.woniu.recycle.gateway.config;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;

@Component
public class SwaggerConfig implements SwaggerResourcesProvider {

	@Override
	public List<SwaggerResource> get() {
		return Arrays.asList(
				createResource("积分商品订单服务","2.0","/integration/v2/api-docs"),
				createResource("回收服务","2.0","/recycle/v2/api-docs"),
				createResource("商品服务","2.0","/product/v2/api-docs"),
				createResource("用户服务","2.0","/user/v2/api-docs"),
				createResource("家政服务","2.0","/housekeeping/v2/api-docs")
		);
	}
	
	private SwaggerResource createResource(String name,String version,String addr) {
		SwaggerResource swaggerResource = new SwaggerResource();
		swaggerResource.setName(name);
		swaggerResource.setSwaggerVersion(version);
		swaggerResource.setLocation(addr);
		return swaggerResource;
	}

}
