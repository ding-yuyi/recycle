package com.woniu.recycle.recycle.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.recycle.recycle.component.RecycleTaskEnum;
import com.woniu.recycle.recycle.component.UploadFileEnum;
import com.woniu.recycle.recycle.domain.RecycleTaskInfo;
import com.woniu.recycle.recycle.exception.RecycleTaskException;
import com.woniu.recycle.recycle.mapper.RecycleTaskInfoMapper;
import com.woniu.recycle.recycle.service.RecycleTaskInfoService;
import com.woniu.recycle.recycle.util.OSSTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.InputStream;
import java.util.Random;

/**
 * <p>
 * 回收工单详情表 服务实现类
 * </p>
 *
 * @author wjh
 * @since 2021-08-12
 */
@Slf4j
@Service
public class RecycleTaskInfoServiceImpl extends ServiceImpl<RecycleTaskInfoMapper, RecycleTaskInfo> implements RecycleTaskInfoService {

    @Resource
    private OSSTemplate ossTemplate;

    /**
     * 确认废品工单时上传废品图片
     * @param file 上传的图片文件
     * @return 上传图片后的图片路径
     */
    @Override
    public String uploadPicture(MultipartFile file) {
        log.info("详情图片上传的参数：{}",file);
        if (file==null) throw new RecycleTaskException(RecycleTaskEnum.PARAM_ERROR);
        String filename = file.getOriginalFilename();
        InputStream inputStream =null;
        try {
           inputStream = file.getInputStream();
           log.info("图片上传inputStream：{}",inputStream);

        }catch (Exception e){
            throw new RecycleTaskException(UploadFileEnum.UPLOAD_FILE_ERROR);
        }
        log.info("设置图片类型");
        String fileType = filename.substring(filename.lastIndexOf("."));
        Random random = new Random();
        String str = "";
        for (int i = 0; i < 6; i++) {
            str = str + random.nextInt(10);
        }
        log.info("设置新的图片名称");
        String newFileName = System.currentTimeMillis()+str+fileType;
        String uploadUrl = ossTemplate.uploadFile("lyl-sava", newFileName, inputStream);
        log.info("最终生成的URL:{}",uploadUrl);
        return uploadUrl;
    }


}
