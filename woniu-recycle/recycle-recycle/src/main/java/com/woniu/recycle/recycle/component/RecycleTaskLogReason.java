package com.woniu.recycle.recycle.component;

import com.woniu.recycle.recycle.domain.RecycleTask;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @Title: RecycleTaskLogReason
 * @ProjectName woniu-recycle
 * @Description: 骑手工单日志原因
 * @Author wl
 * @Date 2021/8/17 16:30
 * @Version:1.0
 */
@Data
@AllArgsConstructor
public class RecycleTaskLogReason {
    private String reason;
    private RecycleTask task;
}
