package com.woniu.recycle.recycle.controller.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * @Title: QueryRecycleOrderForm
 * @ProjectName woniu-recycle
 * @Description: 查询废品回收订单传入的参数
 * @Author wl
 * @Date 2021/8/13 11:47
 * @Version:1.0
 */
@Data
public class QueryRecycleOrderByUserIdForm {

    //时间查询时的开始时间
    @ApiModelProperty(value = "开始时间")
    @Pattern(regexp = "^[1-9]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])\\s+(20|21|22|23|[0-1]\\d):[0-5]\\d:[0-5]\\d$",message = "时间格式：2021-08-01 10:00:00")
    private String startTime;

    //时间查询时的结束时间
    @ApiModelProperty(value = "结束时间")
    @Pattern(regexp = "^[1-9]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])\\s+(20|21|22|23|[0-1]\\d):[0-5]\\d:[0-5]\\d$",message = "时间格式：2021-08-01 10:00:00")
    private String finishTime;

    @ApiModelProperty(value = "订单状态 0 未收货 | 1 已收货（订单结束）| 2 已评价 | 3 已取消")
    private Integer status;

    //分页查询的页码数
    @ApiModelProperty(value = "当前页数")
    @NotNull(message = "页码不能为空")
    private Integer pageIndex;
    //分页查询的数量
    @ApiModelProperty(value = "每页展示数量")
    @NotNull(message = "每页数量不能为空")
    private Integer pageSize=10;
}
