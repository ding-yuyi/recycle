package com.woniu.recycle.recycle.service.impl;

import com.woniu.recycle.recycle.domain.RecycleOrderInfo;
import com.woniu.recycle.recycle.mapper.RecycleOrderInfoMapper;
import com.woniu.recycle.recycle.service.RecycleOrderInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 回收订单明细表 服务实现类
 * </p>
 *
 * @author cy/lyl
 * @since 2021-08-11
 */
@Service
public class RecycleOrderInfoServiceImpl extends ServiceImpl<RecycleOrderInfoMapper, RecycleOrderInfo> implements RecycleOrderInfoService {

}
