package com.woniu.recycle.recycle.component;

/**
 * @Title: RecycleTaskInfoEnum
 * @ProjectName woniu-recycle
 * @Description: TODO
 * @Author wl
 * @Date 2021/8/18 17:09
 * @Version:1.0
 */
public enum  RecycleTaskInfoEnum {
    RECYCLE_TASK_INFO_ERROR("废品详情上传失败",4060),
    TASK_INFO_NULL_ERROR("该工单详情不存在",4061);


    String msg;

    Integer code;

    public String getMsg() {
        return msg;
    }

    public Integer getCode() {
        return code;
    }

    RecycleTaskInfoEnum(String msg, Integer code){
        this.msg=msg;
        this.code=code;
    }

}
