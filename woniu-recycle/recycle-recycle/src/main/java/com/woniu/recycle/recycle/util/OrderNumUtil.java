package com.woniu.recycle.recycle.util;

import com.woniu.recycle.commons.web.util.DateUtil;

import java.util.Random;


public class OrderNumUtil {

    /**
     * 生成废品回收订单编号
     * @param classifyId
     * @return 订单编号
     */
    public static String getNum(Integer classifyId){
        String date = DateUtil.parseToTime(System.currentTimeMillis(), "yyyyMMddHHmmss");
        Random random = new Random();
        String str = "";
        for (int i = 0; i < 6; i++) {
            str = str + random.nextInt(10);
        }
        String orderNum = date + classifyId + str;
        return orderNum;
    }

}
