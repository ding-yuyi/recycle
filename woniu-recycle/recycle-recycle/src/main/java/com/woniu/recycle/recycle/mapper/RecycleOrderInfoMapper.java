package com.woniu.recycle.recycle.mapper;

import com.woniu.recycle.recycle.domain.RecycleOrderInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 回收订单明细表 Mapper 接口
 * </p>
 *
 * @author cy/lyl
 * @since 2021-08-11
 */
public interface RecycleOrderInfoMapper extends BaseMapper<RecycleOrderInfo> {

}
