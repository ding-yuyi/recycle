package com.woniu.recycle.recycle.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.recycle.recycle.controller.form.*;
import com.woniu.recycle.recycle.domain.RecycleTask;
import com.woniu.recycle.recycle.dto.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 回收工单表 服务类
 * </p>
 *
 * @author wjh
 * @since 2021-08-12
 */
public interface RecycleTaskService extends IService<RecycleTask> {

    /**
     * 创建骑手工单
     * @param createForm 创建骑手工单时传入的参数
     * @return 返回创建的骑手工单信息对象
     */
    public CreateRecycleTaskDTO createRiderTask(CreateRiderTaskForm createForm);

    /**
     * 查询骑手工单
     * @param queryForm 查询骑手工单时传入的参数
     * @return 返回查询的骑手工单信息对象
     */
    public QueryRecycleTaskResultDTO queryRiderTask(QueryRiderTaskForm queryForm);

    /**
     * 取消骑手工单
     * @param cancelForm 取消骑手工单时传入的参数
     * @return 返回取消的骑手工单信息对象
     */
    public CancelRecycleTaskDTO cancelRiderTask(CancelRecycleTaskForm cancelForm);

    /**
     * 派单（将工单派发给骑手）
     * @param sendTaskForm 派单时传入的参数
     * @return 返回派发给骑手的工单信息对象
     */
    public SendRecycleTaskDTO sendTask(SendTaskForm sendTaskForm);

    /**
     * 骑手确认到达用户地址
     * @param confirmArriveForm 骑手确认到达传入的参数
     * @return 返回骑手确认到达后的工单信息对象
     */
    public ConfirmRiderArriveDTO confirmArrive(ConfirmArriveForm confirmArriveForm);

    /**
     * 确认工单（确认废品重量、单价、总价、商品种类）
     * @param confirmTaskForm 确认工单传入的参数
     * @return 返回骑手废品回收后确认回收信息对象
     */
    public ConfirmRecycleTaskDTO confirmTask(ConfirmTaskForm confirmTaskForm, MultipartFile file);

    /**
     * 提交工单，完成工单
     * @param finishForm 完成工单时传入的参数
     * @return 返回完成工单的信息对象
     */
    public FinishRecycleTaskDTO finishTask(FinishRecycleTaskForm finishForm);
}
