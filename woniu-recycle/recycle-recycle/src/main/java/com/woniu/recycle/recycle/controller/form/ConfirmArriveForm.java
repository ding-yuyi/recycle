package com.woniu.recycle.recycle.controller.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @Title: ConfirmArriveForm
 * @ProjectName woniu-recycle
 * @Description: TODO
 * @Author wl
 * @Date 2021/8/18 11:06
 * @Version:1.0
 */
@Data
public class ConfirmArriveForm {
    @ApiModelProperty(value = "工单id")
    @NotNull(message = "工单id不能为空")
    private Integer id;

    @ApiModelProperty(value = "订单id")
    @NotNull(message = "订单id不能为空")
    private Integer orderId;


}
