package com.woniu.recycle.recycle.component;

/**
 * @Title: DeletedStatus
 * @ProjectName woniu-recycle
 * @Description: TODO
 * @Author wl
 * @Date 2021/8/13 17:22
 * @Version:1.0
 */
public class DeletedStatus {
    /**
     * 没有被删除
     */
    public static final Integer NOT_DELETED = 0;

    /**
     * 已被删除
     */
    public static final Integer IS_DELETED = 1;
}
