package com.woniu.recycle.recycle.controller.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @Title: CancelRecycleOrderForm
 * @ProjectName woniu-recycle
 * @Description: 取消废品回收订单传入的参数
 * @Author wl
 * @Date 2021/8/14 11:01
 * @Version:1.0
 */
@Data
public class CancelRecycleOrderForm {
    @ApiModelProperty(value = "订单id")
    @NotNull(message = "订单id不能为空")
    private Integer id;


    @ApiModelProperty(value = "取消原因")
    @NotBlank(message = "订单取消原因不能为空")
    private String cancelReason;

}
