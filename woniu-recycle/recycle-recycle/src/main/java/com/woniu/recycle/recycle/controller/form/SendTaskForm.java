package com.woniu.recycle.recycle.controller.form;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @Title: SendTaskForm
 * @ProjectName woniu-recycle
 * @Description: TODO
 * @Author wl
 * @Date 2021/8/18 10:08
 * @Version:1.0
 */
@Data
public class SendTaskForm {

    @ApiModelProperty(value = "工单id")
    @NotNull(message = "工单id不能为空")
    private Integer id;

    @ApiModelProperty(value = "骑手姓名")
    private String workerName;

    @ApiModelProperty(value = "骑手电话")
    private String workerPhone;

}
