package com.woniu.recycle.recycle.exception;

import com.woniu.recycle.commons.core.exception.RecycleException;
import com.woniu.recycle.recycle.component.RecycleOrderEnum;
import com.woniu.recycle.recycle.component.UploadFileEnum;

public class RearFileUploadException extends RecycleException {
    private static final long serialVersionUID=1L;

    public RearFileUploadException(String msg){
           super(msg);
    }
    public RearFileUploadException(String msg, Integer code){
          super(msg,code);
        }
    public RearFileUploadException(UploadFileEnum uploadFileEnum){
        super(uploadFileEnum.getMsg(),uploadFileEnum.getCode());
    }
}
