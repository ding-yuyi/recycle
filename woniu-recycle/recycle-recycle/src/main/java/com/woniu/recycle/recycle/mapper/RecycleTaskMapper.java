package com.woniu.recycle.recycle.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniu.recycle.recycle.domain.RecycleTask;

/**
 * <p>
 * 回收工单表 Mapper 接口
 * </p>
 *
 * @author wjh
 * @since 2021-08-12
 */
public interface RecycleTaskMapper extends BaseMapper<RecycleTask> {

}
