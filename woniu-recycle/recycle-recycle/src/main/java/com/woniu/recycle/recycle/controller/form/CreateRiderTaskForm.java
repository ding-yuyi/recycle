package com.woniu.recycle.recycle.controller.form;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Title: CreateRiderTaskForm
 * @ProjectName woniu-recycle
 * @Description: 创建骑手工单时传入的参数
 * @Author wl
 * @Date 2021/8/16 18:47
 * @Version:1.0
 */
@Data
public class CreateRiderTaskForm {
    @ApiModelProperty(value = "订单id")
    @NotNull(message = "订单id不能为空")
    private Integer orderId;



}
