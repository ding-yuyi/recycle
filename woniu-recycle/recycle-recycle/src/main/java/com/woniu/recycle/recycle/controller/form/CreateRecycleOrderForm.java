package com.woniu.recycle.recycle.controller.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * @Title: CreateRecycleOrderForm
 * @ProjectName woniu-recycle
 * @Description: 创建废品回收订单时传入的参数
 * @Author wl
 * @Date 2021/8/12 15:02
 * @Version:1.0
 */
@Data
public class CreateRecycleOrderForm {

    @ApiModelProperty(value = "商品id")
    private Integer productId;

    @ApiModelProperty(value = "用户电话号码")
    @Pattern(regexp = "^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\\d{8}$")
    private String userPhone;

    @ApiModelProperty(value = "地址id")
    @NotNull(message = "地址id不能为空")
    private Integer addrId;

    @ApiModelProperty(value = "商品类别id")
    private Integer classifyId;

    @ApiModelProperty(value = "商品类别名称")
    private String classifyName;

    @ApiModelProperty(value = "预约上门时间")
    @Pattern(regexp = "^[1-9]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])\\s+(20|21|22|23|[0-1]\\d):[0-5]\\d:[0-5]\\d$",message = "时间格式：2021-08-01 10:00:00")
    private String visitTime;

    @ApiModelProperty(value = "备注")
    private String notes;

    @NotNull
    @ApiModelProperty(value = "页数", example = "1")
    private Integer page;

    @NotNull
    @ApiModelProperty(value = "条数", example = "1")
    private Integer num;
}
