package com.woniu.recycle.recycle.component;

/**
 * @Title: UploadFileEnum
 * @ProjectName woniu-recycle
 * @Description: TODO
 * @Author wl
 * @Date 2021/8/18 15:23
 * @Version:1.0
 */
public enum  UploadFileEnum {
    UPLOAD_FILE_ERROR("文件上传失败",4099);


    String msg;

    Integer code;

    public String getMsg() {
        return msg;
    }

    public Integer getCode() {
        return code;
    }

    UploadFileEnum(String msg, Integer code){
        this.msg=msg;
        this.code=code;
    }
}
