package com.woniu.recycle.recycle.controller.form;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.woniu.recycle.recycle.domain.RecycleTaskInfo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;
import java.util.List;

/**
 * @Title: confirmTaskForm
 * @ProjectName woniu-recycle
 * @Description: TODO
 * @Author wl
 * @Date 2021/8/18 12:18
 * @Version:1.0
 */
@Data
public class ConfirmTaskForm {
    @ApiModelProperty(value = "工单id")
    @NotNull(message = "工单id不能为空")
    private Integer id;

    @ApiModelProperty(value = "订单id")
    @NotNull(message = "订单id不能为空")
    private Integer orderId;

    @ApiModelProperty(value = "产品类型id")
    private Integer productClassifyId;

    @ApiModelProperty(value = "产品类型")
    private String productClassify;

    @ApiModelProperty(value = "回收的废品详情")
    private RecycleTaskInfoForm recycleTaskInfoForm;

}
