package com.woniu.recycle.recycle.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniu.recycle.recycle.domain.RecycleOrderLog;

/**
 * <p>
 * 回收商品订单日志表 Mapper 接口
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-11
 */
public interface RecycleOrderLogMapper extends BaseMapper<RecycleOrderLog> {

}
