package com.woniu.recycle.recycle.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 回收订单明细表 前端控制器
 * </p>
 *
 * @author cy/lyl
 * @since 2021-08-11
 */
@RestController
@RequestMapping("/recycle-order-info")
public class RecycleOrderInfoController {

}

