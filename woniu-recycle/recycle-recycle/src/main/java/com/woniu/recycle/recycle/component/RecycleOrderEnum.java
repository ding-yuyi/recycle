package com.woniu.recycle.recycle.component;

/**
 * @Title: RecycleOrderEnum
 * @ProjectName woniu-recycle
 * @Description: TODO
 * @Author wl
 * @Date 2021/8/12 15:23
 * @Version:1.0
 */
public enum  RecycleOrderEnum {
    PARAM_ERROR("传入参数为空",4001),
    COPY_ERROR("对象复制失败",4002),
    ORDER_ERROR("下单失败",4003),
    ORDER_SEARCH_ERROR("订单查询失败",4004),
    ORDER_CANCEL_ERROR1("该订单不存在",4005),
    ORDER_CANCEL_ERROR2("订单取消失败",4006),
    ORDER_DELETED_ERROR("订单已删除",4007),
    ORDER_DELETED_FAIL_ERROR("订单已删除",4008),
    ORDER_CONFIRM_ERROR("订单确认失败",4009),
    USER_BALANCE_ERROR("用户余额新增失败",4010),
    ORDER_PAY_ERROR("订单未确认",4011);


    String msg;

    Integer code;

    public String getMsg() {
        return msg;
    }

    public Integer getCode() {
        return code;
    }

    RecycleOrderEnum(String msg, Integer code){
        this.msg=msg;
        this.code=code;
    }
}
