package com.woniu.recycle.recycle.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Title: QueryRecycleOrderLogResultDTO
 * @ProjectName woniu-recycle
 * @Description: TODO
 * @Author wl
 * @Date 2021/8/20 16:01
 * @Version:1.0
 */
@Data
public class QueryRecycleOrderLogResultDTO {

    @ApiModelProperty(value = "当前页面")
    private Long current;

    @ApiModelProperty(value = "每页数量")
    private Long size;

    @ApiModelProperty(value = "查询总数")
    private Long total;

    @ApiModelProperty(value = "查询订单的具体数据")
    private List<QueryRecycleOrderLogDTO> recycleTaskLogDTOList;
}
