package com.woniu.recycle;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @ClassName OrderStart
 * @Description TODO
 * @Author DingYuyi
 * @Date 2021/8/11 19:31
 * @Version 1.0
 */
@SpringBootApplication
@MapperScan("com.woniu.recycle.recycle.mapper")
@EnableFeignClients
public class RecycleStart {
    public static void main(String[] args) {
        SpringApplication.run(RecycleStart.class,args);
    }
}
