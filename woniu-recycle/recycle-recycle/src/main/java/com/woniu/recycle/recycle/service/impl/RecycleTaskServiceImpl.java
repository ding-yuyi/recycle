package com.woniu.recycle.recycle.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.recycle.commons.web.util.BeanCopyUtil;
import com.woniu.recycle.commons.web.util.DateUtil;
import com.woniu.recycle.commons.web.util.ServletUtil;
import com.woniu.recycle.recycle.component.LogType;
import com.woniu.recycle.recycle.component.RecycleTaskEnum;
import com.woniu.recycle.recycle.component.RecycleTaskInfoEnum;
import com.woniu.recycle.recycle.component.RecycleTaskLogEnum;
import com.woniu.recycle.recycle.controller.form.*;
import com.woniu.recycle.recycle.domain.RecycleOrder;
import com.woniu.recycle.recycle.domain.RecycleTask;
import com.woniu.recycle.recycle.domain.RecycleTaskInfo;
import com.woniu.recycle.recycle.domain.RecycleTaskLog;
import com.woniu.recycle.recycle.dto.*;
import com.woniu.recycle.recycle.exception.RecycleTaskException;
import com.woniu.recycle.recycle.exception.RecycleTaskLogException;
import com.woniu.recycle.recycle.feign.model.User;
import com.woniu.recycle.recycle.mapper.RecycleOrderMapper;
import com.woniu.recycle.recycle.mapper.RecycleTaskInfoMapper;
import com.woniu.recycle.recycle.mapper.RecycleTaskMapper;
import com.woniu.recycle.recycle.service.RecycleTaskInfoService;
import com.woniu.recycle.recycle.service.RecycleTaskLogService;
import com.woniu.recycle.recycle.service.RecycleTaskService;
import com.woniu.recycle.recycle.util.OrderNumUtil;
import com.woniu.recycle.recycle.util.UserUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 回收工单表 服务实现类
 * </p>
 *
 * @author wjh
 * @since 2021-08-12
 */
@Slf4j
@Service
public class RecycleTaskServiceImpl extends ServiceImpl<RecycleTaskMapper, RecycleTask> implements RecycleTaskService {

    @Resource
    private RecycleTaskMapper recycleTaskMapper;

    @Resource
    private RecycleOrderMapper recycleOrderMapper;

    @Resource
    private RecycleTaskLogService recycleTaskLogService;

    @Resource
    private RecycleTaskInfoMapper recycleTaskInfoMapper;

    @Resource
    private RecycleTaskInfoService recycleTaskInfoService;

    @Resource
    private UserUtil userUtil;



    @Override
    public CreateRecycleTaskDTO createRiderTask(CreateRiderTaskForm createForm) {
        log.info("创建骑手工单时传入的参数:{}",createForm);
        if (createForm==null) throw new RecycleTaskException(RecycleTaskEnum.PARAM_ERROR);
        RecycleOrder recycleOrder = recycleOrderMapper.selectById(createForm.getOrderId());
        //创建骑手工单
        RecycleTask recycleTask = new RecycleTask();
        //解析token，获取user信息
        User user = userUtil.getUser(ServletUtil.getRequest());
        Integer userId = user.getId();
        log.info("解析token，获取userId:{}",userId);

        //userId 通过登录token ！！！！！！！！！
        recycleTask.setUserId(userId);
        recycleTask.setUsername(recycleOrder.getUsername());
        recycleTask.setUserPhone(recycleOrder.getUserPhone());
        recycleTask.setAddress(recycleOrder.getAddrDesc());
        recycleTask.setVisitTime(recycleOrder.getVisitTime());
        //工单状态 0未派单 | 1已派单 | 2 上门中 | 3 收货中 | 4 回收中 | 5 完成 | 6 已取消
        recycleTask.setStatus(0);
        recycleTask.setOrderId(recycleOrder.getId());
        recycleTask.setCreateTime(System.currentTimeMillis());
        recycleTask.setProductClassifyId(recycleOrder.getClassifyId());
        recycleTask.setProductClassify(recycleOrder.getClassifyName());
        recycleTask.setTaskNum(OrderNumUtil.getNum(recycleOrder.getClassifyId()));

        boolean flag = save(recycleTask);
        if (!flag) throw new RecycleTaskException(RecycleTaskEnum.TASK_CREATE_ERROR);
        log.info("记录工单日志");
        RecycleTaskLog recycleTaskLog = new RecycleTaskLog();
        recycleTaskLog.setTaskId(recycleTask.getId());
        recycleTaskLog.setLogType(LogType.MANAGER);
        recycleTaskLog.setUserId(recycleTask.getUserId());
        recycleTaskLog.setCreateTime(System.currentTimeMillis());
        recycleTaskLog.setLogReason("reason:创建骑手工单");
        log.info("完成记录工单日志");

        boolean save = recycleTaskLogService.save(recycleTaskLog);
        if (!save) throw new RecycleTaskLogException(RecycleTaskLogEnum.RECYCLE_TASK_LOG_ERROR);
        //将工单对象复制到返回对象中
        CreateRecycleTaskDTO createRecycleTaskDTO = BeanCopyUtil.copyObject(recycleTask, CreateRecycleTaskDTO::new);

        if (createRecycleTaskDTO ==null) throw new RecycleTaskException(RecycleTaskEnum.COPY_ERROR);


        return createRecycleTaskDTO;
    }

    /**
     * 查询骑手工单
     * @param queryForm 查询骑手工单时传入的参数
     * @return 返回查询的骑手工单信息对象
     */
    @Override
    public QueryRecycleTaskResultDTO queryRiderTask(QueryRiderTaskForm queryForm) {
        log.info("查询骑手工单时传入的参数",queryForm);

        Page<RecycleTask> page = new Page<>(queryForm.getPageIndex(),queryForm.getPageSize());
        //解析token，获取user信息
        User user = userUtil.getUser(ServletUtil.getRequest());
        Integer userId = user.getId();
        log.info("解析token，获取userId:{}",userId);

        QueryWrapper<RecycleTask> queryWrapper = new QueryWrapper<>();
        //根据时间查询工单时 时间转换
        Long startTime =null;
        if (queryForm.getStartTime()!=null && queryForm.getStartTime()!=""){
            startTime = DateUtil.parse(queryForm.getStartTime(), "yyyy-MM-dd HH:mm:ss");
            log.info("查询开始时间",startTime);
        }
        Long finishTime =null;
        if (queryForm.getFinishTime()!=null && queryForm.getFinishTime()!=""){
            finishTime = DateUtil.parse(queryForm.getFinishTime(), "yyyy-MM-dd HH:mm:ss");
            log.info("查询结束时间",finishTime);
        }
        //条件构造器
        queryWrapper.eq("user_id",userId)
                .eq(queryForm.getStatus()!=null,"status",queryForm.getStatus())
                .ge(startTime!=null,"create_time",startTime)
                .le(finishTime!=null,"create_time",finishTime)
                .orderByDesc("create_time");
        Page<RecycleTask> selectPage = recycleTaskMapper.selectPage(page, queryWrapper);
        log.info("查询的结果：{}",selectPage.getRecords());
        if (selectPage==null||selectPage.getRecords().size()<0)
            throw new RecycleTaskException(RecycleTaskEnum.TASK_SEARCH_ERROR);
        QueryRecycleTaskResultDTO resultDTO = BeanCopyUtil.copyObject(selectPage, QueryRecycleTaskResultDTO::new);
        log.info("分页展示的工单数据:{}",resultDTO);

        List<QueryRecycleTaskDTO> queryRecycleTaskDTOS = BeanCopyUtil.copyList(selectPage.getRecords(), QueryRecycleTaskDTO::new);
        if (queryRecycleTaskDTOS ==null) throw new RecycleTaskException(RecycleTaskEnum.COPY_ERROR);
        log.info("查询到的工单信息：{}", queryRecycleTaskDTOS);

        resultDTO.setRecycleTaskDTOList(queryRecycleTaskDTOS);
        log.info("工单数据添加成功");

        return resultDTO;
    }

    /**
     * 取消骑手工单
     * @param cancelForm 取消骑手工单时传入的参数
     * @return 返回取消的骑手工单信息对象
     */
    @Override
    public CancelRecycleTaskDTO cancelRiderTask(CancelRecycleTaskForm cancelForm) {
        log.info("取消订单时传入的参数:{}",cancelForm);
        if (cancelForm==null) throw new RecycleTaskException(RecycleTaskEnum.PARAM_ERROR);

        RecycleOrder recycleOrder = recycleOrderMapper.selectById(cancelForm.getOrderId());

        RecycleTask recycleTask = recycleTaskMapper.selectById(cancelForm.getId());
        if (recycleTask.getStatus()==3) throw new RecycleTaskException("骑手收货中，不能取消");
        if (recycleTask.getStatus()==4) throw new RecycleTaskException("骑手回收中，不能取消");
        if (recycleTask.getStatus()==5) throw new RecycleTaskException("工单已完成，不能取消");
        if (recycleTask.getStatus()==6) throw new RecycleTaskException("工单已取消，不能取消");
        if (recycleTask==null) throw new RecycleTaskException(RecycleTaskEnum.TASK_NULL_ERROR);
        //工单状态 0未派单 | 1已派单 | 2 上门中 | 3 收货中 | 4 回收中 | 5 完成 | 6 已取消
        recycleTask.setStatus(6);
        recycleTask.setCancelReason(cancelForm.getCancelReason());
        recycleTask.setCancelTime(System.currentTimeMillis());

        QueryWrapper<RecycleTask> queryWrapper = new QueryWrapper<>();

        queryWrapper.eq("id",cancelForm.getId());
        int i = recycleTaskMapper.update(recycleTask, queryWrapper);
        if (i<=0) throw new RecycleTaskException(RecycleTaskEnum.TASK_CANCEL_ERROR2);

        log.info("记录工单取消日志");
        RecycleTaskLog recycleTaskLog = new RecycleTaskLog();
        recycleTaskLog.setTaskId(recycleTask.getId());
        recycleTaskLog.setLogType(LogType.MANAGER);
        recycleTaskLog.setUserId(recycleTask.getUserId());
        recycleTaskLog.setCreateTime(System.currentTimeMillis());
        recycleTaskLog.setLogReason("reason:取消骑手工单");
        log.info("完成记录工单日志");
        boolean save = recycleTaskLogService.save(recycleTaskLog);
        if (!save) throw new RecycleTaskLogException(RecycleTaskLogEnum.RECYCLE_TASK_LOG_ERROR);

        CancelRecycleTaskDTO cancelRecycleTaskDTO = BeanCopyUtil.copyObject(recycleTask, CancelRecycleTaskDTO::new);
        if (cancelRecycleTaskDTO==null) throw new RecycleTaskException(RecycleTaskEnum.COPY_ERROR);
        return cancelRecycleTaskDTO;
    }

    /**
     * 派单（将工单派发给骑手）
     * @param sendTaskForm 派单时传入的参数
     * @return 返回派发给骑手的工单信息对象
     */
    @Override
    public SendRecycleTaskDTO sendTask(SendTaskForm sendTaskForm) {
        log.info("派单时传入的参数：{}",sendTaskForm);
        if (sendTaskForm==null) throw new RecycleTaskException(RecycleTaskEnum.PARAM_ERROR);

        RecycleTask recycleTask = recycleTaskMapper.selectById(sendTaskForm.getId());
        if (recycleTask==null) throw new RecycleTaskException(RecycleTaskEnum.TASK_NULL_ERROR);

        //解析token，获取user信息
        User user = userUtil.getUser(ServletUtil.getRequest());
        Integer userId = user.getId();
        log.info("解析token，获取userId:{}",userId);

        recycleTask.setWorkerName(sendTaskForm.getWorkerName());
        recycleTask.setWorkerPhone(sendTaskForm.getWorkerPhone());
        recycleTask.setUserId(userId);

        int i = recycleTaskMapper.updateById(recycleTask);
        if (i<=0) throw new RecycleTaskException(RecycleTaskEnum.TASK_SEND_ERROR);
        SendRecycleTaskDTO sendRecycleTaskDTO = BeanCopyUtil.copyObject(recycleTask, SendRecycleTaskDTO::new);
        if (sendRecycleTaskDTO ==null) throw new RecycleTaskException(RecycleTaskEnum.COPY_ERROR);

        log.info("记录工单派单日志");
        RecycleTaskLog recycleTaskLog = new RecycleTaskLog();
        recycleTaskLog.setTaskId(recycleTask.getId());
        recycleTaskLog.setLogType(LogType.MANAGER);
        recycleTaskLog.setUserId(recycleTask.getUserId());
        recycleTaskLog.setCreateTime(System.currentTimeMillis());
        recycleTaskLog.setLogReason("reason:记录派单");
        log.info("完成记录工单日志");
        boolean save = recycleTaskLogService.save(recycleTaskLog);
        if (!save) throw new RecycleTaskLogException(RecycleTaskLogEnum.RECYCLE_TASK_LOG_ERROR);

        return sendRecycleTaskDTO;
    }

    /**
     * 骑手确认到达用户地址
     * @param confirmArriveForm 骑手确认到达传入的参数
     * @return 返回骑手确认到达后的工单信息对象
     */
    @Override
    public ConfirmRiderArriveDTO confirmArrive(ConfirmArriveForm confirmArriveForm) {
        log.info("骑手确认到达时传入的参数:{}",confirmArriveForm);
        if (confirmArriveForm==null) throw new RecycleTaskException(RecycleTaskEnum.PARAM_ERROR);

        RecycleTask recycleTask = recycleTaskMapper.selectById(confirmArriveForm.getId());
        if (recycleTask==null) throw new RecycleTaskException(RecycleTaskEnum.TASK_NULL_ERROR);

        recycleTask.setArriveTime(System.currentTimeMillis());
        //工单状态 0未派单 | 1已派单 | 2 上门中 | 3 收货中 | 4 回收中 | 5 完成 | 6 已取消
        recycleTask.setStatus(3);

        int i = recycleTaskMapper.updateById(recycleTask);
        if (i<=0) throw new RecycleTaskException(RecycleTaskEnum.TASK_CONFIRM_ARRIVE_ERROR);

        ConfirmRiderArriveDTO confirmRiderArriveDTO = BeanCopyUtil.copyObject(recycleTask, ConfirmRiderArriveDTO::new);
        if (confirmRiderArriveDTO ==null) throw new RecycleTaskException(RecycleTaskEnum.COPY_ERROR);

        log.info("记录骑手确认到达日志");
        RecycleTaskLog recycleTaskLog = new RecycleTaskLog();
        recycleTaskLog.setTaskId(recycleTask.getId());
        recycleTaskLog.setLogType(LogType.MANAGER);
        recycleTaskLog.setUserId(recycleTask.getUserId());
        recycleTaskLog.setCreateTime(System.currentTimeMillis());
        recycleTaskLog.setLogReason("reason:骑手到达确认");
        log.info("完成记录工单日志");
        boolean save = recycleTaskLogService.save(recycleTaskLog);
        if (!save) throw new RecycleTaskLogException(RecycleTaskLogEnum.RECYCLE_TASK_LOG_ERROR);

        return confirmRiderArriveDTO;
    }

    /**
     * 确认工单（确认废品重量、单价、总价、商品种类）
     * @param confirmTaskForm 确认工单传入的参数
     * @return 返回骑手废品回收后确认回收信息对象
     */
    @Override
    public ConfirmRecycleTaskDTO confirmTask(ConfirmTaskForm confirmTaskForm, MultipartFile file) {
        log.info("确认废品工单时传入的参数：{}",confirmTaskForm);
        if (confirmTaskForm==null) throw new RecycleTaskException(RecycleTaskEnum.PARAM_ERROR);

        log.info("查询工单");
        RecycleTask recycleTask = recycleTaskMapper.selectById(confirmTaskForm.getId());

        log.info("创建工单详情");
        RecycleTaskInfo recycleTaskInfo = new RecycleTaskInfo();
        //工单详情的图片上传路径
        log.info("获取图片");
//        MultipartFile file = confirmTaskForm.getRecycleTaskInfoForm().getFile();
        String uploadPictureUrl = recycleTaskInfoService.uploadPicture(file);
        log.info("获取图片路径成功");

        //工单详情的废品名称
        String productName = confirmTaskForm.getRecycleTaskInfoForm().getProductName();
        //工单详情的废品回收单价
        BigDecimal actualPrice = confirmTaskForm.getRecycleTaskInfoForm().getActualPrice();
        //工单详情的废品回收数量
        Integer count = confirmTaskForm.getRecycleTaskInfoForm().getCount();
        //工单详情的废品回收总价
        BigDecimal total = confirmTaskForm.getRecycleTaskInfoForm().getTotal();
        //工单详情的废品回收数量单位
        String units = confirmTaskForm.getRecycleTaskInfoForm().getUnits();
        log.info("添加工单详情数据");
        //添加废品详情数据
        recycleTaskInfo.setTaskId(confirmTaskForm.getId());
        recycleTaskInfo.setProductName(productName);
        recycleTaskInfo.setPictureUrl(uploadPictureUrl);
        recycleTaskInfo.setActualPrice(actualPrice);
        recycleTaskInfo.setCount(count);
        recycleTaskInfo.setUnits(units);
        recycleTaskInfo.setTotal(total);
        log.info("工单详情存入数据库");
        boolean save = recycleTaskInfoService.save(recycleTaskInfo);
        if (!save) throw new RecycleTaskException(RecycleTaskInfoEnum.RECYCLE_TASK_INFO_ERROR);
        //工单状态 0未派单 | 1已派单 | 2 上门中 | 3 收货中 | 4 回收中 | 5 完成 | 6 已取消"
        log.info("确认工单信息");
        recycleTask.setStatus(4);
        recycleTask.setTotal(total);
        recycleTask.setEnsureTime(System.currentTimeMillis());
        recycleTask.setProductClassifyId(confirmTaskForm.getProductClassifyId());
        recycleTask.setProductClassify(confirmTaskForm.getProductClassify());
        log.info("更新确认工单信息");
        int i = recycleTaskMapper.updateById(recycleTask);
        if (i<=0) throw new RecycleTaskException(RecycleTaskEnum.TASK_CONFIRM_ERROR);

        ConfirmRecycleTaskDTO confirmRecycleTaskDTO = BeanCopyUtil.copyObject(recycleTask, ConfirmRecycleTaskDTO::new);
        if (confirmRecycleTaskDTO ==null) throw new RecycleTaskException(RecycleTaskEnum.COPY_ERROR);


        log.info("记录工单确认日志");
        RecycleTaskLog recycleTaskLog = new RecycleTaskLog();
        recycleTaskLog.setTaskId(recycleTask.getId());
        recycleTaskLog.setLogType(LogType.MANAGER);
        recycleTaskLog.setUserId(recycleTask.getUserId());
        recycleTaskLog.setCreateTime(System.currentTimeMillis());
        recycleTaskLog.setLogReason("reason:确认废品回收工单");
        log.info("完成记录工单日志");
        boolean logSave = recycleTaskLogService.save(recycleTaskLog);
        if (!logSave) throw new RecycleTaskLogException(RecycleTaskLogEnum.RECYCLE_TASK_LOG_ERROR);

        return confirmRecycleTaskDTO;
    }

    /**
     * 提交工单，完成工单
     * @param finishForm 完成工单时传入的参数
     * @return 返回完成工单的信息对象
     */
    @Override
    public FinishRecycleTaskDTO finishTask(FinishRecycleTaskForm finishForm) {
        log.info("完成工单时传入的参数：{}",finishForm);
        if (finishForm==null) throw new RecycleTaskException(RecycleTaskEnum.PARAM_ERROR);
        //查询工单
        RecycleTask recycleTask = recycleTaskMapper.selectById(finishForm.getId());
        if (recycleTask==null) throw new RecycleTaskException(RecycleTaskEnum.TASK_NULL_ERROR);
        //查询工单详情
        RecycleTaskInfo recycleTaskInfo = recycleTaskInfoMapper.selectById(finishForm.getTaskInfoId());
        if (recycleTaskInfo==null) throw new RecycleTaskException(RecycleTaskInfoEnum.TASK_INFO_NULL_ERROR);
        //提交工单时判断废品类别是否一致
        if (recycleTask.getProductClassifyId()!=finishForm.getProductClassifyId()&&recycleTask.getProductClassify()!=finishForm.getProductClassify())
            throw new RecycleTaskException(RecycleTaskEnum.TASK_COMMIT_ERROR1);
        //提交工单时判断废品数量是否一致
        if (recycleTaskInfo.getCount()!=finishForm.getCount()&&recycleTaskInfo.getUnits()!=finishForm.getUnits())
            throw new RecycleTaskException(RecycleTaskEnum.TASK_COMMIT_ERROR2);

        //设置要更新工单的数据
        recycleTask.setFinishTime(System.currentTimeMillis());
        //工单状态 0未派单 | 1已派单 | 2 上门中 | 3 收货中 | 4 回收中 | 5 完成 | 6 已取消
        recycleTask.setStatus(5);
        recycleTask.setPayment(finishForm.getPayment());

        //更新工单数据
        int i = recycleTaskMapper.updateById(recycleTask);
        if (i<=0) throw new RecycleTaskException(RecycleTaskEnum.TASK_CONFIRM_ERROR);

        FinishRecycleTaskDTO finishRecycleTaskDTO = BeanCopyUtil.copyObject(recycleTask, FinishRecycleTaskDTO::new);
        if (finishRecycleTaskDTO==null) throw new RecycleTaskException(RecycleTaskEnum.COPY_ERROR);

        log.info("提交工单确认日志");
        RecycleTaskLog recycleTaskLog = new RecycleTaskLog();
        recycleTaskLog.setTaskId(recycleTask.getId());
        recycleTaskLog.setLogType(LogType.MANAGER);
        recycleTaskLog.setUserId(recycleTask.getUserId());
        recycleTaskLog.setCreateTime(System.currentTimeMillis());
        recycleTaskLog.setLogReason("reason:提交完成废品回收工单");
        log.info("完成记录工单日志");
        //记录工单提交完成日志
        boolean logSave = recycleTaskLogService.save(recycleTaskLog);
        if (!logSave) throw new RecycleTaskLogException(RecycleTaskLogEnum.RECYCLE_TASK_LOG_ERROR);

        return finishRecycleTaskDTO;
    }


}