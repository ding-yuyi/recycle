package com.woniu.recycle.recycle.controller;


import com.woniu.recycle.commons.core.model.ResponseEntity;
import com.woniu.recycle.commons.web.component.BaseController;
import com.woniu.recycle.recycle.component.RecycleTaskEnum;
import com.woniu.recycle.recycle.controller.form.*;
import com.woniu.recycle.recycle.dto.*;
import com.woniu.recycle.recycle.exception.RecycleTaskException;
import com.woniu.recycle.recycle.service.RecycleOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 回收商品订单 前端控制器
 * </p>
 *
 * @author cy/lyl
 * @since 2021-08-11
 */
@Slf4j
@RestController
@RequestMapping("/order")
@Api(tags = "废品回收订单管理")
public class RecycleOrderController extends BaseController {


    @Autowired
    private RecycleOrderService recycleOrderService;

    @ApiOperation(value = "用户废品回收订单创建接口方法")
    @PostMapping("/auth/createRecycleOrder")
    public ResponseEntity<CreateRecycleOrderDTO> create(@Valid @RequestBody CreateRecycleOrderForm createForm , BindingResult result) {
        log.info("新增订单传入的参数:{}",createForm);
        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;

        CreateRecycleOrderDTO recycleOrder = recycleOrderService.createRecycleOrder(createForm);
        if (recycleOrder==null){
            return ResponseEntity.BuildError(CreateRecycleOrderDTO.class).setMsg("订单创建失败");
        }else {
            return ResponseEntity.BuildSuccess(CreateRecycleOrderDTO.class).setMsg("订单创建成功").setData(recycleOrder);
        }

    }
    @ApiOperation(value = "用户查询废品回收订单")
    @PostMapping("/auth/queryAll")
    public ResponseEntity<QueryRecycleOrderResultDTO> queryAll(@Valid @RequestBody QueryRecycleOrderByUserIdForm queryForm,BindingResult result){
        log.info("查询所有回收订单时控制层接收的参数：{}",queryForm);
        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;

        QueryRecycleOrderResultDTO recycleOrderResultDTO = recycleOrderService.queryRecycleOrderByUserId(queryForm);

        if (recycleOrderResultDTO ==null||recycleOrderResultDTO.getRecycleOrderDTOList().size()<=0){
            return ResponseEntity.BuildError(QueryRecycleOrderResultDTO.class).setMsg("订单查询失败");
        }else {
            return ResponseEntity.BuildSuccess(QueryRecycleOrderResultDTO.class).setMsg("订单查询成功").setData(recycleOrderResultDTO);
        }
    }
    @ApiOperation(value = "用户根据时间查询用户的废品回收订单")
    @PostMapping("/auth/queryByCreateTime")
    public ResponseEntity<List<QueryRecycleOrderDTO>> queryByCreateTime(@RequestBody QueryRecycleOrderByCreateTimeForm queryForm){
        log.info("根据时间查询回收订单时控制层接收的参数：{}",queryForm);
        List<QueryRecycleOrderDTO> queryRecycleOrderDTOS = recycleOrderService.queryRecycleOrderByCreateTime(queryForm);
        if (queryRecycleOrderDTOS ==null){
            return ResponseEntity.BuildErrorList(QueryRecycleOrderDTO.class).setMsg("订单查询失败");
        }else {
            return ResponseEntity.BuildSuccessList(QueryRecycleOrderDTO.class).setMsg("订单查询成功").setData(queryRecycleOrderDTOS);
        }
    }

    @ApiOperation(value = "用户根据状态查询用户的废品回收订单")
    @PostMapping("/auth/queryByStatus")
    public ResponseEntity<List<QueryRecycleOrderDTO>> queryByStatus(@RequestBody QueryRecycleOrderByStatusForm queryForm){
        log.info("根据状态查询废品回收订单时的接收参数:{}",queryForm);

        List<QueryRecycleOrderDTO> queryRecycleOrderDTOS = recycleOrderService.queryRecycleOrderByStatus(queryForm);

        if (queryRecycleOrderDTOS ==null){
            return ResponseEntity.BuildErrorList(QueryRecycleOrderDTO.class).setMsg("订单查询失败");
        }else {
            return ResponseEntity.BuildSuccessList(QueryRecycleOrderDTO.class).setMsg("订单查询成功").setData(queryRecycleOrderDTOS);
        }


    }


    @ApiOperation(value = "用户取消废品回收订单")
    @PostMapping("/auth/cancelRecycleOrder")
    public ResponseEntity<CancelRecycleOrderDTO> cancelRecycleOrder(@Valid @RequestBody CancelRecycleOrderForm cancelForm,BindingResult result){
        log.info("取消废品回收订单时接收的参数：{}",cancelForm);
        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;
        CancelRecycleOrderDTO cancelRecycleOrderDTO = recycleOrderService.cancelRecycleOrder(cancelForm);
        if (cancelRecycleOrderDTO==null){
            return ResponseEntity.BuildError(CancelRecycleOrderDTO.class).setMsg("订单取消失败");
        }else {
            return ResponseEntity.BuildSuccess(CancelRecycleOrderDTO.class).setMsg("订单取消成功").setData(cancelRecycleOrderDTO);
        }
    }

    @ApiOperation(value = "用户评论废品回收订单")
    @PostMapping("/auth/commentRecycleOrder")
    public ResponseEntity<CommentRecycleOrderDTO> commentRecycleOrder(@Valid @RequestBody CommentRecycleOrderForm commentForm,BindingResult result){
        log.info("评论废品回收订单时传入的参数：{}",commentForm);
        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;
        CommentRecycleOrderDTO commentRecycleOrderDTO = recycleOrderService.commentRecycleOrder(commentForm);
        if (commentRecycleOrderDTO==null){
            return ResponseEntity.BuildError(CommentRecycleOrderDTO.class).setMsg("订单评论失败");
        }else {
            return ResponseEntity.BuildSuccess(CommentRecycleOrderDTO.class).setMsg("订单评论成功").setData(commentRecycleOrderDTO);
        }


    }

    @ApiOperation(value = "用户删除废品回收订单")
    @PostMapping("/auth/deleteRecycleOrder")
    public ResponseEntity<String> deleteRecycleOrder(Integer id){
        log.info("传入要删除的订单id参数：{}",id);
        boolean flag = recycleOrderService.deletedRecycleOrder(id);
        if (!flag){
            return ResponseEntity.BuildError(String.class).setMsg("订单删除失败");
        }else {
            return ResponseEntity.BuildSuccess(String.class).setMsg("订单删除成功");
        }
    }
    @ApiOperation(value = "管理员确认废品回收订单")
    @PostMapping("/auth/updateRecycleOrder")
    public ResponseEntity<UpdateRecycleOrderDTO> updateRecycleOrder(@Valid @RequestBody UpdateRecycleOrderForm updateForm,BindingResult result){
        log.info("确认订单时所传入的参数：{}",updateForm);
        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;
        UpdateRecycleOrderDTO updateRecycleOrderDTO = recycleOrderService.updateRecycleOrder(updateForm);

        if (updateRecycleOrderDTO ==null){
            return ResponseEntity.BuildError(UpdateRecycleOrderDTO.class).setMsg("订单确认失败");
        }else {
            return ResponseEntity.BuildSuccess(UpdateRecycleOrderDTO.class).setMsg("订单确认成功").setData(updateRecycleOrderDTO);
        }
    }

    @ApiOperation(value = "管理员查询所有的废品回收订单")
    @PostMapping("/auth/adminQueryAll")
    public ResponseEntity<QueryRecycleOrderResultDTO> adminQueryAll(@Valid @RequestBody QueryRecycleOrderByUserIdForm queryForm,BindingResult result){
        log.info("管理员查询所有回收订单时控制层接收的参数：{}",queryForm);
        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;
        QueryRecycleOrderResultDTO recycleOrderResultDTO = recycleOrderService.adminQueryAllRecycleOrder(queryForm);

        if (recycleOrderResultDTO ==null){
            return ResponseEntity.BuildError(QueryRecycleOrderResultDTO.class).setMsg("订单查询失败");
        }else {
            return ResponseEntity.BuildSuccess(QueryRecycleOrderResultDTO.class).setMsg("订单查询成功").setData(recycleOrderResultDTO);
        }
    }

    @ApiOperation(value = "管理员支付回收订单")
    @PostMapping("/auth/orderPay")
    public ResponseEntity<String> orderPay(@Valid @RequestBody PayBalanceForm payBalanceForm,BindingResult result){
        log.info("商家支付是传入的参数:{}",payBalanceForm);
        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;        String orderPay = recycleOrderService.orderPay(payBalanceForm);
        if (orderPay ==null){
            return ResponseEntity.BuildError(String.class).setMsg("订单支付失败");
        }else {
            return ResponseEntity.BuildSuccess(String.class).setMsg("订单支付成功").setData(orderPay);
        }
    }


}

