package com.woniu.recycle.recycle.domain;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 回收工单详情表
 * </p>
 *
 * @author wjh
 * @since 2021-08-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="RecycleTaskInfo对象", description="回收工单详情表")
public class RecycleTaskInfo implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "货物名称")
    private String productName;

    @ApiModelProperty(value = "货物图片地址")
    private String pictureUrl;

    @ApiModelProperty(value = "收购数量")
    private Integer count;

    @ApiModelProperty(value = "实际收购价格")
    private BigDecimal actualPrice;

    @ApiModelProperty(value = "货物单位")
    private String units;

    @ApiModelProperty(value = "小计")
    private BigDecimal total;

    @ApiModelProperty(value = "工单id")
    private Integer taskId;




}
