package com.woniu.recycle.recycle.controller.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Title: CancelRecycleTaskForm
 * @ProjectName woniu-recycle
 * @Description: TODO
 * @Author wl
 * @Date 2021/8/17 19:22
 * @Version:1.0
 */
@Data
public class CancelRecycleTaskForm {
    @ApiModelProperty(value = "工单id")
    @NotNull(message = "订单id不能为空")
    private Integer id;

    @ApiModelProperty(value = "订单id")
    @NotNull(message = "订单id不能为空")
    private Integer orderId;

    @ApiModelProperty(value = "取消原因")
    @NotBlank(message = "工单取消原因不能为空")
    private String cancelReason;
}
