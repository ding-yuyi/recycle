package com.woniu.recycle.recycle.feign.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 *
 */
@ApiModel("商品积分变动Dto")
@Data
public class ChangeBalanceSel {

    @ApiModelProperty(value = "id", example = "1")
    private Integer id;

    @ApiModelProperty(value = "余额", example = "1")
    private BigDecimal balance;

}
