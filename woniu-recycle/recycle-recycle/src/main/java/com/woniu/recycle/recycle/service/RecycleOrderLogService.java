package com.woniu.recycle.recycle.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.recycle.recycle.controller.form.QueryRecycleOrderLogForm;
import com.woniu.recycle.recycle.domain.RecycleOrderLog;
import com.woniu.recycle.recycle.dto.QueryRecycleOrderLogResultDTO;

/**
 * <p>
 * 回收商品订单日志表 服务类
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-11
 */
public interface RecycleOrderLogService extends IService<RecycleOrderLog> {
    /**
     * 废品回收订单日志查询
     * @param queryForm 订单日志查询传入的参数
     * @return 订单日志查询信息对象
     */
    public QueryRecycleOrderLogResultDTO queryOrderLog(QueryRecycleOrderLogForm queryForm);
}
