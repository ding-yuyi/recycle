package com.woniu.recycle.recycle.feign.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @Title: SelectRecycleProductForm
 * @ProjectName woniu-recycle
 * @Description: TODO
 * @Author wl
 * @Date 2021/8/21 15:15
 * @Version:1.0
 */
@Data
@ApiModel("openFeign的查询回收商品接口入参对象")
@AllArgsConstructor
public class SelectRecycleProductForm {

    @ApiModelProperty(value = "商品id", example = "1")
    private Integer id;

    @NotNull
    @ApiModelProperty(value = "页数", example = "1")
    private Integer page;

    @NotNull
    @ApiModelProperty(value = "条数", example = "1")
    private Integer num;
}
