package com.woniu.recycle.recycle.controller.form;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @Title: updateRecycleOrderForm
 * @ProjectName woniu-recycle
 * @Description: 确认(修改)废品回收订单传入的参数
 * @Author wl
 * @Date 2021/8/16 10:40
 * @Version:1.0
 */
@Data
public class UpdateRecycleOrderForm {
    @ApiModelProperty(value = "订单id")
    @NotNull(message = "工单id不能为空")
    private Integer id;

    @ApiModelProperty(value = "商品总价")
    @NotNull(message = "商品总价不能为空")
    private BigDecimal total;

    @ApiModelProperty(value = "订单状态 0 未收货 | 1 已收货（订单结束）| 2 已评价 | 3 已取消")
    private Integer status;

}
