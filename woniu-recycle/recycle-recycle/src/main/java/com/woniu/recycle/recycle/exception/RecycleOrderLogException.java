package com.woniu.recycle.recycle.exception;

import com.woniu.recycle.commons.core.exception.RecycleException;
import com.woniu.recycle.recycle.component.RecycleOrderEnum;
import com.woniu.recycle.recycle.component.RecycleOrderLogEnum;

/**
 * @Title: RecycleOrderLogException
 * @ProjectName woniu-recycle
 * @Description: TODO
 * @Author wl
 * @Date 2021/8/13 17:40
 * @Version:1.0
 */
public class RecycleOrderLogException extends RecycleException {
    private static final long serialVersionUID = 1L;

    public RecycleOrderLogException(){}
    public RecycleOrderLogException(String msg){
        super(msg);
    }

    public RecycleOrderLogException(String msg,Integer code){
        super(msg,code);
    }

    public RecycleOrderLogException(RecycleOrderLogEnum recycleOrderLogEnum){
        super(recycleOrderLogEnum.getMsg(),recycleOrderLogEnum.getCode());
    }
}
