package com.woniu.recycle.recycle.feign;

import com.woniu.recycle.commons.core.model.ResponseEntity;
import com.woniu.recycle.recycle.feign.form.ChangeBalanceSelForm;
import com.woniu.recycle.recycle.feign.model.Addr ;
import com.woniu.recycle.recycle.feign.model.ChangeBalanceSel;
import com.woniu.recycle.recycle.feign.model.User ;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "user-server")
public interface UserFeignClient {

    @GetMapping("/user/userModule/auth/UserById")
    ResponseEntity<User> UserById(@RequestParam Integer userId);

    @PostMapping("/user/addr/auth/findAddrById")
    ResponseEntity<Addr> findAddrById(@RequestParam Integer id);

    @PutMapping("/user/userModule/auth/balanceAdd")
    ResponseEntity<ChangeBalanceSel> balanceAdd(@SpringQueryMap ChangeBalanceSelForm changeBalanceSelForm);
}
