package com.woniu.recycle.recycle.controller.form;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @Title: FinishRecycleTaskForm
 * @ProjectName woniu-recycle
 * @Description: TODO
 * @Author wl
 * @Date 2021/8/18 20:51
 * @Version:1.0
 */
@Data
public class FinishRecycleTaskForm {
    @ApiModelProperty(value = "工单id")
    @NotNull(message = "工单id不能为空")
    private Integer id;

    @ApiModelProperty(value = "工单详情id")
    @NotNull(message = "工单详情id不能为空")
    private Integer taskInfoId;

    @ApiModelProperty(value = "工单操作者id")
    @NotNull(message = "操作者id不能为空")
    private Integer userId;

    @ApiModelProperty(value = "骑手工资")
    private BigDecimal payment;

    @ApiModelProperty(value = "产品类型id")
    private Integer productClassifyId;

    @ApiModelProperty(value = "产品类型")
    @NotNull(message = "收购类型不能为空")
    private String productClassify;

    @ApiModelProperty(value = "收购数量")
    @NotNull(message = "收购数量不能为空")
    private Integer count;

    @ApiModelProperty(value = "货物单位")
    @NotNull(message = "数量单位不能为空")
    private String units;

}
