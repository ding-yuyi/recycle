package com.woniu.recycle.recycle.controller.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @Title: QueryRecycleOrderByStatus
 * @ProjectName woniu-recycle
 * @Description: TODO
 * @Author wl
 * @Date 2021/8/13 12:04
 * @Version:1.0
 */
@Data
public class QueryRecycleOrderByStatusForm {
    @ApiModelProperty(value = "订单状态 0 未收货 | 1 已收货（订单结束）| 2 已评价 | 3 已取消")
    private Integer status;

    //分页查询的页码数
    @ApiModelProperty(value = "当前页数")
    @NotNull(message = "页码不能为空")
    private Integer pageIndex;
    //分页查询的数量
    @ApiModelProperty(value = "每页展示数量")
    @NotNull(message = "页数不能为空")
    private Integer pageSize=10;
}
