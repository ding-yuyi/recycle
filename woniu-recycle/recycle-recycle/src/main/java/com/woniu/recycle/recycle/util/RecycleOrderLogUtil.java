package com.woniu.recycle.recycle.util;

import com.woniu.recycle.commons.web.util.ObjectMapperUtil;
import com.woniu.recycle.recycle.component.RecycleOrderLogReason;
import com.woniu.recycle.recycle.domain.RecycleOrder;
import com.woniu.recycle.recycle.domain.RecycleOrderLog;

/**
 * @Title: LogUtil
 * @ProjectName woniu-recycle
 * @Description: 记录废品回收订单日志工具类
 * @Author wl
 * @Date 2021/8/17 16:08
 * @Version:1.0
 */
public class RecycleOrderLogUtil {
    public static RecycleOrderLog createLog(RecycleOrder order, Integer logType, String reason, Integer userId){
        RecycleOrderLog orderLog = new RecycleOrderLog();
        orderLog.setLogCreateTime(System.currentTimeMillis());
        orderLog.setOrderId(order.getId());
        orderLog.setOrderNum(order.getRecycleOrderNum());
        orderLog.setUserId(userId);//需要修改为token解析出来的userId
        orderLog.setLogType(logType);
        orderLog.setOrderStatus(order.getStatus());
        orderLog.setLogReason(ObjectMapperUtil.parseObject(new RecycleOrderLogReason(reason, order)));

        return orderLog;
    }
}
