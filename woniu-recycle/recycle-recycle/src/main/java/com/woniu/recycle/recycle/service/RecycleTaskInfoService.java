package com.woniu.recycle.recycle.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.recycle.recycle.domain.RecycleTaskInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 * 回收工单详情表 服务类
 * </p>
 *
 * @author wjh
 * @since 2021-08-12
 */
public interface RecycleTaskInfoService extends IService<RecycleTaskInfo> {

    /**
     * 确认废品工单时上传废品图片
     * @param file 上传的图片文件
     * @return 上传图片后的图片路径
     */
    public String uploadPicture(MultipartFile file);
}
