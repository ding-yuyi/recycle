package com.woniu.recycle.recycle.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Title: updateRecycleDTO
 * @ProjectName woniu-recycle
 * @Description: 确认后的回收订单信息对象
 * @Author wl
 * @Date 2021/8/16 10:34
 * @Version:1.0
 */
@Data
public class UpdateRecycleOrderDTO {
    @ApiModelProperty(value = "订单id")
    private Integer id;

    @ApiModelProperty(value = "订单编号")
    private String recycleOrderNum;

    @ApiModelProperty(value = "商品总价")
    private BigDecimal total;

    @ApiModelProperty(value = "用户id")
    private Integer userId;

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "用户电话号码")
    private String userPhone;

    @ApiModelProperty(value = "地址id")
    private Integer addrId;

    @ApiModelProperty(value = "收获地址")
    private String addrDesc;

    @ApiModelProperty(value = "骑手名字")
    private String workerName;

    @ApiModelProperty(value = "预约上门时间")
    private Long visitTime;

    @ApiModelProperty(value = "创建时间")
    private Long createTime;

    @ApiModelProperty(value = "骑手电话")
    private String workerTel;

    @ApiModelProperty(value = "订单状态 0 未收货 | 1 已收货（订单结束）| 2 已评价 | 3 已取消")
    private Integer status;

    @ApiModelProperty(value = "取消时间")
    private Long cancelTime;

    @ApiModelProperty(value = "取消原因")
    private String cancelReason;

    @ApiModelProperty(value = "商品类别名称")
    private String classifyName;

    @ApiModelProperty(value = "商品类别id")
    private Integer classifyId;

    @ApiModelProperty(value = "备注")
    private String notes;

    @ApiModelProperty(value = "订单结束时间")
    private Long finishTime;

    @ApiModelProperty(value = "逻辑删除 0 未删除 | 1 已删除（用于用户端查询）")
    private Integer deleted;
}
