package com.woniu.recycle.recycle.exception;

import com.woniu.recycle.commons.core.exception.RecycleException;
import com.woniu.recycle.recycle.component.RecycleOrderEnum;
import com.woniu.recycle.recycle.component.RecycleTaskEnum;
import com.woniu.recycle.recycle.component.RecycleTaskInfoEnum;
import com.woniu.recycle.recycle.component.UploadFileEnum;

/**
 * @Title: RecycleTaskException
 * @ProjectName woniu-recycle
 * @Description: TODO
 * @Author wl
 * @Date 2021/8/16 19:47
 * @Version:1.0
 */
public class RecycleTaskException extends RecycleException {
    private static final long serialVersionUID = 1L;

    public RecycleTaskException(){}
    public RecycleTaskException(String msg){
        super(msg);
    }

    public RecycleTaskException(String msg,Integer code){
        super(msg,code);
    }

    public RecycleTaskException(RecycleTaskEnum recycleTaskEnum){
        super(recycleTaskEnum.getMsg(),recycleTaskEnum.getCode());
    }
    public RecycleTaskException(RecycleTaskInfoEnum recycleTaskInfoEnum){
        super(recycleTaskInfoEnum.getMsg(),recycleTaskInfoEnum.getCode());
    }

    public RecycleTaskException(UploadFileEnum uploadFileError) {

    }
}
