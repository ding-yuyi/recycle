package com.woniu.recycle.recycle.exception;

import com.woniu.recycle.commons.core.exception.RecycleException;
import com.woniu.recycle.recycle.component.RecycleOrderLogEnum;
import com.woniu.recycle.recycle.component.RecycleTaskLogEnum;

/**
 * @Title: RecycleTaskLogException
 * @ProjectName woniu-recycle
 * @Description: TODO
 * @Author wl
 * @Date 2021/8/17 11:15
 * @Version:1.0
 */
public class RecycleTaskLogException extends RecycleException {
    private static final long serialVersionUID = 1L;

    public RecycleTaskLogException(){}
    public RecycleTaskLogException(String msg){
        super(msg);
    }

    public RecycleTaskLogException(String msg,Integer code){
        super(msg,code);
    }

    public RecycleTaskLogException(RecycleTaskLogEnum recycleTaskLogEnum){
        super(recycleTaskLogEnum.getMsg(),recycleTaskLogEnum.getCode());
    }
}
