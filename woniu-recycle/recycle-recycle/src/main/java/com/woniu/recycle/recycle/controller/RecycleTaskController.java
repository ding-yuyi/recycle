package com.woniu.recycle.recycle.controller;


import com.woniu.recycle.commons.core.model.ResponseEntity;
import com.woniu.recycle.commons.web.component.BaseController;
import com.woniu.recycle.recycle.component.RecycleTaskEnum;
import com.woniu.recycle.recycle.controller.form.*;
import com.woniu.recycle.recycle.dto.*;
import com.woniu.recycle.recycle.exception.RecycleTaskException;
import com.woniu.recycle.recycle.service.RecycleTaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;


/**
 * <p>
 * 回收工单表 前端控制器
 * </p>
 *
 * @author wjh
 * @since 2021-08-12
 */
@Slf4j
@RestController
@Api(tags = "骑手工单管理")
@RequestMapping("/task")
public class RecycleTaskController extends BaseController {

    @Autowired
    private RecycleTaskService recycleTaskService;

    @ApiOperation(value = "管理员创建骑手工单")
    @PostMapping("/auth/createTask")
    public ResponseEntity<CreateRecycleTaskDTO> createRiderTask(@Valid @RequestBody CreateRiderTaskForm createForm, BindingResult result){
        log.info("创建骑手工单传入的参数：{}",createForm);
        if (result.hasErrors()) throw new RecycleTaskException(RecycleTaskEnum.PARAM_VALIDATION_ERROR);

        CreateRecycleTaskDTO createRecycleTaskDTO = recycleTaskService.createRiderTask(createForm);
        if (createRecycleTaskDTO ==null) {
            return ResponseEntity.BuildError(CreateRecycleTaskDTO.class).setMsg("创建工单失败");
        }else {
            return ResponseEntity.BuildSuccess(CreateRecycleTaskDTO.class).setMsg("创建工单成功").setData(createRecycleTaskDTO);
        }

    }

    @ApiOperation(value = "管理员查询骑手工单")
    @PostMapping("/auth/queryTask")
    public ResponseEntity<QueryRecycleTaskResultDTO> queryRiderTask(@Valid @RequestBody QueryRiderTaskForm queryForm,BindingResult result){
        log.info("查询骑手工单传入的参数：{}",queryForm);
        if (result.hasErrors()) throw new RecycleTaskException(RecycleTaskEnum.PARAM_VALIDATION_ERROR);

        QueryRecycleTaskResultDTO recycleTaskResultDTO = recycleTaskService.queryRiderTask(queryForm);
        if (recycleTaskResultDTO ==null||recycleTaskResultDTO.getRecycleTaskDTOList().size()<=0){
            return ResponseEntity.BuildError(QueryRecycleTaskResultDTO.class).setMsg("工单查询失败");
        }else {
            return ResponseEntity.BuildSuccess(QueryRecycleTaskResultDTO.class).setMsg("工单查询成功").setData(recycleTaskResultDTO);
        }
    }

    @ApiOperation(value = "管理员取消骑手工单")
    @PostMapping("/auth/cancelTask")
    public ResponseEntity<CancelRecycleTaskDTO> cancelForm(@Valid @RequestBody CancelRecycleTaskForm cancelForm,BindingResult result){
        log.info("取消骑手工单时传入的参数：{}",cancelForm);
        if (result.hasErrors()) throw new RecycleTaskException(RecycleTaskEnum.PARAM_VALIDATION_ERROR);

        CancelRecycleTaskDTO cancelRecycleTaskDTO = recycleTaskService.cancelRiderTask(cancelForm);

        if (cancelRecycleTaskDTO==null){
            return ResponseEntity.BuildError(CancelRecycleTaskDTO.class).setMsg("取消骑手工单失败");
        }else {
            return ResponseEntity.BuildSuccess(CancelRecycleTaskDTO.class).setMsg("取消骑手工单成功").setData(cancelRecycleTaskDTO);
        }
    }

    @ApiOperation(value = "管理员派发工单")
    @PostMapping("/auth/sendTask")
    public ResponseEntity<SendRecycleTaskDTO> sendTask(@Valid @RequestBody SendTaskForm sendForm,BindingResult result){

        log.info("派发工单时传入的参数:{}",sendForm);
        if (result.hasErrors()) throw new RecycleTaskException(RecycleTaskEnum.PARAM_VALIDATION_ERROR);

        SendRecycleTaskDTO sendRecycleTaskDTO = recycleTaskService.sendTask(sendForm);
        if (sendRecycleTaskDTO ==null){
            return ResponseEntity.BuildError(SendRecycleTaskDTO.class).setMsg("派发工单失败");
        }else {
            return ResponseEntity.BuildSuccess(SendRecycleTaskDTO.class).setMsg("派发工单成功").setData(sendRecycleTaskDTO);
        }
    }

    @ApiOperation(value = "骑手确认到达（管理员操作）")
    @PostMapping("/auth/confirmArrive")
    public ResponseEntity<ConfirmRiderArriveDTO> confirmArrive(@Valid @RequestBody ConfirmArriveForm confirmArriveForm,BindingResult result){
        log.info("骑手确认到达传入的参数：{}",confirmArriveForm);
        if (result.hasErrors()) throw new RecycleTaskException(RecycleTaskEnum.PARAM_VALIDATION_ERROR);

        ConfirmRiderArriveDTO confirmRiderArriveDTO = recycleTaskService.confirmArrive(confirmArriveForm);
        if (confirmRiderArriveDTO ==null){
            return ResponseEntity.BuildError(ConfirmRiderArriveDTO.class).setMsg("骑手确认到达失败");
        }else {
            return ResponseEntity.BuildSuccess(ConfirmRiderArriveDTO.class).setMsg("骑手确认到达成功").setData(confirmRiderArriveDTO);
        }
    }

    @ApiOperation(value = "确认废品回收工单（管理员操作）")
    @PostMapping("/auth/confirmTask")
    public ResponseEntity<ConfirmRecycleTaskDTO> confirmTask(@Valid ConfirmTaskForm confirmTaskForm, MultipartFile file,BindingResult result){
        log.info("确认废品回收工单时传入的参数：{}",confirmTaskForm);
        if (result.hasErrors()) throw new RecycleTaskException(RecycleTaskEnum.PARAM_VALIDATION_ERROR);

        ConfirmRecycleTaskDTO confirmRecycleTaskDTO = recycleTaskService.confirmTask(confirmTaskForm,file);
        if (confirmRecycleTaskDTO ==null){
            return ResponseEntity.BuildError(ConfirmRecycleTaskDTO.class).setMsg("确认废品回收工单失败");
        }else {
            return ResponseEntity.BuildSuccess(ConfirmRecycleTaskDTO.class).setMsg("确认废品回收工单成功").setData(confirmRecycleTaskDTO);
        }
    }

    @ApiOperation(value = "提交完成工单")
    @PostMapping("/commitTask")
    public ResponseEntity<FinishRecycleTaskDTO> finishTask(@Valid FinishRecycleTaskForm finishForm,BindingResult result){
        log.info("提交完成工单时传入的参数：{}",finishForm);
        if (result.hasErrors()) throw new RecycleTaskException(RecycleTaskEnum.PARAM_VALIDATION_ERROR);

        FinishRecycleTaskDTO finishRecycleTaskDTO = recycleTaskService.finishTask(finishForm);
        if (finishRecycleTaskDTO==null){
            return ResponseEntity.BuildError(FinishRecycleTaskDTO.class).setMsg("提交完成废品回收工单失败");
        }else {
            return ResponseEntity.BuildSuccess(FinishRecycleTaskDTO.class).setMsg("提交完成废品回收工单成功").setData(finishRecycleTaskDTO);
        }
    }


}

