package com.woniu.recycle.recycle.feign.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 回收商品类别信息表
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("recycle_product_classify")
@ApiModel(value="RecycleProductClassify对象", description="回收商品类别信息表")
public class RecycleProductClassify  {

    @ApiModelProperty(value = "类别id")
    private Integer id;

    @ApiModelProperty(value = "类别名称")
    private String name;

    @ApiModelProperty(value = "商品图片地址")
    private String pictureUrl;

    @ApiModelProperty(value = "上下架状态 0 下架 | 1 上架")
    private Integer status;
}
