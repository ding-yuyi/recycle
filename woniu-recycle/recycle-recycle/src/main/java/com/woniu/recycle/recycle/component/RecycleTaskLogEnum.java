package com.woniu.recycle.recycle.component;

/**
 * @Title: RecycleTaskLogEnum
 * @ProjectName woniu-recycle
 * @Description: 骑手工单日志枚举
 * @Author wl
 * @Date 2021/8/16 19:52
 * @Version:1.0
 */
public enum  RecycleTaskLogEnum {
    COPY_ERROR("对象复制失败",4002),
    PARAM_VALIDATION_ERROR("参数校验错误",4020),
    RECYCLE_TASK_LOG_ERROR("记录废品回收骑手工单日志失败",4052),
    LOG_SEARCH_ERROR("日志查询失败",4053);


    String msg;

    Integer code;

    public String getMsg() {
        return msg;
    }

    public Integer getCode() {
        return code;
    }

    RecycleTaskLogEnum(String msg, Integer code){
        this.msg=msg;
        this.code=code;
    }
}
