package com.woniu.recycle.recycle.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Title: QueryRecycleOrderLogDTO
 * @ProjectName woniu-recycle
 * @Description: TODO
 * @Author wl
 * @Date 2021/8/20 10:36
 * @Version:1.0
 */
@Data
public class QueryRecycleOrderLogDTO {
    @ApiModelProperty(value = "回收商品订单日记id")
    private Integer id;

    @ApiModelProperty(value = "订单id")
    private Integer orderId;

    @ApiModelProperty(value = "订单编号")
    private String orderNum;

    @ApiModelProperty(value = "日记创建时间")
    private Long logCreateTime;

    @ApiModelProperty(value = "订单状态")
    private Integer orderStatus;

    @ApiModelProperty(value = "记录日记生成原因")
    private String logReason;

    @ApiModelProperty(value = "操作者类型,0普通用户,1管理员")
    private Integer logType;

    @ApiModelProperty(value = "操作者ID")
    private Integer userId;
}
