package com.woniu.recycle.recycle.exception;

import com.woniu.recycle.commons.core.exception.RecycleException;
import com.woniu.recycle.recycle.component.RecycleOrderEnum;

/**
 * @Title: RecycleOrderException
 * @ProjectName woniu-recycle
 * @Description: TODO
 * @Author wl
 * @Date 2021/8/12 15:21
 * @Version:1.0
 */
public class RecycleOrderException extends RecycleException {
    private static final long serialVersionUID = 1L;

    public RecycleOrderException(){}
    public RecycleOrderException(String msg){
        super(msg);
    }

    public RecycleOrderException(String msg,Integer code){
        super(msg,code);
    }

    public RecycleOrderException(RecycleOrderEnum roe){
        super(roe.getMsg(),roe.getCode());
    }
}
