package com.woniu.recycle.recycle.controller.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @Title: PayBalanceForm
 * @ProjectName woniu-recycle
 * @Description: TODO
 * @Author wl
 * @Date 2021/8/20 16:50
 * @Version:1.0
 */
@Data
public class PayBalanceForm {
    @ApiModelProperty(value = "订单id")
    @NotNull(message = "订单id不能为空")
    private Integer id;

    @ApiModelProperty(value = "商品总价")
    @NotNull(message = "商品总价不能为空")
    private BigDecimal total;

    @ApiModelProperty(value = "用户id")
    @NotNull(message = "用户id不能为空")
    private Integer userId;

    @ApiModelProperty(value = "用户名")
    @NotNull(message = "用户名不能为空")
    private String username;

    @ApiModelProperty(value = "订单编号")
    @NotNull(message = "订单编号不能为空")
    private String recycleOrderNum;
}
