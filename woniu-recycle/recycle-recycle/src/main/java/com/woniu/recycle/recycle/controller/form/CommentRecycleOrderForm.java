package com.woniu.recycle.recycle.controller.form;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;

/**
 * @Title: CommentRecycleOrderForm
 * @ProjectName woniu-recycle
 * @Description: 废品回收订单评论传入的参数
 * @Author wl
 * @Date 2021/8/14 14:19
 * @Version:1.0
 */
@Data
public class CommentRecycleOrderForm {

    @ApiModelProperty(value = "订单id")
    @NotNull(message = "订单id不能为空")
    private Integer id;

    @ApiModelProperty(value = "评价内容（可不评价）")
    @Length(max = 500,message = "评论内容最多500字符串")
    private String comment;

    @ApiModelProperty(value = "评价等级 1 | 2 | 3 | 4 | 5  5星最好")
    @Range(min = 1,max = 5,message = "评论等级在1~5")
    private Integer level;
}
