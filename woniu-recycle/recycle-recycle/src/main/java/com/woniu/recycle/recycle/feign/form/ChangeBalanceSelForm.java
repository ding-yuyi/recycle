package com.woniu.recycle.recycle.feign.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 *
 */
@ApiModel("商品积分变动form")
@Data
@AllArgsConstructor
public class ChangeBalanceSelForm {

    @ApiModelProperty(value = "id", example = "1")
    private Integer id;


    @NotNull
    @ApiModelProperty(value = "余额", example = "1")
    private BigDecimal balance;

    @ApiModelProperty(value = "订单编号")
    @NotNull(message = "订单编号不能为空")
    private String payNum;


}
