package com.woniu.recycle.recycle.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.recycle.commons.web.util.BeanCopyUtil;
import com.woniu.recycle.commons.web.util.DateUtil;
import com.woniu.recycle.recycle.component.RecycleTaskEnum;
import com.woniu.recycle.recycle.component.RecycleTaskLogEnum;
import com.woniu.recycle.recycle.controller.form.QueryRecycleTaskLogForm;
import com.woniu.recycle.recycle.domain.RecycleTaskLog;
import com.woniu.recycle.recycle.dto.QueryRecycleOrderLogDTO;
import com.woniu.recycle.recycle.dto.QueryRecycleTaskLogDTO;
import com.woniu.recycle.recycle.dto.QueryRecycleTaskLogResultDTO;
import com.woniu.recycle.recycle.exception.RecycleTaskException;
import com.woniu.recycle.recycle.exception.RecycleTaskLogException;
import com.woniu.recycle.recycle.mapper.RecycleTaskLogMapper;
import com.woniu.recycle.recycle.service.RecycleTaskLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 回收工单日志表 服务实现类
 * </p>
 *
 * @author wjh
 * @since 2021-08-12
 */
@Slf4j
@Service
public class RecycleTaskLogServiceImpl extends ServiceImpl<RecycleTaskLogMapper, RecycleTaskLog> implements RecycleTaskLogService {

    @Resource
    private RecycleTaskLogMapper recycleTaskLogMapper;

    /**
     * 废品回收工单日志查询
     * @param queryForm 工单日志查询传入的参数
     * @return 工单日志查询信息对象
     */
    @Override
    public QueryRecycleTaskLogResultDTO queryOrderLog(QueryRecycleTaskLogForm queryForm) {
        log.info("查询工单日志传入的参数：{}",queryForm);

        Page<RecycleTaskLog> page = new Page<>(queryForm.getPageIndex(), queryForm.getPageSize());

        //前端传入的时间格式进行转换
        Long startTime =null;
        if (queryForm.getStartTime()!=null && queryForm.getStartTime()!=""){
            startTime = DateUtil.parse(queryForm.getStartTime(), "yyyy-MM-dd HH:mm:ss");
            log.info("查询开始时间",startTime);
        }
        Long finishTime =null;
        if (queryForm.getFinishTime()!=null && queryForm.getFinishTime()!=""){
            finishTime = DateUtil.parse(queryForm.getFinishTime(), "yyyy-MM-dd HH:mm:ss");
            log.info("查询结束时间",finishTime);
        }
        //条件构造器
        QueryWrapper<RecycleTaskLog> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(queryForm.getTaskId()!=null,"task_id",queryForm.getTaskId())
                .eq(queryForm.getLogReason()!=null&&queryForm.getLogReason()!="","log_reason",queryForm.getLogReason())
                .ge(startTime!=null,"create_time",startTime)
                .le(finishTime!=null,"create_time",finishTime)
                .orderByDesc("create_time");

        Page<RecycleTaskLog> selectPage = recycleTaskLogMapper.selectPage(page, queryWrapper);
        if (selectPage==null||selectPage.getRecords().size()<=0)
            throw new RecycleTaskLogException(RecycleTaskLogEnum.LOG_SEARCH_ERROR);
        QueryRecycleTaskLogResultDTO resultDTO = BeanCopyUtil.copyObject(selectPage, QueryRecycleTaskLogResultDTO::new);
        if (resultDTO==null) throw new RecycleTaskLogException(RecycleTaskLogEnum.COPY_ERROR);
        log.info("分页展示查询到的日志数据",resultDTO);

        List<QueryRecycleTaskLogDTO> recycleTaskLogDTOS = BeanCopyUtil.copyList(selectPage.getRecords(), QueryRecycleTaskLogDTO::new);

        if (recycleTaskLogDTOS==null) throw new RecycleTaskLogException(RecycleTaskLogEnum.COPY_ERROR);
        log.info("查询到的工单信息：{}", recycleTaskLogDTOS);

        resultDTO.setRecycleTaskLogDTOList(recycleTaskLogDTOS);
        log.info("工单数据添加成功");

        return resultDTO;
    }
}
