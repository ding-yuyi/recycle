package com.woniu.recycle.recycle.feign;

import com.woniu.recycle.commons.core.model.ResponseEntity;
import com.woniu.recycle.recycle.feign.form.SelectRecycleClassifyForm;
import com.woniu.recycle.recycle.feign.form.SelectRecycleProductForm;
import com.woniu.recycle.recycle.feign.model.RecycleProduct;
import com.woniu.recycle.recycle.feign.model.RecycleProductClassify;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


@FeignClient(name = "product-server")
public interface ProductFeignClient {
    @GetMapping("/product/recycleProduct/allcycleproduct")
    ResponseEntity<List<RecycleProduct>> findCycleProduct(@SpringQueryMap SelectRecycleProductForm selectForm);

    @GetMapping("/product/recycleProductClassify/selectall")
    ResponseEntity<List<RecycleProductClassify>> selectClassify(@SpringQueryMap SelectRecycleClassifyForm selectForm);


}
