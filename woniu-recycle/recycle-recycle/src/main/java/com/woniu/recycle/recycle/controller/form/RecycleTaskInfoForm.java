package com.woniu.recycle.recycle.controller.form;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @Title: RecycleTaskInfoForm
 * @ProjectName woniu-recycle
 * @Description: TODO
 * @Author wl
 * @Date 2021/8/18 16:11
 * @Version:1.0
 */
@Data
public class RecycleTaskInfoForm {

    @ApiModelProperty(value = "货物名称")
    private String productName;

    @ApiModelProperty(value = "收购数量")
    private Integer count;

    @ApiModelProperty(value = "实际收购价格")
    private BigDecimal actualPrice;

    @ApiModelProperty(value = "货物单位")
    private String units;

    @ApiModelProperty(value = "小计")
    private BigDecimal total;



}
