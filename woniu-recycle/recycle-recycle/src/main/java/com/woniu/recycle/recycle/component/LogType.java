package com.woniu.recycle.recycle.component;

/**
 * @Title: LogType
 * @ProjectName woniu-recycle
 * @Description: TODO
 * @Author wl
 * @Date 2021/8/13 17:18
 * @Version:1.0
 */
public class LogType {
    /**
     * 普通用户
     */
    public static final Integer NORMAL_USER = 0;

    /**
     * 管理员
     */
    public static final Integer MANAGER = 1;
}
