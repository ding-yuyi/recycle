package com.woniu.recycle.recycle.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.recycle.commons.web.util.BeanCopyUtil;
import com.woniu.recycle.commons.web.util.DateUtil;
import com.woniu.recycle.recycle.component.RecycleOrderLogEnum;
import com.woniu.recycle.recycle.controller.form.QueryRecycleOrderLogForm;
import com.woniu.recycle.recycle.domain.RecycleOrderLog;
import com.woniu.recycle.recycle.dto.QueryRecycleOrderLogDTO;
import com.woniu.recycle.recycle.dto.QueryRecycleOrderLogResultDTO;
import com.woniu.recycle.recycle.exception.RecycleOrderException;
import com.woniu.recycle.recycle.exception.RecycleOrderLogException;
import com.woniu.recycle.recycle.mapper.RecycleOrderLogMapper;
import com.woniu.recycle.recycle.service.RecycleOrderLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


/**
 * <p>
 * 回收商品订单日志表 服务实现类
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-11
 */
@Slf4j
@Service
public class RecycleOrderLogServiceImpl extends ServiceImpl<RecycleOrderLogMapper, RecycleOrderLog> implements RecycleOrderLogService {

    @Resource
    private RecycleOrderLogMapper recycleOrderLogMapper;

    @Override
    public QueryRecycleOrderLogResultDTO queryOrderLog(QueryRecycleOrderLogForm queryForm) {
        log.info("查询订单日志传入的参数：{}",queryForm);
        if (queryForm==null) throw new RecycleOrderLogException(RecycleOrderLogEnum.COPY_ERROR);

        Page<RecycleOrderLog> page = new Page<>(queryForm.getPageIndex(), queryForm.getPageSize());

        QueryWrapper<RecycleOrderLog> queryWrapper = new QueryWrapper<>();
        //前端传入的时间进行转换
        Long startTime =null;
        if (queryForm.getStartTime()!=null && queryForm.getStartTime()!=""){
            startTime = DateUtil.parse(queryForm.getStartTime(), "yyyy-MM-dd HH:mm:ss");
            log.info("查询开始时间",startTime);
        }
        Long finishTime =null;
        if (queryForm.getFinishTime()!=null && queryForm.getFinishTime()!=""){
            finishTime = DateUtil.parse(queryForm.getFinishTime(), "yyyy-MM-dd HH:mm:ss");
            log.info("查询结束时间",finishTime);
        }
        //条件构造器
        queryWrapper.eq(queryForm.getOrderId()!=null,"order_id",queryForm.getOrderId())
                    .eq(queryForm.getOrderStatus()!=null,"order_status",queryForm.getOrderStatus())
                    .ge(startTime!=null,"log_create_time",startTime)
                    .le(finishTime!=null,"log_create_time",finishTime)
                    .orderByDesc("log_create_time");
        //分页条件查询日志
        Page<RecycleOrderLog> selectPage = recycleOrderLogMapper.selectPage(page, queryWrapper);
        if (selectPage==null||selectPage.getRecords().size()<=0)
            throw new RecycleOrderLogException(RecycleOrderLogEnum.LOG_SEARCH_ERROR);
        //结果对象复制
        QueryRecycleOrderLogResultDTO resultDTO = BeanCopyUtil.copyObject(selectPage, QueryRecycleOrderLogResultDTO::new);
        if (resultDTO==null) throw new RecycleOrderLogException(RecycleOrderLogEnum.COPY_ERROR);
        log.info("分页展示查询到的订单日志数据",resultDTO);

        List<QueryRecycleOrderLogDTO> recycleOrderLogDTOS = BeanCopyUtil.copyList(selectPage.getRecords(), QueryRecycleOrderLogDTO::new);
        if (recycleOrderLogDTOS==null) throw new RecycleOrderLogException(RecycleOrderLogEnum.COPY_ERROR);
        log.info("返回查询到的订单日志数据：{}", recycleOrderLogDTOS);

        resultDTO.setRecycleTaskLogDTOList(recycleOrderLogDTOS);
        log.info("日志数据添加成功");
        return resultDTO;
    }

}
