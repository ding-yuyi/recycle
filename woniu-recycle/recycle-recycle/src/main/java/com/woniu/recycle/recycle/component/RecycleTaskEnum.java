package com.woniu.recycle.recycle.component;

/**
 * @Title: RecycleTaskEnum
 * @ProjectName woniu-recycle
 * @Description: 骑手工单异常枚举
 * @Author wl
 * @Date 2021/8/16 19:49
 * @Version:1.0
 */
public enum  RecycleTaskEnum {
    PARAM_ERROR("传入参数为空",4001),
    COPY_ERROR("对象复制失败",4002),
    TASK_CREATE_ERROR("骑手工单创建失败",4010),
    TASK_SEARCH_ERROR("工单查询失败",4011),
    TASK_NULL_ERROR("该工单不存在",4012),
    TASK_CANCEL_ERROR2("取消工单失败",4013),
    TASK_CANCEL_ERROR3("用户订单未取消，不能取消工单，或工单已完成",4014),
    TASK_SEND_ERROR("派单失败",4015),
    TASK_CONFIRM_ARRIVE_ERROR("骑手到达确认失败",4016),
    TASK_CONFIRM_ERROR("工单确认失败",4017),
    TASK_COMMIT_ERROR1("工单提交失败，废品类别不一致",4018),
    TASK_COMMIT_ERROR2("工单提交失败，废品重量不一致",4019),
    PARAM_VALIDATION_ERROR("参数校验错误",4020);


    String msg;

    Integer code;

    public String getMsg() {
        return msg;
    }

    public Integer getCode() {
        return code;
    }

    RecycleTaskEnum(String msg, Integer code){
        this.msg=msg;
        this.code=code;
    }
}
