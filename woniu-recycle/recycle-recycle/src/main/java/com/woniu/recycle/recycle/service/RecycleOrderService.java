package com.woniu.recycle.recycle.service;

import com.woniu.recycle.recycle.controller.form.*;
import com.woniu.recycle.recycle.domain.RecycleOrder;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.recycle.recycle.dto.*;

import java.util.List;

/**
 * <p>
 * 回收商品订单 服务类
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
public interface RecycleOrderService extends IService<RecycleOrder> {

    /**
     * 回收订单创建(废品回收下单)
     * @param createForm 下单传入的参数
     * @return CreateRecycleOrderDTO 返回的订单信息对象
     */
    public CreateRecycleOrderDTO createRecycleOrder(CreateRecycleOrderForm createForm);

    /**
     * 查询所有回收订单
     * @param queryForm 根据用户id查询订单传入的参数
     * @return QueryOrderDTO 返回查询到的订单信息对象
     */
    public QueryRecycleOrderResultDTO queryRecycleOrderByUserId(QueryRecycleOrderByUserIdForm queryForm);

    /**
     * 条件查询回收订单
     * @param queryForm 根据创建时间查询订单传入的参数
     * @return QueryOrderDTO 返回查询到的订单信息对象
     */
    public List<QueryRecycleOrderDTO> queryRecycleOrderByCreateTime(QueryRecycleOrderByCreateTimeForm queryForm);

    /**
     * 条件查询回收订单
     * @param queryForm 根据订单状态查询订单传入的参数
     * @return QueryOrderDTO 返回查询到的订单信息对象
     */
    public List<QueryRecycleOrderDTO> queryRecycleOrderByStatus(QueryRecycleOrderByStatusForm queryForm);

    /**
     * 取消订单
     * @param cancelForm 取消订单传入的参数
     * @return 返回取消的订单信息对象
     */
    public CancelRecycleOrderDTO cancelRecycleOrder(CancelRecycleOrderForm cancelForm);

    /**
     * 订单评论
     * @param commentForm 订单评论时传入的参数
     * @return 返回评论的订单信息对象
     */
    public CommentRecycleOrderDTO commentRecycleOrder(CommentRecycleOrderForm commentForm);

    /**
     * 删除订单
     * @param id 传入要删除订单的id
     * @return 返回true、false
     */
    public boolean deletedRecycleOrder(Integer id);

    /**
     * 管理员确认订单
     * @param updateForm 确认订单时所传入的参数
     * @return 确认后的回收订单信息对象
     */
    public UpdateRecycleOrderDTO updateRecycleOrder(UpdateRecycleOrderForm updateForm);


    /**
     * 管理员查询回收订单
     * @param queryForm 管理员查询回收订单传入的参数
     * @return 返回查询到的订单信息对象
     */
    public QueryRecycleOrderResultDTO adminQueryAllRecycleOrder(QueryRecycleOrderByUserIdForm queryForm);

    /**
     * 订单支付(商家向用户付款)
     * @param payBalanceForm 付款传入的参数
     * @return 返回付款后的订单信息对象
     */
    public String orderPay(PayBalanceForm payBalanceForm);

}
