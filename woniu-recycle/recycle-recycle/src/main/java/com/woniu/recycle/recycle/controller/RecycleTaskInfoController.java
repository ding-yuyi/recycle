package com.woniu.recycle.recycle.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 回收工单详情表 前端控制器
 * </p>
 *
 * @author wjh
 * @since 2021-08-12
 */
@RestController
@RequestMapping("/recycle-task-info")
public class RecycleTaskInfoController {

}

