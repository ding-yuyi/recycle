package com.woniu.recycle.recycle.component;

/**
 * @Title: RecycleOrderLogEnum
 * @ProjectName woniu-recycle
 * @Description: TODO
 * @Author wl
 * @Date 2021/8/13 9:52
 * @Version:1.0
 */
public enum  RecycleOrderLogEnum {
    COPY_ERROR("对象复制失败",4002),
    PARAM_VALIDATION_ERROR("参数校验错误",4020),
    RECYCLE_ORDER_LOG_ERROR("记录废品回收订单日志失败",4051),
    LOG_SEARCH_ERROR("日志查询失败",4053);


    String msg;

    Integer code;

    public String getMsg() {
        return msg;
    }

    public Integer getCode() {
        return code;
    }

    RecycleOrderLogEnum(String msg, Integer code){
        this.msg=msg;
        this.code=code;
    }
}
