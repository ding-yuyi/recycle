package com.woniu.recycle.recycle.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.recycle.recycle.controller.form.QueryRecycleTaskLogForm;
import com.woniu.recycle.recycle.domain.RecycleTaskLog;
import com.woniu.recycle.recycle.dto.QueryRecycleTaskLogResultDTO;

/**
 * <p>
 * 回收工单日志表 服务类
 * </p>
 *
 * @author wjh
 * @since 2021-08-12
 */
public interface RecycleTaskLogService extends IService<RecycleTaskLog> {
    /**
     * 废品回收工单日志查询
     * @param queryForm 工单日志查询传入的参数
     * @return 工单日志查询信息对象
     */
    public QueryRecycleTaskLogResultDTO queryOrderLog(QueryRecycleTaskLogForm queryForm);
}
