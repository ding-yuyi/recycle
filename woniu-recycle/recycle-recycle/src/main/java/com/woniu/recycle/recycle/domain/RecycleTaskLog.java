package com.woniu.recycle.recycle.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 回收工单日志表
 * </p>
 *
 * @author wjh
 * @since 2021-08-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="RecycleTaskLog对象", description="回收工单日志表")
public class RecycleTaskLog implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "工单id")
    private Integer taskId;

    @ApiModelProperty(value = "记录日志生成原因，状态")
    private String logReason;

    @ApiModelProperty(value = "操作者类型 0 普通用户 | 1 管理员")
    private Integer logType;

    @ApiModelProperty(value = "操作者id")
    private Integer userId;

    @ApiModelProperty(value = "日志生成时间")
    private Long createTime;



}
