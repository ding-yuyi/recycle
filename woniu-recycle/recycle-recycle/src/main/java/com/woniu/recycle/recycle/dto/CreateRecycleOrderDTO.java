package com.woniu.recycle.recycle.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Title: CreateOrderDTO
 * @ProjectName woniu-recycle
 * @Description: 创建废品回收订单信息对象
 * @Author wl
 * @Date 2021/8/12 14:55
 * @Version:1.0
 */
@Data
public class CreateRecycleOrderDTO {

//    @ApiModelProperty(value = "订单id")
//    private Integer id;

    @ApiModelProperty(value = "订单编号")
    private String recycleOrderNum;

    @ApiModelProperty(value = "商品总价")
    private BigDecimal total;

    @ApiModelProperty(value = "用户id")
    private Integer userId;

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "地址id")
    private Integer addrId;

    @ApiModelProperty(value = "用户电话号码")
    private String userPhone;

    @ApiModelProperty(value = "收获地址")
    private String addrDesc;


    @ApiModelProperty(value = "预约上门时间")
    private Long visitTime;

    @ApiModelProperty(value = "创建时间")
    private Long createTime;

    @ApiModelProperty(value = "订单状态 0 未收货 | 1 已收货（订单结束）| 2 已评价 | 3 已取消")
    private Integer status;

    @ApiModelProperty(value = "商品类别名称")
    private String classifyName;

    @ApiModelProperty(value = "商品类别id")
    private Integer classifyId;




}
