package com.woniu.recycle.recycle.component;

import com.woniu.recycle.recycle.domain.RecycleOrder;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @Title: LogReason
 * @ProjectName woniu-recycle
 * @Description: 废品回收订单日志原因
 * @Author wl
 * @Date 2021/8/17 16:10
 * @Version:1.0
 */
@Data
@AllArgsConstructor
public class RecycleOrderLogReason {
    private String reason;
    private RecycleOrder order;
}
