package com.woniu.recycle.recycle.controller;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.woniu.recycle.commons.core.model.ResponseEntity;
import com.woniu.recycle.commons.web.component.BaseController;
import com.woniu.recycle.recycle.component.RecycleOrderLogEnum;
import com.woniu.recycle.recycle.controller.form.QueryRecycleOrderLogForm;
import com.woniu.recycle.recycle.dto.QueryRecycleOrderLogResultDTO;
import com.woniu.recycle.recycle.exception.RecycleOrderLogException;
import com.woniu.recycle.recycle.service.RecycleOrderLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * <p>
 * 回收商品订单日志表
 * </p>
 *
 * @author cccyyy
 * @since 2021-08-11
 */
@Slf4j
@RestController
@RequestMapping("/order/log")
@Api(tags = "订单日志查询管理")
public class RecycleOrderLogController extends BaseController {

    @Autowired
    private RecycleOrderLogService recycleOrderLogService;
    @ApiOperation(value = "订单日志查询")
    @PostMapping("/queryOrderLog")
    public ResponseEntity<QueryRecycleOrderLogResultDTO> queryOrderLog(@Valid @RequestBody QueryRecycleOrderLogForm queryForm, BindingResult result){
        log.info("查询订单日志传入的参数：{}",queryForm);
        if (result.hasErrors()) throw new RecycleOrderLogException(RecycleOrderLogEnum.PARAM_VALIDATION_ERROR);
        QueryRecycleOrderLogResultDTO logResultDTO = recycleOrderLogService.queryOrderLog(queryForm);
        if (logResultDTO==null){
            return ResponseEntity.BuildError(QueryRecycleOrderLogResultDTO.class).setMsg("订单日志查询失败");
        }else {
            return ResponseEntity.BuildSuccess(QueryRecycleOrderLogResultDTO.class).setMsg("订单日志查询成功").setData(logResultDTO);
        }
    }
}
