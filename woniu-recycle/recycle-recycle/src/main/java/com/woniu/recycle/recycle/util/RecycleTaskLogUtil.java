package com.woniu.recycle.recycle.util;

import com.woniu.recycle.commons.web.util.ObjectMapperUtil;
import com.woniu.recycle.recycle.component.RecycleTaskLogReason;
import com.woniu.recycle.recycle.domain.RecycleTask;
import com.woniu.recycle.recycle.domain.RecycleTaskLog;

/**
 * @Title: RecycleTaskLogUtil
 * @ProjectName woniu-recycle
 * @Description: 记录骑手工单日志工具类
 * @Author wl
 * @Date 2021/8/17 16:19
 * @Version:1.0
 */
public class RecycleTaskLogUtil {
    public static RecycleTaskLog createLog(RecycleTask task, Integer logType, String reason, Integer userId){
        RecycleTaskLog recycleTaskLog = new RecycleTaskLog();
        recycleTaskLog.setTaskId(task.getId());
        recycleTaskLog.setCreateTime(System.currentTimeMillis());
        recycleTaskLog.setUserId(task.getUserId());
        recycleTaskLog.setLogType(logType);
        recycleTaskLog.setLogReason(ObjectMapperUtil.parseObject(new RecycleTaskLogReason(reason, task)));

        return recycleTaskLog;
    }
}
