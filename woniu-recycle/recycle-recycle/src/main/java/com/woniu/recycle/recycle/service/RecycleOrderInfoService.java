package com.woniu.recycle.recycle.service;

import com.woniu.recycle.recycle.domain.RecycleOrderInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 回收订单明细表 服务类
 * </p>
 *
 * @author cy/lyl
 * @since 2021-08-11
 */
public interface RecycleOrderInfoService extends IService<RecycleOrderInfo> {

}
