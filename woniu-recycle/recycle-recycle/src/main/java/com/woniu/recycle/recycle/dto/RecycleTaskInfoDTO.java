package com.woniu.recycle.recycle.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Title: RecycleTaskInfoDTO
 * @ProjectName woniu-recycle
 * @Description: TODO
 * @Author wl
 * @Date 2021/8/18 16:25
 * @Version:1.0
 */
@Data
public class RecycleTaskInfoDTO {

    @ApiModelProperty(value = "废品回收工单详情id")
    private Integer id;

    @ApiModelProperty(value = "货物名称")
    private String productName;

    @ApiModelProperty(value = "货物图片地址")
    private String pictureUrl;

    @ApiModelProperty(value = "收购数量")
    private Integer count;

    @ApiModelProperty(value = "实际收购价格")
    private BigDecimal actualPrice;

    @ApiModelProperty(value = "货物单位")
    private String units;

    @ApiModelProperty(value = "小计")
    private BigDecimal total;

    @ApiModelProperty(value = "工单id")
    private Integer taskId;
}
