package com.woniu.recycle.recycle.feign;

/**
 *
 */

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotNull;


@Data
@ApiModel("openFeign的changeNumsel接口入参对象")
@AllArgsConstructor
public class ChangeProductNum {

    @NotNull
    @ApiModelProperty(value = "主键", example = "1")
    private Integer id;

    @NotNull
    @ApiModelProperty(value = "更改数量", example = "1")
    private Integer inventory;
}
