package com.woniu.recycle.recycle.domain;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 回收工单表
 * </p>
 *
 * @author wjh
 * @since 2021-08-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="RecycleTask对象", description="回收工单表")
public class RecycleTask implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "工单id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "工单号")
    private String taskNum;

    @ApiModelProperty(value = "骑手姓名")
    private String workerName;

    @ApiModelProperty(value = "骑手电话")
    private String workerPhone;

    @ApiModelProperty(value = "工单操作者id")
    private Integer userId;

    @ApiModelProperty(value = "用户姓名")
    private String username;

    @ApiModelProperty(value = "用户电话")
    private String userPhone;

    @ApiModelProperty(value = "地址")
    private String address;

    @ApiModelProperty(value = "预约上门时间")
    private Long visitTime;

    @ApiModelProperty(value = "骑手到达时间")
    private Long arriveTime;

    @ApiModelProperty(value = "总价")
    private BigDecimal total;

    @ApiModelProperty(value = "骑手工资")
    private BigDecimal payment;

    @ApiModelProperty(value = "取消原因")
    private String cancelReason;

    @ApiModelProperty(value = "取消时间")
    private Long cancelTime;

    @ApiModelProperty(value = "工单状态 0未派单 | 1已派单 | 2 上门中 | 3 收货中 | 4 回收中 | 5 完成 | 6 已取消")
    private Integer status;

    @ApiModelProperty(value = "工单完成时间")
    private Long finishTime;

    @ApiModelProperty(value = "回收确认时间(骑手把信息传回管理员，管理员确认)")
    private Long ensureTime;

    @ApiModelProperty(value = "订单id")
    private Integer orderId;

    @ApiModelProperty(value = "派单时间")
    private Long sendTime;

    @ApiModelProperty(value = "工单生成时间")
    private Long createTime;

    @ApiModelProperty(value = "产品类型id")
    private Integer productClassifyId;

    @ApiModelProperty(value = "产品类型")
    private String productClassify;




}
