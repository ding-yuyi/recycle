package com.woniu.recycle.recycle.mapper;

import com.woniu.recycle.recycle.domain.RecycleOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 回收商品订单 Mapper 接口
 * </p>
 *
 * @author cy/lyl
 * @since 2021-08-11
 */
public interface RecycleOrderMapper extends BaseMapper<RecycleOrder> {

}
