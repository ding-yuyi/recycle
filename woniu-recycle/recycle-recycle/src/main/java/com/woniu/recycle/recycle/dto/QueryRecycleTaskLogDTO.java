package com.woniu.recycle.recycle.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Title: QueryRecycleTaskLogDTO
 * @ProjectName woniu-recycle
 * @Description: TODO
 * @Author wl
 * @Date 2021/8/20 11:16
 * @Version:1.0
 */
@Data
public class QueryRecycleTaskLogDTO {
    @ApiModelProperty(value = "回收工单日志id")
    private Integer id;

    @ApiModelProperty(value = "工单id")
    private Integer taskId;

    @ApiModelProperty(value = "记录日志生成原因，状态")
    private String logReason;

    @ApiModelProperty(value = "操作者类型 0 普通用户 | 1 管理员")
    private Integer logType;

    @ApiModelProperty(value = "操作者id")
    private Integer userId;

    @ApiModelProperty(value = "日志生成时间")
    private Long createTime;
}
