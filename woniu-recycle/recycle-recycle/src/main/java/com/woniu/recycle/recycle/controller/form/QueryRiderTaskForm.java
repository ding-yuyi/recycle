package com.woniu.recycle.recycle.controller.form;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Title: QueryRiderTaskForm
 * @ProjectName woniu-recycle
 * @Description: 查询骑手工单传入的参数
 * @Author wl
 * @Date 2021/8/16 18:46
 * @Version:1.0
 */
@Data
public class QueryRiderTaskForm {

    @ApiModelProperty(value = "工单状态 0未派单 | 1 上门中 | 2 收货中 | 3 回收中 | 4 完成 | 5 已取消")
    private Integer status;

    //时间查询时的开始时间(派单时间)
    @ApiModelProperty(value = "开始时间")
    @Pattern(regexp = "^[1-9]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])\\s+(20|21|22|23|[0-1]\\d):[0-5]\\d:[0-5]\\d$",message = "时间格式：2021-08-01 10:00:00")
    private String startTime;

    //时间查询时的结束时间
    @ApiModelProperty(value = "结束时间")
    @Pattern(regexp = "^[1-9]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])\\s+(20|21|22|23|[0-1]\\d):[0-5]\\d:[0-5]\\d$",message = "时间格式：2021-08-01 10:00:00")
    private String finishTime;


    //分页查询的页码数
    @ApiModelProperty(value = "当前页数")
    @NotNull(message = "页码不能为空")
    private Integer pageIndex;

    //分页查询的数量
    @ApiModelProperty(value = "每页展示数量")
    @NotNull(message = "每页数量不能为空")
    private Integer pageSize=10;




}
