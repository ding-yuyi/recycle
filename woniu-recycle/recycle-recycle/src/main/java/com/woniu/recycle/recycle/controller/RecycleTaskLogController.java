package com.woniu.recycle.recycle.controller;


import com.woniu.recycle.commons.core.model.ResponseEntity;
import com.woniu.recycle.recycle.component.RecycleTaskLogEnum;
import com.woniu.recycle.recycle.controller.form.QueryRecycleOrderLogForm;
import com.woniu.recycle.recycle.controller.form.QueryRecycleTaskLogForm;
import com.woniu.recycle.recycle.dto.QueryRecycleOrderLogResultDTO;
import com.woniu.recycle.recycle.dto.QueryRecycleTaskLogResultDTO;
import com.woniu.recycle.recycle.exception.RecycleTaskLogException;
import com.woniu.recycle.recycle.service.RecycleTaskLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * <p>
 * 回收工单日志表 前端控制器
 * </p>
 *
 * @author wjh
 * @since 2021-08-12
 */
@Slf4j
@RestController
@RequestMapping("/task/log")
@Api(tags = "工单日志查询管理")
public class RecycleTaskLogController {

    @Autowired
    private RecycleTaskLogService recycleTaskLogService;
    @ApiOperation(value = "工单日志查询")
    @PostMapping("/queryOrderLog")
    public ResponseEntity<QueryRecycleTaskLogResultDTO> queryOrderLog(@Valid @RequestBody QueryRecycleTaskLogForm queryForm, BindingResult result){
        log.info("查询工单日志传入的参数：{}",queryForm);
        if (result.hasErrors()) throw new RecycleTaskLogException(RecycleTaskLogEnum.PARAM_VALIDATION_ERROR);
        QueryRecycleTaskLogResultDTO logResultDTO = recycleTaskLogService.queryOrderLog(queryForm);

        if (logResultDTO==null){
            return ResponseEntity.BuildError(QueryRecycleTaskLogResultDTO.class).setMsg("工单日志查询失败");
        }else {
            return ResponseEntity.BuildSuccess(QueryRecycleTaskLogResultDTO.class).setMsg("工单日志查询成功").setData(logResultDTO);
        }
    }
}

