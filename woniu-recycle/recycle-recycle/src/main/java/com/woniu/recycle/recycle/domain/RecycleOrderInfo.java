package com.woniu.recycle.recycle.domain;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 回收订单明细表
 * </p>
 *
 * @author cy/lyl
 * @since 2021-08-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="RecycleOrderInfo对象", description="回收订单明细表")
public class RecycleOrderInfo implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "订单id")
    private Integer orderId;

    @ApiModelProperty(value = "回收商品id")
    private Integer productId;

    @ApiModelProperty(value = "商品图片路径")
    private String productUrl;

    @ApiModelProperty(value = "商品数量")
    private Double count;

    @ApiModelProperty(value = "商品名称")
    private String productName;

    @ApiModelProperty(value = "商品单价")
    private BigDecimal actualPrice;

    @ApiModelProperty(value = "商品小计")
    private BigDecimal total;




}
