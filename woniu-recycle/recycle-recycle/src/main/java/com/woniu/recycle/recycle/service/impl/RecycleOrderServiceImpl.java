package com.woniu.recycle.recycle.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.recycle.commons.core.model.ResponseEntity;
import com.woniu.recycle.commons.web.util.BeanCopyUtil;
import com.woniu.recycle.commons.web.util.DateUtil;
import com.woniu.recycle.commons.web.util.ServletUtil;
import com.woniu.recycle.recycle.component.DeletedStatus;
import com.woniu.recycle.recycle.component.RecycleOrderEnum;
import com.woniu.recycle.recycle.component.RecycleOrderLogEnum;
import com.woniu.recycle.recycle.controller.form.*;
import com.woniu.recycle.recycle.domain.RecycleOrder;
import com.woniu.recycle.recycle.domain.RecycleOrderLog;
import com.woniu.recycle.recycle.domain.RecycleTask;
import com.woniu.recycle.recycle.dto.*;
import com.woniu.recycle.recycle.exception.RecycleOrderException;
import com.woniu.recycle.recycle.exception.RecycleOrderLogException;
import com.woniu.recycle.recycle.feign.form.ChangeBalanceSelForm;
import com.woniu.recycle.recycle.feign.ProductFeignClient;
import com.woniu.recycle.recycle.feign.UserFeignClient;
import com.woniu.recycle.recycle.feign.form.SelectRecycleClassifyForm;
import com.woniu.recycle.recycle.feign.form.SelectRecycleProductForm;
import com.woniu.recycle.recycle.feign.model.*;
import com.woniu.recycle.recycle.mapper.RecycleOrderMapper;
import com.woniu.recycle.recycle.mapper.RecycleTaskMapper;
import com.woniu.recycle.recycle.service.RecycleOrderLogService;
import com.woniu.recycle.recycle.service.RecycleOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.recycle.recycle.service.RecycleTaskService;
import com.woniu.recycle.recycle.util.OrderNumUtil;
import com.woniu.recycle.recycle.util.UserUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 回收商品订单 服务实现类
 * </p>
 *
 * @author cy/lyl
 * @since 2021-08-11
 */
@Service
@Slf4j
public class RecycleOrderServiceImpl extends ServiceImpl<RecycleOrderMapper, RecycleOrder> implements RecycleOrderService {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private RecycleOrderMapper recycleOrderMapper;
    @Resource
    private RecycleOrderLogService recycleOrderLogService;

    @Resource
    private RecycleTaskMapper recycleTaskMapper;
    @Resource
    private RecycleTaskService recycleTaskService;

    @Resource
    private UserUtil userUtil;

    @Resource
    private UserFeignClient userFeignClient;

    @Resource
    private ProductFeignClient productFeignClient;
    /**
     * 创建回收订单方法
     * @param createForm 下单传入的参数
     * @return CreateRecycleOrderDTO 返回的订单信息对象
     */
    @Override
    public CreateRecycleOrderDTO createRecycleOrder(CreateRecycleOrderForm createForm) {
        log.info("创建回收订单传入的参数：{}",createForm);
        //新增一个回收订单
        if (createForm==null) throw new RecycleOrderException(RecycleOrderEnum.PARAM_ERROR);



        //查询商品是否存在,不存在则抛异常（需要调用openfeign）
        ResponseEntity<List<RecycleProduct>> responseProduct = productFeignClient.findCycleProduct(new SelectRecycleProductForm(createForm.getProductId(), createForm.getPage(), createForm.getNum()));
        if(null == responseProduct.getData()) throw new RecycleOrderException(responseProduct.getMsg(),responseProduct.getCode());
        List<RecycleProduct> data = responseProduct.getData();
        RecycleProduct product = data.get(0);

        Integer productId = product.getId();
        String productName = product.getName();
        log.info("获取商品信息：{},商品id:{}",productId,productName);
        /*
        //商品类别查询，获取商品类别id，商品类别名称
        ResponseEntity<List<RecycleProductClassify>> responseEntity = productFeignClient.selectClassify(new SelectRecycleClassifyForm(createForm.getClassifyId(), createForm.getPage(), createForm.getNum()));
        if(null == responseEntity.getData()) throw new RecycleOrderException(responseProduct.getMsg(),responseProduct.getCode());
        List<RecycleProductClassify> classifyList = responseEntity.getData();
        RecycleProductClassify classify = classifyList.get(0);
        Integer classifyId = classify.getId();
        String classifyName = classify.getName();
        log.info("获取商品类别信息：{},商品类别id:{}",classifyName,classifyId);
        */

        //查询地址是否存在
        ResponseEntity<Addr> responseAddr = userFeignClient.findAddrById(createForm.getAddrId());
        if(null == responseAddr.getData()) throw new RecycleOrderException(responseProduct.getMsg(),responseProduct.getCode());
        Addr addr = responseAddr.getData();
        String addrDesc = addr.getAddrDesc();
        log.info("获取地址信息：{},地址id:{}",addrDesc);

        //解析token，获取user信息
        User user = userUtil.getUser(ServletUtil.getRequest());
        Integer userId = user.getId();
        String username = user.getUsername();
        String tel = user.getTel();
        log.info("解析token，获取userId:{},username:{},tel:{}",userId,username,tel);


        RecycleOrder recycleOrder = new RecycleOrder();
        //将当前时间毫秒转换为yyyy-MM-dd HH:mm:ss格式
        log.info("添加新增回收订单的数据");

        recycleOrder.setCreateTime(System.currentTimeMillis());
        //订单状态 0 未收货 | 1 已收货（订单结束）|2 已付款| 3 已评价 | 4 已取消
        recycleOrder.setStatus(0);

        //地址需要连接地址表查询，记得要修改！！！！！！！！！！
        recycleOrder.setAddrId(createForm.getAddrId());
        recycleOrder.setAddrDesc(addrDesc);
        //废品类需要连接类别表查询，记得修改！！！！！！！！！！
        recycleOrder.setClassifyId(createForm.getClassifyId());
        recycleOrder.setClassifyName(createForm.getClassifyName());
        recycleOrder.setNotes(createForm.getNotes());
        //传入的预约时间 string转换为long
        Long visitTime = DateUtil.parse(createForm.getVisitTime(), "yyyy-MM-dd HH:mm:ss");
        recycleOrder.setVisitTime(visitTime);
        recycleOrder.setUserId(userId);
        recycleOrder.setUsername(username);
        recycleOrder.setUserPhone(createForm.getUserPhone());

        //设置订单号：年月日时分秒（14位）+用户ID+回收类别ID+随机数字（6位）
        recycleOrder.setRecycleOrderNum(OrderNumUtil.getNum(createForm.getClassifyId()));
        log.info("新增回收订单");

//        int insert = recycleOrderMapper.insert(recycleOrder);
        boolean flag = save(recycleOrder);
        if (!flag) throw new RecycleOrderException(RecycleOrderEnum.ORDER_ERROR);
        log.info("新增订单成功");
        //生成工单
        CreateRiderTaskForm createRiderTaskForm = new CreateRiderTaskForm();
        createRiderTaskForm.setOrderId(recycleOrder.getId());
        CreateRecycleTaskDTO riderTask = recycleTaskService.createRiderTask(createRiderTaskForm);
        log.info("废品工单生成成功：{}",riderTask);

        //添加日志记录
        log.info("添加回收订单日志");
        RecycleOrderLog recycleOrderLog = new RecycleOrderLog();
        recycleOrderLog.setLogCreateTime(recycleOrder.getCreateTime());
        recycleOrderLog.setOrderId(recycleOrder.getId());
        recycleOrderLog.setOrderNum(recycleOrder.getRecycleOrderNum());
        recycleOrderLog.setOrderStatus(recycleOrder.getStatus());
        //"操作者类型,0普通用户,1管理员
        recycleOrderLog.setLogType(0);
        recycleOrderLog.setUserId(recycleOrder.getUserId());
        recycleOrderLog.setLogReason("新增订单");
        boolean logFlag = recycleOrderLogService.save(recycleOrderLog);
        if (!logFlag) throw new RecycleOrderLogException(RecycleOrderLogEnum.RECYCLE_ORDER_LOG_ERROR);
        log.info("新增回收订单日志成功");
        //将下单信息copy到返回封装类CreateRecycleOrderDTO用于返回
        CreateRecycleOrderDTO createRecycleOrderDTO = BeanCopyUtil.copyObject(recycleOrder, CreateRecycleOrderDTO::new);
        if (createRecycleOrderDTO==null) throw new RecycleOrderException(RecycleOrderEnum.COPY_ERROR);
        log.info("开始redis存储");
        stringRedisTemplate.opsForHash().put("recycleOrder:user:id:"+1,"userId",createRecycleOrderDTO.getUserId()+"");
        stringRedisTemplate.opsForHash().put("recycleOrder:user:id:"+1,"username",createRecycleOrderDTO.getUsername());
        stringRedisTemplate.opsForHash().put("recycleOrder:user:id:"+1,"classifyId",createRecycleOrderDTO.getClassifyId()+"");
        stringRedisTemplate.opsForHash().put("recycleOrder:user:id:"+1,"classifyName",createRecycleOrderDTO.getClassifyName());
        stringRedisTemplate.opsForHash().put("recycleOrder:user:id:"+1,"visitTime",createRecycleOrderDTO.getVisitTime()+"");
        stringRedisTemplate.opsForHash().put("recycleOrder:user:id:"+1,"recycleOrderNum",createRecycleOrderDTO.getRecycleOrderNum());
        stringRedisTemplate.opsForHash().put("recycleOrder:user:id:"+1,"addrId",createRecycleOrderDTO.getAddrId()+"");
        stringRedisTemplate.opsForHash().put("recycleOrder:user:id:"+1,"addrDesc",createRecycleOrderDTO.getAddrDesc());
        stringRedisTemplate.expire("recycleOrder:user:id:"+1,60*60*24, TimeUnit.MINUTES);
        log.info("redis存储成功");

        log.info("返回创建的回收订单明细：{}",createRecycleOrderDTO);
        return createRecycleOrderDTO;

    }

    /**
     * 查询所有回收订单
     * @param queryForm 根据用户id查询订单传入的参数
     * @return QueryOrderDTO 返回查询到的订单信息对象
     */
    @Override
    public QueryRecycleOrderResultDTO queryRecycleOrderByUserId(QueryRecycleOrderByUserIdForm queryForm) {
        log.info("查询所有回收订单时的参数{}",queryForm);
        if (queryForm==null) throw new RecycleOrderException(RecycleOrderEnum.PARAM_ERROR);


        //解析token，获取user信息
        User user = userUtil.getUser(ServletUtil.getRequest());
        Integer userId = user.getId();
        log.info("解析token，获取userId:{}",userId);

        Page<RecycleOrder> page = new Page<>(queryForm.getPageIndex(),queryForm.getPageSize());

        //通过token获取userId，username;
//        String token = ServletUtil.getRequest().getHeader("auth_token");
//        Claims claims = jwtTemplate.parseJwt(token);
//        String accountId= claims.get("userId").toString();
//        String username = stringRedisTemplate.opsForHash().get("task:user:account:id:" + accountId, "userName").toString();
//        Integer userId =Integer.parseInt(stringRedisTemplate.opsForHash().get("task:user:account:id:" + accountId,"userId").toString());

        log.info("分页展示设置条件");
        QueryWrapper<RecycleOrder> queryWrapper = new QueryWrapper<>();
//        queryWrapper.eq(queryForm.getUserId().equals(userId),"user_id",queryForm.getUserId()).eq("deleted",0);
        log.info("进行分页查询");
        Long startTime =null;
        if (queryForm.getStartTime()!=null && queryForm.getStartTime()!=""){
            startTime = DateUtil.parse(queryForm.getStartTime(), "yyyy-MM-dd HH:mm:ss");
            log.info("查询开始时间",startTime);
        }
        Long finishTime =null;
        if (queryForm.getFinishTime()!=null && queryForm.getFinishTime()!=""){
            finishTime = DateUtil.parse(queryForm.getFinishTime(), "yyyy-MM-dd HH:mm:ss");
            log.info("查询结束时间",finishTime);
        }



        queryWrapper.eq("user_id",userId)
                .eq(queryForm.getStatus()!=null,"status",queryForm.getStatus())
                .ge(startTime!=null,"create_time",startTime)
                .le(finishTime!=null,"create_time",finishTime)
                .orderByDesc("create_time")
                .eq("deleted",0);


        Page<RecycleOrder> selectPage = recycleOrderMapper.selectPage(page, queryWrapper);
        log.info("分页展示所有回收订单的结果：{}",selectPage.getRecords());
        if (selectPage==null||selectPage.getRecords().size()<=0) throw new RecycleOrderException(RecycleOrderEnum.ORDER_SEARCH_ERROR);
        //将查询的订单信息copy到返回封装类QueryOrderDTO用于返回
        QueryRecycleOrderResultDTO resultDTO = BeanCopyUtil.copyObject(selectPage, QueryRecycleOrderResultDTO::new);
        log.info("分页展示的工单数据",resultDTO);

        List<QueryRecycleOrderDTO> queryRecycleOrderDTOS = BeanCopyUtil.copyList(selectPage.getRecords(), QueryRecycleOrderDTO::new);
        if (queryRecycleOrderDTOS ==null) throw new RecycleOrderException(RecycleOrderEnum.COPY_ERROR);
        log.info("返回查询到的回收订单的明细：{}", queryRecycleOrderDTOS);

        resultDTO.setRecycleOrderDTOList(queryRecycleOrderDTOS);
        log.info("工单数据添加成功");

        return resultDTO;
    }

    /**
     * 条件查询回收订单
     * @param queryForm 根据创建时间查询订单传入的参数
     * @return QueryOrderDTO 返回查询到的订单信息对象
     */
    @Override
    public List<QueryRecycleOrderDTO> queryRecycleOrderByCreateTime(QueryRecycleOrderByCreateTimeForm queryForm) {
        log.info("根据时间查询回收订单接收的参数：{}",queryForm);
        if (queryForm==null) throw new RecycleOrderException(RecycleOrderEnum.PARAM_ERROR);

        Page<RecycleOrder> page = new Page<>(queryForm.getPageIndex(),queryForm.getPageSize());

        //通过token获取userId，username;
//        String token = ServletUtil.getRequest().getHeader("auth_token");
//        Claims claims = jwtTemplate.parseJwt(token);
//        String accountId= claims.get("userId").toString();
//        String username = stringRedisTemplate.opsForHash().get("task:user:account:id:" + accountId, "userName").toString();
//        Integer userId =Integer.parseInt(stringRedisTemplate.opsForHash().get("task:user:account:id:" + accountId,"userId").toString());


        //解析token，获取user信息
        User user = userUtil.getUser(ServletUtil.getRequest());
        Integer userId = user.getId();
        log.info("解析token，获取userId:{}",userId);

        QueryWrapper<RecycleOrder> queryWrapper = new QueryWrapper<>();
        Long startTime = DateUtil.parse(queryForm.getStartTime(), "yyyy-MM-dd HH:mm:ss");
        Long finishTime = DateUtil.parse(queryForm.getFinishTime(), "yyyy-MM-dd HH:mm:ss");

        //最终的user_id通过token获取，要修改！！！！！！！！
        queryWrapper.gt("create_time",startTime)
        .lt("create_time",finishTime)
        .eq("user_id",userId)
        .eq("deleted",0);

        IPage<RecycleOrder> selectPage = recycleOrderMapper.selectPage(page, queryWrapper);
        log.info("根据时间查询订单结果：{}",selectPage.getRecords());
        if (selectPage==null||selectPage.getRecords().size()<=0) throw new RecycleOrderException(RecycleOrderEnum.ORDER_SEARCH_ERROR);
        //将查询的订单信息copy到返回封装类QueryOrderDTO用于返回
        List<QueryRecycleOrderDTO> queryRecycleOrderDTOS = BeanCopyUtil.copyList(selectPage.getRecords(), QueryRecycleOrderDTO::new);
        if (queryRecycleOrderDTOS ==null) throw new RecycleOrderException(RecycleOrderEnum.COPY_ERROR);
        log.info("返回根据时间查询的回收订单的明细：{}", queryRecycleOrderDTOS);
        return queryRecycleOrderDTOS;
    }

    /**
     * 条件查询回收订单
     * @param queryForm 根据订单状态查询订单传入的参数
     * @return QueryOrderDTO 返回查询到的订单信息对象
     */
    @Override
    public List<QueryRecycleOrderDTO> queryRecycleOrderByStatus(QueryRecycleOrderByStatusForm queryForm) {
        log.info("根据订单状态查询回收订单时传入的参数：{}",queryForm);
        if (queryForm==null||queryForm.getPageIndex()==null||queryForm.getPageSize()==null) throw new RecycleOrderException(RecycleOrderEnum.PARAM_ERROR);

        //通过token获取userId，username;
//        String token = ServletUtil.getRequest().getHeader("auth_token");
//        Claims claims = jwtTemplate.parseJwt(token);
//        String accountId= claims.get("userId").toString();
//        String username = stringRedisTemplate.opsForHash().get("task:user:account:id:" + accountId, "userName").toString();
//        Integer userId =Integer.parseInt(stringRedisTemplate.opsForHash().get("task:user:account:id:" + accountId,"userId").toString());


        //解析token，获取user信息
        User user = userUtil.getUser(ServletUtil.getRequest());
        Integer userId = user.getId();
        log.info("解析token，获取userId:{}",userId);

        Page<RecycleOrder> page = new Page<>(queryForm.getPageIndex(), queryForm.getPageSize());

        QueryWrapper<RecycleOrder> queryWrapper = new QueryWrapper<>();

        //最终的user_id通过token获取，要修改！！！！！！！！
        queryWrapper.eq("status",queryForm.getStatus())
                .eq("user_id",userId)
                .eq("deleted",0);

        IPage<RecycleOrder> selectPage = recycleOrderMapper.selectPage(page, queryWrapper);

        if (selectPage==null||selectPage.getRecords().size()<=0) throw new RecycleOrderException(RecycleOrderEnum.ORDER_SEARCH_ERROR);

        List<QueryRecycleOrderDTO> queryRecycleOrderDTOS = BeanCopyUtil.copyList(selectPage.getRecords(), QueryRecycleOrderDTO::new);
        if (queryRecycleOrderDTOS ==null) throw new RecycleOrderException(RecycleOrderEnum.COPY_ERROR);
        log.info("返回根据状态查询的回收订单的明细：{}", queryRecycleOrderDTOS);
        return queryRecycleOrderDTOS;
    }

    /**
     * 取消订单
     * @param cancelForm 取消订单传入的参数
     * @return 返回取消的订单信息对象
     */
    @Override
    public CancelRecycleOrderDTO cancelRecycleOrder(CancelRecycleOrderForm cancelForm) {
        log.info("取消废品回收订单时接收的参数：{}",cancelForm);
        if (cancelForm==null) throw new RecycleOrderException(RecycleOrderEnum.PARAM_ERROR);

        RecycleOrder recycleOrder = recycleOrderMapper.selectById(cancelForm.getId());
        if (recycleOrder==null) throw new RecycleOrderException(RecycleOrderEnum.ORDER_CANCEL_ERROR1);
        //订单状态 0 未收货 | 1 已收货（订单结束）|2 已付款| 3 已评价 | 4 已取消
        if (recycleOrder.getStatus()==1) throw new RecycleOrderException("订单已收货，取消失败");
        if (recycleOrder.getStatus()==2) throw new RecycleOrderException("订单已付款，取消失败");
        if (recycleOrder.getStatus()==3) throw new RecycleOrderException("订单已评论，取消失败");
        if (recycleOrder.getStatus()==4) throw new RecycleOrderException("订单已取消，取消失败");

        recycleOrder.setStatus(4);
        recycleOrder.setCancelReason(cancelForm.getCancelReason());
        recycleOrder.setCancelTime(System.currentTimeMillis());
        //通过token获取userId，username;
//        String token = ServletUtil.getRequest().getHeader("auth_token");
//        Claims claims = jwtTemplate.parseJwt(token);
//        String accountId= claims.get("userId").toString();
//        String username = stringRedisTemplate.opsForHash().get("task:user:account:id:" + accountId, "userName").toString();
//        Integer userId =Integer.parseInt(stringRedisTemplate.opsForHash().get("task:user:account:id:" + accountId,"userId").toString());


        //解析token，获取user信息
        User user = userUtil.getUser(ServletUtil.getRequest());
        Integer userId = user.getId();
        log.info("解析token，获取userId:{}",userId);

        //最终的user_id通过token获取，要修改！！！！！！！！
        QueryWrapper<RecycleOrder> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id",userId).eq("id",recycleOrder.getId());
        int i = recycleOrderMapper.update(recycleOrder, queryWrapper);

        if (i<=0) throw new RecycleOrderException(RecycleOrderEnum.ORDER_CANCEL_ERROR2);
        //添加日志记录
        log.info("取消回收订单日志");
        RecycleOrderLog recycleOrderLog = new RecycleOrderLog();
        recycleOrderLog.setLogCreateTime(System.currentTimeMillis());
        recycleOrderLog.setOrderId(recycleOrder.getId());
        recycleOrderLog.setOrderNum(recycleOrder.getRecycleOrderNum());
        recycleOrderLog.setOrderStatus(recycleOrder.getStatus());
        //"操作者类型,0普通用户,1管理员
        recycleOrderLog.setLogType(0);
        recycleOrderLog.setUserId(recycleOrder.getUserId());
        recycleOrderLog.setLogReason("取消订单");
        boolean logFlag = recycleOrderLogService.save(recycleOrderLog);
        if (!logFlag) throw new RecycleOrderLogException(RecycleOrderLogEnum.RECYCLE_ORDER_LOG_ERROR);
        log.info("新增回收订单日志成功");

        CancelRecycleOrderDTO cancelRecycleOrderDTO = BeanCopyUtil.copyObject(recycleOrder, CancelRecycleOrderDTO::new);
        if (cancelRecycleOrderDTO==null) throw new RecycleOrderException(RecycleOrderEnum.COPY_ERROR);
        log.info("返回取消回收订单的明细：{}",cancelRecycleOrderDTO);

        return cancelRecycleOrderDTO;
    }

    /**
     * 订单评论
     * @param commentForm 订单评论时传入的参数
     * @return 返回评论的订单信息对象
     */
    @Override
    public CommentRecycleOrderDTO commentRecycleOrder(CommentRecycleOrderForm commentForm) {
        log.info("评论订单时传入的参数：{}",commentForm);
        if (commentForm==null) throw new RecycleOrderException(RecycleOrderEnum.PARAM_ERROR);


        //解析token，获取user信息
        User user = userUtil.getUser(ServletUtil.getRequest());
        Integer userId = user.getId();
        log.info("解析token，获取userId:{}",userId);

        RecycleOrder recycleOrder = recycleOrderMapper.selectById(commentForm.getId());
        if (recycleOrder==null) throw new RecycleOrderException(RecycleOrderEnum.ORDER_CANCEL_ERROR1);
        if (recycleOrder.getStatus()==0) throw new RecycleOrderException("订单未收货，评论失败");
        if (recycleOrder.getStatus()==1) throw new RecycleOrderException("订单已收货，未付款，评论失败");
        if (recycleOrder.getStatus()==3) throw new RecycleOrderException("订单已评论，评论失败");
        if (recycleOrder.getStatus()==4) throw new RecycleOrderException("订单已取消，评论失败");
        QueryWrapper<RecycleOrder> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id",userId).eq("id",recycleOrder.getId());


        recycleOrder.setComment(commentForm.getComment());
        recycleOrder.setLevel(commentForm.getLevel());
        //订单状态 0 未收货 | 1 已收货（订单结束）|2 已付款| 3 已评价 | 4 已取消
        recycleOrder.setStatus(3);

        int i = recycleOrderMapper.update(recycleOrder, queryWrapper);
        if (i<=0) throw new RecycleOrderException(RecycleOrderEnum.ORDER_CANCEL_ERROR2);
        log.info("评论后的回收订单明细：{}",recycleOrder);
        CommentRecycleOrderDTO commentRecycleOrderDTO = BeanCopyUtil.copyObject(recycleOrder, CommentRecycleOrderDTO::new);
        if (commentRecycleOrderDTO==null) throw new RecycleOrderException(RecycleOrderEnum.COPY_ERROR);

        log.info("返回评论后的回收订单明细：{}",commentRecycleOrderDTO);
        return commentRecycleOrderDTO;
    }

    /**
     * 删除订单
     * @param id 传入要删除订单的id
     * @return 返回true、false
     */
    @Override
    public boolean deletedRecycleOrder(Integer id) {
        log.info("要删除的废品回收订单的id：{}",id);
        if (id==null) throw new RecycleOrderException(RecycleOrderEnum.PARAM_ERROR);

        //解析token，获取user信息
        User user = userUtil.getUser(ServletUtil.getRequest());
        Integer userId = user.getId();
        log.info("解析token，获取userId:{}",userId);

        RecycleOrder recycleOrder = recycleOrderMapper.selectById(id);
        if (recycleOrder.getDeleted()==DeletedStatus.IS_DELETED) throw new RecycleOrderException(RecycleOrderEnum.ORDER_DELETED_ERROR);

        //逻辑删除 0 未删除 | 1 已删除（用于用户端查询）
        recycleOrder.setDeleted(DeletedStatus.IS_DELETED);

        boolean flag = updateById(recycleOrder);
        if (flag){
            //添加日志记录
            log.info("删除回收订单日志");
            RecycleOrderLog recycleOrderLog = new RecycleOrderLog();
            recycleOrderLog.setLogCreateTime(System.currentTimeMillis());
            recycleOrderLog.setOrderId(recycleOrder.getId());
            recycleOrderLog.setOrderNum(recycleOrder.getRecycleOrderNum());
            recycleOrderLog.setOrderStatus(recycleOrder.getStatus());
            //"操作者类型,0普通用户,1管理员
            recycleOrderLog.setLogType(0);
            recycleOrderLog.setUserId(recycleOrder.getUserId());
            recycleOrderLog.setLogReason("删除订单");
            boolean logFlag = recycleOrderLogService.save(recycleOrderLog);
            if (!logFlag) throw new RecycleOrderLogException(RecycleOrderLogEnum.RECYCLE_ORDER_LOG_ERROR);
            log.info("新增回收订单日志成功");
        }else {
           throw new RecycleOrderException(RecycleOrderEnum.ORDER_DELETED_FAIL_ERROR);

        }
        log.info("返回是否删除成功：{}",flag);
        return flag;
    }

    /**
     * 管理员确认订单
     * @param updateForm 确认订单时所传入的参数
     * @return 确认后的回收订单信息对象
     */
    @Override
    public UpdateRecycleOrderDTO updateRecycleOrder(UpdateRecycleOrderForm updateForm) {
        log.info("确认订单时修改订单传入的参数：{}",updateForm);
        if (updateForm==null) throw new RecycleOrderException(RecycleOrderEnum.PARAM_ERROR);
        RecycleOrder recycleOrder = recycleOrderMapper.selectById(updateForm.getId());
        if (recycleOrder.getStatus()==1) throw new RecycleOrderException("订单已确认，订单确认失败");
        if (recycleOrder.getStatus()==2) throw new RecycleOrderException("订单已付款，订单确认失败");
        if (recycleOrder.getStatus()==3) throw new RecycleOrderException("订单已评价，订单确认失败");
        if (recycleOrder.getStatus()==4) throw new RecycleOrderException("订单已取消，订单确认失败");
        recycleOrder.setTotal(updateForm.getTotal());
        //订单状态 0 未收货 | 1 已收货（订单结束）|2 已付款| 3 已评价 | 4 已取消
        recycleOrder.setStatus(1);
        recycleOrder.setFinishTime(System.currentTimeMillis());


        boolean flag = updateById(recycleOrder);
        if (flag){
            //添加日志记录
            log.info("确认回收订单日志");
            RecycleOrderLog recycleOrderLog = new RecycleOrderLog();
            recycleOrderLog.setLogCreateTime(System.currentTimeMillis());
            recycleOrderLog.setOrderId(recycleOrder.getId());
            recycleOrderLog.setOrderNum(recycleOrder.getRecycleOrderNum());
            //订单状态 0 未收货 | 1 已收货（订单结束）|2 已付款| 3 已评价 | 4 已取消
            recycleOrderLog.setOrderStatus(recycleOrder.getStatus());
            //"操作者类型,0普通用户,1管理员
            recycleOrderLog.setLogType(1);
            recycleOrderLog.setUserId(recycleOrder.getUserId());
            recycleOrderLog.setLogReason("确认订单并付款,付款金额为："+recycleOrder.getTotal());
            boolean logFlag = recycleOrderLogService.save(recycleOrderLog);
            if (!logFlag) throw new RecycleOrderLogException(RecycleOrderLogEnum.RECYCLE_ORDER_LOG_ERROR);
            log.info("新增回收订单日志成功");
        }else {
            throw new RecycleOrderException(RecycleOrderEnum.ORDER_CONFIRM_ERROR);

        }
        log.info("确认并付款后的订单明细：{}",recycleOrder);
        UpdateRecycleOrderDTO updateRecycleOrderDTO = BeanCopyUtil.copyObject(recycleOrder, UpdateRecycleOrderDTO::new);
        log.info("返回的确认并付款的订单明细：{}", updateRecycleOrderDTO);

        return updateRecycleOrderDTO;
    }

    /**
     * 管理员查询所有的订单
     * @param queryForm
     * @return 返回查询到的订单信息对象
     */
    @Override
    public QueryRecycleOrderResultDTO adminQueryAllRecycleOrder(QueryRecycleOrderByUserIdForm queryForm) {
        log.info("根据时间查询回收订单接收的参数：{}",queryForm);
        if (queryForm==null) throw new RecycleOrderException(RecycleOrderEnum.PARAM_ERROR);

        //解析token，获取user信息
        User user = userUtil.getUser(ServletUtil.getRequest());
        Integer userId = user.getId();
        log.info("解析token，获取userId:{}",userId);

        Page<RecycleOrder> page = new Page<>(queryForm.getPageIndex(), queryForm.getPageSize());
        log.info("进行分页查询");
        Long startTime =null;
        if (queryForm.getStartTime()!=null && queryForm.getStartTime()!=""){
            startTime = DateUtil.parse(queryForm.getStartTime(), "yyyy-MM-dd HH:mm:ss");
            log.info("查询开始时间",startTime);
        }
        Long finishTime =null;
        if (queryForm.getFinishTime()!=null && queryForm.getFinishTime()!=""){
            finishTime = DateUtil.parse(queryForm.getFinishTime(), "yyyy-MM-dd HH:mm:ss");
            log.info("查询结束时间",finishTime);
        }


        QueryWrapper<RecycleOrder> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(userId!=null,"user_id",userId)
                .eq(queryForm.getStatus()!=null,"status",queryForm.getStatus())
                .gt(startTime!=null,"create_time",startTime)
                .lt(finishTime!=null,"create_time",finishTime)
                .orderByDesc("create_time");
        IPage<RecycleOrder> selectPage = recycleOrderMapper.selectPage(page, queryWrapper);

        log.info("分页展示管理员查询的所有回收订单的结果：{}",selectPage.getRecords());
        if (selectPage==null||selectPage.getRecords().size()<=0) throw new RecycleOrderException(RecycleOrderEnum.ORDER_SEARCH_ERROR);
        //将查询的订单信息copy到返回封装类QueryOrderDTO用于返回
        QueryRecycleOrderResultDTO recycleOrderResultDTO = BeanCopyUtil.copyObject(selectPage, QueryRecycleOrderResultDTO::new);
        log.info("分页展示的工单数据",recycleOrderResultDTO);


        List<QueryRecycleOrderDTO> queryRecycleOrderDTOS = BeanCopyUtil.copyList(selectPage.getRecords(), QueryRecycleOrderDTO::new);
        if (queryRecycleOrderDTOS ==null) throw new RecycleOrderException(RecycleOrderEnum.COPY_ERROR);
        log.info("返回查询到的回收订单的明细：{}", queryRecycleOrderDTOS);

        recycleOrderResultDTO.setRecycleOrderDTOList(queryRecycleOrderDTOS);
        log.info("工单数据添加成功");

        return recycleOrderResultDTO;
    }

    /**
     * 订单支付(商家向用户付款)
     * @param payBalanceForm 付款传入的参数
     * @return 返回付款后的订单信息对象
     */
    @Override
    public String orderPay(PayBalanceForm payBalanceForm) {
        log.info("付款时传入的参数：{}",payBalanceForm);
        if (payBalanceForm==null) throw new RecycleOrderException(RecycleOrderEnum.PARAM_ERROR);
        //解析token，获取user信息
        User user = userUtil.getUser(ServletUtil.getRequest());
        Integer userId = user.getId();
        BigDecimal balance = user.getBalance();
        user.setBalance(balance.add(payBalanceForm.getTotal()));
        //查询订单
        RecycleOrder recycleOrder = recycleOrderMapper.selectById(payBalanceForm.getId());
        if (recycleOrder.getStatus()!=1) throw new RecycleOrderException(RecycleOrderEnum.ORDER_PAY_ERROR);
        //调用用户余额增加接口
        ResponseEntity<ChangeBalanceSel> balanceAdd = userFeignClient.balanceAdd(new ChangeBalanceSelForm(payBalanceForm.getUserId(), payBalanceForm.getTotal(),payBalanceForm.getRecycleOrderNum()));
        if (balanceAdd.getData()==null) throw new RecycleOrderException(RecycleOrderEnum.USER_BALANCE_ERROR);

        //添加日志记录
        log.info("支付回收订单日志");
        RecycleOrderLog recycleOrderLog = new RecycleOrderLog();
        recycleOrderLog.setLogCreateTime(System.currentTimeMillis());
        recycleOrderLog.setOrderId(recycleOrder.getId());
        recycleOrderLog.setOrderNum(recycleOrder.getRecycleOrderNum());
        ////订单状态 0 未收货 | 1 已收货（订单结束）|2 已付款| 3 已评价 | 4 已取消
        recycleOrderLog.setOrderStatus(2);
        //"操作者类型,0普通用户,1管理员
        recycleOrderLog.setLogType(1);
        recycleOrderLog.setUserId(recycleOrder.getUserId());
        recycleOrderLog.setLogReason("确认订单并付款,付款金额为："+recycleOrder.getTotal());
        boolean logFlag = recycleOrderLogService.save(recycleOrderLog);
        if (!logFlag) throw new RecycleOrderLogException(RecycleOrderLogEnum.RECYCLE_ORDER_LOG_ERROR);
        log.info("新增回收订单日志成功");

        return "支付成功";
    }


}
