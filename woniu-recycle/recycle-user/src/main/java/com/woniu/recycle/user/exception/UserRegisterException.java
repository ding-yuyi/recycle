package com.woniu.recycle.user.exception;


public class UserRegisterException extends UserException{
    private static final long serialVersionUID=1L;
    public UserRegisterException(){}
    public UserRegisterException(String msg,Integer code){super(msg,code);}
    public UserRegisterException(UserRegisterExceptionEnum mee){
        super(mee.getMsg(),mee.getCode());
    }
}
