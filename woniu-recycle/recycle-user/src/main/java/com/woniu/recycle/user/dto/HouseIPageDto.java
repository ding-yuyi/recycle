package com.woniu.recycle.user.dto;

import com.woniu.recycle.user.domain.Housekeeper;
import lombok.Data;

import java.util.List;

@Data
public class HouseIPageDto {
    private Long size;
    private Long current;
    private List<HouseKeeperDto> HkdList;
}
