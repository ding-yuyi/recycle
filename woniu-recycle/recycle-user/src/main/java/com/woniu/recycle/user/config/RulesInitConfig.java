package com.woniu.recycle.user.config;
import com.woniu.recycle.user.domain.RolePermission;
import com.woniu.recycle.user.service.RolePermissionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;


@Component
@Slf4j
public class RulesInitConfig implements ApplicationRunner {

	@Resource
	private StringRedisTemplate stringRedisTemplate;

	@Resource
	private RolePermissionService rolePermissionService;

	@Override
	public void run(ApplicationArguments args) throws Exception {
		log.info("===开始向redis写入权限列表数据===");
		List<RolePermission> permissionList = rolePermissionService.list();
		for(RolePermission permission : permissionList) {
			stringRedisTemplate.opsForSet().add("user:role:"+permission.getRole(), permission.getPermissionUrl());
		}
		log.info("===写入权限列表数据完成===");
	}

}
