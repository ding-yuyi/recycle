package com.woniu.recycle.user.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class HouseKeeperDto {
    @ApiModelProperty(value = "家政员id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "家政员工名称")
    private String housekeeperName;

    @ApiModelProperty(value = "电话号码")
    private String housekeeperTel;

    @ApiModelProperty(value = "性别 0 保密 | 1 男 | 2 女")
    private Integer housekeeperGender;

    @ApiModelProperty(value = "工号")
    private String housekeeperNum;
}
