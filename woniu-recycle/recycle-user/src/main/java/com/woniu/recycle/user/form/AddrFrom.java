package com.woniu.recycle.user.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AddrFrom {

    @ApiModelProperty(value = "国家")
    private String nation;

    @ApiModelProperty(value = "省")
    private String province;

    @ApiModelProperty(value = "市")
    private String city;

    @ApiModelProperty(value = "区或县")
    private String country;

    @ApiModelProperty(value = "地址详情")
    private String addrDesc;

}
