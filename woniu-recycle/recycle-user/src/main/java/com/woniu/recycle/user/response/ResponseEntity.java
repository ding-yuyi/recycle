//package com.woniu.recycle.user.response;
//
//import java.util.List;
//
//
///**
//   *   响应类
// * @author liwei
// * @param <T>
// */
//public class ResponseEntity<T> {
//
//	private T data;
//
//	private Integer code;
//
//	private String msg;
//
//	public Object getData() {
//		return data;
//	}
//
//	public ResponseEntity<T> setData(T data) {
//		this.data = data;
//		return this;
//	}
//
//	public Integer getCode() {
//		return code;
//	}
//
//	public ResponseEntity<T> setCode(Integer code) {
//		this.code = code;
//		return this;
//	}
//
//	public String getMsg() {
//		return msg;
//	}
//
//	public ResponseEntity<T> setMsg(String msg) {
//		this.msg = msg;
//		return this;
//	}
//
//	public static <S> ResponseEntity<List<S>> BuildSuccessList(Class<S> type) {
//		return new ResponseEntity<>(ResponseEnum.RES_SUCCESS.getCode(),ResponseEnum.RES_SUCCESS.getMsg());
//	}
//
//	public static <S> ResponseEntity<S> BuildSuccess(Class<S> type) {
//		return new ResponseEntity<>(ResponseEnum.RES_SUCCESS.getCode(),ResponseEnum.RES_SUCCESS.getMsg());
//	}
//
//	public static ResponseEntity<?> BuildSuccess() {
//		return new ResponseEntity<>(ResponseEnum.RES_SUCCESS.getCode(),ResponseEnum.RES_SUCCESS.getMsg());
//	}
//
//	public static <S> ResponseEntity<List<S>> BuildErrorList(Class<S> type) {
//		return new ResponseEntity<>(ResponseEnum.RES_ERROR.getCode(),ResponseEnum.RES_ERROR.getMsg());
//	}
//
//	public static <S> ResponseEntity<S> BuildError(Class<S> type) {
//		return new ResponseEntity<>(ResponseEnum.RES_ERROR.getCode(),ResponseEnum.RES_ERROR.getMsg());
//	}
//
//	public static ResponseEntity<?> BuildError() {
//		return new ResponseEntity<>(ResponseEnum.RES_ERROR.getCode(),ResponseEnum.RES_ERROR.getMsg());
//	}
//
//	public ResponseEntity() {}
//
//	public ResponseEntity(Integer code, String msg) {
//		this.code = code;
//		this.msg = msg;
//	}
//
//}
