package com.woniu.recycle.user.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 地址表
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="Addr对象", description="地址表")
public class Addr implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "地址id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "用户id")
    private Integer userId;

    @ApiModelProperty(value = "国家")
    private String nation;

    @ApiModelProperty(value = "省")
    private String province;

    @ApiModelProperty(value = "市")
    private String city;

    @ApiModelProperty(value = "区或县")
    private String country;

    @ApiModelProperty(value = "地址详情")
    private String addrDesc;

    @ApiModelProperty(value = "0 不是默认地址 | 1 默认地址")
    private Integer isDefault;

}
