//package com.woniu.recycle.user.response;
//
///**
//   *   常规响应信息枚举
//   *   枚举一种对象,就是用来封装常量,一个枚举对象可以包括多个常量
//   *
// * @author liwei
// *
// */
//public enum ResponseEnum {
//
//	RES_SUCCESS(1111,"访问成功"),
//	RES_ERROR(500,"访问错误");
//
//
//
//	Integer code;
//	String msg;
//
//	public Integer getCode() {
//		return code;
//	}
//
//	public String getMsg() {
//		return msg;
//	}
//
//	ResponseEnum(Integer code, String msg) {
//		this.code = code;
//		this.msg = msg;
//	}
//
//}
