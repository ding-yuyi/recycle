package com.woniu.recycle.user.service.impl;

import com.woniu.recycle.user.domain.SpreadLog;
import com.woniu.recycle.user.mapper.SpreadLogMapper;
import com.woniu.recycle.user.service.SpreadLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 推广日志表 服务实现类
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
@Service
public class SpreadLogServiceImpl extends ServiceImpl<SpreadLogMapper, SpreadLog> implements SpreadLogService {

}
