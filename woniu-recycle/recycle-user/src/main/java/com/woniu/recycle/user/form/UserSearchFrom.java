package com.woniu.recycle.user.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UserSearchFrom {

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "角色 0 普通用户 | 1 管理员 | 2 超级管理员")
    private Integer role;

    private Long size;

    private Long current;
}
