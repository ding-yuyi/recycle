package com.woniu.recycle.user.exception;

public enum  UserLoginExceptionEnum {
    user_empty("账号或密码不对",1021),
    username_empty("请填写账号",1022),
    pwd_empty("请填写密码",1023),
    user_null("该用户不存在",1024),
    WORK_OUT("任务暂时不能被接受",1024),
    WORK_ARRAY("不能完成，任务已被完成",1025);

    String msg;
    Integer code;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    UserLoginExceptionEnum(String msg, Integer code) {
        this.msg = msg;
        this.code = code;
    }
}
