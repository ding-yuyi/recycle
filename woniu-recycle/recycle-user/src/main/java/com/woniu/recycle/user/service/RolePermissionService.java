package com.woniu.recycle.user.service;

import com.woniu.recycle.user.domain.RolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色权限表 服务类
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
public interface RolePermissionService extends IService<RolePermission> {

}
