package com.woniu.recycle.user.config;

/* *
 *类名：AliPayConfig
 *功能：基础配置类
 *详细：设置帐户有关信息及返回路径
 *修改日期：2017-04-05
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */

//public class AliPayConfig {
//
////↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
//
//    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
//    public static String app_id = "2021000117661871";
//
//    // 商户私钥，您的PKCS8格式RSA2私钥
//    public static String merchant_private_key = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCqeXn94g/S6jIWAQLdalYFsGQ/JEg3DzBRNrN11ZuZAAejyqsejCdc8de6mrSwjzjQyKWVQiPD4OXiDXfLQaZFkJqkHc7B7Wxyxw5RaJOPaJMCavKpl8A0vwsJdIHwnAnLM9yoJ4xq/P5kgkYv/1OfYxsfeVgMKUcHtgoglBJ50CPX3II7XzcYsUh9/DyxWb2AsabYVgSJiXAWt6Rr/aviy83kDHj+jLn/gL47wS38ssEaKuCUi3o0r5A/zfcESgDbtMTyxt5gYquL1yqkcfPXG5pryt7ijMcY0J1WiBAE9sOvFNR5625codG2KVrNNSJxFOInTYCyMlj6E0/i9E1VAgMBAAECggEAFSHR2YL4azDU3J1reX8BAtuG0bzxpsGfP7IENQsxqpmyWL96zuKYzpC8KUmcNHbMmDUHMaVtQY7TcF4zB2Mv9Vejk8yOIMQJuYCrabQI9rID5zzSr+s7zE4SVC25HdZ58FKRe9bLNUf/UP5FLRX9i7iBWDl2jKthPrd2MeupDqhmt3pXyuRf8i7gop1lXpQuOPuaG4u/1sJD8Dzc3OLF2PoR85LqYAdTfmw/uMzv2ssU2ijXKLamjpWZ3CTQqQFsPK5/GVrGHkLaIHwsMgnBfI87M7QiRBxhs0BQ06OIw8ueWRzxgk+B+1wM3rvHAumRLnqYBx0wyqv5hW2fpL2LUQKBgQDXnDWGAE9l3Q6Q6uzFJXVu0vxw1FUT/2ASYDS/srAtitvo2JpBpognt4KTC2Z7VvGhkI9xZSXPOm13dN3uavguciRiNrpu/chYPwACCx8v1AsiEr4OcBct0CKZZHpYzOk6oUUg7Zi/vLris6BRu8RP/wDDNj5slWe6a6C10C3O7wKBgQDKaL5TqvxRFt3+/uRcEbRCKc5Sp63k/Y29beImVr75n+mAB6Mv0HbpEm5oilSggVxMHmob0dBTdminK4fk3tTe819cT5b6AOLqc1aR52kXQW9Dsa54NP3MN9ohEEsWZ6+we777CjYPdr84yQmsdyRPGpwLupGKGJuZF5zrbLwn+wKBgD4usg0Mh9mT8ICBEHonPIAS33VW95aF31pNXskuqrrnwlacKZjng3sY5NKtxgeqYpO7EnOcvVPPwQRjsyHwvdT/XFjq+cfK733r5X+xX3Q2zmMsTp3xW+39igz9On/j0Rv0qPA/Nf+OWcPKFGGeKpb7r2edfFFqoXjVU4s8GAoHAoGAchzjUKBRaBYQSjbB3/ZLPhH7tDW8oCEiy+0rVpoJSQqSxg+IPOi2YWh2vpFebK8Brhhnkg+T97oF/llYX/wQ3zEuTgjBAWmGhM9JuzBP20CUzgFab4LQo1XJsGJTMup57nGVlkqHd+hmnkkZHxZ8bex+lU41xzlgX8skOMYwvgkCgYEAiavtwfiF5C/h21Pu1vj4T8MtVlyMnG2KNWKOFW+68BuKlO2apqwrUVPX292gpqJfoxhIWvuq++osB/VfDusys8sZWiK4ThNT24eYoyIk5pz1fwm5P/nBVK0KnpTIMDnBdUPlT5e6yd5x87r8UV5BX9wJEayFpaqPe4EeBwmx83s=";
//
//    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
//    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApUimyCNF9SBjCk/TA4DQneCOwsM4CIW+fAu/pJph4jZzMtKFpJX/UzA4JvkAXNsgujQ4LvHpygyThAylbDSJ8Slgdex0YnBRk/hpFQaiGRHfZRcKl+mZnGEJOAF2KH+zd38+YZ6OvB3a3Jep8nhZmnzUlDlWu5/+CX3nRQX30nMNmSz0loFzUYkORB6OMjU9ctKzBC9kxWIS/dNnEmQ8nYzsAkwbA/jDUUZfHN1eHCflqITUf4uIbBPIm7onHBFh6NBLkoqlOFVBLNRu/PuMJsCUp8diVOzYajeY+CYw+Ng4K1j2eyPdFqb720kYDXrG7tB2JF5k+0GPr1PXu0aGrQIDAQAB";
//
//    // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
//    public static String notify_url = "http://ehkwix.natappfree.cc/notify.do" ;
//
//    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
//    public static String return_url = "http://ehkwix.natappfree.cc/return.do";
//
//    // 签名方式
//    public static String sign_type = "RSA2";
//
//    // 字符编码格式
//    public static String charset = "utf-8";
//
//    // 支付宝网关
//    public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";
//
//    // 支付宝网关
//    public static String log_path = "C:\\";
//
//
////↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
//
//    /**
//     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
//     * @param sWord 要写入日志里的文本内容
//     */
//    public static void logResult(String sWord) {
//        FileWriter writer = null;
//        try {
//            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
//            writer.write(sWord);
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (writer != null) {
//                try {
//                    writer.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    }
//}


public class AliPayConfig {
    // 应用ID,您的APPID
    public static String APP_ID = "2021000117610611";

    // 商户私钥 就是生成的运用私钥
    public static String APP_PRIVATE_KEY = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCXJ4bpE0Ppa2muWjmUcKKcnbxmU8QwMt0EulqvtaiAggvrQgANViP3B/BQGhlbypagoJjKIhWkmhIB36FngiUnGtnw2PkSLvhlHlFP+p3z1pGX4qaOeEqcUhkL8ZX14eBNWolFb7qKtwABj3grHMkzR59H2bVnAQHVJFTYT946rKFZhoooReSXzviixp1EI1CDo7LGbH96SPlyrb1IInG2bV7el9OrKvVnuCE1mQwnzfn4Oc945yuRvg7obVVTyiFoAAfiQqaQ1/oSgzU90cVNJgIZ15Tzi5Sa8OJR79zo9RfdMCr7Xmj/bLuPz2CqwEPkv5K0XjPDsvdbtFk7xP0DAgMBAAECggEALAk696pCLb26Uy47+i/6X/BgU9txm60l0NvN0cT1q0K34DkhNMF0xuJMM7n2y1QxH+jJupHFhmfQeTVCm5pB4qui9NTxc0e13BfCDq/v1nsbfYlthrIq4nHUlDM0wialJ0QKG+2aBhGccdfvZXgEceka0V7ux2x2sTbddQuHrcXscbSdlzHNrxYHxwy4Ya7auBzgTLyY4PuKCkdhPMV7zpgNFF6PRLRZhOE22VchDjCbz6G42Mf7W4EXF0eWnqF0hsNrfrooOMALRg1aNNjDqWF1OxCH9Jk0HQccJ6/qvkrTGV7kwV51zqbq/2hVmdBqlcKNA+BeoLFh3xXFGYI32QKBgQDS902vShBoHsQE80Vc3vshxGUVbyDfM5c7ZAygbeXgsTy9jiQszKYpH0tELw1n+In/jNEOYMPZpLJeu3+/x6NaXw6mxpbIsWzn+L0vtYjdVDdIdDzuLMRznymKDwXcGAOTziaJgmPdN6vddd3WxpCdAE0hgSrgydtcrKGVwG6J1wKBgQC3a6/32GlPWy/W22+4r9LBJmleC/hoTZt1Oh6UD70MUdiA3HM6RhnhD/Q/hvD5YAj/A+sucZJuQ5ognElIgHjYmLfa9WHy4meZAuCo8nM98WHWqxdYmxQwVSde7YtE/kuV/wBuXvbUYiac05mzzY2YyOREJKk24p6LntBrBm24tQKBgFr/u62px+vxoWH1QW0u7aqCGYwmVEqS6V//5wZogSjrqqSlJGuClwXT7tZpaWH4riXiKucFzFUcGqiMPi2OQqMfr6U2siG9jpl9DLYAmxnFUtdY9FNUUiUBeZyIoGFwplJiMPNNoB/y/JpZVihnnEdsstiJ6+HskEeoW0bKK6QfAoGAYjGPcgL0Vdx+Xur5A4l1LZwvun50DPrHJ9nmduN6FLdYTZw3P9I5lohg/QVzyj1Ec6mYZTO97bPZNm4UuXC1wVb4zoqhVlMonPyyYx/w3A92jmLny7Kx+zILMsG+1j/BHVr30x6Zy5eEDl8loOGBVXLu6SArSsvFjv+FmlZFtSUCgYBJKx2tIhfoBlowAIs6cEFV/d3UCw34yfrITc4iQJ/R8LVmhW0BRGSB9UPIjg7dpiE3kc3KDk135v6ssrP4j0wzjPLkHDRgGBYn12jv0aX31KK/yVhnOqG93UPXA59daw+3r+wPv75wcF4h3EOBjDVlxk3CrErLHWsY484i+oQ8KQ==";

    // 支付宝公钥
    public static String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAp4Ypy2nDfxgtUL23h+2mlaHJDCvT4IoHFcbQ4Jck/E/YYc6HgLIWgtbsalxpXUioz3v/t5k+ULw+gvn8X0QcGBA5Q5SJFlNc+aVrB8P/TACOfoKrS11IQ9/ovjKVfPRswBVJJOsIOnKXy6ts8U3CX+UI6te8u+2VC8q5X7GIFstFaUxqGVaePLuLmjO3/7KghYqzO+aeSkCxV3bbwMh+mN/CpylQhWHXqCL0e7PNy7ZsS2Hagn0ZoRgKT9VMa+IeNZWEDUkxyL9otcQs27+GiOuyji73IViArnjuXRY647ORhRJpjVojx3C4Ut6Pvrh5gn9PshAgpwC8MdhZulH28QIDAQAB";

    // 服务器异步通知页面路径  需http://格式的完整路径,必须外网可以正常访问
    public static String notify_url = "http://6sqh2v.natappfree.cc/user/pay/NotifyController";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，必须外网可以正常访问(其实就是支付成功后返回的页面)
    public static String return_url = "http://6sqh2v.natappfree.cc/user/pay/Controller";

    // 签名方式
    public static String sign_type = "RSA2";

    // 字符编码格式
    public static String CHARSET = "UTF-8";

    // 支付宝网关，这是沙箱的网关
    public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";


}
