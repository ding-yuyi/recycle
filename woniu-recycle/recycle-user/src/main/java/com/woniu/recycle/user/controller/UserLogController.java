package com.woniu.recycle.user.controller;

import com.woniu.recycle.commons.core.model.ResponseEntity;
import com.woniu.recycle.commons.web.component.BaseController;
import com.woniu.recycle.user.domain.UserLog;
import com.woniu.recycle.user.dto.UserRegisterDto;
import com.woniu.recycle.user.exception.UserDataException;
import com.woniu.recycle.user.exception.UserDataExceptionEnum;
import com.woniu.recycle.user.exception.UserRegisterException;
import com.woniu.recycle.user.exception.UserRegisterExceptionEnum;
import com.woniu.recycle.user.form.UserRegisterFrom;
import com.woniu.recycle.user.service.UserLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 *
 */
@Slf4j
@RestController
@RequestMapping("/userLog")
@Api(tags = "用户日志中心")
public class UserLogController extends BaseController {
    @Resource
    private UserLogService userLogService;

    @PostMapping("/findUserLog")
    @ApiOperation("根据用户ID查日志")
    public ResponseEntity<List<UserLog>> findUserLog(Integer userId) {
        log.info("入参参数:{}", userId);
        if (userId == null)
            throw new UserDataException(UserDataExceptionEnum.id_is_null);
        List<UserLog> userLogList = userLogService.findUserLogById(userId);
        log.info("返回数据:{}", userId);
        if (userLogList == null) return ResponseEntity.BuildErrorList(UserLog.class).setMsg("账号不存在");
        return ResponseEntity.BuildSuccessList(UserLog.class).setMsg("查询成功").setData(userLogList);
    }
}
