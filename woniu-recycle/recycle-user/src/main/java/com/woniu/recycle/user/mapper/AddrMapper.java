package com.woniu.recycle.user.mapper;

import com.woniu.recycle.user.domain.Addr;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniu.recycle.user.domain.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * <p>
 * 地址表 Mapper 接口
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
public interface AddrMapper extends BaseMapper<Addr> {

    @Select("select * from addr where user_id=#{userId}")
    List<Addr> findAddr(String userId);

//    @Select("select username from user where username=#{username}")
//    User selectUsername(String username);

    @Update("update addr set is_default = 0 where user_id=#{userId}")
    boolean updateState(Integer userId);

    @Delete("delete from addr where id=#{id} and user_id=#{userId}")
    boolean deleteAddr(@Param("id") Integer id,@Param("userId") Integer userId);

    @Update("update addr set is_default = 1 where id=#{id}")
    boolean updateStateById(Integer id);
}
