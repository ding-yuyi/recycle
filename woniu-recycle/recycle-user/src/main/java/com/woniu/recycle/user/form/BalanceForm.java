package com.woniu.recycle.user.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 *
 */
@Data
@ApiModel("充值金额form")
public class BalanceForm {

    @ApiModelProperty(value = "充值金额",example = "1.0")
    @NotNull
    private  BigDecimal balance;
}
