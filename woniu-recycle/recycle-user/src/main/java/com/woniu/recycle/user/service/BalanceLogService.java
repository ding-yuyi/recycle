package com.woniu.recycle.user.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.recycle.user.domain.BalanceLog;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.recycle.user.dto.BalanceLogResultDto;
import com.woniu.recycle.user.form.SearchByCreateTimeForm;
import com.woniu.recycle.user.form.SearchByUserIdForm;

/**
 * <p>
 * 余额日志表 服务类
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
public interface BalanceLogService extends IService<BalanceLog> {

    BalanceLogResultDto searchByUserId(SearchByUserIdForm form);

    BalanceLogResultDto search(Page page, QueryWrapper<BalanceLog> wrapper);

    BalanceLogResultDto searchByCreateTime(SearchByCreateTimeForm form);
}
