//package com.woniu.recycle.user.swagger;
//
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.stereotype.Component;
//
//@Data
//@AllArgsConstructor
//@NoArgsConstructor
//@Component
//@ConfigurationProperties(prefix = "swagger")
//public class SwaggerProperties {
//    private String title;
//    private String version;
//    private String description;
//    private String basePackage;
//    private Contact contact;
//}
