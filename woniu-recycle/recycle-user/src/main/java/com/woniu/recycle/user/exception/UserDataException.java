package com.woniu.recycle.user.exception;

public class UserDataException extends UserException{
    private static final long serialVersionUID=1L;
    public UserDataException(){}
    public UserDataException(String msg, Integer code){super(msg,code);}
    public UserDataException(UserDataExceptionEnum mee){
        super(mee.getMsg(),mee.getCode());
    }
}
