package com.woniu.recycle.user.controller;


/**
 *
 */

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.alipay.api.response.AlipayTradePagePayResponse;
import com.woniu.recycle.commons.core.util.JwtTemplate;
import com.woniu.recycle.commons.web.util.ServletUtil;
import com.woniu.recycle.user.config.AliPayConfig;
import com.woniu.recycle.user.domain.BalanceLog;
import com.woniu.recycle.user.exception.UserDataException;
import com.woniu.recycle.user.exception.UserDataExceptionEnum;
import com.woniu.recycle.user.form.BalanceForm;
import com.woniu.recycle.user.mapper.BalanceLogMapper;
import com.woniu.recycle.user.utils.OrderNumUtil;
import io.jsonwebtoken.Claims;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author honor
 */
@RestController
@RequestMapping("/alipay")
@Api(tags = "支付宝调用控制器")
public class ALiOrderController {
    //支付宝支付确认付款跳转页面
    @Resource
    BalanceLogMapper balanceLogMapper;
    @Resource
    private JwtTemplate jwtTemplate;

    @ApiOperation("支付宝订单确认")
    @PostMapping("/order")
    public void oderConfirm(@RequestBody(required = false) BalanceForm order, HttpServletRequest request, HttpServletResponse response) {
        System.out.println("ALIPAYGO");
        //虚拟一个订单，将其信息定义如下
        //订单编号
        String orderSn = OrderNumUtil.getNum();
        //金额
        String total = String.valueOf(order.getBalance());
        //订单信息
        String message = "充值订单";

        AlipayClient alipayClient = new DefaultAlipayClient(
                AliPayConfig.gatewayUrl,
                AliPayConfig.APP_ID,
                AliPayConfig.APP_PRIVATE_KEY, "json",
                AliPayConfig.CHARSET,
                AliPayConfig.ALIPAY_PUBLIC_KEY,
                AliPayConfig.sign_type);
        //设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(AliPayConfig.return_url);
        alipayRequest.setNotifyUrl(AliPayConfig.notify_url);
        //商户订单号，商户网站订单系统中唯一订单号，必填
        String out_trade_no = orderSn;
        //付款金额，必填
        String total_amount = total;
        //订单名称，必填
        String subject = message;
        //商品描述，可空
        String body = message;
        alipayRequest.setBizContent("{\"out_trade_no\":\"" + out_trade_no + "\"," + "\"total_amount\":\""
                + total_amount + "\"," + "\"subject\":\"" + subject + "\"," + "\"body\":\"" + body + "\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

        AlipayTradePagePayResponse alipayResponse = null;
        try {
            alipayResponse = alipayClient.pageExecute(alipayRequest);
            response.setContentType("text/html;charset=" + AliPayConfig.CHARSET);
            //直接将完整的表单html输出到页面
            response.getWriter().write(alipayResponse.getBody());
        } catch (AlipayApiException e) {
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println("error");
            e.printStackTrace();
        }
        //cy获取用户信息操作主业务
        //存日记
        //获取token
        String token = null  ;
        token = ServletUtil.getRequest().getHeader("auth_token");
        if (token == null) throw new UserDataException(UserDataExceptionEnum.no_login);
        Claims claims = jwtTemplate.parseJwt(token);
        String userId = claims.get("userId").toString();
        //获取user的id
        BalanceLog balanceLog = new BalanceLog();
        balanceLog.setUserId(Integer.parseInt(userId));
        balanceLog.setBalanceAmount(order.getBalance().intValue());
        long l = System.currentTimeMillis();
        balanceLog.setCreateTime(l);
        balanceLog.setBalanceChangeReason(4);
        balanceLog.setOrderId(0);
        balanceLog.setPayNum(out_trade_no);
        int insert = balanceLogMapper.insert(balanceLog);
        if (insert <= 0) throw new UserDataException(UserDataExceptionEnum.SQL_ERROR);
    }
}

