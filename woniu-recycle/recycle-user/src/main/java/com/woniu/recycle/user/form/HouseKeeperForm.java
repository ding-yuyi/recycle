package com.woniu.recycle.user.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class HouseKeeperForm {

    @NotNull(message = "用户名不能为空")
    @Length(min = 3,max = 15,message = "用户名长度必须在3-15之间")
    @ApiModelProperty(value = "家政员工名称")
    private String housekeeperName;

    @NotNull(message = "手机号不能为空")
    @Length(min = 11,max = 11,message = "电话长度只能在是11位")
    @ApiModelProperty(value = "电话号码")
    private String housekeeperTel;

    @Min(0)
    @Max(2)
    @NotNull(message = "性别不能为空")
    @ApiModelProperty(value = "性别 0 保密 | 1 男 | 2 女")
    private Integer housekeeperGender;


}
