package com.woniu.recycle.user.exception;

public class RearFileUploadException extends UserException{
    private static final long serialVersionUID=1L;

    public RearFileUploadException(String msg){
           super(msg);
    }
    public RearFileUploadException(String msg,Integer code){
          super(msg,code);
        }

}
