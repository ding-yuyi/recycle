package com.woniu.recycle.user.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @ClassName BalanceLogResultDto
 * @Description 余额日志查询接口返回对象
 * @Author DingYuyi
 * @Date 2021/8/21 10:27
 * @Version 1.0
 */
@Data
@ApiModel("日志查询接口返回对象")
public class BalanceLogResultDto {
    @ApiModelProperty(value = "当前页面")
    private Long current;

    @ApiModelProperty(value = "每页数量")
    private Long size;

    @ApiModelProperty(value = "查询总数")
    private Long total;

    @ApiModelProperty(value = "日志的具体数据")
    private List<BalanceLogDto> dtoList;
}
