package com.woniu.recycle.user.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @ClassName SearchByCreateTimeForm
 * @Description TODO
 * @Author DingYuyi
 * @Date 2021/8/21 11:25
 * @Version 1.0
 */
@ApiModel(value = "根据创建时间查询日志接口入参对象")
@Data
public class SearchByCreateTimeForm {
    @NotNull(message = "起始查询时间不能为空")
    @ApiModelProperty(value = "查询起始时间")
    @Min(value = 0,message = "时间不能为负数")
    private Long logCreateTimeStart;

    @ApiModelProperty(value = "查询结束时间")
    private Long logCreateTimeEnd;

    @NotNull(message = "当前页码不能为空")
    @ApiModelProperty(value = "当前页码")
    private Integer current;

    @NotNull(message = "每页数量不能为空")
    @ApiModelProperty(value = "每页数量")
    private Integer size;
}
