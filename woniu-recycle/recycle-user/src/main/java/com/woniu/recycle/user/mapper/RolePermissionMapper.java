package com.woniu.recycle.user.mapper;

import com.woniu.recycle.user.domain.RolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色权限表 Mapper 接口
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
public interface RolePermissionMapper extends BaseMapper<RolePermission> {

}
