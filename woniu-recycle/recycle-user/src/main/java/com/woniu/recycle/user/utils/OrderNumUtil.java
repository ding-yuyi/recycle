package com.woniu.recycle.user.utils;

import com.woniu.recycle.commons.web.util.DateUtil;

import java.util.Random;

public class OrderNumUtil {

    /**
     * 生成订单编号
     * @param
     * @return
     */
    public static String getNum( ){
        String date2String = DateUtil.parseToTime(System.currentTimeMillis(), "yyyyMMddHHmmss");
        Random random = new Random();
        String str = "";
        for (int i = 0; i < 6; i++) {
            str = str + random.nextInt(10);
        }
        String orderNum = date2String + str;
        return orderNum;
    }
}
