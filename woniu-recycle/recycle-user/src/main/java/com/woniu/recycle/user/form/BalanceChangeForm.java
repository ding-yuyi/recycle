package com.woniu.recycle.user.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 *
 */
@Data
@ApiModel("余额变动form")
public class BalanceChangeForm {
    @ApiModelProperty(value = "用户id",example = "1")
    @NotNull
    private Integer id;

    @DecimalMin("0")
    @ApiModelProperty(value = "余额变动",example = "1")
    private BigDecimal balance;

    @NotNull
    @ApiModelProperty("订单号")
    private String payNum;


}
