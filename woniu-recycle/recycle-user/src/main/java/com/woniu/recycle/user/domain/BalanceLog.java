package com.woniu.recycle.user.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 余额日志表
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="BalanceLog对象", description="余额日志表")
public class BalanceLog implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "余额日志id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "用户id")
    private Integer userId;

    @ApiModelProperty(value = "余额变动量")
    private Integer balanceAmount;

    @ApiModelProperty(value = "日志创建时间（即余额变动时间）")
    private Long createTime;

    @ApiModelProperty(value = "余额变动原因 0 回收增加 | 1 充值增加 | 2 提现减少 | 3 家政消费")
    private Integer balanceChangeReason;

    @ApiModelProperty(value = "如果因为订单变动，则关联订单id")
    private Integer orderId;

    @ApiModelProperty(value = "订单号")
    private String payNum;

}
