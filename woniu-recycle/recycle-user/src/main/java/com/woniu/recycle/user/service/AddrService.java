package com.woniu.recycle.user.service;

import com.woniu.recycle.user.domain.Addr;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.recycle.user.dto.AddrDto;
import com.woniu.recycle.user.dto.UserLoginDto;
import com.woniu.recycle.user.form.AddrFrom;

import java.util.List;

/**
 * <p>
 * 地址表 服务类
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
public interface AddrService extends IService<Addr> {

    //新增收获地址
    public AddrDto addAddr(AddrFrom AddrFrom);

    //查询所有地址
    public List<AddrDto> findAddr();

    //删除地址
    public List<AddrDto> deleteAddr(Integer id);

    //更改默认地址
    public List<AddrDto> updateAddr(Integer id);


}
