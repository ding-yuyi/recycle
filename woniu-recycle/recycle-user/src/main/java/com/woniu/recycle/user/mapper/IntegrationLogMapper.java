package com.woniu.recycle.user.mapper;

import com.woniu.recycle.user.domain.IntegrationLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 积分日志表 Mapper 接口
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
public interface IntegrationLogMapper extends BaseMapper<IntegrationLog> {

}
