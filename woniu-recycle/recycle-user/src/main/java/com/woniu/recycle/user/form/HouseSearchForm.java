package com.woniu.recycle.user.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class HouseSearchForm {

    @ApiModelProperty(value = "家政员工名称")
    private String housekeeperName;

    @ApiModelProperty(value = "电话号码")
    private String housekeeperTel;

    private Long size;

    private Long current;
}
