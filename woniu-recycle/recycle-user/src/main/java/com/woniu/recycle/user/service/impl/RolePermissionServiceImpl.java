package com.woniu.recycle.user.service.impl;

import com.woniu.recycle.user.domain.RolePermission;
import com.woniu.recycle.user.mapper.RolePermissionMapper;
import com.woniu.recycle.user.service.RolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色权限表 服务实现类
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements RolePermissionService {

}
