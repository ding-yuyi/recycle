package com.woniu.recycle.user.form;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Data
@ApiModel("管理员注册form")
public class UserRegisterFrom {

    @NotNull(message = "用户名不能为空")
    @Length(min = 3,max = 15,message = "用户名长度必须在3-15之间")
    @ApiModelProperty(value = "用户名")
    private String username;

    @NotNull(message = "密码不能为空")
    @Length(min = 3,max = 15,message = "密码长度只能在3-15之间")
    @ApiModelProperty(value = "密码")
    private String pwd;

    @Min(0)
    @Max(2)
    @NotNull(message = "性别不能为空")
    @ApiModelProperty(value = "性别 0 保密 | 1 男 | 2 女",example = "1")
    private Integer gender;

//    @ApiModelProperty(value = "角色 0 普通用户 | 1 管理员 | 2 超级管理员")
//    private Integer role;

//    @ApiModelProperty(value = "积分")
//    private Integer integration;
//
//    @ApiModelProperty(value = "余额")
//    private BigDecimal balance;
//
//    @ApiModelProperty(value = "二维码地址")
//    @TableField("QR_url")
//    private String qrUrl;

//    @ApiModelProperty(value = "注册时间")
//    private Long createTime;
//
//    @ApiModelProperty(value = "上次登录时间")
//    private Long lastLoginTime;


}
