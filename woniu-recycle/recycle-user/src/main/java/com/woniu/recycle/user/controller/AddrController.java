package com.woniu.recycle.user.controller;


import com.woniu.recycle.commons.core.model.ResponseEntity;
import com.woniu.recycle.commons.core.util.JwtTemplate;
import com.woniu.recycle.commons.web.util.BeanCopyUtil;
import com.woniu.recycle.commons.web.util.ServletUtil;
import com.woniu.recycle.user.domain.Addr;
import com.woniu.recycle.user.dto.AddrDto;
import com.woniu.recycle.user.dto.UserRegisterDto;
import com.woniu.recycle.user.exception.UserDataException;
import com.woniu.recycle.user.exception.UserDataExceptionEnum;
import com.woniu.recycle.user.exception.UserRegisterException;
import com.woniu.recycle.user.exception.UserRegisterExceptionEnum;
import com.woniu.recycle.user.form.AddrFrom;
import com.woniu.recycle.user.mapper.AddrMapper;
import com.woniu.recycle.user.service.AddrService;
import io.jsonwebtoken.Claims;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 地址表 前端控制器
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
@RestController
@RequestMapping("/addr")
@Api(tags = "用户地址管理")
@Slf4j
public class AddrController {

    @Resource
    private AddrService addrService;

    @Resource
    private JwtTemplate jwtTemplate;

    @Resource
    private AddrMapper addrMapper;

    /**
     * 新增地址
     *
     */
    @PostMapping("/auth/addAddr")
    @ApiOperation("增加地址")
    public ResponseEntity<AddrDto> addAddr(AddrFrom addrFrom){
        log.info("入参参数:{}", addrFrom);

        String token=null;
        token = ServletUtil.getRequest().getHeader("auth_token");
        if(token==null) throw new UserDataException(UserDataExceptionEnum.no_login);
        Claims claims = jwtTemplate.parseJwt(token);
        String Id = claims.get("userId").toString();

        if(addrFrom.getNation()==null) throw new UserDataException(UserDataExceptionEnum.Addr_nation_null);
        if(addrFrom.getProvince()==null) throw new UserDataException(UserDataExceptionEnum.Addr_province_null);
        if(addrFrom.getCity()==null) throw new UserDataException(UserDataExceptionEnum.Addr_city_null);
        if(addrFrom.getCountry()==null) throw new UserDataException(UserDataExceptionEnum.Addr_country_null);
        if(addrFrom.getAddrDesc()==null) throw new UserDataException(UserDataExceptionEnum.Addr_addrDesc_null);
        AddrDto addrDto = addrService.addAddr(addrFrom);
        log.info("返回数据:{}", addrDto);
        if(addrDto==null) return ResponseEntity.BuildError(AddrDto.class).setMsg("新添地址失败");
        return ResponseEntity.BuildSuccess(AddrDto.class).setMsg("新增地址成功").setData(addrDto);
    }


    /**
     * 查询所有地址
     *
     */
    @GetMapping("/auth/findAddr")
    @ApiOperation("查询所有地址")
    public ResponseEntity<List<AddrDto>> findAddr(){
        List<AddrDto> addr = addrService.findAddr();
        log.info("返回数据:{}", addr);
        if(addr==null) return ResponseEntity.BuildErrorList(AddrDto.class).setMsg("无详情地址，请添加");
        return ResponseEntity.BuildSuccessList(AddrDto.class).setMsg("查询成功").setData(addr);
    }

    /**
     * 删除地址
     *
     */
    @GetMapping("/auth/deleteAddr")
    @ApiOperation("删除一个地址")
    public ResponseEntity<List<AddrDto>> deleteAddr(Integer id){
        log.info("入参参数:{}", id);
        List<AddrDto> addrDtos = addrService.deleteAddr(id);
        log.info("返回数据:{}", addrDtos);
        if(addrDtos!=null) return ResponseEntity.BuildSuccessList(AddrDto.class).setMsg("删除成功").setData(addrDtos);
        return ResponseEntity.BuildErrorList(AddrDto.class).setMsg("删除失败,至少保留一个地址");
    }

    /**
     * 更改默认地址
     *
     */
    @GetMapping("/auth/updateAddr")
    @ApiOperation("更改默认地址")
    public ResponseEntity<List<AddrDto>> updateAddr(Integer id){
        log.info("入参参数:{}", id);
        List<AddrDto> addrDtos = addrService.updateAddr(id);
        log.info("返回数据:{}", addrDtos);
        if(addrDtos!=null) return ResponseEntity.BuildSuccessList(AddrDto.class).setMsg("更改成功").setData(addrDtos);
        return ResponseEntity.BuildErrorList(AddrDto.class).setMsg("更改失败");
    }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //后台管理
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     *  根据id查询地址
     *
     */
    @PostMapping("/auth/findAddrById")
    @ApiOperation("根据id查询地址")
    public ResponseEntity<AddrDto> findAddrById(Integer id){
        log.info("入参参数:{}", id);
        Addr addr = addrMapper.selectById(id);
        if(addr==null) return ResponseEntity.BuildError(AddrDto.class).setMsg("无详情地址，请添加");
        AddrDto addrDto = BeanCopyUtil.copyObject(addr, AddrDto::new);
        log.info("返回数据:{}", addrDto);
        return ResponseEntity.BuildSuccess(AddrDto.class).setMsg("查询成功").setData(addrDto);
    }

}

