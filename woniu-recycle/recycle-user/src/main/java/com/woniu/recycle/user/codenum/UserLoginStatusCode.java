package com.woniu.recycle.user.codenum;

import lombok.Data;

@Data
public class UserLoginStatusCode {
    //保密
    public static final Integer gender_secrecy=0;
    //男性
    public static final Integer gender_male=1;
    //女性
    public static final Integer gender_female=2;
}
