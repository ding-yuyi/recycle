package com.woniu.recycle.user.service;

import com.woniu.recycle.user.domain.SpreadLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 推广日志表 服务类
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
public interface SpreadLogService extends IService<SpreadLog> {

}
