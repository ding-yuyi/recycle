package com.woniu.recycle.user.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class SendMail {
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    public String sendVerificationCode(String email) throws MessagingException {
        log.info("准备发送验证码");
        //随机生成四个数字的验证码
        String verification = MathUtil.getVerification();
        log.info("用户验证码：" + verification);

        //存入缓存
        String key = "email:" + email;
        ValueOperations<String, String> ops = stringRedisTemplate.opsForValue();
        ops.set(key, verification, 60 * 3, TimeUnit.SECONDS);

        MailUtil.sendEmail("15215127742@163.com", "QOUBUOEVVHZUMVFO", email, "验证码", "验证码是：" + verification);

        return verification;
    }
}
