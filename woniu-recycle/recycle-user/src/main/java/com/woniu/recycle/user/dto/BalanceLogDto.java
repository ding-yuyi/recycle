package com.woniu.recycle.user.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @ClassName BalanceLogDto
 * @Description 余额日志查询返回对象
 * @Author DingYuyi
 * @Date 2021/8/21 10:00
 * @Version 1.0
 */
@Data
@ApiModel(value="余额日志查询返回对象")
public class BalanceLogDto {
    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "余额日志id")
    private Integer id;

    @ApiModelProperty(value = "用户id")
    private Integer userId;

    @ApiModelProperty(value = "余额变动量")
    private Integer balanceAmount;

    @ApiModelProperty(value = "日志创建时间（即余额变动时间）")
    private Long createTime;

    @ApiModelProperty(value = "余额变动原因 0 回收增加 | 1 充值增加 | 2 提现减少 | 3 家政消费")
    private Integer balanceChangeReason;

    @ApiModelProperty(value = "如果因为订单变动，则关联订单id")
    private Integer orderId;

    @ApiModelProperty(value = "订单号")
    private String payNum;
}
