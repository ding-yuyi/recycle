package com.woniu.recycle.user.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 *
 */
@Data
@ApiModel("余额变动Dto")
public class BalanceChangeDto {
    @ApiModelProperty("主键")
    private Integer userId;

    @ApiModelProperty("剩余余额")
    private BigDecimal balanceAmount;

    @ApiModelProperty("订单号")
    private String  payNum;
}