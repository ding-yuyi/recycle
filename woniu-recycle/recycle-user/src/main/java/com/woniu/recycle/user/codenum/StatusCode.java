package com.woniu.recycle.user.codenum;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 *
 * @ProjectName SpringMvc02
 * @ClassName StatusCode.java
 * @Auther: lemon
 * @Date: 2021/06/04/22:59
 * @Description: 自定义状态码
 * @Version:1.0
 */
@Data
public class StatusCode {
    public static final Integer OK=0000;
    public static final Integer ERROR=9999;
}
