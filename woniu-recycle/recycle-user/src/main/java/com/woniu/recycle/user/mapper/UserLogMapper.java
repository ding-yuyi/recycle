package com.woniu.recycle.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniu.recycle.user.domain.SpreadLog;
import com.woniu.recycle.user.domain.UserLog;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 *
 */
public interface UserLogMapper extends BaseMapper<UserLog> {

    @Select("select * from user_log where user_id={userId}")
    List<UserLog> findUserLogById(Integer userId);
}
