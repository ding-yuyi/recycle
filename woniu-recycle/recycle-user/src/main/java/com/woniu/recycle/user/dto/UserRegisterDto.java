package com.woniu.recycle.user.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class UserRegisterDto {

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "密码")
    private String pwd;

    @ApiModelProperty(value = "性别 0 保密 | 1 男 | 2 女")
    private Integer gender;

    @ApiModelProperty(value = "角色 0 普通用户 | 1 管理员 | 2 超级管理员")
    private Integer role;

    @ApiModelProperty(value = "积分")
    private Integer integration;

    @ApiModelProperty(value = "余额")
    private BigDecimal balance;

//    @ApiModelProperty(value = "二维码地址")
//    @TableField("QR_url")
//    private String qrUrl;

    @ApiModelProperty(value = "注册时间")
    private Long createTime;

    @ApiModelProperty(value = "上次登录时间")
    private Long lastLoginTime;

}
