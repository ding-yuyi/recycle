package com.woniu.recycle.user.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 家政员工表
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="Housekeeper对象", description="家政员工表")
public class Housekeeper implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "家政员工id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "家政员工名称")
    private String housekeeperName;

    @ApiModelProperty(value = "工号")
    private String housekeeperNum;

    @ApiModelProperty(value = "电话号码")
    private String housekeeperTel;

    @ApiModelProperty(value = "性别 0 保密 | 1 男 | 2 女")
    private Integer housekeeperGender;

}
