package com.woniu.recycle.user.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="User日志对象", description="用户日志表")
public class UserLog {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer logId;

    @ApiModelProperty(value = "用户userId")
    private Integer userId;

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "密码")
    private String pwd;

    @ApiModelProperty(value = "电话号码")
    private String tel;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "qq号")
    private String qqNum;

    @ApiModelProperty(value = "角色 0 普通用户 | 1 管理员 | 2 超级管理员")
    private Integer role;

    @ApiModelProperty(value = "头像地址")
    private String iconUrl;

    @ApiModelProperty(value = "性别 0 保密 | 1 男 | 2 女")
    private Integer gender;

    @ApiModelProperty(value = "昵称")
    private String nickname;

    @ApiModelProperty(value = "个人简介")
    private String introduction;

    @ApiModelProperty(value = "积分")
    private Integer integration;

    @ApiModelProperty(value = "余额")
    private BigDecimal balance;

    @ApiModelProperty(value = "二维码地址")
    @TableField("QR_url")
    private String qrUrl;

    @ApiModelProperty(value = "注册时间")
    private Long createTime;

    @ApiModelProperty(value = "上次登录时间")
    private Long lastLoginTime;

    @ApiModelProperty(value = "推荐二维码地址")
    @TableField("QR_invite")
    private String qrInvite;

    @ApiModelProperty(value = "变更原因")
    private String reason;

}
