package com.woniu.recycle.user.controller;


import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 积分日志表 前端控制器
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
@RestController
@RequestMapping("/integration/log")
@Slf4j
@Api(tags = "积分日志管理接口")
public class IntegrationLogController {

}

