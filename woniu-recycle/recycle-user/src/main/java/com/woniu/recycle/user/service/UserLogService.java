package com.woniu.recycle.user.service;

import com.baomidou.mybatisplus.extension.service.IService;

import com.woniu.recycle.user.domain.UserLog;

import java.util.List;

/**
 *
 */
public interface UserLogService extends IService<UserLog> {
    public List<UserLog> findUserLogById(Integer userId);
}
