package com.woniu.recycle.user.dto;

import lombok.Data;

@Data
public class UserDto {

    private String token;

    private UserLoginDto userLoginDto;
}
