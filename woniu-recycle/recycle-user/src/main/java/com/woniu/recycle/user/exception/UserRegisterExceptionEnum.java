package com.woniu.recycle.user.exception;

public enum  UserRegisterExceptionEnum {


    gender_error("性别填写错误",1011),
    register_error("注册失败",1012),
    parameter_empty("数据不能为空",1013),
    username_empty("用户名不能为空",1014),
    password_empty("密码不能为空",1015),
    gender_empty("请填写性别",1016),
    username_repeat("该用户名已存在",1017),
    tel_error("电话不能为空",1018);
    String msg;
    Integer code;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    UserRegisterExceptionEnum(String msg, Integer code) {
        this.msg = msg;
        this.code = code;
    }
}
