package com.woniu.recycle.user.exception;

import com.woniu.recycle.user.domain.Addr;
import io.swagger.annotations.ApiModelProperty;
//
//@ApiModelProperty(value = "国家")
//private String nation;
//
//@ApiModelProperty(value = "省")
//private String province;
//
//@ApiModelProperty(value = "市")
//private String city;
//
//@ApiModelProperty(value = "区或县")
//private String country;
//
//@ApiModelProperty(value = "地址详情")
//private String addrDesc;

public enum  UserDataExceptionEnum {
    id_null("当前id不存在",1030),
    data_empty("查询错误，请重新登录",1031),
    no_login("请登录后操作",1032),
    pwd_empty("请填写密码",1033),
    image_exist("头像已存在，不能继续上传",1034),
    image_null("请先上传头像，在点击生成二维码",1035),
    QRcode_exist("二维码已存在，无需重复生成",1036),
    nick_null("昵称为空,请填写昵称",1037),
    intro_null("简介输入为空,请填写简介",1038),
    Addr_nation_null("请填写国家",1041),
    Addr_province_null("请填写省份",1042),
    Addr_city_null("请填写城市",1043),
    Addr_country_null("请填写区县",1044),
    Addr_addrDesc_null("请填写详细地址",1045),
    Addr_null("地址为空,请输入一个有效地址",1046),
    email_exits("邮箱已存在，无需绑定",1051),
    email_valid("请传入一个有效邮箱",1052),
    email_time_ex("邮箱验证码时间已过期",1053),
    email_code_error("邮箱验证码错误",1054),
    email_code_empty("请输入验证码",1055),
    email_null("邮箱不存在，请先绑定邮箱",1056),
    username_null("用户名为空",1057),
    page_null("请输入页面页数和页码显示数",1061),
    id_is_null("该用户不存在",1062),
    do_error("操作失败",1077),
    log_user_error("该用户不存在",1078),
    image_no("请先上传图片",1079),
    tel_no("请填写手机号",1076),
    email_no("请先绑定邮箱",1075),

    CODE_ERROR("参数错误",1080),
    SQL_ERROR("数据异常",1081),
    PAYNUM_ERROR("数据异常",1082);
    String msg;
    Integer code;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    UserDataExceptionEnum(String msg, Integer code) {
        this.msg = msg;
        this.code = code;
    }
}
