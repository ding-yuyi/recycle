package com.woniu.recycle.user.service.impl;

import com.woniu.recycle.user.domain.IntegrationLog;
import com.woniu.recycle.user.mapper.IntegrationLogMapper;
import com.woniu.recycle.user.service.IntegrationLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 积分日志表 服务实现类
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
@Service
public class IntegrationLogServiceImpl extends ServiceImpl<IntegrationLogMapper, IntegrationLog> implements IntegrationLogService {

}
