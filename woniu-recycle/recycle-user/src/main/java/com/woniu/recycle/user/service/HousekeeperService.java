package com.woniu.recycle.user.service;

import com.woniu.recycle.user.domain.Housekeeper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.recycle.user.dto.*;
import com.woniu.recycle.user.form.HouseKeeperForm;
import com.woniu.recycle.user.form.HouseSearchForm;
import com.woniu.recycle.user.form.ManageAddFrom;
import com.woniu.recycle.user.form.UserSearchFrom;

/**
 * <p>
 * 家政员工表 服务类
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
public interface HousekeeperService extends IService<Housekeeper> {

    //家政员搜索
    public HouseIPageDto houseSearch(HouseSearchForm houseSearchForm);

    //添加家政员
    public HouseKeeperDto houseAdd(HouseKeeperForm houseKeeperForm);

    //开除家政员
    public HouseKeeperDto houseFire(Integer id);


}
