package com.woniu.recycle.user.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.recycle.user.domain.User;
import com.woniu.recycle.user.domain.UserLog;
import com.woniu.recycle.user.exception.UserDataException;
import com.woniu.recycle.user.exception.UserDataExceptionEnum;
import com.woniu.recycle.user.mapper.UserLogMapper;
import com.woniu.recycle.user.service.UserLogService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 *
 */
@Service
public class UserLogServiceImpl extends ServiceImpl<UserLogMapper, UserLog> implements UserLogService {

    @Resource
    private UserLogMapper userLogMapper;

    @Override
    public List<UserLog> findUserLogById(Integer userId) {
        List<UserLog> userLogList = userLogMapper.findUserLogById(userId);
        if(userLogList==null) throw new UserDataException(UserDataExceptionEnum.log_user_error);
        return userLogList;
    }
}
