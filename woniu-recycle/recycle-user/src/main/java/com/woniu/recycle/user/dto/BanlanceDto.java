package com.woniu.recycle.user.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 *
 */
@Data
@ApiModel("充值金额Dto")
public class BanlanceDto {
    @ApiModelProperty(value = "充值金额",example = "1.0")
    private BigDecimal balance;
    @ApiModelProperty(value = "订单号")
    private  String  ordernum;
    @ApiModelProperty(value = "订单名称")
    private  String  title;


}
