package com.woniu.recycle.user.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 推广日志表
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="SpreadLog对象", description="推广日志表")
public class SpreadLog implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "推广日志id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "推广用户id")
    private Integer sourceUserId;

    @ApiModelProperty(value = "积分变动量")
    private Integer integrationAmount;

    @ApiModelProperty(value = "日志创建时间（即邀请成功时间）")
    private Long createTime;

    @ApiModelProperty(value = "受邀请用户id")
    private Integer newUserId;




}
