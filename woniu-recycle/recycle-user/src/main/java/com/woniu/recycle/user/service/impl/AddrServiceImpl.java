package com.woniu.recycle.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.woniu.recycle.commons.core.util.JwtTemplate;
import com.woniu.recycle.commons.web.util.BeanCopyUtil;
import com.woniu.recycle.commons.web.util.ObjectMapperUtil;
import com.woniu.recycle.commons.web.util.ServletUtil;
import com.woniu.recycle.user.codenum.UserStatusCode;
import com.woniu.recycle.user.domain.Addr;
import com.woniu.recycle.user.domain.User;
import com.woniu.recycle.user.dto.AddrDto;
import com.woniu.recycle.user.dto.UserLoginDto;
import com.woniu.recycle.user.exception.UserDataException;
import com.woniu.recycle.user.exception.UserDataExceptionEnum;
import com.woniu.recycle.user.form.AddrFrom;
import com.woniu.recycle.user.mapper.AddrMapper;
import com.woniu.recycle.user.service.AddrService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.recycle.user.service.UserService;
import io.jsonwebtoken.Claims;
import org.checkerframework.checker.units.qual.A;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 地址表 服务实现类
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
@Service
public class AddrServiceImpl extends ServiceImpl<AddrMapper, Addr> implements AddrService {

    @Resource
    private UserService userService;

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private AddrMapper addrMapper;

    @Resource
    private JwtTemplate jwtTemplate;

    /**
     添加地址
     */
    @Override
    public AddrDto addAddr(AddrFrom addrFrom) {

        UserLoginDto userDto = userService.findUser();
        Integer userId = userDto.getId();
        boolean b = addrMapper.updateState(userId);
           //设置状态
        Addr addr = BeanCopyUtil.copyObject(addrFrom,Addr::new);
        addr.setUserId(userId);
        addr.setIsDefault(UserStatusCode.default_addr);
        int i = addrMapper.insert(addr);

        AddrDto addrDto = new AddrDto();
        if(i>0){
            addrDto=BeanCopyUtil.copyObject(addr, AddrDto::new);
            //存redis
            String json = ObjectMapperUtil.parseObject(addrDto);
            stringRedisTemplate.opsForValue().set("user:id:addr"+addr.getId(),json);
            stringRedisTemplate.expire("user:id:addr"+addr.getId(),60*30, TimeUnit.SECONDS);
            return addrDto;
        }
        return addrDto;
    }

     /**
     查询地址
     */
    @Override
    public List<AddrDto> findAddr() {
        String token = ServletUtil.getRequest().getHeader("auth_token");
        if(token==null) throw new UserDataException(UserDataExceptionEnum.no_login);
        Claims claims = jwtTemplate.parseJwt(token);
        String userId = claims.get("userId").toString();
        //调用查询方法
        List<Addr> addr = addrMapper.findAddr(userId);
        List<AddrDto> addrDtos = BeanCopyUtil.copyList(addr, AddrDto::new);
        return addrDtos;
    }

     /**
     删除地址
     */
    @Override
    public List<AddrDto> deleteAddr(Integer id) {
        String token = ServletUtil.getRequest().getHeader("auth_token");
        if(token==null) throw new UserDataException(UserDataExceptionEnum.no_login);
        Claims claims = jwtTemplate.parseJwt(token);
        String userId = claims.get("userId").toString();
        Integer userID = Integer.valueOf(userId);
        List<AddrDto> addr=null;
        //调用删除方法
        boolean b = addrMapper.deleteAddr(id, userID);
        if(b) {
            stringRedisTemplate.delete("user:id:addr" + id);
            addr = findAddr();
            return addr;
        }
        return addr;
    }

     /**
     更新地址为默认地址
     */
    @Override
    public List<AddrDto> updateAddr(Integer id) {
        String token = ServletUtil.getRequest().getHeader("auth_token");
        if(token==null) throw new UserDataException(UserDataExceptionEnum.no_login);
        Claims claims = jwtTemplate.parseJwt(token);
        String userId = claims.get("userId").toString();
        Integer userID = Integer.valueOf(userId);
        //调用更改方法
        boolean b = addrMapper.updateState(userID);
        List<AddrDto> addr=null;
        if(b) {
            addrMapper.updateStateById(id);
            addr = findAddr();
            return addr;
        }
        return addr;
    }

}
