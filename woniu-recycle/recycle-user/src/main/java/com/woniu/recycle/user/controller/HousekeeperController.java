package com.woniu.recycle.user.controller;


import com.woniu.recycle.commons.core.model.ResponseEntity;
import com.woniu.recycle.commons.web.component.BaseController;
import com.woniu.recycle.user.dto.*;
import com.woniu.recycle.user.exception.UserRegisterException;
import com.woniu.recycle.user.exception.UserRegisterExceptionEnum;
import com.woniu.recycle.user.form.HouseKeeperForm;
import com.woniu.recycle.user.form.HouseSearchForm;
import com.woniu.recycle.user.form.ManageAddFrom;
import com.woniu.recycle.user.form.UserSearchFrom;
import com.woniu.recycle.user.service.HousekeeperService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 * 家政员工表 前端控制器
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
@RestController
@RequestMapping("/housekeeper")
@Api(tags = "员工管理")
@Slf4j
public class HousekeeperController extends BaseController {

    @Resource
    private HousekeeperService housekeeperService;

    /**
     * 根据条件查看员工
     * 有权限
     */
    @GetMapping("/auth/houseSearch")
    @ApiOperation("根据条件查看员工")
    public ResponseEntity<HouseIPageDto> houseSearch(HouseSearchForm houseSearchForm){
        log.info("搜索数据为进：{}",houseSearchForm);
        HouseIPageDto houseIPageDto = housekeeperService.houseSearch(houseSearchForm);
        log.info("搜索数据出：{}",houseIPageDto);
        if(houseIPageDto==null) return ResponseEntity.BuildError(HouseIPageDto.class).setMsg("未查询到该家政员工");
        return ResponseEntity.BuildSuccess(HouseIPageDto.class).setMsg("查询成功").setData(houseIPageDto);
    }

    /**
     * 新增员工
     * 有权限
     */
    @PostMapping("/auth/houseAdd")
    @ApiOperation("新增员工")
    public ResponseEntity<HouseKeeperDto> houseAdd(@Valid HouseKeeperForm houseKeeperForm,BindingResult bindingResult) {
        log.info("入参参数:{}", houseKeeperForm);
        //参数验证

        ResponseEntity responseEntity = extractError(bindingResult);
        if (responseEntity != null) {
            return responseEntity;
        }

        if (houseKeeperForm.getHousekeeperName() == null)
            throw new UserRegisterException(UserRegisterExceptionEnum.username_empty);
        if (houseKeeperForm.getHousekeeperTel() == null)
            throw new UserRegisterException(UserRegisterExceptionEnum.tel_error);
        if (houseKeeperForm.getHousekeeperGender() == null)
            throw new UserRegisterException(UserRegisterExceptionEnum.gender_empty);
        HouseKeeperDto houseKeeperDto = housekeeperService.houseAdd(houseKeeperForm);
        log.info("返回数据:{}", houseKeeperDto);
        if (houseKeeperDto == null) return ResponseEntity.BuildError(HouseKeeperDto.class).setMsg("员工账号创建失败");
        return ResponseEntity.BuildSuccess(HouseKeeperDto.class).setMsg("员工账号创建成功").setData(houseKeeperDto);
    }

    /**
     * 删除员工
     * 有权限
     */
    @GetMapping("/auth/deleteHouse")
    @ApiOperation("删除员工")
    public ResponseEntity<HouseKeeperDto> deleteHouse(Integer id){
        log.info("入参参数:{}", id);
        HouseKeeperDto houseKeeperDto = housekeeperService.houseFire(id);
        log.info("返回数据:{}", houseKeeperDto);
        if(houseKeeperDto!=null) return ResponseEntity.BuildSuccess(HouseKeeperDto.class).setMsg("删除成功").setData(houseKeeperDto);
        return ResponseEntity.BuildError(HouseKeeperDto.class).setMsg("删除失败");
    }

}

