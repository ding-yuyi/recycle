package com.woniu.recycle.user.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 推广日志表 前端控制器
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
@RestController
@RequestMapping("/spread-log")
public class SpreadLogController {

}

