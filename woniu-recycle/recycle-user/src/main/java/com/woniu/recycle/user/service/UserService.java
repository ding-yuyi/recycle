package com.woniu.recycle.user.service;
import com.google.zxing.WriterException;
import com.woniu.recycle.user.form.BalanceChangeForm;
import com.woniu.recycle.user.domain.User;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.recycle.user.dto.*;
import com.woniu.recycle.user.form.ChangeIntegationSelForm;
import com.woniu.recycle.user.form.UserLoginFrom;
import com.woniu.recycle.user.form.UserRegisterFrom;
import com.woniu.recycle.user.form.*;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import java.io.IOException;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
public interface UserService extends IService<User> {

////////////////用户接口/////////////////////////////////////////////////////////////////////////////////////////

    //用户注册
    public UserRegisterDto register(UserRegisterFrom userRegisterFrom);

    //用户账号登录
    public UserDto login(UserLoginFrom loginFrom);

    //查看个人信息
    public UserLoginDto findUser();

    //注销用户
    public void loginOut();

    //上传头像
    public String UploadPicture(MultipartFile file) throws Exception;

    //二维码生成
    public String QRcodeCreat() throws IOException, WriterException;

    //邀请注册
    public String inviteUser() throws IOException, WriterException;

    //更改昵称
    public UserLoginDto updateNick(String nickname);

    //更改简介
    public UserLoginDto updateIntroduction(String introduction);

    //更改手机号
    public UserLoginDto updateTel(String tel);

    //邮箱绑定发送验证
    public String EmailBingSend(String email) throws MessagingException;

    //邮箱绑定接受验证
    public UserLoginDto EmailBingAccept(String emailCode) throws MessagingException;

    //邮箱登录
    public String EmailLoginSend(String username) throws MessagingException;

    //邮箱登录接受验证码
    public UserDto loginAcceptCode(String emailCode);

    //邮箱修改密码发送验证
    public String EmailUpdatePwdSend() throws MessagingException;

    //邮箱修改密码接受
    public UserLoginDto EmailUpdatePwdAccept(String emailCode,String pwd) throws MessagingException;

///////////////////后台接口//////////////////////////////////////////////////////////////////////////////////////

    //用户搜索
    public UserIPageDto userSearch(UserSearchFrom userSearchFrom);

    //添加管理员
    public UserRegisterDto manageAdd(ManageAddFrom manageAddFrom);

    //删除管理员或者用户
    public UserLoginDto manageFire(Integer id);

///////////////////积分余额支付接口//////////////////////////////////////////////////////////////////////////////////

    //更改积分接口，减少积分
    ChangeIntegationSelDto changeIntegrationProductDel(ChangeIntegationSelForm changeIntegationSelForms);
    //更改积分接口，增加积分
    ChangeIntegationSelDto changeIntegrationProductAdd(ChangeIntegationSelForm changeIntegationSelForms);
    //支付成功异步回调函数
    void addBanlance(String out_trade_no, String trade_no);
    //更改余额接口，增加余额
    BalanceChangeDto changeBalanceAdd(BalanceChangeForm balanceChangeForm);
    //更改余额接口，减少余额
    BalanceChangeDto changeBalanceSel(BalanceChangeForm balanceChangeForm);

}
