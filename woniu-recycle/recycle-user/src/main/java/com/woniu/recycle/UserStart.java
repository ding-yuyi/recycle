package com.woniu.recycle;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ClassName UserStart
 * @Description TODO
 * @Author Aierbing
 * @Date 2021/8/11 19:02
 * @Version 1.0
 */
@SpringBootApplication
@MapperScan("/com.woniu.recycle.user.mapper")
public class UserStart {
    public static void main(String[] args) {
        SpringApplication.run(UserStart.class,args);
    }
}
