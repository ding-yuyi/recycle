package com.woniu.recycle.user.controller;


import com.woniu.recycle.commons.core.model.ResponseEntity;
import com.woniu.recycle.commons.web.component.BaseController;
import com.woniu.recycle.user.dto.BalanceLogResultDto;
import com.woniu.recycle.user.form.SearchByCreateTimeForm;
import com.woniu.recycle.user.form.SearchByUserIdForm;
import com.woniu.recycle.user.service.BalanceLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 * 余额日志表 前端控制器
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
@RestController
@RequestMapping("/balance/log")
@Slf4j
@Api(tags = "余额日志管理接口")
public class BalanceLogController extends BaseController {

    @Resource
    private BalanceLogService logService;

    /**
     *@author DingYuyi
     *@Description 根据用户ID查询余额日志接口
     *@Date 10:51 2021/8/21
     *@param form
     *@param result
     *@Return ResponseEntity<BalanceLogResultDto>
     */
    @ApiOperation(value = "根据用户ID查询余额日志接口")
    @PostMapping("/auth/searchByUserId")
    public ResponseEntity<BalanceLogResultDto> searchByUserId(@RequestBody @Valid SearchByUserIdForm form, BindingResult result){
        log.info("入参信息,{}",form);

        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;

        BalanceLogResultDto resultDto = logService.searchByUserId(form);
        if(null == resultDto || resultDto.getDtoList().size() <= 0) return ResponseEntity.BuildError(BalanceLogResultDto.class).setMsg("查询为空");
        return ResponseEntity.BuildSuccess(BalanceLogResultDto.class).setMsg("查询成功").setData(resultDto);
    }

    @ApiOperation(value = "根据用户ID查询余额日志接口")
    @PostMapping("/auth/searchByCreateTime")
    public ResponseEntity<BalanceLogResultDto> searchByCreateTime(@RequestBody @Valid SearchByCreateTimeForm form, BindingResult result){
        log.info("入参信息,{}",form);

        //参数校验
        ResponseEntity responseEntity = extractError(result);
        if(null != responseEntity) return responseEntity;

        BalanceLogResultDto resultDto = logService.searchByCreateTime(form);
        if(null == resultDto || resultDto.getDtoList().size() <= 0) return ResponseEntity.BuildError(BalanceLogResultDto.class).setMsg("查询为空");
        return ResponseEntity.BuildSuccess(BalanceLogResultDto.class).setMsg("查询成功").setData(resultDto);
    }
}

