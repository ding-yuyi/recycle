package com.woniu.recycle.user.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class ManageAddFrom {

    @NotNull(message = "用户名不能为空")
    @Length(min = 3,max = 15,message = "管理员名长度必须在3-15之间")
    @ApiModelProperty(value = "用户名")
    private String username;

    @NotNull(message = "密码不能为空")
    @Length(min = 3,max = 15,message = "密码长度只能在3-15之间")
    @ApiModelProperty(value = "密码")
    private String pwd;

    @Min(0)
    @Max(2)
    @NotNull(message = "性别不能为空")
    @ApiModelProperty(value = "性别 0 保密 | 1 男 | 2 女",example = "1")
    private Integer gender;

}
