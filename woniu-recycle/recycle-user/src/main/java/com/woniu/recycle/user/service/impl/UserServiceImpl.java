package com.woniu.recycle.user.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.zxing.WriterException;
import com.woniu.recycle.commons.core.util.JwtTemplate;
import com.woniu.recycle.commons.web.util.BeanCopyUtil;
import com.woniu.recycle.commons.web.util.EncryptionUtil;
import com.woniu.recycle.commons.web.util.ObjectMapperUtil;
import com.woniu.recycle.commons.web.util.ServletUtil;
import com.woniu.recycle.user.codenum.RuleCode;
import com.woniu.recycle.user.codenum.UserRegisterStatus;
import com.woniu.recycle.user.codenum.UserStatusCode;
import com.woniu.recycle.user.form.BalanceChangeForm;
import com.woniu.recycle.user.domain.BalanceLog;
import com.woniu.recycle.user.domain.User;
import com.woniu.recycle.user.domain.UserLog;
import com.woniu.recycle.user.dto.*;
import com.woniu.recycle.user.exception.*;
import com.woniu.recycle.user.form.*;
import com.woniu.recycle.user.mapper.BalanceLogMapper;
import com.woniu.recycle.user.mapper.UserLogMapper;
import com.woniu.recycle.user.mapper.UserMapper;
import com.woniu.recycle.user.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.recycle.user.utils.OSSTemplate;
import com.woniu.recycle.user.utils.QRUtil;
import com.woniu.recycle.user.utils.SendMail;
import io.jsonwebtoken.Claims;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import javax.annotation.Resource;
import javax.mail.MessagingException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
@Slf4j
@Service
@GlobalTransactional
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Resource
    private UserMapper userMapper;

    @Resource
    private JwtTemplate jwtTemplate;

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private OSSTemplate ossTemplate;

    @Resource
    private SendMail sendMail;

    @Resource
    private BalanceLogMapper balanceLogMapper;

    @Resource
    private UserLogMapper userLogMapper;


    /*
    用户注册
     */
    @Override
    public UserRegisterDto register(UserRegisterFrom userRegisterFrom) {
        //密码加密
        if (userRegisterFrom == null) throw new UserRegisterException(UserRegisterExceptionEnum.parameter_empty);
        String pwd = userRegisterFrom.getPwd();
        String encryption = EncryptionUtil.encryption(pwd);
        User user = BeanCopyUtil.copyObject(userRegisterFrom, User::new);
        //判断性别字段异常
        System.out.println(user.getGender());
        System.out.println(UserRegisterStatus.gender_female);
        if (user.getGender() != UserRegisterStatus.gender_female &&
                user.getGender() != UserRegisterStatus.gender_male &&
                user.getGender() != UserRegisterStatus.gender_secrecy) {
            throw new UserRegisterException(UserRegisterExceptionEnum.gender_error);
        }
        //判断账号是否重复
        String username = user.getUsername();
        User us = userMapper.selectUsername(username);
        if (us != null) throw new UserRegisterException(UserRegisterExceptionEnum.username_repeat);
        //初始化账号其他数据
        user.setRole(RuleCode.user_level);
        user.setIntegration(UserStatusCode.integration_create);
        BigDecimal decimal = new BigDecimal("0.00");
        user.setBalance(decimal);
        user.setCreateTime(System.currentTimeMillis());
        user.setLastLoginTime(System.currentTimeMillis());
        user.setPwd(encryption);
        //二维码 后面生成
        UserRegisterDto urd = null;
        int i = userMapper.insert(user);
        if (i > 0) {
            urd = BeanCopyUtil.copyObject(user, UserRegisterDto::new);
            //存日志
            UserLog userLog = BeanCopyUtil.copyObject(user, UserLog::new);
            userLog.setUserId(user.getId());
            userLog.setReason("注册");
            userLogMapper.insert(userLog);
            return urd;
        }
        return urd;
    }

    /*
    用户登录
     */
    @Override
    public UserDto login(UserLoginFrom loginFrom) {
        //加密对比，查询用户
        String pwd = loginFrom.getPwd();
        String encryption = EncryptionUtil.encryption(pwd);
        User user = getOne(new QueryWrapper<User>().eq("username", loginFrom.getUsername())
                .eq("pwd", encryption));
        if (user == null) throw new UserLoginException(UserLoginExceptionEnum.user_empty);

        //设置上次登录时间
//        Object time = stringRedisTemplate.opsForHash().get("user:loginTime:" + user.getId(), "time");
//        long l = Long.valueOf(String.valueOf(time));
//        System.out.println(l);
//        System.out.println("时间："+time);
//        if(time!=null){user.setLastLoginTime(l);
//            update(user, new UpdateWrapper<User>().eq("id", user.getId()));}

        UserLoginDto userLoginDto = BeanCopyUtil.copyObject(user, UserLoginDto::new);
        //设置token
        HashMap<String, Object> maps = new HashMap<>();
        maps.put("userId", userLoginDto.getId());
        String token = jwtTemplate.createJwt(maps);
        UserDto userDto = new UserDto();
        userDto.setToken(token);
        userDto.setUserLoginDto(userLoginDto);
        stringRedisTemplate.opsForHash().put("user:id:" + userLoginDto.getId(), "role", userLoginDto.getRole() + "");
        stringRedisTemplate.expire("user:id:" + userLoginDto.getId(), jwtTemplate.getJc().getExpire(), TimeUnit.MINUTES);
        //存日志
        UserLog userLog = BeanCopyUtil.copyObject(user, UserLog::new);
        userLog.setUserId(user.getId());
        userLog.setReason("登录");
        userLogMapper.insert(userLog);
        //存入redis
        String json = ObjectMapperUtil.parseObject(userLoginDto);
        stringRedisTemplate.opsForValue().set("user:id" + userLoginDto.getId(), json);
        stringRedisTemplate.expire("user:id" + userLoginDto.getId(), 300, TimeUnit.SECONDS);
        //存入这次登录时间
//        stringRedisTemplate.opsForHash().put("user:loginTime:"+userLoginDto.getId(),"time",System.currentTimeMillis()+"");
        return userDto;
    }

    /*
   用户查看个人信息
   */
    @Override
    public UserLoginDto findUser() {
        //解析token，获取id
        String token = ServletUtil.getRequest().getHeader("auth_token");
        if (token == null) throw new UserDataException(UserDataExceptionEnum.no_login);
        Claims claims = jwtTemplate.parseJwt(token);
        String userId = claims.get("userId").toString();
        //从redis获取数据，如果没有从数据获取
        UserLoginDto uld = null;
        String json = stringRedisTemplate.opsForValue().get("user:id" + userId);
        if (json == null || json.trim().equals("")) {
            User user = getById(userId);
            uld = BeanCopyUtil.copyObject(user, UserLoginDto::new);
            return uld;
        }
        uld = ObjectMapperUtil.parseObject(json, UserLoginDto.class);
        return uld;
    }

    /*
  注销用户
   */
    @Override
    public void loginOut() {
        //获取userId，清空redis
        String token = null;
        token = ServletUtil.getRequest().getHeader("auth_token");
        if (token == null) throw new UserDataException(UserDataExceptionEnum.no_login);
        Claims claims = jwtTemplate.parseJwt(token);
        String userId = claims.get("userId").toString();
        Boolean b = stringRedisTemplate.delete("user:id" + userId);
    }

    /*
   上传头像
    */
    @Override
    public String UploadPicture(MultipartFile file) throws Exception {
//        String token = null;
//        token = ServletUtil.getRequest().getHeader("auth_token");
//        if (token == null) throw new UserDataException(UserDataExceptionEnum.no_login);
//        Claims claims = jwtTemplate.parseJwt(token);
//        String userId = claims.get("userId").toString();
        //上传图片
        UserLoginDto userDto = findUser();
//        if (userDto.getIconUrl() != null) throw new UserDataException(UserDataExceptionEnum.image_exist);
        String name = file.getOriginalFilename();
        InputStream inputStream = file.getInputStream();
        String contentType = name.substring(name.lastIndexOf("."));
        String fileName = System.currentTimeMillis() + "" + new Random().nextInt(10) + contentType;
        String url = ossTemplate.uploadFile("lyl-sava", fileName, inputStream);
        if (url != null) {
            //存数据库
            User user = getById(userDto.getId());
            user.setIconUrl(url);
            boolean update = update(user, new UpdateWrapper<User>().eq("id", userDto.getId()));
            UserLoginDto uld = BeanCopyUtil.copyObject(user, UserLoginDto::new);
            //存日志
            UserLog userLog = BeanCopyUtil.copyObject(user, UserLog::new);
            userLog.setUserId(user.getId());
            userLog.setReason("上传头像");
            userLogMapper.insert(userLog);
            //存redis
            String json = ObjectMapperUtil.parseObject(uld);
            stringRedisTemplate.opsForValue().set("user:id" + uld.getId(), json);
            stringRedisTemplate.expire("user:id" + uld.getId(), 60 * 30, TimeUnit.SECONDS);
            return url;
        }
        return url;
    }

    /*
   生成二维码
   */
    @Override
    public String QRcodeCreat() throws IOException, WriterException {
//        String token = ServletUtil.getRequest().getHeader("auth_token");
//        if (token == null) throw new UserDataException(UserDataExceptionEnum.no_login);
//        Claims claims = jwtTemplate.parseJwt(token);
//        String userId = claims.get("userId").toString();

        UserLoginDto userDto = findUser();
        if (userDto.getIconUrl() == null) throw new UserDataException(UserDataExceptionEnum.image_null);
        InputStream ist = QRUtil.createQR("http://localhost:8001/user/userModule/auth/findUser", "png", userDto.getIconUrl());
        if (userDto.getQrUrl() != null) throw new UserDataException(UserDataExceptionEnum.QRcode_exist);
        String url = ossTemplate.uploadFile("lyl-sava", System.currentTimeMillis() + ".png", ist);
        if (url != null) {
            User user = getById(userDto.getId());
            user.setQrUrl(url);
            boolean update = update(user, new UpdateWrapper<User>().eq("id", userDto.getId()));
            UserLoginDto uld = BeanCopyUtil.copyObject(user, UserLoginDto::new);
            //存日志
            UserLog userLog = BeanCopyUtil.copyObject(user, UserLog::new);
            userLog.setUserId(user.getId());
            userLog.setReason("生成二维码");
            userLogMapper.insert(userLog);
            //存redis
            String json = ObjectMapperUtil.parseObject(uld);
            stringRedisTemplate.opsForValue().set("user:id" + uld.getId(), json);
            stringRedisTemplate.expire("user:id" + uld.getId(), 60 * 30, TimeUnit.SECONDS);
            return url;
        }
        return url;
    }

    /*
 邀请好友
 */
    @Override
    public String inviteUser() throws IOException, WriterException {
//        String token = ServletUtil.getRequest().getHeader("auth_token");
//        if (token == null) throw new UserDataException(UserDataExceptionEnum.no_login);
//        Claims claims = jwtTemplate.parseJwt(token);
//        String userId = claims.get("userId").toString();

        UserLoginDto userDto = findUser();
        if (userDto.getIconUrl() == null) throw new UserDataException(UserDataExceptionEnum.image_null);
        InputStream ist = QRUtil.createQR("http://localhost:8001/user/userModule/register", "png", userDto.getIconUrl());
        if (userDto.getQrInvite() != null) throw new UserDataException(UserDataExceptionEnum.QRcode_exist);
        String url = ossTemplate.uploadFile("lyl-sava", System.currentTimeMillis() + "INVATE" + ".png", ist);
        if (url != null) {
            User user = getById(userDto.getId());
            user.setQrInvite(url);
            boolean update = update(user, new UpdateWrapper<User>().eq("id", userDto.getId()));
            UserLoginDto uld = BeanCopyUtil.copyObject(user, UserLoginDto::new);
            //存redis
            String json = ObjectMapperUtil.parseObject(uld);
            stringRedisTemplate.opsForValue().set("user:id" + uld.getId(), json);
            stringRedisTemplate.expire("user:id" + uld.getId(), 60 * 30, TimeUnit.SECONDS);
            return url;
        }
        return url;
    }

    /*
    更改昵称
    */
    @Override
    public UserLoginDto updateNick(String nickname) {
//        String token = ServletUtil.getRequest().getHeader("auth_token");
//        if (token == null) throw new UserDataException(UserDataExceptionEnum.no_login);
//        Claims claims = jwtTemplate.parseJwt(token);
//        String userId = claims.get("userId").toString();

        UserLoginDto userDto = findUser();
        User user = getById(userDto.getId());
        user.setNickname(nickname);
        boolean update = update(user, new UpdateWrapper<User>().eq("id", userDto.getId()));
        UserLoginDto uld = new UserLoginDto();
        if (update) {
            uld = BeanCopyUtil.copyObject(user, UserLoginDto::new);
            //存日志
            UserLog userLog = BeanCopyUtil.copyObject(user, UserLog::new);
            userLog.setUserId(user.getId());
            userLog.setReason("更改昵称");
            userLogMapper.insert(userLog);
            //存redis
            String json = ObjectMapperUtil.parseObject(uld);
            stringRedisTemplate.opsForValue().set("user:id" + uld.getId(), json);
            stringRedisTemplate.expire("user:id" + uld.getId(), 60 * 30, TimeUnit.SECONDS);
            return uld;
        }
        return uld;
    }

    /*
    更改简介
    */
    @Override
    public UserLoginDto updateIntroduction(String introduction) {
//        System.out.println("!!!!!!!!!!!!!!!!!!!");
//        String token = ServletUtil.getRequest().getHeader("auth_token");
//        if (token == null) throw new UserDataException(UserDataExceptionEnum.no_login);
//        Claims claims = jwtTemplate.parseJwt(token);
//        String userId = claims.get("userId").toString();

        UserLoginDto userDto = findUser();
        User user = getById(userDto.getId());
        user.setIntroduction(introduction);
        boolean update = update(user, new UpdateWrapper<User>().eq("id", userDto.getId()));
        UserLoginDto uld = new UserLoginDto();
        System.out.println("====================================");
        //存日志
        UserLog userLog = BeanCopyUtil.copyObject(user,UserLog::new);
        System.out.println("日志："+userLog);
        userLog.setUserId(user.getId());
        userLog.setReason("更改简介");
        System.out.println(userLog);
        int insert = userLogMapper.insert(userLog);
        System.out.println("数值："+insert);

        if (update) {
            uld = BeanCopyUtil.copyObject(user, UserLoginDto::new);
            //存redis
            String json = ObjectMapperUtil.parseObject(uld);
            stringRedisTemplate.opsForValue().set("user:id" + uld.getId(), json);
            stringRedisTemplate.expire("user:id" + uld.getId(), 60 * 30, TimeUnit.SECONDS);
            return uld;
        }
        return uld;
    }

    /*
    更改手机号码
    */
    @Override
    public UserLoginDto updateTel(String tel) {
//        String token = ServletUtil.getRequest().getHeader("auth_token");
//        if (token == null) throw new UserDataException(UserDataExceptionEnum.no_login);
//        Claims claims = jwtTemplate.parseJwt(token);
//        String userId = claims.get("userId").toString();

        UserLoginDto userDto = findUser();
        User user = getById(userDto.getId());
        user.setTel(tel);
        boolean update = update(user, new UpdateWrapper<User>().eq("id", userDto.getId()));
        UserLoginDto uld = new UserLoginDto();
        if (update) {
            uld = BeanCopyUtil.copyObject(user, UserLoginDto::new);
            //存日志
            UserLog userLog = BeanCopyUtil.copyObject(user, UserLog::new);
            userLog.setUserId(user.getId());
            userLog.setReason("更改手机号");
            userLogMapper.insert(userLog);
            //存redis
            String json = ObjectMapperUtil.parseObject(uld);
            stringRedisTemplate.opsForValue().set("user:id" + uld.getId(), json);
            stringRedisTemplate.expire("user:id" + uld.getId(), 60 * 30, TimeUnit.SECONDS);
            return uld;
        }
        return uld;
    }

    /*
 邮箱绑定发送验证码
 */
    @Override
    public String EmailBingSend(String email) throws MessagingException {
        //取token
        String token = ServletUtil.getRequest().getHeader("auth_token");
        if(token==null) throw new UserDataException(UserDataExceptionEnum.no_login);
        Claims claims = jwtTemplate.parseJwt(token);
        String userId = claims.get("userId").toString();
        //从redis和数据库中查出user对象，判断是否有邮箱存在
        User user=null;
        String json = stringRedisTemplate.opsForValue().get("user:id" + userId);
        if(json==null||json.trim().equals("")){
            user = getById(userId);
        }
        UserLoginDto uld = ObjectMapperUtil.parseObject(json, UserLoginDto.class);
        user = BeanCopyUtil.copyObject(uld, User::new);
        //存日志
        UserLog userLog = BeanCopyUtil.copyObject(user, UserLog::new);
        userLog.setUserId(user.getId());
        userLog.setReason("绑定邮箱发验证码");
        userLogMapper.insert(userLog);

//        if(user.getEmail()!=null) throw new UserDataException(UserDataExceptionEnum.email_exits);
        //发送邮箱验证
        String resEmail = sendMail.sendVerificationCode(email);
        stringRedisTemplate.opsForValue().set("emailKey",email,60*3, TimeUnit.SECONDS);
        return resEmail;

    }

    /*
  邮箱绑定验证码验证
  */
    @Override
    public UserLoginDto EmailBingAccept(String emailCode) throws MessagingException {
        String token = ServletUtil.getRequest().getHeader("auth_token");
        if(token==null) throw new UserDataException(UserDataExceptionEnum.no_login);
        Claims claims = jwtTemplate.parseJwt(token);
        String userId = claims.get("userId").toString();
        User user = getById(userId);

        String email = stringRedisTemplate.opsForValue().get("emailKey");
        System.out.println("q:"+email);
        System.out.println("w:"+stringRedisTemplate.opsForValue().get("email:" + email));
        System.out.println("e:"+emailCode);
        if(stringRedisTemplate.getExpire("email:" +email)==null)throw new UserDataException(UserDataExceptionEnum.email_time_ex);
        if(!(stringRedisTemplate.opsForValue().get("email:" +email)).equals(emailCode)) throw new UserDataException(UserDataExceptionEnum.email_code_error);
        //存入数据库，redis
        user.setEmail(email);
        int i = userMapper.update(user, new UpdateWrapper<User>().eq("id", userId));
        UserLoginDto userLoginDto = BeanCopyUtil.copyObject(user, UserLoginDto::new);
        if(i>0) {
            //存日志
            UserLog userLog = BeanCopyUtil.copyObject(user, UserLog::new);
            userLog.setUserId(user.getId());
            userLog.setReason("邮箱绑定成功");
            userLogMapper.insert(userLog);

            String json = ObjectMapperUtil.parseObject(userLoginDto);
            stringRedisTemplate.opsForValue().set("user:id"+userLoginDto.getId(),json);
            stringRedisTemplate.expire("user:id"+userLoginDto.getId(),300, TimeUnit.SECONDS);
            return userLoginDto;
        }
        return userLoginDto;
    }
       /*
  邮箱登录发送邮件登录
  */

    @Override
    public String EmailLoginSend(String username) throws MessagingException {
        User user = userMapper.selectUser(username);
        System.out.println("user:"+user);
        if(user==null)throw new UserLoginException(UserLoginExceptionEnum.user_null);
        stringRedisTemplate.opsForHash().put("user:login:","userName",username);
        if(user.getEmail()==null)throw new UserDataException(UserDataExceptionEnum.email_null);
        String resEmail = sendMail.sendVerificationCode(user.getEmail());
        stringRedisTemplate.opsForValue().set("emailKey",user.getEmail(),60*3, TimeUnit.SECONDS);
        return resEmail;

    }

    /*
   邮箱绑定登录
    */
    @Override
    public UserDto loginAcceptCode(String emailCode) {
        String email = stringRedisTemplate.opsForValue().get("emailKey");
        System.out.println("q:"+email);
        System.out.println("w:"+stringRedisTemplate.opsForValue().get("email:" + email));
        System.out.println("e:"+emailCode);
        if(stringRedisTemplate.getExpire("email:" +email)==null)throw new UserDataException(UserDataExceptionEnum.email_time_ex);
        if(!(stringRedisTemplate.opsForValue().get("email:" +email)).equals(emailCode)) throw new UserDataException(UserDataExceptionEnum.email_code_error);

        String userName = (String)stringRedisTemplate.opsForHash().get("user:login:", "userName");
        System.out.println("xxxxxxxxxxxx"+userName);

        User user = userMapper.selectUser(userName);
        UserLoginDto userLoginDto=BeanCopyUtil.copyObject(user,UserLoginDto::new);
        //存日志
        UserLog userLog = BeanCopyUtil.copyObject(user, UserLog::new);
        userLog.setUserId(user.getId());
        userLog.setReason("邮箱登录成功");
        userLogMapper.insert(userLog);
        //设置token
        HashMap<String, Object> maps = new HashMap<>();
        maps.put("userId",userLoginDto.getId());
        String token = jwtTemplate.createJwt(maps);
        UserDto userDto = new UserDto();
        userDto.setToken(token);
        userDto.setUserLoginDto(userLoginDto);
        stringRedisTemplate.opsForHash().put("user:id:"+userLoginDto.getId(), "role", userLoginDto.getRole() + "");
        stringRedisTemplate.expire("user:id:"+userLoginDto.getId(), jwtTemplate.getJc().getExpire(), TimeUnit.MINUTES);
        //存入redis
        String json = ObjectMapperUtil.parseObject(userLoginDto);
        stringRedisTemplate.opsForValue().set("user:id"+userLoginDto.getId(),json);
        stringRedisTemplate.expire("user:id"+userLoginDto.getId(),300, TimeUnit.SECONDS);
        return userDto;
    }
     /*
      密码改变，邮箱发送
       */
    @Override
    public String EmailUpdatePwdSend() throws MessagingException {
        //取token
        String token = ServletUtil.getRequest().getHeader("auth_token");
        if(token==null) throw new UserDataException(UserDataExceptionEnum.no_login);
        Claims claims = jwtTemplate.parseJwt(token);
        String userId = claims.get("userId").toString();
        //从redis和数据库中查出user对象，判断是否有邮箱存在
        User user=null;
        String json = stringRedisTemplate.opsForValue().get("user:id" + userId);
        if(json==null||json.trim().equals("")){
            user = getById(userId);
        }
        UserLoginDto uld = ObjectMapperUtil.parseObject(json, UserLoginDto.class);
        user = BeanCopyUtil.copyObject(uld, User::new);

        if(user.getEmail()==null) throw new UserDataException(UserDataExceptionEnum.email_no);
        //发送邮箱验证
        String resEmail = sendMail.sendVerificationCode(user.getEmail());
        System.out.println("email:"+user.getEmail());
        stringRedisTemplate.opsForValue().set("emailKey",user.getEmail(),60*3, TimeUnit.SECONDS);
        return resEmail;
    }

    /*
    密码改变，邮箱接受
     */
    @Override
    public UserLoginDto EmailUpdatePwdAccept(String emailCode,String pwd) throws MessagingException {
        String token = ServletUtil.getRequest().getHeader("auth_token");
        if(token==null) throw new UserDataException(UserDataExceptionEnum.no_login);
        Claims claims = jwtTemplate.parseJwt(token);
        String userId = claims.get("userId").toString();
        User user = getById(userId);

        String email = stringRedisTemplate.opsForValue().get("emailKey");
        System.out.println("q:"+email);
        System.out.println("w:"+stringRedisTemplate.opsForValue().get("email:" + email));
        System.out.println("e:"+emailCode);
        if(stringRedisTemplate.getExpire("email:" +email)==null)throw new UserDataException(UserDataExceptionEnum.email_time_ex);
        if(!(stringRedisTemplate.opsForValue().get("email:" +email)).equals(emailCode)) throw new UserDataException(UserDataExceptionEnum.email_code_error);
        //存入数据库，redis
        if(pwd==null) throw new UserDataException(UserDataExceptionEnum.pwd_empty);
        String encryption = EncryptionUtil.encryption(pwd);
        System.out.println(pwd+"|||||"+encryption);
        user.setPwd(encryption);
        int i = userMapper.update(user, new UpdateWrapper<User>().eq("id", userId));
        UserLoginDto userLoginDto = BeanCopyUtil.copyObject(user, UserLoginDto::new);

        //存日志
        UserLog userLog = BeanCopyUtil.copyObject(user, UserLog::new);
        userLog.setUserId(user.getId());
        userLog.setReason("邮箱修改密码成功");
        userLogMapper.insert(userLog);
        if(i>0) {
            String json = ObjectMapperUtil.parseObject(userLoginDto);
            stringRedisTemplate.opsForValue().set("user:id"+userLoginDto.getId(),json);
            stringRedisTemplate.expire("user:id"+userLoginDto.getId(),300, TimeUnit.SECONDS);
            return userLoginDto;
        }
        return userLoginDto;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////后台接口/////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /*
    管理员进行用户搜索
     */
    @Override
    public UserIPageDto userSearch(UserSearchFrom userSearchFrom) {
//        String token = ServletUtil.getRequest().getHeader("auth_token");
//        if(token==null) throw new UserDataException(UserDataExceptionEnum.no_login);
//        Claims claims = jwtTemplate.parseJwt(token);
//        String userId = claims.get("userId").toString();
//        User user = getById(userId);

        if(userSearchFrom.getCurrent()==null||userSearchFrom.getSize()==null) throw new UserDataException(UserDataExceptionEnum.page_null);
        Page<User> page = new Page<>(userSearchFrom.getCurrent(),userSearchFrom.getSize());

        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.like(userSearchFrom.getUsername()!=null&&userSearchFrom.getUsername().length()>0,"username",userSearchFrom.getUsername())
                .eq(userSearchFrom.getEmail()!=null&&userSearchFrom.getEmail().length()>0,"email", userSearchFrom.getEmail())
                .eq(userSearchFrom.getRole()!=null,"role",userSearchFrom.getRole());
        IPage<User> applyIPage = userMapper.selectPage(page,wrapper);
        UserIPageDto userIPageDto = null;

        userIPageDto = BeanCopyUtil.copyObject(applyIPage, UserIPageDto::new);
        List<User> usdList = applyIPage.getRecords();
        List<UserSearchDto> userSearchDtos = BeanCopyUtil.copyList(usdList, UserSearchDto::new);
        userIPageDto.setUsdList(userSearchDtos);
        return userIPageDto;
    }

    /*
   管理员增加
    */
    @Override
    public UserRegisterDto manageAdd(ManageAddFrom manageAddFrom) {
        //密码加密
        if (manageAddFrom == null) throw new UserRegisterException(UserRegisterExceptionEnum.parameter_empty);
        String pwd = manageAddFrom.getPwd();
        String encryption = EncryptionUtil.encryption(pwd);
        User user = BeanCopyUtil.copyObject(manageAddFrom, User::new);
        //判断性别字段异常
        System.out.println(user.getGender());
        System.out.println(UserRegisterStatus.gender_female);
        if (user.getGender() != UserRegisterStatus.gender_female &&
                user.getGender() != UserRegisterStatus.gender_male &&
                user.getGender() != UserRegisterStatus.gender_secrecy) {
            throw new UserRegisterException(UserRegisterExceptionEnum.gender_error);
        }
        //判断账号是否重复
        String username = user.getUsername();
        User us = userMapper.selectUsername(username);
        if (us != null) throw new UserRegisterException(UserRegisterExceptionEnum.username_repeat);
        //初始化账号其他数据
        user.setRole(RuleCode.admin_level);
        user.setIntegration(UserStatusCode.admin_integration_create);
        BigDecimal decimal = new BigDecimal("1000.00");
        user.setBalance(decimal);
        user.setCreateTime(System.currentTimeMillis());
        user.setLastLoginTime(System.currentTimeMillis());
        user.setPwd(encryption);
        UserRegisterDto urd = null;
        int i = userMapper.insert(user);
        if (i > 0) {
            //存日志
            UserLog userLog = BeanCopyUtil.copyObject(user, UserLog::new);
            userLog.setUserId(user.getId());
            userLog.setReason("管理员新增成功");
            userLogMapper.insert(userLog);
            urd = BeanCopyUtil.copyObject(user, UserRegisterDto::new);
            return urd;
        }
        return urd;
    }

    /*
    管理员/用户删除
    */
    @Override
    public UserLoginDto manageFire(Integer id) {
        //查询数据库中是否存在
        User user = getById(id);
        if(user==null) throw new UserDataException(UserDataExceptionEnum.id_is_null);
        //存入Dto
        UserLoginDto userLoginDto = BeanCopyUtil.copyObject(user, UserLoginDto::new);
        //存日志
        UserLog userLog = BeanCopyUtil.copyObject(user, UserLog::new);
        userLog.setUserId(user.getId());
        userLog.setReason("管理员删除操作成功");
        userLogMapper.insert(userLog);
        //数据库中删除
        int i = userMapper.deleteById(id);
        if(i<1) throw new UserDataException(UserDataExceptionEnum.do_error);
        //redis中删除
        String token = null;
        token = ServletUtil.getRequest().getHeader("auth_token");
        if (token == null) throw new UserDataException(UserDataExceptionEnum.no_login);
        Claims claims = jwtTemplate.parseJwt(token);
        String userId = claims.get("userId").toString();
        stringRedisTemplate.delete("user:id" + userId);
        return userLoginDto;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////积分余额支付接口////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /*购买积分商品，扣除积分
     */
    @Override
    public ChangeIntegationSelDto changeIntegrationProductDel(ChangeIntegationSelForm changeIntegationSelForms) {
        //判断参数
        if (changeIntegationSelForms.getIntegration() == 0) throw new UserDataException(UserDataExceptionEnum.CODE_ERROR);
        //如果id是空,从token中获取id值
        if (changeIntegationSelForms.getId() == null) {
            String token = null;
            token = ServletUtil.getRequest().getHeader("auth_token");
            if (token == null) throw new UserDataException(UserDataExceptionEnum.no_login);
            Claims claims = jwtTemplate.parseJwt(token);
            String userId = claims.get("userId").toString();
            changeIntegationSelForms.setId(Integer.parseInt(userId));
        }
        //如果不是空,进行逻辑处理,减积分
        User user = userMapper.selectById(changeIntegationSelForms.getId());
        if(user==null) throw  new UserDataException(UserDataExceptionEnum.SQL_ERROR);
        Integer integration = user.getIntegration();
        int newintegration = integration - changeIntegationSelForms.getIntegration();
        System.out.println(newintegration);
        user.setIntegration(newintegration);
        //进行更改
        int i = userMapper.updateById(user);
        if (i <= 0) throw new UserDataException(UserDataExceptionEnum.SQL_ERROR);
        //新建转换类
        ChangeIntegationSelDto result = BeanCopyUtil.copyObject(user, ChangeIntegationSelDto::new);
        //存redis
        String s = ObjectMapperUtil.parseObject(user);
        stringRedisTemplate.opsForValue().set("user:id" + user.getId(), s, 60 * 30, TimeUnit.SECONDS);
        //
        return result;
    }

    //取消订单，归还积分，前台直接获取token的id返回积分,后台通过直接获取id
    @Override
    public ChangeIntegationSelDto changeIntegrationProductAdd(ChangeIntegationSelForm changeIntegationSelForms) {
        //判断参数
        if (changeIntegationSelForms.getIntegration() == 0) throw new UserDataException(UserDataExceptionEnum.CODE_ERROR);
        //如果id是空,从token中获取id值
        if (changeIntegationSelForms.getId() == null) {
            String token = null;
            token = ServletUtil.getRequest().getHeader("auth_token");
            if (token == null) throw new UserDataException(UserDataExceptionEnum.no_login);
            Claims claims = jwtTemplate.parseJwt(token);
            String userId = claims.get("userId").toString();
            changeIntegationSelForms.setId(Integer.parseInt(userId));
        }
        //如果不是空,进行逻辑处理,减积分
        User user = userMapper.selectById(changeIntegationSelForms.getId());
        if(user==null) throw  new UserDataException(UserDataExceptionEnum.SQL_ERROR);
        Integer integration = user.getIntegration();
        int newintegration = integration + changeIntegationSelForms.getIntegration();
        user.setIntegration(newintegration);

        //进行更改
        int i = userMapper.updateById(user);
        if (i <= 0) throw new UserDataException(UserDataExceptionEnum.SQL_ERROR);
        //新建转化类
        ChangeIntegationSelDto result = BeanCopyUtil.copyObject(user, ChangeIntegationSelDto::new);
        //存redis
        String s = ObjectMapperUtil.parseObject(user);
        stringRedisTemplate.opsForValue().set("user:id" + user.getId(), s, 60 * 30, TimeUnit.SECONDS);
        //
        return result;
    }

    //新增支付宝支付成功异步回调接口执行业务操作
    @Override
    public void addBanlance(String out_trade_no, String trade_no) {
        System.out.println("aplipaygo");
        QueryWrapper<BalanceLog> wrapper = new QueryWrapper<>();
        wrapper.eq("pay_num", out_trade_no);
        BalanceLog balanceLog = balanceLogMapper.selectOne(wrapper);
        System.out.println(balanceLog);
        if (balanceLog == null) throw new UserDataException(UserDataExceptionEnum.SQL_ERROR);
        //查询结果,新增日记结果
        BalanceLog balanceLog1 = BeanCopyUtil.copyObject(balanceLog, BalanceLog::new);
        long l = System.currentTimeMillis();
        balanceLog1.setCreateTime(l);
        balanceLog1.setBalanceChangeReason(0);
        int insert = balanceLogMapper.insert(balanceLog1);
        if (insert <= 0) throw new UserDataException(UserDataExceptionEnum.SQL_ERROR);
        //获取user信息
        Integer userId = balanceLog.getUserId();
        User user = userMapper.selectById(userId);
        if (user == null) throw new UserDataException(UserDataExceptionEnum.SQL_ERROR);
        //查询余额
        BigDecimal oldOrder = user.getBalance();
        BigDecimal newOrder = BigDecimal.valueOf(balanceLog1.getBalanceAmount());
        BigDecimal add = oldOrder.add(newOrder);
        //更新user余额
        user.setBalance(add);
        int i = userMapper.updateById(user);
        if (i <= 0) throw new UserDataException(UserDataExceptionEnum.SQL_ERROR);
    }

    //更改账户余额操作（+操作）
    @Override
    public BalanceChangeDto changeBalanceAdd(BalanceChangeForm balanceChangeForm) {
        //验参
        if(balanceChangeForm==null||balanceChangeForm.getId()==null) throw new UserDataException(UserDataExceptionEnum.CODE_ERROR);
        if(balanceChangeForm.getBalance().intValue()<=0) throw new UserDataException(UserDataExceptionEnum.id_null);
        User user1 = userMapper.selectById(balanceChangeForm.getId());
        if(user1==null) throw  new UserDataException(UserDataExceptionEnum.id_null);
        QueryWrapper<BalanceLog> wrapper = new QueryWrapper<>();
        wrapper.eq("pay_num",balanceChangeForm.getPayNum());
        BalanceLog balanceLog1 = balanceLogMapper.selectOne(wrapper);
        if(balanceLog1==null) throw  new UserDataException(UserDataExceptionEnum.PAYNUM_ERROR);
        //业务操作
        User user = userMapper.selectById(balanceChangeForm.getId());
        if(user==null)throw new UserDataException(UserDataExceptionEnum.SQL_ERROR);
        user.setBalance(user.getBalance().add(balanceChangeForm.getBalance()));
        System.out.println("111111"+user.getId());
        int i = userMapper.updateById(user);
        if(i<=0) throw new UserDataException(UserDataExceptionEnum.SQL_ERROR);
        //存redis
        String s = ObjectMapperUtil.parseObject(user);
        stringRedisTemplate.opsForValue().set("user:id"+balanceChangeForm.getId(),s,30*60,TimeUnit.SECONDS);
        log.info("余额修改成功{}",s);
        //存日记
        BalanceLog balanceLog = new BalanceLog();
        balanceLog.setUserId(user.getId());
        balanceLog.setBalanceAmount(balanceChangeForm.getBalance().intValue());
        balanceLog.setCreateTime(System.currentTimeMillis());
        balanceLog.setBalanceChangeReason(0);
        balanceLog.setOrderId(0);
        balanceLog.setPayNum(balanceChangeForm.getPayNum());
        int insert = balanceLogMapper.insert(balanceLog);
        if(insert<=0) throw new UserDataException(UserDataExceptionEnum.SQL_ERROR);
        log.info("lod存储成功");
        //返回dto
        BalanceChangeDto balanceChangeDto = BeanCopyUtil.copyObject(balanceLog, BalanceChangeDto::new);
        balanceChangeDto.setBalanceAmount(user.getBalance());
        return balanceChangeDto;
    }
    //更改账户余额操作（-操作）
    @Override
    public BalanceChangeDto changeBalanceSel(BalanceChangeForm balanceChangeForm) {
        //验参
        if(balanceChangeForm==null||balanceChangeForm.getId()==null) throw new UserDataException(UserDataExceptionEnum.CODE_ERROR);
        if(balanceChangeForm.getBalance().intValue()<=0) throw new UserDataException(UserDataExceptionEnum.id_null);
        User user1 = userMapper.selectById(balanceChangeForm.getId());
        if(user1==null) throw  new UserDataException(UserDataExceptionEnum.id_null);
        QueryWrapper<BalanceLog> wrapper = new QueryWrapper<>();
        wrapper.eq("pay_num",balanceChangeForm.getPayNum());
        BalanceLog balanceLog1 = balanceLogMapper.selectOne(wrapper);
        if(balanceLog1!=null) throw  new UserDataException(UserDataExceptionEnum.PAYNUM_ERROR);
        //业务操作
        User user = userMapper.selectById(balanceChangeForm.getId());
        if(user==null)throw new UserDataException(UserDataExceptionEnum.SQL_ERROR);
        if(balanceChangeForm.getBalance().compareTo(user.getBalance())>0){
            throw new UserException("余额不足",1001);
        }
        user.setBalance(user.getBalance().subtract(balanceChangeForm.getBalance()));
        int i = userMapper.updateById(user);
        if(i<=0) throw new UserDataException(UserDataExceptionEnum.SQL_ERROR);
        //存redis
        String s = ObjectMapperUtil.parseObject(user);
        stringRedisTemplate.opsForValue().set("user:id"+balanceChangeForm.getId(),s,30*60,TimeUnit.SECONDS);
        log.info("余额修改成功{}",s);
        //存日记
        BalanceLog balanceLog = new BalanceLog();
        balanceLog.setUserId(user.getId());
        balanceLog.setBalanceAmount(balanceChangeForm.getBalance().intValue());
        balanceLog.setCreateTime(System.currentTimeMillis());
        balanceLog.setBalanceChangeReason(1);
        balanceLog.setOrderId(0);
        balanceLog.setPayNum(balanceChangeForm.getPayNum());
        int insert = balanceLogMapper.insert(balanceLog);
        if(insert<=0) throw new UserDataException(UserDataExceptionEnum.SQL_ERROR);
        log.info("lod存储成功");
        //返回dto
        BalanceChangeDto balanceChangeDto = BeanCopyUtil.copyObject(balanceLog, BalanceChangeDto::new);
        balanceChangeDto.setBalanceAmount(user.getBalance());
        return balanceChangeDto;
    }
}
