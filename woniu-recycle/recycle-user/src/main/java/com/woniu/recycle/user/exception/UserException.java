package com.woniu.recycle.user.exception;

import com.woniu.recycle.commons.core.exception.RecycleException;

public class UserException extends RecycleException {

    public UserException(){
        super();
    }

    public UserException(String msg, Integer code) {
        super(msg,code);

    }

    public UserException(String msg) {
        super(msg);
    }
}
