package com.woniu.recycle.user.service;

import com.woniu.recycle.user.domain.IntegrationLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 积分日志表 服务类
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
public interface IntegrationLogService extends IService<IntegrationLog> {

}
