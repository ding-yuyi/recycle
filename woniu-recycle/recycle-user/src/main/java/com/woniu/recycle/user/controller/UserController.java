package com.woniu.recycle.user.controller;


import com.woniu.recycle.commons.core.model.ResponseEntity;
import com.woniu.recycle.commons.web.component.BaseController;
import com.woniu.recycle.user.form.BalanceChangeForm;
import com.woniu.recycle.user.domain.User;
import com.woniu.recycle.user.dto.*;
import com.woniu.recycle.user.exception.*;
import com.woniu.recycle.user.form.*;
import com.woniu.recycle.user.mapper.UserMapper;
import com.woniu.recycle.user.service.UserService;
import com.woniu.recycle.user.utils.OSSTemplate;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.validation.Valid;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
@Slf4j
@RestController
@RequestMapping("/userModule")
@Api(tags = "用户个人中心管理")
public class UserController extends BaseController {

    @Resource
    private UserService userService;

    @Resource
    private OSSTemplate ossTemplate;

    @Resource
    private UserMapper userMapper;

    /**
     * 用户注册
     * 无权限
     */
    @PostMapping("/register")
    @ApiOperation("注册")
    public ResponseEntity<UserRegisterDto> register(@Valid UserRegisterFrom userRegisterFrom,BindingResult bindingResult){
        log.info("入参参数:{}", userRegisterFrom);

        if (userRegisterFrom.getUsername() == null)
            throw new UserRegisterException(UserRegisterExceptionEnum.username_empty);
        if (userRegisterFrom.getPwd() == null)
            throw new UserRegisterException(UserRegisterExceptionEnum.password_empty);
        if (userRegisterFrom.getGender() == null)
            throw new UserRegisterException(UserRegisterExceptionEnum.gender_empty);

        //参数验证
        ResponseEntity responseEntity = extractError(bindingResult);
        if (responseEntity != null) {
            return responseEntity;
        }

        UserRegisterDto user = userService.register(userRegisterFrom);
        log.info("返回数据:{}", user);
        if (user == null) return ResponseEntity.BuildError(UserRegisterDto.class).setMsg("账号创建失败");
        return ResponseEntity.BuildSuccess(UserRegisterDto.class).setMsg("账号创建成功").setData(user);
    }

    /**
     * 用户登录
     * 无权限
     */
    @PostMapping("/login")
    @ApiOperation("登录")
    public ResponseEntity<UserDto> login(UserLoginFrom userLoginFrom) {
        log.info("入参参数:{}", userLoginFrom);
        if (userLoginFrom.getUsername() == null) throw new UserLoginException(UserLoginExceptionEnum.username_empty);
        if (userLoginFrom.getPwd() == null) throw new UserLoginException(UserLoginExceptionEnum.pwd_empty);
        UserDto userDto = userService.login(userLoginFrom);
        log.info("返回数据:{}", userDto);
        if (userDto == null) return ResponseEntity.BuildError(UserDto.class).setMsg("登录失败");
        return ResponseEntity.BuildSuccess(UserDto.class).setMsg("登录成功").setData(userDto);
    }

    /**
     * 用户查询个人信息
     * 有权限
     */
    @GetMapping("/auth/findUser")
    @ApiOperation("查看个人信息")
    public ResponseEntity<UserLoginDto> findUser() {
        UserLoginDto uld = userService.findUser();
        if (uld == null) throw new UserDataException(UserDataExceptionEnum.data_empty);
        log.info("返回数据:{}", uld);
        return ResponseEntity.BuildSuccess(UserLoginDto.class).setMsg("个人信息").setData(uld);
    }

    /**
     * 用户注销
     * 有权限
     */
    @GetMapping("/auth/loginOut")
    @ApiOperation("注销")
    public ResponseEntity loginOut() {
        userService.loginOut();
        return ResponseEntity.BuildSuccess().setMsg("用户注销成功");
    }


    /**
     * 用户头像上传
     * 有权限
     */
    @PostMapping("/auth/upload")
    @ApiOperation("用户头像上传")
    public ResponseEntity<String> upLoad(MultipartFile file) throws Exception {
        if(file==null) throw new UserDataException(UserDataExceptionEnum.image_no);
        String url = userService.UploadPicture(file);
        if (url == null) return ResponseEntity.BuildError(String.class).setMsg("上传失败，请更换符合规格的图片");
        log.info("返回数据:{}", url);
        return ResponseEntity.BuildSuccess(String.class).setMsg("上传成功").setData(url);
    }

    /**
     * 二维码生成
     * 有权限
     */
    @PostMapping("/auth/QRcreate")
    @ApiOperation("个人二维码生成")
    public ResponseEntity<String> QRcreate() throws Exception {
        String url = userService.QRcodeCreat();
        log.info("返回数据:{}", url);
        if (url == null) return ResponseEntity.BuildError(String.class).setMsg("二维码生成失败，请更换符合规格的图片");
        return ResponseEntity.BuildSuccess(String.class).setMsg("生成成功").setData(url);
    }

    /**
     * 二维码生成
     * 有权限
     */
    @PostMapping("/auth/QRinvate")
    @ApiOperation("邀请注册二维码生成")
    public ResponseEntity<String> QRinvate() throws Exception {
        String url = userService.inviteUser();
        log.info("返回数据:{}", url);
        if (url == null) return ResponseEntity.BuildError(String.class).setMsg("二维码生成失败，请更换符合规格的图片");
        return ResponseEntity.BuildSuccess(String.class).setMsg("生成成功,扫描邀请好友").setData(url);
    }

    /**
     * 更改昵称
     * 有权限
     */
    @GetMapping("/auth/updateNick")
    @ApiOperation("更改昵称")
    public ResponseEntity<UserLoginDto> updateNick(String nickName) {
        log.info("入参参数:{}", nickName);
        if (nickName == null) throw new UserDataException(UserDataExceptionEnum.nick_null);
        UserLoginDto userLoginDto = userService.updateNick(nickName);
        log.info("返回数据:{}", userLoginDto);
        if (userLoginDto == null) return ResponseEntity.BuildError(UserLoginDto.class).setMsg("昵称更改失败");
        return ResponseEntity.BuildSuccess(UserLoginDto.class).setMsg("昵称更改成功").setData(userLoginDto);
    }

    /**
     * 更改简介
     * 有权限
     */
    @GetMapping("/auth/updateIntro")
    @ApiOperation("更改简介")
    public ResponseEntity<UserLoginDto> updateIntro(String introduction) {
        log.info("入参参数:{}", introduction);
        if (introduction == null) throw new UserDataException(UserDataExceptionEnum.intro_null);
        UserLoginDto userLoginDto = userService.updateIntroduction(introduction);
        log.info("返回数据:{}", userLoginDto);
        if (userLoginDto == null) return ResponseEntity.BuildError(UserLoginDto.class).setMsg("简介更改失败");
        return ResponseEntity.BuildSuccess(UserLoginDto.class).setMsg("简介更改成功").setData(userLoginDto);
    }

    /**
     * 更改手机号
     * 有权限
     */
    @GetMapping("/auth/updateTel")
    @ApiOperation("更改手机号")
    public ResponseEntity<UserLoginDto> updateTel(String tel) {
        log.info("入参参数:{}", tel);
        if (tel == null) throw new UserDataException(UserDataExceptionEnum.tel_no);
        UserLoginDto userLoginDto = userService.updateTel(tel);
        log.info("返回数据:{}", userLoginDto);
        if (userLoginDto == null) return ResponseEntity.BuildError(UserLoginDto.class).setMsg("手机号更改失败");
        return ResponseEntity.BuildSuccess(UserLoginDto.class).setMsg("手机号更改成功").setData(userLoginDto);
    }

    /**
     * 绑定邮箱发送
     * 有权限
     */
    @PostMapping("/auth/bindEmail")
    @ApiOperation("绑定邮箱,发送验证码")
    public ResponseEntity<String> bindEmail(String email) throws MessagingException {
        log.info("入参参数:{}", email);
        if(email==null) throw new UserDataException(UserDataExceptionEnum.email_valid);
        String emailCode = userService.EmailBingSend(email);
        log.info("返回数据:{}", emailCode);
        if(emailCode==null) return ResponseEntity.BuildError(String.class).setMsg("验证码发送失败");
        return ResponseEntity.BuildSuccess(String.class).setMsg("验证码发送成功").setData(emailCode);
    }


    /**
     * 绑定邮箱接受
     * 有权限
     */
    @PostMapping("/auth/bindEmailAccept")
    @ApiOperation("绑定邮箱,接受验证码")
    public ResponseEntity<UserLoginDto> bindEmailAccept(String emailCode) throws MessagingException {
        log.info("入参参数:{}", emailCode);
        if(emailCode==null) throw new UserDataException(UserDataExceptionEnum.email_code_empty);
        UserLoginDto userLoginDto = userService.EmailBingAccept(emailCode);
        log.info("返回数据:{}", userLoginDto);
        if(userLoginDto==null) return ResponseEntity.BuildError(UserLoginDto.class).setMsg("邮箱绑定失败");
        return ResponseEntity.BuildSuccess(UserLoginDto.class).setMsg("邮箱绑定成功").setData(userLoginDto);
    }

    /**
     * 邮箱登录发送验证码
     * 有权限
     */
    @PostMapping("/auth/loginEmailSend")
    @ApiOperation("邮箱登录发送验证码")
    public ResponseEntity<String> loginEmailSend(String username) throws MessagingException {
        log.info("入参参数:{}", username);
        if(username==null) throw new UserDataException(UserDataExceptionEnum.username_null);
        String emailCode = userService.EmailLoginSend(username);
        log.info("返回数据:{}", emailCode);
        if(emailCode==null) return ResponseEntity.BuildError(String.class).setMsg("验证码发送失败");
        return ResponseEntity.BuildSuccess(String.class).setMsg("验证码发送成功").setData(emailCode);
    }

    /**
     * 邮箱登录接受验证码
     * 有权限
     */
    @PostMapping("/auth/loginEmailAccept")
    @ApiOperation("邮箱登录接受验证码")
    public ResponseEntity<UserDto> loginEmailAccept(String emailCode) throws MessagingException {
        log.info("入参参数:{}", emailCode);
        if(emailCode==null) throw new UserDataException(UserDataExceptionEnum.email_code_empty);
        UserDto userDto = userService.loginAcceptCode(emailCode);
        log.info("返回数据:{}", userDto);
        if(userDto==null) return ResponseEntity.BuildError(UserDto.class).setMsg("登录失败");
        return ResponseEntity.BuildSuccess(UserDto.class).setMsg("登录成成功").setData(userDto);
    }

    /**
     * 修改密码发验证码
     * 有权限
     */
    @GetMapping("/auth/EmailUpdatePwdSend")
    @ApiOperation("修改密码发验证码")
    public ResponseEntity<String> EmailUpdatePwdSend() throws MessagingException {
        String emailCode = userService.EmailUpdatePwdSend();
        log.info("返回数据:{}", emailCode);
        if(emailCode==null) return ResponseEntity.BuildError(String.class).setMsg("验证码发送失败");
        return ResponseEntity.BuildSuccess(String.class).setMsg("验证码发送成功").setData(emailCode);
    }


    /**
     * 修改密码接受验证码
     * 有权限
     */
    @PostMapping("/auth/EmailUpdatePwdAccept")
    @ApiOperation("修改密码接受验证码")
    public ResponseEntity<UserLoginDto> EmailUpdatePwdAccept(String emailCode,String pwd) throws MessagingException {
        log.info("入参参数:{}", emailCode+"|||"+pwd);
        if(emailCode==null) throw new UserDataException(UserDataExceptionEnum.email_code_empty);
        UserLoginDto userLoginDto = userService.EmailUpdatePwdAccept(emailCode,pwd);
        log.info("返回数据:{}", userLoginDto);
        if(userLoginDto==null) return ResponseEntity.BuildError(UserLoginDto.class).setMsg("密码修改失败");
        return ResponseEntity.BuildSuccess(UserLoginDto.class).setMsg("密码修改成功").setData(userLoginDto);
    }




////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////后台接口/////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * 管理员根据id查看用户信息
     * 有权限
     */
    @GetMapping("/auth/UserById")
    @ApiOperation("管理员根据id查看用户信息")
    public ResponseEntity<User> UserById(Integer userId) {
        log.info("入参参数:{}", userId);
        if (userId == null) throw new UserDataException(UserDataExceptionEnum.id_null);
        User user = userMapper.selectById(userId);
        if (user != null) return ResponseEntity.BuildSuccess(User.class).setMsg("查询成功").setData(user);
        return ResponseEntity.BuildError(User.class).setMsg("用户信息不存在");
    }

    /**
     * 根据条件搜寻用户信息
     * 有权限
     */
    @GetMapping("/auth/userSearch")
    @ApiOperation("管理员根据条件搜寻用户信息")
    public ResponseEntity<UserIPageDto> userSearch(UserSearchFrom userSearchFrom){
        log.info("搜索数据为进：{}",userSearchFrom);
        UserIPageDto userIPageDto = userService.userSearch(userSearchFrom);

        log.info("搜索数据出：{}",userIPageDto);
        if (userIPageDto == null) return ResponseEntity.BuildSuccess(UserIPageDto.class).setMsg("无该条件用户");
        return ResponseEntity.BuildSuccess(UserIPageDto.class).setMsg("查询成功").setData(userIPageDto);
    }

    /**
     * 管理员新增
     * 有权限
     */
    @PostMapping("/auth/manageAdd")
    @ApiOperation("管理员新增")
    public ResponseEntity<UserRegisterDto> manageAdd(@Valid ManageAddFrom manageAddFrom, BindingResult bindingResult) {
        log.info("入参参数:{}", manageAddFrom);

        //参数验证
        ResponseEntity responseEntity = extractError(bindingResult);
        if (responseEntity != null) {
            return responseEntity;
        }
        if (manageAddFrom.getUsername() == null)
            throw new UserRegisterException(UserRegisterExceptionEnum.username_empty);
        if (manageAddFrom.getPwd() == null)
            throw new UserRegisterException(UserRegisterExceptionEnum.password_empty);
        if (manageAddFrom.getGender() == null)
            throw new UserRegisterException(UserRegisterExceptionEnum.gender_empty);
        UserRegisterDto user = userService.manageAdd(manageAddFrom);
        log.info("返回数据:{}", user);
        if (user == null) return ResponseEntity.BuildError(UserRegisterDto.class).setMsg("管理员账号创建失败");
        return ResponseEntity.BuildSuccess(UserRegisterDto.class).setMsg("管理员账号创建成功").setData(user);
    }

    /**
     * 管理员/用户删除
     * 有权限
     */
    @GetMapping("/auth/deleteManager")
    @ApiOperation("管理员/用户删除")
    public ResponseEntity<UserLoginDto> deleteManager(Integer id){
        log.info("入参参数:{}", id);
        UserLoginDto userLoginDto = userService.manageFire(id);
        log.info("返回数据:{}", userLoginDto);
        if(userLoginDto!=null) return ResponseEntity.BuildSuccess(UserLoginDto.class).setMsg("删除成功").setData(userLoginDto);
        return ResponseEntity.BuildError(UserLoginDto.class).setMsg("删除失败");
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////积分余额支付接口////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //下单后扣除积分接口
    @PutMapping("/auth/changeDel")
    @ApiOperation("购买积分商品后扣除积分")
    public ResponseEntity<ChangeIntegationSelDto> changeDel(@Valid ChangeIntegationSelForm changeIntegationSelForms, BindingResult bindingResult) {
        //日记记录
        log.info("入参参数是：{}", changeIntegationSelForms);
        //参数校验
        ResponseEntity responseEntity = extractError(bindingResult);
        if (responseEntity != null) {
            return responseEntity;
        }
        //主业务逻辑
        ChangeIntegationSelDto result = userService.changeIntegrationProductDel(changeIntegationSelForms);
        if (result == null) return ResponseEntity.BuildError(ChangeIntegationSelDto.class).setMsg("购买失败");
        return ResponseEntity.BuildSuccess(ChangeIntegationSelDto.class).setMsg("购买成功").setData(result);
    }

    //下单后扣除积分接口
    @PutMapping("/auth/changeAdd")
    @ApiOperation("增加积分jiekou")
    public ResponseEntity<ChangeIntegationSelDto> changeAdd(@Valid ChangeIntegationSelForm changeIntegationSelForms, BindingResult bindingResult) {
        //日记记录
        log.info("入参参数是：{}", changeIntegationSelForms);
        //参数校验
        ResponseEntity responseEntity = extractError(bindingResult);
        if (responseEntity != null) {
            return responseEntity;
        }
        //主业务逻辑
        ChangeIntegationSelDto result = userService.changeIntegrationProductAdd(changeIntegationSelForms);
        if (result == null) return ResponseEntity.BuildError(ChangeIntegationSelDto.class).setMsg("退还积分失败");
        return ResponseEntity.BuildSuccess(ChangeIntegationSelDto.class).setMsg("退还积分成功").setData(result);
    }
    //余额充值接口(支付宝充值)
//    @PostMapping("/auth/addBalance")
//    @ApiOperation("余额充值接口(支付宝充值)")

    //增加余额接口
    @PutMapping("/auth/balanceAdd")
    @ApiOperation("增加余额接口")
    public ResponseEntity<BalanceChangeDto> balanceAdd(@Valid BalanceChangeForm balanceChangeForm, BindingResult bindingResult) {
        //日记记录
        log.info("入参参数是：{}", balanceChangeForm);
        //参数校验
        ResponseEntity responseEntity = extractError(bindingResult);
        if (responseEntity != null) {
            return responseEntity;
        }
        //主业务逻辑
        BalanceChangeDto result = userService.changeBalanceAdd(balanceChangeForm);
        if (result == null) return ResponseEntity.BuildError(BalanceChangeDto.class).setMsg("新增余额失败");
        return ResponseEntity.BuildSuccess(BalanceChangeDto.class).setMsg("新增余额成功").setData(result);
    }
    //减少余额接口
    @PutMapping("/auth/balanceSel")
    @ApiOperation("减少余额接口")
    public ResponseEntity<BalanceChangeDto> balanceSel(@Valid BalanceChangeForm balanceChangeForm, BindingResult bindingResult) {
        //日记记录
        log.info("入参参数是：{}", balanceChangeForm);
        //参数校验
        ResponseEntity responseEntity = extractError(bindingResult);
        if (responseEntity != null) {
            return responseEntity;
        }
        //主业务逻辑
        BalanceChangeDto result = userService.changeBalanceSel(balanceChangeForm);
        if (result == null) return ResponseEntity.BuildError(BalanceChangeDto.class).setMsg("减少余额失败").setCode(3011);
        return ResponseEntity.BuildSuccess(BalanceChangeDto.class).setMsg("减少余额成功").setData(result).setCode(200);
    }

}

