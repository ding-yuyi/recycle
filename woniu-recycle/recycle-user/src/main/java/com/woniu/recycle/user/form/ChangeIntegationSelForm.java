package com.woniu.recycle.user.form;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;
import javax.validation.constraints.NotNull;


/**
 *
 */
@ApiModel("商品积分变动form")
@Data
public class ChangeIntegationSelForm {

    @ApiModelProperty(value = "id", example = "1")
    private Integer id;


    @NotNull
    @ApiModelProperty(value = "积分变动数值", example = "1")
    private Integer integration;


}
