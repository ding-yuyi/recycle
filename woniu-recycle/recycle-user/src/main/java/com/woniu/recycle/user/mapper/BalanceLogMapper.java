package com.woniu.recycle.user.mapper;

import com.woniu.recycle.user.domain.BalanceLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 余额日志表 Mapper 接口
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
public interface BalanceLogMapper extends BaseMapper<BalanceLog> {

}
