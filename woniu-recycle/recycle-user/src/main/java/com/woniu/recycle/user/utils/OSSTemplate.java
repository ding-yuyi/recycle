package com.woniu.recycle.user.utils;
import com.woniu.recycle.user.config.OssConfig;
import com.woniu.recycle.user.exception.RearFileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.CannedAccessControlList;
import com.aliyun.oss.model.CreateBucketRequest;
import org.springframework.stereotype.Component;

@Component
public class OSSTemplate {
    @Autowired
    private OssConfig config;

    public OSSTemplate(OssConfig config) {
        this.config = config;
    }

    public String uploadFile(String bucketName,String path,File file) throws FileNotFoundException {
        OSS ossClient = null;
        try {
            ossClient = new OSSClientBuilder().build(config.getEndpoint(), config.getAccessKeyId(), config.getAccessKeySecret());
            if(!ossClient.doesBucketExist(bucketName)) {
                ossClient.createBucket(bucketName);
                CreateBucketRequest createBucketRequest= new CreateBucketRequest(bucketName);
                createBucketRequest.setCannedACL(CannedAccessControlList.PublicRead);
                ossClient.createBucket(createBucketRequest);
            }
            ossClient.putObject(bucketName,path,file);
            return "http://"+bucketName+"."+config.getEndpoint()+"/"+path;
        } catch (Exception e) {
            throw new RearFileUploadException("上传失败",5001);
        } finally {
            if(ossClient != null) ossClient.shutdown();
        }
    }

    /**
     * OSS上传文件
     * @param bucketName
     * @param path 文件全路径名
     * @param ist 文件对象流
     * @return
     */
    public String uploadFile(String bucketName,String path,InputStream ist) {
        OSS ossClient = null;
        try {
            ossClient = new OSSClientBuilder().build(config.getEndpoint(), config.getAccessKeyId(), config.getAccessKeySecret());
            if(!ossClient.doesBucketExist(bucketName)) {
                ossClient.createBucket(bucketName);
                CreateBucketRequest createBucketRequest= new CreateBucketRequest(bucketName);
                createBucketRequest.setCannedACL(CannedAccessControlList.PublicRead);
                ossClient.createBucket(createBucketRequest);
            }
            ossClient.putObject(bucketName,path,ist);
            return "http://"+bucketName+"."+config.getEndpoint()+"/"+path;
        } catch (Exception e) {
            throw new RearFileUploadException("上传失败",5001);
        } finally {
            if(ossClient != null) ossClient.shutdown();
        }

    }

    /**
     * OSS删除文件
     * @param bucketName
     * @param path 文件全路径名
     */
    public void deleteFile(String bucketName,String path) {
        OSS ossClient = null;
        try {
            ossClient = new OSSClientBuilder().build(config.getEndpoint(), config.getAccessKeyId(), config.getAccessKeySecret());
            ossClient.deleteObject(bucketName, path);
        } catch(Exception e) {
            throw new RearFileUploadException("删除失败",5002);
        } finally {
            if(ossClient != null) ossClient.shutdown();
        }

    }


}
