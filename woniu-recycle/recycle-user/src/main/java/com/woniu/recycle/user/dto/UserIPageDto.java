package com.woniu.recycle.user.dto;

import lombok.Data;

import java.util.List;

@Data
public class UserIPageDto {
    private Long size;
    private Long current;
//  private Long total;
    private List<UserSearchDto> usdList;
}
