package com.woniu.recycle.user.codenum;

import lombok.Data;

@Data
public class RuleCode {
    public static final Integer user_level=0;
    public static final Integer admin_level=1;
    public static final Integer super_admin_level=2;
}
