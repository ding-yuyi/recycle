package com.woniu.recycle.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.recycle.commons.web.util.BeanCopyUtil;
import com.woniu.recycle.commons.web.util.EncryptionUtil;
import com.woniu.recycle.commons.web.util.ServletUtil;
import com.woniu.recycle.user.codenum.RuleCode;
import com.woniu.recycle.user.codenum.UserRegisterStatus;
import com.woniu.recycle.user.codenum.UserStatusCode;
import com.woniu.recycle.user.domain.Housekeeper;
import com.woniu.recycle.user.domain.User;
import com.woniu.recycle.user.dto.*;
import com.woniu.recycle.user.exception.UserDataException;
import com.woniu.recycle.user.exception.UserDataExceptionEnum;
import com.woniu.recycle.user.exception.UserRegisterException;
import com.woniu.recycle.user.exception.UserRegisterExceptionEnum;
import com.woniu.recycle.user.form.HouseKeeperForm;
import com.woniu.recycle.user.form.HouseSearchForm;
import com.woniu.recycle.user.mapper.HousekeeperMapper;
import com.woniu.recycle.user.service.HousekeeperService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.recycle.user.utils.Algorithm;
import io.jsonwebtoken.Claims;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 家政员工表 服务实现类
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
@Service
public class HousekeeperServiceImpl extends ServiceImpl<HousekeeperMapper, Housekeeper> implements HousekeeperService {

    @Resource
    private HousekeeperMapper HousekeeperMapper;

    @Resource
    private Algorithm algorithm;

    @Override
    public HouseIPageDto houseSearch(HouseSearchForm houseSearchForm) {

        if(houseSearchForm.getCurrent()==null||houseSearchForm.getSize()==null) throw new UserDataException(UserDataExceptionEnum.page_null);
        Page<Housekeeper> page = new Page<>(houseSearchForm.getCurrent(),houseSearchForm.getSize());

        QueryWrapper<Housekeeper> wrapper = new QueryWrapper<>();
        wrapper.like(houseSearchForm.getHousekeeperName()!=null&&houseSearchForm.getHousekeeperName().length()>0,"housekeeper_name",houseSearchForm.getHousekeeperName())
                .eq(houseSearchForm.getHousekeeperTel()!=null&&houseSearchForm.getHousekeeperTel().length()>0,"housekeeper_tel", houseSearchForm.getHousekeeperTel());

        Page<Housekeeper> housekeeperPage = HousekeeperMapper.selectPage(page, wrapper);
        if(housekeeperPage==null){
            System.out.println(housekeeperPage.getRecords());
            System.out.println(housekeeperPage.getSize());
            System.out.println(housekeeperPage.getPages());
        }
        List<Housekeeper> HkdList = housekeeperPage.getRecords();
        System.out.println(HkdList);
        HouseIPageDto houseIPageDto=null;
        houseIPageDto = BeanCopyUtil.copyObject(housekeeperPage, HouseIPageDto::new);
        List<Housekeeper> housekeepers = BeanCopyUtil.copyList(HkdList, Housekeeper::new);
        List<HouseKeeperDto> houseKeeperDtos = BeanCopyUtil.copyList(housekeepers, HouseKeeperDto::new);
        houseIPageDto.setHkdList(houseKeeperDtos);
        return houseIPageDto;
    }

    @Override
    public HouseKeeperDto houseAdd(HouseKeeperForm houseKeeperForm) {
        Housekeeper housekeeper = BeanCopyUtil.copyObject(houseKeeperForm, Housekeeper::new);
        //判断性别字段异常
        System.out.println(housekeeper.getHousekeeperGender());
        System.out.println(UserRegisterStatus.gender_female);
        if (housekeeper.getHousekeeperGender() != UserRegisterStatus.gender_female &&
                housekeeper.getHousekeeperGender() != UserRegisterStatus.gender_male &&
                housekeeper.getHousekeeperGender() != UserRegisterStatus.gender_secrecy) {
            throw new UserRegisterException(UserRegisterExceptionEnum.gender_error);
        }
        //初始化账号其他数据
        housekeeper.setHousekeeperNum(algorithm.algorithm());
        HouseKeeperDto ukd = null;
        int i = HousekeeperMapper.insert(housekeeper);
        if (i > 0) {
            ukd = BeanCopyUtil.copyObject(housekeeper, HouseKeeperDto::new);
            return ukd;
        }
        return ukd;
    }

    @Override
    public HouseKeeperDto houseFire(Integer id) {
        //查询数据库中是否存在
        Housekeeper housekeeper = getById(id);
        if(housekeeper==null) throw new UserDataException(UserDataExceptionEnum.id_is_null);
        //存入Dto
        HouseKeeperDto houseKeeperDto = BeanCopyUtil.copyObject(housekeeper, HouseKeeperDto::new);
        //数据库中删除
        int i = HousekeeperMapper.deleteById(id);
        if(i<1) throw new UserDataException(UserDataExceptionEnum.do_error);
        return houseKeeperDto;
    }
}
