package com.woniu.recycle.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.recycle.commons.web.util.BeanCopyUtil;
import com.woniu.recycle.commons.web.util.ObjectMapperUtil;
import com.woniu.recycle.user.domain.BalanceLog;
import com.woniu.recycle.user.dto.BalanceLogDto;
import com.woniu.recycle.user.dto.BalanceLogResultDto;
import com.woniu.recycle.user.form.SearchByCreateTimeForm;
import com.woniu.recycle.user.form.SearchByUserIdForm;
import com.woniu.recycle.user.mapper.BalanceLogMapper;
import com.woniu.recycle.user.service.BalanceLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 余额日志表 服务实现类
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
@Service
public class BalanceLogServiceImpl extends ServiceImpl<BalanceLogMapper, BalanceLog> implements BalanceLogService {

    @Override
    public BalanceLogResultDto searchByUserId(SearchByUserIdForm form) {
        QueryWrapper<BalanceLog> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id",form.getUserId())
                .orderByDesc("create_time");

        BalanceLogResultDto resultDto = search(new Page(form.getCurrent(), form.getSize()), wrapper);
        return resultDto;
    }



    /**
     *@author DingYuyi
     *@Description 查询的公共方法
     *@Date 10:50 2021/8/21
     *@param *@param page
     *@param wrapper
     *@Return BalanceLogResultDto
     */
    @Override
    public  BalanceLogResultDto search(Page page,QueryWrapper<BalanceLog> wrapper) {
        Page selectPage = page(page, wrapper);
        BalanceLogResultDto resultDto = null;
        if(!(selectPage.getRecords().size() <= 0 || null == page)) {
            resultDto = BeanCopyUtil.copyObject(page, BalanceLogResultDto::new);
            List<BalanceLogDto> logDtos = BeanCopyUtil.copyList(page.getRecords(), BalanceLogDto::new);
            resultDto.setDtoList(logDtos);
        }
        return resultDto;
    }

    /**
     * 根据创建时间查询
     */
    @Override
    public BalanceLogResultDto searchByCreateTime(SearchByCreateTimeForm form) {
        QueryWrapper<BalanceLog> wrapper = new QueryWrapper<>();
        wrapper.ge("create_time",form.getLogCreateTimeStart())
                .le(form.getLogCreateTimeEnd() != null,"create_time",form.getLogCreateTimeEnd())
                .orderByDesc("create_time");

        BalanceLogResultDto resultDto = search(new Page(form.getCurrent(), form.getSize()), wrapper);
        return resultDto;
    }
}
