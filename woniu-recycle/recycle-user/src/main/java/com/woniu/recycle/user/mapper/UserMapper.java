package com.woniu.recycle.user.mapper;

import com.woniu.recycle.user.domain.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
public interface UserMapper extends BaseMapper<User> {

    @Select("select username from user where username=#{username}")
    User selectUsername(String username);

    @Select("select * from user where username=#{username}")
    User selectUser(String username);

}
