package com.woniu.recycle.user.mapper;

import com.woniu.recycle.user.domain.SpreadLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 推广日志表 Mapper 接口
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
public interface SpreadLogMapper extends BaseMapper<SpreadLog> {

}
