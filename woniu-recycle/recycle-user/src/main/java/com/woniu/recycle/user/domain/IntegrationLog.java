package com.woniu.recycle.user.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 积分日志表
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="IntegrationLog对象", description="积分日志表")
public class IntegrationLog implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "积分日志id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "用户id")
    private Integer userId;

    @ApiModelProperty(value = "积分变动量")
    private Integer integrationAmount;

    @ApiModelProperty(value = "日志创建时间（即积分变动时间）")
    private Long createTime;

    @ApiModelProperty(value = "积分变动原因 0 回收增加 | 1 消费家政服务增加 | 2 推广增加 | 3购买商品减")
    private Integer integrationChangeReason;

    @ApiModelProperty(value = "如果因为订单变动，则关联订单id")
    private Integer orderId;

}
