package com.woniu.recycle.user.mapper;

import com.woniu.recycle.user.domain.Housekeeper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 家政员工表 Mapper 接口
 * </p>
 *
 * @author dhc
 * @since 2021-08-11
 */
public interface HousekeeperMapper extends BaseMapper<Housekeeper> {

}
